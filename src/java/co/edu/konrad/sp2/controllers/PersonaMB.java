package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Persona;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosPersona;
import co.edu.konrad.sp2.servicios.ServiciosPersonaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "personaMB")
@ViewScoped
public class PersonaMB implements Serializable {

    private final static Logger log = Logger.getLogger(PersonaMB.class);
    private List<Persona> Listpersona;
    private List<Persona> personaFiltradas;
    private Persona persona;
    private String operacion;
    private ServiciosPersona servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosPersonaImp();
        this.Listpersona = new ArrayList();
        this.persona = new Persona();
        this.Listpersona = servicios.consultarPersonaJoin();

    }

    public List<Persona> getListPersona() {
        return Listpersona;
    }

    public void setListPersona(List<Persona> Listpersona) {
        this.Listpersona = Listpersona;
    }

    public List<Persona> getPersonaFiltradas() {
        return personaFiltradas;
    }

    public void setPersonaFiltradas(List<Persona> personaFiltradas) {
        this.personaFiltradas = personaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.persona = new Persona();
        this.operacion = Constantes.CREAR;
//
//        this.persona.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.persona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.persona != null) {
            this.operacion = Constantes.ACTUALIZAR;
            persona.setTipoIdentificacion(persona.getTipoIdentificacion());
            persona.setNombres(persona.getNombres());
            persona.setApellidos(persona.getApellidos());
            persona.setCiudad(persona.getCiudad());
            persona.setCedula(persona.getCedula());
            persona.setLugarExpedicion(persona.getLugarExpedicion());
            persona.setCodigo(persona.getCodigo());
            persona.setCargo(persona.getCargo());
            persona.setCorreoInstiCorp(persona.getCorreoInstiCorp());
            persona.setCorreoAlternativo(persona.getCorreoAlternativo());
            persona.setTelefono(persona.getTelefono());
            persona.setCelular(persona.getCelular());
            persona.setDireccion(persona.getDireccion());
            persona.setEstadoPersona(persona.getEstadoPersona());
            persona.setDatVigenciaDesde(persona.getDatVigenciaDesde());
            persona.setDatVigenciaHasta(persona.getDatVigenciaHasta());
            persona.setDatCreacion(persona.getDatCreacion());
            persona.setDatModificacion(persona.getDatModificacion());
            persona.setUsrCreacion(persona.getUsrCreacion());
//            persona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearPersona(persona);
        } else {
            this.servicios.actualizarPersona(persona);
        }
        this.lista = true;
        this.crear = false;
        this.Listpersona = servicios.consultarPersona();
    }

    public void onBorrar() {
        if (this.persona != null) {
            servicios.borrarPersona(persona);
            this.Listpersona = servicios.consultarPersona();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
