package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.AgendaEvento;
import co.edu.konrad.sp2.bean.CalendarioEvento;
import co.edu.konrad.sp2.bean.DetallePracticante;
//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Evento;
import co.edu.konrad.sp2.bean.Persona;
import co.edu.konrad.sp2.bean.SesionConsejeria;
import co.edu.konrad.sp2.constant.Constantes;
//import co.edu.konrad.sp2.helper.EventoAsistente;
//import co.edu.konrad.sp2.helper.SesionContexto;
import co.edu.konrad.sp2.servicios.ServiciosAgendaEvento;
import co.edu.konrad.sp2.servicios.ServiciosAgendaEventoImp;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticante;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticanteImp;
import co.edu.konrad.sp2.servicios.ServiciosEvento;
import co.edu.konrad.sp2.servicios.ServiciosEventoImp;
import co.edu.konrad.sp2.servicios.ServiciosPersona;
import co.edu.konrad.sp2.servicios.ServiciosPersonaImp;
import co.edu.konrad.sp2.servicios.ServiciosSesionConsejeria;
import co.edu.konrad.sp2.servicios.ServiciosSesionConsejeriaImp;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean(name = "eventoMB")
@ViewScoped
public class EventoMB implements Serializable {

    private final static Logger log = Logger.getLogger(EventoMB.class);
    private List<Evento> Listevento;
    private List<AgendaEvento> eventoFiltradas;
    private List<AgendaEvento> ListEventoNotificaciones;
    private List<AgendaEvento> ListAgenda;
    private List<ScheduleEvent> scheduleEvents;
    private List<SesionConsejeria> ListsesionConsejeria;
    private HttpServletRequest request;
    private String idRequest;

    private Evento evento = new Evento();
    private Evento eventoConstante;
    private String operacion;
    private ServiciosEvento servicios;
    public Persona correo = new Persona();
    private ScheduleModel model;
    private ScheduleEvent event;
    private Date fechaFin;
    private String codigo, programa, codigoRegistroSesion;
    private Persona practicante, PersonaTemporal;
    private SesionConsejeria consultarSesionConsejeria;
    private AgendaEvento agendaEvento;
    private ServiciosAgendaEvento servicioAgendaEvento;
    private ServiciosSesionConsejeria serviciosSesionConsejeria;
    private SesionConsejeria sesionConsejeria;
    private ServiciosDetallePracticante servicioDetallePracticante;

    private boolean crear, btnOcultarPersonaBuscar, btnMostrarPersonaBuscar;
    private boolean lista;
    private boolean activarEliminar, BtnEliminar;
    private boolean varPanelInfoPersonal;
    private boolean menuTalleres;
    private boolean idBtnCancelarFormSesionConsejeria;
    private boolean pnEstudianteRegistro, pnEstudianteConsultar;

    /*
    Rendered principal consAcademica
     */
    private Persona persona;
    private ServiciosPersona servicioPersona;
    private boolean varPanelEditarPerfil = true;
    private boolean varPanelEditarInf = false;
    private boolean VarPanelOpcionesMenu = true;
    private boolean menuestudiante = false;
    private boolean menuestudianteDetalle = false;
    private boolean menuSesionConsejeria = false;
    private boolean menuNotificaciones = false;
    private boolean pnConsultarNotificaciones = false;

    @PostConstruct
    public void init() {

//        if (SesionContexto.getInstance().getAttribute(Constantes.USUARIO) == null) {
//            SesionContexto.getInstance().setAttribute(Constantes.PERSONA, AsistenteSesion.getInstance().getPersona());
//            SesionContexto.getInstance().setAttribute(Constantes.USUARIO, AsistenteSesion.getInstance().getUsuario());
//            SesionContexto.getInstance().setAttribute(Constantes.CODIGO_PERSONA, AsistenteSesion.getInstance().getPersona().getCodigo());
//            SesionContexto.getInstance().setAttribute("CARGO", AsistenteSesion.getInstance().getPersona().getCargo());
//            String cargo = (String) SesionContexto.getInstance().getAttribute("CARGO");
//            if (!"Practicante".equals(cargo)) {
//                SesionContexto.getInstance().setAttribute(Constantes.CEDULA, AsistenteSesion.getInstance().getPersona().getCedula());
//            }
//        }
        try {
            this.sesionConsejeria = new SesionConsejeria();
            this.eventoConstante = new Evento();
            this.practicante = new Persona();
            this.agendaEvento = new AgendaEvento();
            this.PersonaTemporal = new Persona();
            this.persona = new Persona();
            this.evento = new Evento();
            this.consultarSesionConsejeria = new SesionConsejeria();
//            EventoAsistente.getInstance().setEvento(evento);
//            EventoAsistente.getInstance().setAgendaEvento(agendaEvento);
//            EventoAsistente.getInstance().setSesionConsejeria(sesionConsejeria);
            this.model = new DefaultScheduleModel();
            this.event = new CalendarioEvento();

            this.servicios = new ServiciosEventoImp();
            this.servicioAgendaEvento = new ServiciosAgendaEventoImp();
            this.servicioPersona = new ServiciosPersonaImp();
            this.serviciosSesionConsejeria = new ServiciosSesionConsejeriaImp();
            this.servicioDetallePracticante = new ServiciosDetallePracticanteImp();

            this.Listevento = new ArrayList();
            this.ListEventoNotificaciones = new ArrayList();
            this.ListsesionConsejeria = new ArrayList();

            this.Listevento = servicios.consultarEventoJoin();
//            this.ListEventoNotificaciones = this.grillaListarInformacion();
            this.ListAgenda = servicioAgendaEvento.consultarAgendaEventoJoin();
            CargueDatos();
            listar();
            fechaSistemaFin();

            this.lista = true;
            this.crear = false;
            this.activarEliminar = true;
            this.idBtnCancelarFormSesionConsejeria = true;
            this.BtnEliminar = false;
            this.inicializarParametros();
            this.obtenerParametrosRequest();
            this.funcionalidad();

        } catch (ParseException ex) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "NO se puede listar la Información" + ex));
        }

    }

    private void inicializarParametros() {
        servicioPersona = new ServiciosPersonaImp();
        request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

    private void obtenerParametrosRequest() {
        this.idRequest = request.getParameter("id");
    }

    private void funcionalidad() {
        this.obtenerPersona();
    }

    private void obtenerPersona() {
        persona = this.servicioPersona.consultarPersonaById(Long.parseLong(idRequest));
    }

    public List<Evento> getListevento() {
        return Listevento;
    }

    public void setListevento(List<Evento> Listevento) {
        this.Listevento = Listevento;
    }

    public boolean isPnEstudianteRegistro() {
        return pnEstudianteRegistro;
    }

    public void setPnEstudianteRegistro(boolean pnEstudianteRegistro) {
        this.pnEstudianteRegistro = pnEstudianteRegistro;
    }

    public boolean isPnEstudianteConsultar() {
        return pnEstudianteConsultar;
    }

    public void setPnEstudianteConsultar(boolean pnEstudianteConsultar) {
        this.pnEstudianteConsultar = pnEstudianteConsultar;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public boolean isIdBtnCancelarFormSesionConsejeria() {
        return idBtnCancelarFormSesionConsejeria;
    }

    public void setIdBtnCancelarFormSesionConsejeria(boolean idBtnCancelarFormSesionConsejeria) {
        this.idBtnCancelarFormSesionConsejeria = idBtnCancelarFormSesionConsejeria;
    }

    public boolean isVarPanelInfoPersonal() {
        return varPanelInfoPersonal;
    }

    public void setVarPanelInfoPersonal(boolean varPanelInfoPersonal) {
        this.varPanelInfoPersonal = varPanelInfoPersonal;
    }

    public boolean isMenuTalleres() {
        return menuTalleres;
    }

    public void setMenuTalleres(boolean menuTalleres) {
        this.menuTalleres = menuTalleres;
    }

    public boolean isBtnEliminar() {
        return BtnEliminar;
    }

    public void setBtnEliminar(boolean BtnEliminar) {
        this.BtnEliminar = BtnEliminar;
    }

    public boolean isBtnMostrarPersonaBuscar() {
        return btnMostrarPersonaBuscar;
    }

    public void setBtnMostrarPersonaBuscar(boolean btnMostrarPersonaBuscar) {
        this.btnMostrarPersonaBuscar = btnMostrarPersonaBuscar;
    }

    public boolean isBtnOcultarPersonaBuscar() {
        return btnOcultarPersonaBuscar;
    }

    public void setBtnOcultarPersonaBuscar(boolean btnOcultarPersonaBuscar) {
        this.btnOcultarPersonaBuscar = btnOcultarPersonaBuscar;
    }

    public List<AgendaEvento> getListAgenda() {
        return ListAgenda;
    }

    public void setListAgenda(List<AgendaEvento> ListAgenda) {
        this.ListAgenda = ListAgenda;
    }

    public Persona getPracticante() {
        return practicante;
    }

    public void setPracticante(Persona practicante) {
        this.practicante = practicante;
    }

    public AgendaEvento getAgendaEvento() {
        return agendaEvento;
    }

    public void setAgendaEvento(AgendaEvento agendaEvento) {
        this.agendaEvento = agendaEvento;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean isActivarEliminar() {
        return activarEliminar;
    }

    public void setActivarEliminar(boolean activarEliminar) {
        this.activarEliminar = activarEliminar;
    }

    public ScheduleModel getModel() {
        return model;
    }

    public void setModel(ScheduleModel model) {
        this.model = model;
    }

    public ScheduleEvent getEvent() {
        return event;
    }

    public void setEvent(ScheduleEvent event) {
        this.event = event;
    }

    public List<Evento> getListEvento() {
        return Listevento;
    }

    public void setListEvento(List<Evento> Listevento) {
        this.Listevento = Listevento;
    }

    public List<AgendaEvento> getEventoFiltradas() {
        return eventoFiltradas;
    }

    public void setEventoFiltradas(List<AgendaEvento> eventoFiltradas) {
        this.eventoFiltradas = eventoFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Evento getEventoConstante() {
        return eventoConstante;
    }

    public void setEventoConstante(Evento eventoConstante) {
        this.eventoConstante = eventoConstante;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public SesionConsejeria getSesionConsejeria() {
        return sesionConsejeria;
    }

    public void setSesionConsejeria(SesionConsejeria sesionConsejeria) {
        this.sesionConsejeria = sesionConsejeria;
    }

    public List<AgendaEvento> getListEventoNotificaciones() {
        return ListEventoNotificaciones;
    }

    public void setListEventoNotificaciones(List<AgendaEvento> ListEventoNotificaciones) {
        this.ListEventoNotificaciones = ListEventoNotificaciones;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public boolean isVarPanelEditarPerfil() {
        return varPanelEditarPerfil;
    }

    public void setVarPanelEditarPerfil(boolean varPanelEditarPerfil) {
        this.varPanelEditarPerfil = varPanelEditarPerfil;
    }

    public boolean isVarPanelEditarInf() {
        return varPanelEditarInf;
    }

    public void setVarPanelEditarInf(boolean varPanelEditarInf) {
        this.varPanelEditarInf = varPanelEditarInf;
    }

    public boolean isVarPanelOpcionesMenu() {
        return VarPanelOpcionesMenu;
    }

    public void setVarPanelOpcionesMenu(boolean VarPanelOpcionesMenu) {
        this.VarPanelOpcionesMenu = VarPanelOpcionesMenu;
    }

    public boolean isMenuestudiante() {
        return menuestudiante;
    }

    public void setMenuestudiante(boolean menuestudiante) {
        this.menuestudiante = menuestudiante;
    }

    public boolean isMenuSesionConsejeria() {
        return menuSesionConsejeria;
    }

    public void setMenuSesionConsejeria(boolean menuSesionConsejeria) {
        this.menuSesionConsejeria = menuSesionConsejeria;
    }

    public boolean isMenuNotificaciones() {
        return menuNotificaciones;
    }

    public void setMenuNotificaciones(boolean menuNotificaciones) {
        this.menuNotificaciones = menuNotificaciones;
    }

    public boolean isPnConsultarNotificaciones() {
        return pnConsultarNotificaciones;
    }

    public void setPnConsultarNotificaciones(boolean pnConsultarNotificaciones) {
        this.pnConsultarNotificaciones = pnConsultarNotificaciones;
    }

    public String getCodigoRegistroSesion() {
        return codigoRegistroSesion;
    }

    public void setCodigoRegistroSesion(String codigoRegistroSesion) {
        this.codigoRegistroSesion = codigoRegistroSesion;
    }

    public boolean isMenuestudianteDetalle() {
        return menuestudianteDetalle;
    }

    public void setMenuestudianteDetalle(boolean menuestudianteDetalle) {
        this.menuestudianteDetalle = menuestudianteDetalle;
    }

    public void onCancelar() {
//        this.lista = true;
//        this.crear = false;
    }

    public void onCancelarRegistro() {
        idBtnCancelarFormSesionConsejeria = false;
    }

    public void onCrearRegistro() {
        System.out.println("crear registro");
        this.lista = false;
        this.crear = true;
        this.evento = new Evento();
        this.operacion = Constantes.CREAR;

//        this.evento.setUsrCreacion((String) SesionContexto.getInstance().getAttribute(Constantes.USUARIO));
//        this.evento.setUsrModificacion((String) SesionContexto.getInstance().getAttribute(Constantes.USUARIO));
    }

    /*
    Se actualiza el evento con agenda evento
     */
    public void onActulizarRegistro() throws ParseException {
        boolean evaluarCondicion = false;
        if (this.evento != null) {
            this.operacion = Constantes.ACTUALIZAR;
//            evento.setUsrModificacion((String) SesionContexto.getInstance().getAttribute(Constantes.USUARIO));
            evento.setTipoEvento(evento.getTipoEvento());
            evento.setAsunto(evento.getAsunto());
            evento.setUbicacion(evento.getUbicacion());
            evento.setFechaInicio(event.getStartDate());
            evento.setHoraInicio(evento.getFechaInicio());
            evento.setFechaFin(event.getEndDate());
            evento.setHoraFin(evento.getFechaFin());
            evento.setDatCreacion(evento.getDatCreacion());
            evento.setDatModifcacion(evento.getDatModifcacion());
            evento.setUsrCreacion(evento.getUsrCreacion());
            this.eventoConstante = this.servicios.actualizarEvento(evento);
            if (eventoConstante.getConstantes().equalsIgnoreCase(Constantes.OK)) {
                agendaEvento.setObservacion(agendaEvento.getObservacion());
                agendaEvento.setEstadoEvento(agendaEvento.getEstadoEvento());
                if (codigo != null) {
                    this.PersonaTemporal = this.servicioPersona.ConsultarPersonaId(codigo);
                    if (this.PersonaTemporal.getConstante().equals(Constantes.OK)) {
                        agendaEvento.setPersonaInvitada(this.PersonaTemporal.getSeqPersona().intValue());
                        evaluarCondicion = true;
                    } else {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Practicante no se encuentra en la base de datos");
                    }
                } else {
                    agendaEvento.setPersonaInvitada(agendaEvento.getPersona().getSeqPersona().intValue());
                    evaluarCondicion = true;
                }
                if (evaluarCondicion) {
                    agendaEvento.setEventos(evento.getSeqDetalleEvento().intValue());
                    Persona personaSesion = new Persona();
//                    personaSesion = this.servicioPersona.ConsultarSupPersonaId((String) SesionContexto.getInstance().getAttribute(Constantes.CEDULA));
                    agendaEvento.setPersonaSesion(personaSesion.getSeqPersona().intValue());
                    agendaEvento.setUsrCreacion(agendaEvento.getUsrCreacion());
//                    agendaEvento.setUsrModificacion((String) SesionContexto.getInstance().getAttribute(Constantes.USUARIO));
                    this.agendaEvento = this.servicioAgendaEvento.actualizarAgendaEvento(agendaEvento);
                    if (agendaEvento.getConstante().equals(Constantes.OK)) {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro actualizado", "Registro actualizado con éxito");
//                        this.ListEventoNotificaciones = this.grillaListarInformacion();
                        PrimeFaces.current().dialog().showMessageDynamic(message);
                    } else {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Error al actualizar registro");
                        PrimeFaces.current().dialog().showMessageDynamic(message);
                    }
                }
            } else {
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Información", "No se puede guardar el evento"));
            }

        } else {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Información", "No Selecciono Ningún evento"));
        }
    }

    /*
    Se guarda la informacion de evento y agenda evento
     */
    public void onGuardarRegistro() {
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            evento = this.servicios.crearEvento(evento);
            if (evento.getConstantes().equalsIgnoreCase(Constantes.OK)) {
                this.crearAgendaEvento();
//                this.grillaListarInformacion();
            } else {
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No puede crear evento"));
            }
        } else {
            this.servicios.actualizarEvento(evento);
        }
//        this.ListEventoNotificaciones = grillaListarInformacion();
    }

    /*
    Elimina un agendaEvento y un evento
     */
    public void onBorrar() throws ParseException {
        if (this.evento != null) {
            agendaEvento = servicioAgendaEvento.borrarAgendaEvento(agendaEvento);
            if (agendaEvento.getConstante().equals(Constantes.OK)) {
                evento = servicios.borrarEvento(evento);
                if (evento.getConstantes().equals(Constantes.OK)) {
                    model.deleteEvent(event);
                    ListEventoNotificaciones.remove(evento);
//                    this.ListEventoNotificaciones = this.grillaListarInformacion();
                    this.listar();
                    RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Evento Eliminado"));
                }

            } else {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Error al borrar registro");
                PrimeFaces.current().dialog().showMessageDynamic(message);
            }

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

    /*
    Se consulta la informacion para visualizar al usuario del evento y agenda evento (calendario)
     */
    public void onEventSelect(SelectEvent selectEvent) throws ParseException {
        event = (CalendarioEvento) selectEvent.getObject();
        this.agendaEvento = new AgendaEvento();
        this.activarEliminar = true;
        this.BtnEliminar = true;
        this.btnOcultarPersonaBuscar = true;
        this.btnMostrarPersonaBuscar = false;
//        List<AgendaEvento> eventos = this.grillaListarInformacion();
//        for (AgendaEvento agendaActual : eventos) {
//            if (Objects.equals(agendaActual.getEvento().getSeqDetalleEvento(), event.getData()) && Objects.equals(agendaActual.getEvento().getSeqDetalleEvento(), event.getData())) {
//                this.evento = new Evento(agendaActual.getEvento().getSeqDetalleEvento(), agendaActual.getEvento().getAsunto(), agendaActual.getEvento().getUbicacion(), agendaActual.getEvento().getFechaInicio(), agendaActual.getEvento().getFechaFin(), agendaActual.getEvento().getTipoEvento(), agendaActual.getEvento().getDatCreacion(), agendaActual.getEvento().getUsrCreacion());
//                this.agendaEvento = new AgendaEvento(agendaActual.getSeqAgendaEvento(), agendaActual.getObservacion(), agendaActual.getEstadoEvento(), agendaActual.getDatCreacion(), agendaActual.getPersona(), agendaActual.getUsrCreacion());
//                break;
//            }
//        }
    }

    /*
    Se crea un espacio de memoria para crear un agenda evento y un evento (calendario)
     */
    public void onDateSelect(SelectEvent selectEvent) {
        this.evento = new Evento();
//        EventoAsistente.getInstance().setEvento(evento);
        this.agendaEvento = new AgendaEvento();
//        EventoAsistente.getInstance().setAgendaEvento(agendaEvento);
//        EventoAsistente.getInstance().setSesionConsejeria(sesionConsejeria);
        this.codigo = "";
        this.practicante = new Persona();
        this.btnOcultarPersonaBuscar = false;
        this.btnMostrarPersonaBuscar = true;
        this.BtnEliminar = false;
        Calendar currentDate = Calendar.getInstance();
        Date currentDate2 = (Date) selectEvent.getObject();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        try {
            Date fechaInicial = dateFormat.parse(dateFormat.format(currentDate.getTime()));
            Date fechaFinal = dateFormat.parse(dateFormat.format(currentDate2));
            int diferencia = (int) ((fechaFinal.getTime() - fechaInicial.getTime()) / 1000);
            if (diferencia < 0) {
                event = new CalendarioEvento("", (Date) currentDate.getTime(), (Date) currentDate.getTime());
            } else {
                event = new CalendarioEvento("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
            }
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(EventoMB.class.getName()).log(Level.SEVERE, null, ex);
            event = new CalendarioEvento("", (Date) selectEvent.getObject(), (Date) selectEvent.getObject());
        }
    }

    /*
    Se evalua si todos los campos son validos y no estan vacios para guardar la informacion del calendario
     */
    public void addEvent() throws ParseException {
//        EventoAsistente.getInstance().setEvento(this.evento);
//        EventoAsistente.getInstance().setAgendaEvento(this.agendaEvento);
//        this.sesionConsejeria = EventoAsistente.getInstance().getSesionConsejeria();
        if ("".equals(this.evento.getUbicacion()) || "".equals(this.evento.getAsunto()) || this.evento.getTipoEvento().getSeqTipoEvento() == null || "".equals(this.agendaEvento.getObservacion())) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Información", "Falta Completar informacion");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        } else {
            Long resta = this.restaFecha();
            if (resta == 1) {
                ScheduleEvent newEvent = new CalendarioEvento(this.evento.getAsunto(), this.evento.getUbicacion(), this.event.getStartDate(), this.event.getEndDate(), this.evento.getTipoEvento(), this.codigo, this.agendaEvento.getEstadoEvento(), this.agendaEvento.getObservacion(), this.evento);
                if (event.getId() == null) {
                    if (!"".equals(this.agendaEvento.getEstadoEvento()) && this.agendaEvento.getEstadoEvento() != null) {
                        this.operacion = Constantes.CREAR;
                        this.asignarVariables();
                        model.addEvent(newEvent);
                        PrimeFaces.current().executeScript("PF('myschedule').update();PF('eventDialog').hide();");
                    } else {
                        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Información", "Falta seleccionar el estado");
                        PrimeFaces.current().dialog().showMessageDynamic(message);
                    }
                } else {
                    this.activarEliminar = true;
                    this.onActulizarRegistro();
                    model.updateEvent(newEvent);
                    PrimeFaces.current().executeScript("PF('myschedule').update();PF('eventDialog').hide();");
                }
                event = new CalendarioEvento();
                this.Listevento = servicios.consultarEventoJoin();
                this.listar();
            } else {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Información", "Fecha inicial mayor que la final");
                PrimeFaces.current().dialog().showMessageDynamic(message);
            }
        }
    }

    /*
    Se consulta la informacion de evento y agenda evento para mostrarla en el calendario.
     */
    public void listar() throws ParseException {
        if (this.model != null) {

//            List<AgendaEvento> eventos = this.grillaListarInformacion();
            this.model.clear();
            this.scheduleEvents = null;
            if (this.scheduleEvents == null) {
                this.scheduleEvents = new ArrayList();
            }
//            for (AgendaEvento agendaActual : eventos) {
//                if (Objects.equals(agendaActual.getEvento().getSeqDetalleEvento(), agendaActual.getEvento().getSeqDetalleEvento())) {
////                    if (agendaActual.getUsrCreacion().equals(SesionContexto.getInstance().getAttribute(Constantes.USUARIO)) && agendaActual.getEvento().getUsrCreacion().equals(SesionContexto.getInstance().getAttribute(Constantes.USUARIO))) {
////                        ScheduleEvent newEvent = new CalendarioEvento(agendaActual.getEvento().getAsunto(), agendaActual.getEvento().getUbicacion(), agendaActual.getEvento().getFechaInicio(), agendaActual.getEvento().getFechaFin(), agendaActual.getEvento().getTipoEvento(), codigo, agendaActual.getEstadoEvento(), agendaActual.getObservacion(), agendaActual.getEvento().getSeqDetalleEvento());
////                        if (!this.scheduleEvents.contains(newEvent)) {
////                            newEvent.setId(agendaActual.getEvento().getSeqDetalleEvento().toString());
////                            this.scheduleEvents.add(newEvent);
////                            this.model.addEvent(newEvent);
////                        }
//                    }
//                }
        }
    }


    /*
    Se obtiene fecha del sistema
     */
    public Date fechaSistema() {
        Calendar currentDate = Calendar.getInstance();
        return currentDate.getTime();
    }

    public Date ObtenerFechaSeleccionada(Date FechaFin) {
        return FechaFin;
    }

    /*
    Se obtiene la fecha final seleccionada
     */
    public Date fechaSistemaFin() {
        Calendar currentDate = Calendar.getInstance();
        if (this.fechaFin == null) {
            return currentDate.getTime();
        } else {
            return this.fechaFin;
        }

    }

    /*
    Se obtiene la hora del sistema para evaluar 
     */
    public Number horaSistema() throws ParseException {
        Calendar calendario = Calendar.getInstance();
        if (calendario.getTime() == event.getStartDate()) {
            return calendario.get(Calendar.HOUR_OF_DAY);
        } else {
            return 0;
        }
    }

    /*
    Se obtiene el minutos del sistema para evaluar 
     */
    public Number minutosSistema() throws ParseException {
        Calendar calendario = Calendar.getInstance();
        if (calendario.getTime() == event.getStartDate()) {
            return calendario.get(Calendar.MINUTE);
        } else {
            return 0;
        }

    }

    /*
    Asignacion de variables para guardar la informacion
     */
    private void asignarVariables() throws ParseException {
//        this.evento.setUsrCreacion((String) SesionContexto.getInstance().getAttribute(Constantes.USUARIO));
//        this.evento.setUsrModificacion((String) SesionContexto.getInstance().getAttribute(Constantes.USUARIO));
        evento.setHoraInicio(this.event.getStartDate());
        evento.setHoraFin(this.event.getStartDate());
        evento.setFechaInicio(this.event.getStartDate());
        evento.setFechaFin(this.event.getEndDate());
        this.onGuardarRegistro();
    }

    /*
    se resta la informacion para evaluar el calendario
     */
    private long restaFecha() {
        long resta;
        resta = this.event.getStartDate().getTime() - this.event.getEndDate().getTime();
        if (resta > 0) {
            return 0;
        } else {
            return 1;
        }
    }

    /*
    Se consulta el practicante  por el codigo
     */
    public void consultarPracticante(boolean Valor) {
        if (Valor == false) {
            codigo = "";
        } else {
            codigoRegistroSesion = "";
        }
//        this.evento = EventoAsistente.getInstance().getEvento();
//        this.agendaEvento = EventoAsistente.getInstance().getAgendaEvento();
//        this.sesionConsejeria = EventoAsistente.getInstance().getSesionConsejeria();
        this.practicante = new Persona();
        this.btnOcultarPersonaBuscar = false;
        this.btnMostrarPersonaBuscar = true;
        if ("".equals(codigo) && "".equals(codigoRegistroSesion)) {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Información", "Por favor ingrese un código");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        } else {
            if (!"".equals(codigo)) {
//                this.sesionConsejeria.setPersonaSesion(AsistenteSesion.getInstance().getPersona().getSeqPersona().intValue());
                this.sesionConsejeria.setPersonaInvitada(this.servicioPersona.ConsultarCodigoPersona(codigo).getSeqPersona().intValue());
                this.consultarSesionConsejeria = this.serviciosSesionConsejeria.consultarSesionConsejeriaJoinPorCcaYSeqPracticante(sesionConsejeria);
                if (!this.consultarSesionConsejeria.getConstante().equals(Constantes.FAILED)) {
                    this.practicante = this.consultarSesionConsejeria.getDetallePracticante().getPersona();
                    this.practicante.setConstante(Constantes.OK);
                } else {
                    this.practicante.setConstante(Constantes.FAILED);
                }

            } else {
//                this.sesionConsejeria.setPersonaSesion(AsistenteSesion.getInstance().getPersona().getSeqPersona().intValue());
                this.sesionConsejeria.setPersonaInvitada(this.servicioPersona.ConsultarCodigoPersona(codigoRegistroSesion).getSeqPersona().intValue());
                this.consultarSesionConsejeria = this.serviciosSesionConsejeria.consultarSesionConsejeriaJoinPorCcaYSeqPracticante(sesionConsejeria);
                if (!this.consultarSesionConsejeria.getConstante().equals(Constantes.FAILED)) {
                    this.practicante = this.consultarSesionConsejeria.getDetallePracticante().getPersona();
                    this.practicante.setConstante(Constantes.OK);
                } else {
                    this.practicante.setConstante(Constantes.FAILED);
                }
            }
            if (this.practicante.getConstante().equals(Constantes.FAILED)) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "El código ingresado no se encuentra registrado en el sistema");
                PrimeFaces.current().dialog().showMessageDynamic(message);
            } else if (this.practicante.getConstante().equals(Constantes.OK)) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Estudiante encontrado");
                PrimeFaces.current().dialog().showMessageDynamic(message);
            }
        }
    }

    /*
    Se crea agenda evento cuando se crea el evento
     */
    private void crearAgendaEvento() {
        this.PersonaTemporal = new Persona();
        this.PersonaTemporal = this.servicioPersona.ConsultarPersonaId(codigo);
        if (this.PersonaTemporal.getConstante().equals(Constantes.OK)) {
            Persona personaSesion = new Persona();
//            personaSesion = this.servicioPersona.ConsultarSupPersonaId((String) SesionContexto.getInstance().getAttribute(Constantes.CEDULA));
            if (personaSesion.getConstante().equals(Constantes.OK)) {
//                this.agendaEvento = new AgendaEvento(this.servicios.ConsultarUltimoInsertado(), personaSesion.getSeqPersona().intValue(), this.PersonaTemporal.getSeqPersona().intValue(), this.agendaEvento.getObservacion(), this.agendaEvento.getEstadoEvento(), (String) SesionContexto.getInstance().getAttribute(Constantes.USUARIO), (String) SesionContexto.getInstance().getAttribute(Constantes.USUARIO));
                this.agendaEvento = this.servicioAgendaEvento.crearAgendaEvento(this.agendaEvento);
                if (agendaEvento.getConstante().equals(Constantes.OK)) {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro creado", "Registro creado con éxito");
                    PrimeFaces.current().dialog().showMessageDynamic(message);
                } else {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Error al crear registro");
                    PrimeFaces.current().dialog().showMessageDynamic(message);
                }
            } else {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "No tiene permisos");
                PrimeFaces.current().dialog().showMessageDynamic(message);
            }
        } else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Código del practicante no existe en la base de datos");
            PrimeFaces.current().dialog().showMessageDynamic(message);
        }

    }

    /*
    Para mostrar la informacion de la grilla evento
     */
    public void grillaEvento() {
        List<AgendaEvento> agenda2 = servicioAgendaEvento.consultarAgendaEventoJoin();
        List<Evento> eventos = servicios.consultarEventoJoin();
        for (AgendaEvento agendaActual : agenda2) {
            for (Evento eventoActual : eventos) {
                if (Objects.equals(agendaActual.getEvento().getSeqDetalleEvento(), eventoActual.getSeqDetalleEvento())) {
                    try {
                        this.evento = new Evento(eventoActual.getSeqDetalleEvento(), eventoActual.getAsunto(), eventoActual.getUbicacion(), eventoActual.getFechaInicio(), eventoActual.getFechaFin(), eventoActual.getTipoEvento(), eventoActual.getDatCreacion(), eventoActual.getUsrCreacion());
                        this.agendaEvento = new AgendaEvento(agendaActual.getSeqAgendaEvento(), agendaActual.getObservacion(), agendaActual.getEstadoEvento(), agendaActual.getDatCreacion(), agendaActual.getPersona(), agendaActual.getUsrCreacion());
                    } catch (ParseException ex) {
                        java.util.logging.Logger.getLogger(EventoMB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    /*
    Mostrar informacion de Agenda Evento (consultar consejeria)
     */
    public void grillaEventoInformacion(Long idEvento) {
//        List<AgendaEvento> agenda2 = servicioAgendaEvento.consultarAgendaEventoJoin();
//        for (AgendaEvento agendaActual : agenda2) {
//            if (Objects.equals(agendaActual.getEvento().getSeqDetalleEvento(), idEvento)) {
//                if (agendaActual.getUsrCreacion().equals(SesionContexto.getInstance().getAttribute(Constantes.USUARIO))) {
//                    this.agendaEvento = new AgendaEvento(agendaActual.getSeqAgendaEvento(), agendaActual.getObservacion(), agendaActual.getEstadoEvento(), agendaActual.getDatCreacion(), agendaActual.getPersona(), agendaActual.getUsrCreacion());
//                    programa = this.servicioPersona.consultarPrograma(agendaActual.getPersona().getSeqPersona());
////                    if ("Act".equals(agendaActual.getEstadoEvento())) {
////                        this.agendaEvento.setEstadoEvento("Activo ");
////                    } else if ("Inc".equals(agendaActual.getEstadoEvento())) {
////                        this.agendaEvento.setEstadoEvento("Inactivo ");
////                    }
//                }
//            }
//        }
    }

    /*
    Lista Mostrar consulta consejeria
     */
//    public List<AgendaEvento> grillaListarInformacion() {
////        List<AgendaEvento> agenda = servicioAgendaEvento.consultarAgendaEventoJoinPorRol(AsistenteSesion.getInstance().getUsuario());
////        return agenda;
//    }

    /*
    Carga de datos
     */
    public void CargueDatos() {
//        this.persona = servicioPersona.consultarPersonaById(AsistenteSesion.getInstance().getPersona().getSeqPersona());
    }

    /*
    Actualiar el perfil de consAcademica
     */
    public void ActualizacionDatosPerfil() {
        if (this.persona != null) {
            this.servicioPersona.actualizarPersona(persona);
//            this.persona = servicioPersona.consultarPersonaJoinById(AsistenteSesion.getInstance().getPersona().getSeqPersona());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Información Guardada."));
        }
    }

    /*
    Menu principal direccionamiento
     */
    public void Menu(int numero) {

        this.sesionConsejeria = new SesionConsejeria();
        this.agendaEvento = new AgendaEvento();
        this.practicante = new Persona();
        switch (numero) {
            case -1: {
                this.VarPanelOpcionesMenu = false;
                this.varPanelEditarPerfil = false;
                break;
            }
            case 0: {
                menuestudiante = false;
                menuNotificaciones = false;
                menuSesionConsejeria = false;
                break;
            }
            case 1: {
                this.VarPanelOpcionesMenu = false;
                this.varPanelEditarPerfil = false;
                this.varPanelEditarInf = true;
                this.persona.setStrVigenciaDesde(fechaDatVigenciasDesde());
                //ActualizacionDatosPerfil();
                break;
            }
            case 2: { //cancelar volver al menu principal
                this.VarPanelOpcionesMenu = true;
                this.varPanelEditarPerfil = true;
                this.varPanelEditarInf = false;
                break;
            }
            case 3: {
                if (menuestudiante == true) {
                    menuestudiante = false;

                } else {
                    menuestudiante = true;
                    menuestudianteDetalle = false;
                    menuNotificaciones = false;
                    menuSesionConsejeria = false;
                }
                Menu(-1);
                break;
            }
            case 4: {
                if (menuSesionConsejeria == true) {
                    menuSesionConsejeria = false;
                } else {
                    menuSesionConsejeria = true;
                    menuestudiante = false;
                    menuNotificaciones = false;
                }
                Menu(-1);
                break;
            }
            case 5: {
                if (menuNotificaciones == true) {
                    menuNotificaciones = false;
                } else {
                    menuNotificaciones = true;
                    menuestudiante = false;
                    menuSesionConsejeria = false;
                }
                Menu(-1);
                break;
            }
            case 6: {
                break;
            }

        }
    }

    public void MenuInterno(int numero) {

        this.sesionConsejeria = new SesionConsejeria();
        this.agendaEvento = new AgendaEvento();
        this.practicante = new Persona();
//        EventoAsistente.getInstance().setSesionConsejeria(sesionConsejeria);
        switch (numero) {
            case 0: {
                Menu(2);
                Menu(0);
                break;
            }
            case 1: {
                Menu(3);
                break;
            }
            case 2: {
                Menu(4);
                break;
            }
            case 3: {
                Menu(5);
                break;
            }
        }
    }

    public void DetallePracticanteEstudiante(Long idPracticante) {
        this.practicante = new Persona();
        this.practicante = this.servicioPersona.consultarPersonaJoinById(idPracticante);
        this.menuestudiante = false;
        this.menuestudianteDetalle = true;
    }

    public void devolver() {
        this.menuestudiante = true;
        this.menuestudianteDetalle = false;
    }

    public void onGuardarRegistroSesionConsejeria() {
        this.agendaEvento = new AgendaEvento();
        this.evento = new Evento();
        if ((this.sesionConsejeria.getNombreSesion() == null) || (sesionConsejeria.getTipoConsulta() == null) || (sesionConsejeria.getDescripcionSesion() == null) || (sesionConsejeria.getAsistencia() == null)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Falta completar información"));
        } else if ("".equals(this.codigoRegistroSesion)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Falta codigo de estudiante"));
        } else if (true) {
//            this.sesionConsejeria.setUsrCreacion(AsistenteSesion.getInstance().getUsuario()); //AsistenteSesion.getInstance().getUsuarioSesion();
//            this.sesionConsejeria.setUsrModificacion(AsistenteSesion.getInstance().getUsuario());
            DetallePracticante detallePracticante = this.servicioDetallePracticante.consultarDetallePracticanteJoinById(practicante.getSeqPersona());
            if (detallePracticante == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encontro codigo del estudiante"));
            } else {
                sesionConsejeria.setPersonaInvitada(detallePracticante.getSeqDetallePracticante().intValue());
//                sesionConsejeria.setPersonaSesion(AsistenteSesion.getInstance().getPersona().getSeqPersona().intValue());
            }
            sesionConsejeria = this.serviciosSesionConsejeria.crearSesionConsejeria(sesionConsejeria);
            if (sesionConsejeria.getConstante().equalsIgnoreCase(Constantes.OK)) {
//                this.ListsesionConsejeria = this.grillaListarIformacion();
                this.codigo = "";
                this.codigoRegistroSesion = "";
                this.practicante = new Persona();
                this.sesionConsejeria = new SesionConsejeria();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Se registro correctamente"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede registrar la sesión"));
            }
        } else {
            this.serviciosSesionConsejeria.actualizarSesionConsejeria(sesionConsejeria);
        }
    }

    public String fechaDatVigenciasDesde() {
        String cadena = "";
        try {
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
            Date date = persona.getDatVigenciaDesde();
            cadena = formato.format(date);
            return cadena;
        } catch (Exception e) {
        }
        return cadena;
    }

    public void onToggle(ToggleEvent event) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, event.getComponent().getId() + " toggled", "Status:" + event.getVisibility().name());
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

//    public List<SesionConsejeria> grillaListarIformacion() {
////        List<SesionConsejeria> novedades = serviciosSesionConsejeria.consultarSesionConsejeriaJoinPorRol(AsistenteSesion.getInstance().getUsuario());
////        return novedades;
//    }
}
