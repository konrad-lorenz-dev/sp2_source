package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.HorarioPractica;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosHorarioPractica;
import co.edu.konrad.sp2.servicios.ServiciosHorarioPracticaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "horarioPracticaMB")
@ViewScoped
public class HorarioPracticaMB implements Serializable {

    private final static Logger log = Logger.getLogger(HorarioPracticaMB.class);
    private List<HorarioPractica> ListhorarioPractica;
    private List<HorarioPractica> horarioPracticaFiltradas;
    private HorarioPractica horarioPractica;
    private String operacion;
    private ServiciosHorarioPractica servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosHorarioPracticaImp();
        this.ListhorarioPractica = new ArrayList();
        this.horarioPractica = new HorarioPractica();
        this.ListhorarioPractica = servicios.consultarHorarioPracticaJoin();

    }

    public List<HorarioPractica> getListHorarioPractica() {
        return ListhorarioPractica;
    }

    public void setListHorarioPractica(List<HorarioPractica> ListhorarioPractica) {
        this.ListhorarioPractica = ListhorarioPractica;
    }

    public List<HorarioPractica> getHorarioPracticaFiltradas() {
        return horarioPracticaFiltradas;
    }

    public void setHorarioPracticaFiltradas(List<HorarioPractica> horarioPracticaFiltradas) {
        this.horarioPracticaFiltradas = horarioPracticaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public HorarioPractica getHorarioPractica() {
        return horarioPractica;
    }

    public void setHorarioPractica(HorarioPractica horarioPractica) {
        this.horarioPractica = horarioPractica;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.horarioPractica = new HorarioPractica();
        this.operacion = Constantes.CREAR;

//        this.horarioPractica.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.horarioPractica != null) {
            this.operacion = Constantes.ACTUALIZAR;
            horarioPractica.setPractica(horarioPractica.getPractica());
            horarioPractica.setDia(horarioPractica.getDia());
            horarioPractica.setHoraInicio(horarioPractica.getHoraInicio());
            horarioPractica.setHoraFin(horarioPractica.getHoraFin());
            horarioPractica.setTipoPractica(horarioPractica.getTipoPractica());
            horarioPractica.setDatCreacion(horarioPractica.getDatCreacion());
            horarioPractica.setDatModificacion(horarioPractica.getDatModificacion());
            horarioPractica.setUsrCreacion(horarioPractica.getUsrCreacion());
            horarioPractica.setUsrModifcacion(horarioPractica.getUsrModifcacion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearHorarioPractica(horarioPractica);
        } else {
            this.servicios.actualizarHorarioPractica(horarioPractica);
        }
        this.lista = true;
        this.crear = false;
        this.ListhorarioPractica = servicios.consultarHorarioPractica();
    }

    public void onBorrar() {
        if (this.horarioPractica != null) {
            servicios.borrarHorarioPractica(horarioPractica);
            this.ListhorarioPractica = servicios.consultarHorarioPractica();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
