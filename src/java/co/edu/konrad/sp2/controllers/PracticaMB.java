package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Practica;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosPractica;
import co.edu.konrad.sp2.servicios.ServiciosPracticaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "practicaMB")
@ViewScoped
public class PracticaMB implements Serializable {

    private final static Logger log = Logger.getLogger(PracticaMB.class);
    private List<Practica> Listpractica;
    private List<Practica> practicaFiltradas;
    private Practica practica;
    private String operacion;
    private ServiciosPractica servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosPracticaImp();
        this.Listpractica = new ArrayList();
        this.practica = new Practica();
        this.Listpractica = servicios.consultarPracticaJoin();

    }

    public List<Practica> getListPractica() {
        return Listpractica;
    }

    public void setListPractica(List<Practica> Listpractica) {
        this.Listpractica = Listpractica;
    }

    public List<Practica> getPracticaFiltradas() {
        return practicaFiltradas;
    }

    public void setPracticaFiltradas(List<Practica> practicaFiltradas) {
        this.practicaFiltradas = practicaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.practica = new Practica();
        this.operacion = Constantes.CREAR;

//        this.practica.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.practica.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.practica != null) {
            this.operacion = Constantes.ACTUALIZAR;
            practica.setPracticanteConvenio(practica.getPracticanteConvenio());
            practica.setDetalleSupPractica(practica.getDetalleSupPractica());
            practica.setTipoPractica(practica.getTipoPractica());
            practica.setDatVigenciaDesde(practica.getDatVigenciaDesde());
            practica.setDatVigenciaHasta(practica.getDatVigenciaHasta());
            practica.setEstadoPractica(practica.getEstadoPractica());
            practica.setPazYSalvo(practica.getPazYSalvo());
            practica.setCartaReferenciacion(practica.getCartaReferenciacion());
            practica.setDatCreacion(practica.getDatCreacion());
            practica.setDatModificacion(practica.getDatModificacion());
//            practica.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            practica.setUsrCreacion(practica.getUsrCreacion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearPractica(practica);
        } else {
            this.servicios.actualizarPractica(practica);
        }
        this.lista = true;
        this.crear = false;
        this.Listpractica = servicios.consultarPractica();
    }

    public void onBorrar() {
        if (this.practica != null) {
            servicios.borrarPractica(practica);
            this.Listpractica = servicios.consultarPractica();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
