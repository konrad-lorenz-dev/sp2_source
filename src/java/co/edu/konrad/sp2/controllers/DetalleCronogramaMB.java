package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.DetalleCronograma;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosDetalleCronograma;
import co.edu.konrad.sp2.servicios.ServiciosDetalleCronogramaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "detalleCronogramaMB")
@ViewScoped
public class DetalleCronogramaMB implements Serializable {

    private final static Logger log = Logger.getLogger(DetalleCronogramaMB.class);
    private List<DetalleCronograma> ListdetalleCronograma;
    private List<DetalleCronograma> detalleCronogramaFiltradas;
    private DetalleCronograma detalleCronograma;
    private String operacion;
    private ServiciosDetalleCronograma servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosDetalleCronogramaImp();
        this.ListdetalleCronograma = new ArrayList();
        this.detalleCronograma = new DetalleCronograma();
        this.ListdetalleCronograma = servicios.consultarDetalleCronogramaJoin();

    }

    public List<DetalleCronograma> getListDetalleCronograma() {
        return ListdetalleCronograma;
    }

    public void setListDetalleCronograma(List<DetalleCronograma> ListdetalleCronograma) {
        this.ListdetalleCronograma = ListdetalleCronograma;
    }

    public List<DetalleCronograma> getDetalleCronogramaFiltradas() {
        return detalleCronogramaFiltradas;
    }

    public void setDetalleCronogramaFiltradas(List<DetalleCronograma> detalleCronogramaFiltradas) {
        this.detalleCronogramaFiltradas = detalleCronogramaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public DetalleCronograma getDetalleCronograma() {
        return detalleCronograma;
    }

    public void setDetalleCronograma(DetalleCronograma detalleCronograma) {
        this.detalleCronograma = detalleCronograma;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.detalleCronograma = new DetalleCronograma();
        this.operacion = Constantes.CREAR;

//        this.detalleCronograma.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.detalleCronograma.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.detalleCronograma != null) {
            this.operacion = Constantes.ACTUALIZAR;
            detalleCronograma.setActividadCronograma(detalleCronograma.getActividadCronograma());
            detalleCronograma.setDatInicio(detalleCronograma.getDatInicio());
            detalleCronograma.setDatTerminacion(detalleCronograma.getDatTerminacion());
            detalleCronograma.setEntregable(detalleCronograma.getEntregable());
            detalleCronograma.setCumplimiento(detalleCronograma.getCumplimiento());
            detalleCronograma.setDatCreacion(detalleCronograma.getDatCreacion());
            detalleCronograma.setDatModificacion(detalleCronograma.getDatModificacion());
            detalleCronograma.setUsrCreacion(detalleCronograma.getUsrCreacion());
//            detalleCronograma.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearDetalleCronograma(detalleCronograma);
        } else {
            this.servicios.actualizarDetalleCronograma(detalleCronograma);
        }
        this.lista = true;
        this.crear = false;
        this.ListdetalleCronograma = servicios.consultarDetalleCronograma();
    }

    public void onBorrar() {
        if (this.detalleCronograma != null) {
            servicios.borrarDetalleCronograma(detalleCronograma);
            this.ListdetalleCronograma = servicios.consultarDetalleCronograma();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
