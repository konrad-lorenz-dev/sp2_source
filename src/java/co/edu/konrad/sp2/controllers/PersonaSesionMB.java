package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.PersonaSesion;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosPersonaSesion;
import co.edu.konrad.sp2.servicios.ServiciosPersonaSesionImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "personaSesionMB")
@ViewScoped
public class PersonaSesionMB implements Serializable {

    private final static Logger log = Logger.getLogger(PersonaSesionMB.class);
    private List<PersonaSesion> ListpersonaSesion;
    private List<PersonaSesion> personaSesionFiltradas;
    private PersonaSesion personaSesion;
    private String operacion;
    private ServiciosPersonaSesion servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosPersonaSesionImp();
        this.ListpersonaSesion = new ArrayList();
        this.personaSesion = new PersonaSesion();
        this.ListpersonaSesion = servicios.consultarPersonaSesionJoin();

    }

    public List<PersonaSesion> getListPersonaSesion() {
        return ListpersonaSesion;
    }

    public void setListPersonaSesion(List<PersonaSesion> ListpersonaSesion) {
        this.ListpersonaSesion = ListpersonaSesion;
    }

    public List<PersonaSesion> getPersonaSesionFiltradas() {
        return personaSesionFiltradas;
    }

    public void setPersonaSesionFiltradas(List<PersonaSesion> personaSesionFiltradas) {
        this.personaSesionFiltradas = personaSesionFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public PersonaSesion getPersonaSesion() {
        return personaSesion;
    }

    public void setPersonaSesion(PersonaSesion personaSesion) {
        this.personaSesion = personaSesion;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.personaSesion = new PersonaSesion();
        this.operacion = Constantes.CREAR;

//        this.personaSesion.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.personaSesion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.personaSesion != null) {
            this.operacion = Constantes.ACTUALIZAR;
            personaSesion.setPersona(personaSesion.getPersona());
            personaSesion.setDatUltimaSesion(personaSesion.getDatUltimaSesion());
            personaSesion.setEstado(personaSesion.getEstado());
            personaSesion.setDatCreacion(personaSesion.getDatCreacion());
            personaSesion.setDatModificacion(personaSesion.getDatModificacion());
            personaSesion.setUsrCreacion(personaSesion.getUsrCreacion());
//            personaSesion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearPersonaSesion(personaSesion);
        } else {
            this.servicios.actualizarPersonaSesion(personaSesion);
        }
        this.lista = true;
        this.crear = false;
        this.ListpersonaSesion = servicios.consultarPersonaSesion();
    }

    public void onBorrar() {
        if (this.personaSesion != null) {
            servicios.borrarPersonaSesion(personaSesion);
            this.ListpersonaSesion = servicios.consultarPersonaSesion();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
