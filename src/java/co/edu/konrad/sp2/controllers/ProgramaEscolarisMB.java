package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.ProgramaEscolaris;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosProgramaEscolaris;
import co.edu.konrad.sp2.servicios.ServiciosProgramaEscolarisImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "programaEscolarisMB")
@ViewScoped
public class ProgramaEscolarisMB implements Serializable {

    private final static Logger log = Logger.getLogger(ProgramaEscolarisMB.class);
    private List<ProgramaEscolaris> ListprogramaEscolaris;
    private List<ProgramaEscolaris> programaEscolarisFiltradas;
    private ProgramaEscolaris programaEscolaris;
    private String operacion;
    private ServiciosProgramaEscolaris servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosProgramaEscolarisImp();
        this.ListprogramaEscolaris = new ArrayList();
        this.programaEscolaris = new ProgramaEscolaris();
        this.ListprogramaEscolaris = servicios.consultarProgramaEscolarisJoin();

    }

    public List<ProgramaEscolaris> getListProgramaEscolaris() {
        return ListprogramaEscolaris;
    }

    public void setListProgramaEscolaris(List<ProgramaEscolaris> ListprogramaEscolaris) {
        this.ListprogramaEscolaris = ListprogramaEscolaris;
    }

    public List<ProgramaEscolaris> getProgramaEscolarisFiltradas() {
        return programaEscolarisFiltradas;
    }

    public void setProgramaEscolarisFiltradas(List<ProgramaEscolaris> programaEscolarisFiltradas) {
        this.programaEscolarisFiltradas = programaEscolarisFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public ProgramaEscolaris getProgramaEscolaris() {
        return programaEscolaris;
    }

    public void setProgramaEscolaris(ProgramaEscolaris programaEscolaris) {
        this.programaEscolaris = programaEscolaris;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.programaEscolaris = new ProgramaEscolaris();
        this.operacion = Constantes.CREAR;

    }

    public void onActulizarRegistro() {

        if (this.programaEscolaris != null) {
            this.operacion = Constantes.ACTUALIZAR;
            programaEscolaris.setFacultadEscolaris(programaEscolaris.getFacultadEscolaris());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearProgramaEscolaris(programaEscolaris);
        } else {
            this.servicios.actualizarProgramaEscolaris(programaEscolaris);
        }
        this.lista = true;
        this.crear = false;
        this.ListprogramaEscolaris = servicios.consultarProgramaEscolaris();
    }

    public void onBorrar() {
        if (this.programaEscolaris != null) {
            servicios.borrarProgramaEscolaris(programaEscolaris);
            this.ListprogramaEscolaris = servicios.consultarProgramaEscolaris();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
