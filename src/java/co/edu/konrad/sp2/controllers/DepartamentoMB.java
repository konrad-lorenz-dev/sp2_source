package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Departamento;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosDepartamento;
import co.edu.konrad.sp2.servicios.ServiciosDepartamentoImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "departamentoMB")
@ViewScoped
public class DepartamentoMB implements Serializable {

    private final static Logger log = Logger.getLogger(DepartamentoMB.class);
    private List<Departamento> Listdepartamento;
    private List<Departamento> departamentoFiltradas;
    private Departamento departamento;
    private String operacion;
    private ServiciosDepartamento servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosDepartamentoImp();
        this.Listdepartamento = new ArrayList();
        this.departamento = new Departamento();
        this.Listdepartamento = servicios.consultarDepartamentoJoin();

    }

    public List<Departamento> getListDepartamento() {
        return Listdepartamento;
    }

    public void setListDepartamento(List<Departamento> Listdepartamento) {
        this.Listdepartamento = Listdepartamento;
    }

    public List<Departamento> getDepartamentoFiltradas() {
        return departamentoFiltradas;
    }

    public void setDepartamentoFiltradas(List<Departamento> departamentoFiltradas) {
        this.departamentoFiltradas = departamentoFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.departamento = new Departamento();
        this.operacion = Constantes.CREAR;

//        this.departamento.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.departamento.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.departamento != null) {
            this.operacion = Constantes.ACTUALIZAR;
            departamento.setPais(departamento.getPais());
            departamento.setCodigo(departamento.getCodigo());
            departamento.setDepartamento(departamento.getDepartamento());
            departamento.setDatCreacion(departamento.getDatCreacion());
            departamento.setDatModificacion(departamento.getDatModificacion());
            departamento.setUsrCreacion(departamento.getUsrCreacion());
//            departamento.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearDepartamento(departamento);
        } else {
            this.servicios.actualizarDepartamento(departamento);
        }
        this.lista = true;
        this.crear = false;
        this.Listdepartamento = servicios.consultarDepartamento();
    }

    public void onBorrar() {
        if (this.departamento != null) {
            servicios.borrarDepartamento(departamento);
            this.Listdepartamento = servicios.consultarDepartamento();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
