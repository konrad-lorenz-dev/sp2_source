/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.constant.Constantes;
import java.io.IOException;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;

/**
 *
 * @author cesard.chacond
 */
@ManagedBean
@ViewScoped
@WebServlet(name = "SalirMB")
@Controller
public class SalirMB implements Serializable {

    private final static Logger log = Logger.getLogger(SalirMB.class);
    private HttpServletRequest request;
    private String tipoPersona;
    private Boolean externo;

    @PostConstruct
    public void init() {
        this.inicializarParametros();
        this.obtenerParametrosRequest();
    }

    private void inicializarParametros() {
        request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        this.externo = false;
    }

    private void obtenerParametrosRequest() {
        this.tipoPersona = request.getParameter(Constantes.TIPOPERSONA);
        if(this.tipoPersona == null){
            this.tipoPersona = " ";
        }
        this.externo = tipoPersona.equalsIgnoreCase(Constantes.EXTERNO);
    }

    public String salirAplicacion() {
        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            if (this.externo) {
                // salir cuando no es microsoft               
                ec.redirect(Constantes.SALIRAPLICACIONEXTERNO);
            } else {
                ec.redirect(Constantes.SALIRAPLICACOIN);
            }

        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
        return "#";
    }
}
