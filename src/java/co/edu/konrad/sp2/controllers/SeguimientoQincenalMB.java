package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.SeguimientoQincenal;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosSeguimientoQincenal;
import co.edu.konrad.sp2.servicios.ServiciosSeguimientoQincenalImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "seguimientoQincenalMB")
@ViewScoped
public class SeguimientoQincenalMB implements Serializable {

    private final static Logger log = Logger.getLogger(SeguimientoQincenalMB.class);
    private List<SeguimientoQincenal> ListseguimientoQincenal;
    private List<SeguimientoQincenal> seguimientoQincenalFiltradas;
    private SeguimientoQincenal seguimientoQincenal;
    private String operacion;
    private ServiciosSeguimientoQincenal servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosSeguimientoQincenalImp();
        this.ListseguimientoQincenal = new ArrayList();
        this.seguimientoQincenal = new SeguimientoQincenal();
        this.ListseguimientoQincenal = servicios.consultarSeguimientoQincenalJoin();

    }

    public List<SeguimientoQincenal> getListSeguimientoQincenal() {
        return ListseguimientoQincenal;
    }

    public void setListSeguimientoQincenal(List<SeguimientoQincenal> ListseguimientoQincenal) {
        this.ListseguimientoQincenal = ListseguimientoQincenal;
    }

    public List<SeguimientoQincenal> getSeguimientoQincenalFiltradas() {
        return seguimientoQincenalFiltradas;
    }

    public void setSeguimientoQincenalFiltradas(List<SeguimientoQincenal> seguimientoQincenalFiltradas) {
        this.seguimientoQincenalFiltradas = seguimientoQincenalFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public SeguimientoQincenal getSeguimientoQincenal() {
        return seguimientoQincenal;
    }

    public void setSeguimientoQincenal(SeguimientoQincenal seguimientoQincenal) {
        this.seguimientoQincenal = seguimientoQincenal;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.seguimientoQincenal = new SeguimientoQincenal();
        this.operacion = Constantes.CREAR;

//        this.seguimientoQincenal.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.seguimientoQincenal.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.seguimientoQincenal != null) {
            this.operacion = Constantes.ACTUALIZAR;
            seguimientoQincenal.setPractica(seguimientoQincenal.getPractica());
            seguimientoQincenal.setDatRegistro(seguimientoQincenal.getDatRegistro());
            seguimientoQincenal.setHoraRegistro(seguimientoQincenal.getHoraRegistro());
            seguimientoQincenal.setTema(seguimientoQincenal.getTema());
            seguimientoQincenal.setDatCreacion(seguimientoQincenal.getDatCreacion());
            seguimientoQincenal.setDatModificacion(seguimientoQincenal.getDatModificacion());
            seguimientoQincenal.setUsrCreacion(seguimientoQincenal.getUsrCreacion());
//            seguimientoQincenal.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearSeguimientoQincenal(seguimientoQincenal);
        } else {
            this.servicios.actualizarSeguimientoQincenal(seguimientoQincenal);
        }
        this.lista = true;
        this.crear = false;
        this.ListseguimientoQincenal = servicios.consultarSeguimientoQincenal();
    }

    public void onBorrar() {
        if (this.seguimientoQincenal != null) {
            servicios.borrarSeguimientoQincenal(seguimientoQincenal);
            this.ListseguimientoQincenal = servicios.consultarSeguimientoQincenal();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
