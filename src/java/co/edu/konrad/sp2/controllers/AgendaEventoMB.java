package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.AgendaEvento;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosAgendaEvento;
import co.edu.konrad.sp2.servicios.ServiciosAgendaEventoImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "agendaEventoMB")
@ViewScoped
public class AgendaEventoMB implements Serializable {

    private final static Logger log = Logger.getLogger(AgendaEventoMB.class);
    private List<AgendaEvento> ListagendaEvento;
    private List<AgendaEvento> agendaEventoFiltradas;
    private AgendaEvento agendaEvento;
    private String operacion;
    private ServiciosAgendaEvento servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosAgendaEventoImp();
        this.ListagendaEvento = new ArrayList();
        this.agendaEvento = new AgendaEvento();
        this.ListagendaEvento = servicios.consultarAgendaEventoJoin();

    }

    public List<AgendaEvento> getListAgendaEvento() {
        return ListagendaEvento;
    }

    public void setListAgendaEvento(List<AgendaEvento> ListagendaEvento) {
        this.ListagendaEvento = ListagendaEvento;
    }

    public List<AgendaEvento> getAgendaEventoFiltradas() {
        return agendaEventoFiltradas;
    }

    public void setAgendaEventoFiltradas(List<AgendaEvento> agendaEventoFiltradas) {
        this.agendaEventoFiltradas = agendaEventoFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public AgendaEvento getAgendaEvento() {
        return agendaEvento;
    }

    public void setAgendaEvento(AgendaEvento agendaEvento) {
        this.agendaEvento = agendaEvento;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.agendaEvento = new AgendaEvento();
        this.operacion = Constantes.CREAR;

        this.agendaEvento.setUsrCreacion("mio");
        this.agendaEvento.setUsrModificacion("mio");//AsistenteSesion.getInstance().getUsuarioSesion()

    }

    public void onActulizarRegistro() {

        if (this.agendaEvento != null) {
            this.operacion = Constantes.ACTUALIZAR;
            agendaEvento.setPersona(agendaEvento.getPersona());
            agendaEvento.setPersonaProgramadorEvento(agendaEvento.getPersonaProgramadorEvento());
            agendaEvento.setEvento(agendaEvento.getEvento());
            agendaEvento.setObservacion(agendaEvento.getObservacion());
            agendaEvento.setEstadoEvento(agendaEvento.getEstadoEvento());
            agendaEvento.setDatCreacion(agendaEvento.getDatCreacion());
            agendaEvento.setDatModificacion(agendaEvento.getDatModificacion());
            agendaEvento.setUsrCreacion(agendaEvento.getUsrCreacion());
            agendaEvento.setUsrModificacion("mio");

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearAgendaEvento(agendaEvento);
        } else {
            this.servicios.actualizarAgendaEvento(agendaEvento);
        }
        this.lista = true;
        this.crear = false;
        this.ListagendaEvento = servicios.consultarAgendaEvento();
    }

    public void onBorrar() {
        if (this.agendaEvento != null) {
            servicios.borrarAgendaEvento(agendaEvento);
            this.ListagendaEvento = servicios.consultarAgendaEvento();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
