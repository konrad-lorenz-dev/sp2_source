package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.FacultadEscolaris;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosFacultadEscolaris;
import co.edu.konrad.sp2.servicios.ServiciosFacultadEscolarisImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "facultadEscolarisMB")
@ViewScoped
public class FacultadEscolarisMB implements Serializable {

    private final static Logger log = Logger.getLogger(FacultadEscolarisMB.class);
    private List<FacultadEscolaris> ListfacultadEscolaris;
    private List<FacultadEscolaris> facultadEscolarisFiltradas;
    private FacultadEscolaris facultadEscolaris;
    private String operacion;
    private ServiciosFacultadEscolaris servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosFacultadEscolarisImp();
        this.ListfacultadEscolaris = new ArrayList();
        this.facultadEscolaris = new FacultadEscolaris();
        this.ListfacultadEscolaris = servicios.consultarFacultadEscolaris();

    }

    public List<FacultadEscolaris> getListFacultadEscolaris() {
        return ListfacultadEscolaris;
    }

    public void setListFacultadEscolaris(List<FacultadEscolaris> ListfacultadEscolaris) {
        this.ListfacultadEscolaris = ListfacultadEscolaris;
    }

    public List<FacultadEscolaris> getFacultadEscolarisFiltradas() {
        return facultadEscolarisFiltradas;
    }

    public void setFacultadEscolarisFiltradas(List<FacultadEscolaris> facultadEscolarisFiltradas) {
        this.facultadEscolarisFiltradas = facultadEscolarisFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public FacultadEscolaris getFacultadEscolaris() {
        return facultadEscolaris;
    }

    public void setFacultadEscolaris(FacultadEscolaris facultadEscolaris) {
        this.facultadEscolaris = facultadEscolaris;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.facultadEscolaris = new FacultadEscolaris();
        this.operacion = Constantes.CREAR;

    }

    public void onActulizarRegistro() {

        if (this.facultadEscolaris != null) {
            this.operacion = Constantes.ACTUALIZAR;

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearFacultadEscolaris(facultadEscolaris);
        } else {
            this.servicios.actualizarFacultadEscolaris(facultadEscolaris);
        }
        this.lista = true;
        this.crear = false;
        this.ListfacultadEscolaris = servicios.consultarFacultadEscolaris();
    }

    public void onBorrar() {
        if (this.facultadEscolaris != null) {
            servicios.borrarFacultadEscolaris(facultadEscolaris);
            this.ListfacultadEscolaris = servicios.consultarFacultadEscolaris();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
