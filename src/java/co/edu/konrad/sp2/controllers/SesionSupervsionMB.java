package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.SesionSupervsion;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosSesionSupervsion;
import co.edu.konrad.sp2.servicios.ServiciosSesionSupervsionImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "sesionSupervsionMB")
@ViewScoped
public class SesionSupervsionMB implements Serializable {

    private final static Logger log = Logger.getLogger(SesionSupervsionMB.class);
    private List<SesionSupervsion> ListsesionSupervsion;
    private List<SesionSupervsion> sesionSupervsionFiltradas;
    private SesionSupervsion sesionSupervsion;
    private String operacion;
    private ServiciosSesionSupervsion servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosSesionSupervsionImp();
        this.ListsesionSupervsion = new ArrayList();
        this.sesionSupervsion = new SesionSupervsion();
        this.ListsesionSupervsion = servicios.consultarSesionSupervsionJoin();

    }

    public List<SesionSupervsion> getListSesionSupervsion() {
        return ListsesionSupervsion;
    }

    public void setListSesionSupervsion(List<SesionSupervsion> ListsesionSupervsion) {
        this.ListsesionSupervsion = ListsesionSupervsion;
    }

    public List<SesionSupervsion> getSesionSupervsionFiltradas() {
        return sesionSupervsionFiltradas;
    }

    public void setSesionSupervsionFiltradas(List<SesionSupervsion> sesionSupervsionFiltradas) {
        this.sesionSupervsionFiltradas = sesionSupervsionFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public SesionSupervsion getSesionSupervsion() {
        return sesionSupervsion;
    }

    public void setSesionSupervsion(SesionSupervsion sesionSupervsion) {
        this.sesionSupervsion = sesionSupervsion;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.sesionSupervsion = new SesionSupervsion();
        this.operacion = Constantes.CREAR;

//        this.sesionSupervsion.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.sesionSupervsion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.sesionSupervsion != null) {
            this.operacion = Constantes.ACTUALIZAR;
            sesionSupervsion.setPractica(sesionSupervsion.getPractica());
            sesionSupervsion.setNumeroSesion(sesionSupervsion.getNumeroSesion());
            sesionSupervsion.setDatSesion(sesionSupervsion.getDatSesion());
            sesionSupervsion.setHoraInicio(sesionSupervsion.getHoraInicio());
            sesionSupervsion.setHoraFinal(sesionSupervsion.getHoraFinal());
            sesionSupervsion.setObjetivos(sesionSupervsion.getObjetivos());
            sesionSupervsion.setResumen(sesionSupervsion.getResumen());
            sesionSupervsion.setCompromisos(sesionSupervsion.getCompromisos());
            sesionSupervsion.setCalificacionCompromiso(sesionSupervsion.getCalificacionCompromiso());
            sesionSupervsion.setObservaciones(sesionSupervsion.getObservaciones());
            sesionSupervsion.setAsistencia(sesionSupervsion.getAsistencia());
            sesionSupervsion.setPuntualidad(sesionSupervsion.getPuntualidad());
            sesionSupervsion.setObservacionInasistencia(sesionSupervsion.getObservacionInasistencia());
            sesionSupervsion.setAspectos(sesionSupervsion.getAspectos());
            sesionSupervsion.setCalificacionAspectos(sesionSupervsion.getCalificacionAspectos());
            sesionSupervsion.setBitacora(sesionSupervsion.getBitacora());
            sesionSupervsion.setPlanMejoramiento(sesionSupervsion.getPlanMejoramiento());
            sesionSupervsion.setCalificacionSesion(sesionSupervsion.getCalificacionSesion());
            sesionSupervsion.setPromedio40(sesionSupervsion.getPromedio40());
            sesionSupervsion.setPromedio60(sesionSupervsion.getPromedio60());
            sesionSupervsion.setDatCreacion(sesionSupervsion.getDatCreacion());
            sesionSupervsion.setDatModificacion(sesionSupervsion.getDatModificacion());
            sesionSupervsion.setUsrCreacion(sesionSupervsion.getUsrCreacion());
//            sesionSupervsion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearSesionSupervsion(sesionSupervsion);
        } else {
            this.servicios.actualizarSesionSupervsion(sesionSupervsion);
        }
        this.lista = true;
        this.crear = false;
        this.ListsesionSupervsion = servicios.consultarSesionSupervsion();
    }

    public void onBorrar() {
        if (this.sesionSupervsion != null) {
            servicios.borrarSesionSupervsion(sesionSupervsion);
            this.ListsesionSupervsion = servicios.consultarSesionSupervsion();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
