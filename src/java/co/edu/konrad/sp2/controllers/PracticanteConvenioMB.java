package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.PracticanteConvenio;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosPracticanteConvenio;
import co.edu.konrad.sp2.servicios.ServiciosPracticanteConvenioImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "practicanteConvenioMB")
@ViewScoped
public class PracticanteConvenioMB implements Serializable {

    private final static Logger log = Logger.getLogger(PracticanteConvenioMB.class);
    private List<PracticanteConvenio> ListpracticanteConvenio;
    private List<PracticanteConvenio> practicanteConvenioFiltradas;
    private PracticanteConvenio practicanteConvenio;
    private String operacion;
    private ServiciosPracticanteConvenio servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosPracticanteConvenioImp();
        this.ListpracticanteConvenio = new ArrayList();
        this.practicanteConvenio = new PracticanteConvenio();
        this.ListpracticanteConvenio = servicios.consultarPracticanteConvenioJoin();

    }

    public List<PracticanteConvenio> getListPracticanteConvenio() {
        return ListpracticanteConvenio;
    }

    public void setListPracticanteConvenio(List<PracticanteConvenio> ListpracticanteConvenio) {
        this.ListpracticanteConvenio = ListpracticanteConvenio;
    }

    public List<PracticanteConvenio> getPracticanteConvenioFiltradas() {
        return practicanteConvenioFiltradas;
    }

    public void setPracticanteConvenioFiltradas(List<PracticanteConvenio> practicanteConvenioFiltradas) {
        this.practicanteConvenioFiltradas = practicanteConvenioFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public PracticanteConvenio getPracticanteConvenio() {
        return practicanteConvenio;
    }

    public void setPracticanteConvenio(PracticanteConvenio practicanteConvenio) {
        this.practicanteConvenio = practicanteConvenio;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.practicanteConvenio = new PracticanteConvenio();
        this.operacion = Constantes.CREAR;

//        this.practicanteConvenio.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.practicanteConvenio.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.practicanteConvenio != null) {
            this.operacion = Constantes.ACTUALIZAR;
            practicanteConvenio.setDetalleSupEntidad(practicanteConvenio.getDetalleSupEntidad());
            practicanteConvenio.setDetallePracticante(practicanteConvenio.getDetallePracticante());
            practicanteConvenio.setConvenio(practicanteConvenio.getConvenio());
            practicanteConvenio.setAreaEscolaris(practicanteConvenio.getAreaEscolaris());
            practicanteConvenio.setSeqCronograma(practicanteConvenio.getSeqCronograma());
            practicanteConvenio.setSeqOfertaLaboral(practicanteConvenio.getSeqOfertaLaboral());
            practicanteConvenio.setEstadoAprobado(practicanteConvenio.getEstadoAprobado());
            practicanteConvenio.setEstadoConvenio(practicanteConvenio.getEstadoConvenio());
            practicanteConvenio.setEstadoPracticante(practicanteConvenio.getEstadoPracticante());
            practicanteConvenio.setAprobado(practicanteConvenio.getAprobado());
            practicanteConvenio.setObservacion(practicanteConvenio.getObservacion());
            practicanteConvenio.setDatVigPeriodoDesde(practicanteConvenio.getDatVigPeriodoDesde());
            practicanteConvenio.setDatVigPeriodoHasta(practicanteConvenio.getDatVigPeriodoHasta());
            practicanteConvenio.setDatCreacion(practicanteConvenio.getDatCreacion());
            practicanteConvenio.setDatModificacion(practicanteConvenio.getDatModificacion());
            practicanteConvenio.setUsrCreacion(practicanteConvenio.getUsrCreacion());
//            practicanteConvenio.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearPracticanteConvenio(practicanteConvenio);
        } else {
            this.servicios.actualizarPracticanteConvenio(practicanteConvenio);
        }
        this.lista = true;
        this.crear = false;
        this.ListpracticanteConvenio = servicios.consultarPracticanteConvenio();
    }

    public void onBorrar() {
        if (this.practicanteConvenio != null) {
            servicios.borrarPracticanteConvenio(practicanteConvenio);
            this.ListpracticanteConvenio = servicios.consultarPracticanteConvenio();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
