/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.Persona;
import co.edu.konrad.sp2.bean.RolPersona;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.ms.AuthHelper;
import co.edu.konrad.sp2.servicios.ServiciosPersona;
import co.edu.konrad.sp2.servicios.ServiciosPersonaImp;
import co.edu.konrad.sp2.servicios.ServiciosRolPersona;
import co.edu.konrad.sp2.servicios.ServiciosRolPersonaImp;
import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;
import com.microsoft.aad.adal4j.ClientCredential;
import com.nimbusds.oauth2.sdk.AuthorizationCode;
import com.nimbusds.openid.connect.sdk.AuthenticationErrorResponse;
import com.nimbusds.openid.connect.sdk.AuthenticationResponse;
import com.nimbusds.openid.connect.sdk.AuthenticationResponseParser;
import com.nimbusds.openid.connect.sdk.AuthenticationSuccessResponse;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.ServiceUnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import javax.servlet.http.HttpSession;
/**
 *
 * @author cesard.chacond
 */
@ManagedBean
@ViewScoped
@WebServlet(name = "direccionamientoMB")
@Controller
public class DireccionamientoMB implements Serializable {

    private final static Logger log = Logger.getLogger(DireccionamientoMB.class);
    private HttpServletRequest request;
    //servicios
    private ServiciosPersona servicioPersona;
    private ServiciosRolPersona serviciosRolPersona;

    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
    private Persona persona;
    private List<RolPersona> rolPersona;
    private String nombrePersona; 
    private boolean administrador;
    private boolean asesorJuridico;
    private boolean consejeroAcademico;
    private boolean cordinadorPracticas;
    private boolean practicante;
    private boolean subEntidad;
    private boolean subPractica;
    private HttpSession session;

    @PostConstruct
    public void init() {
        this.inicializarParametros();
        this.processAuthenticationData(request, Constantes.INICIOPRINCIPAL, Constantes.INICIOPRINCIPAL);
    }

    private void inicializarParametros() {
        request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        servicioPersona = new ServiciosPersonaImp();
        serviciosRolPersona = new ServiciosRolPersonaImp();
        persona = new Persona();
        rolPersona = new ArrayList<>();
        session = request.getSession();
        //atributos sencillos
        nombrePersona = new String();
    }

    private void processAuthenticationData(HttpServletRequest httpRequest, String currentUri, String fullUrl) //Constantes.INICIOPRINCIPAL
    {
        try {
            HashMap<String, String> params = new HashMap<>();
            for (String key : httpRequest.getParameterMap().keySet()) {
                params.put(key, httpRequest.getParameterMap().get(key)[0]);
            }

            AuthenticationResponse authResponse = AuthenticationResponseParser.parse(new URI(fullUrl), params);
            if (AuthHelper.isAuthenticationSuccessful(authResponse)) {
                log.info("Obtener Datos personas");
                AuthenticationSuccessResponse oidcResponse = (AuthenticationSuccessResponse) authResponse;
                // validate that OIDC Auth Response matches Code Flow (contains only requested artifacts)

                AuthenticationResult authData = this.getAccessToken(oidcResponse.getAuthorizationCode(), currentUri);
                persona = servicioPersona.consultarPersonaByCorreo(authData.getUserInfo().getDisplayableId());
                if (persona != null) {
                    rolPersona = serviciosRolPersona.consultarRolPersonaJoinBySeqPersona(persona.getSeqPersona());
                    nombrePersona = persona.getNombres() + " " + persona.getApellidos();
                    for (RolPersona tmp : rolPersona) {
                        switch (tmp.getRol().getSeqRolPersona().toString()) {
                            case Constantes.ROL_ADMINISTRADOR: {
                                administrador = true;
                                break;
                            }
                            case Constantes.ROL_ASESOR_JURIDICO: {
                                asesorJuridico = true;
                                break;
                            }
                            case Constantes.ROL_CONSEJERO_ACADEMICO: {
                                consejeroAcademico = true;
                                break;
                            }
                            case Constantes.ROL_COORDINADOR_PRACTICAS: {
                                cordinadorPracticas = true;
                                break;
                            }
                            case Constantes.ROL_PRACTICANTE: {
                                practicante = true;
                                break;
                            }
                            case Constantes.ROL_SUP_ENTIDAD: {
                                subEntidad = true;
                                break;
                            }
                            case Constantes.ROL_SUP_PRACTICA: {
                                subPractica = true;
                                break;
                            }

                        }
                    }
                }
            } else {
                AuthenticationErrorResponse oidcResponse = (AuthenticationErrorResponse) authResponse;
                throw new Exception(String.format("Request for auth code failed: %s - %s",
                        oidcResponse.getErrorObject().getCode(),
                        oidcResponse.getErrorObject().getDescription()));
            }
        } catch (Throwable e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", ""));
            log.info(e.getMessage());
        }
    }

    public AuthenticationResult getAccessToken(
            AuthorizationCode authorizationCode, String currentUri)
            throws Throwable {
        String authCode = authorizationCode.getValue();
        ClientCredential credential = new ClientCredential(Constantes.CLIEND_ID,
                Constantes.CLAVE_SECRET);
        AuthenticationContext context;
        AuthenticationResult result = null;
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(1);
            context = new AuthenticationContext(Constantes.AUTHORITY + Constantes.TENANT + "/", true,
                    service);
            Future<AuthenticationResult> future = context
                    .acquireTokenByAuthorizationCode(authCode, new URI(
                            currentUri), credential, null);
            result = future.get();
        } catch (ExecutionException e) {
            throw e.getCause();
        } finally {
            service.shutdown();
        }

        if (result == null) {
            throw new ServiceUnavailableException("authentication result was null");
        }
        return result;
    }

    public void redireccionAdministrador() {
        try {
//            session.setAttribute("formularioPersona", "frmFormularioAdministrador");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/faces/vistas/VistaPrincipalAdministrador.xhtml?id="+persona.getSeqPersona().toString());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
    }

    public void redireccionPracticante() {
        try {
//                        session.setAttribute("formularioPersona", "frmFormularioPracticante");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/faces/vistas/VistaPrincipalPracticante.xhtml?id="+persona.getSeqPersona().toString());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
    }

    public void redireccionAsesor() {
        try {
//                        session.setAttribute("formularioPersona", "frmFormularioAsesorJuridico");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/faces/vistas/VistaPrincipalAsesorJuridico.xhtml?id="+persona.getSeqPersona().toString());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
    }

    public void redireccionConsejeroAcademico() {
        try {
            session.setAttribute("formularioUpdate", "frmFormularioConsejeriaAcademica");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/faces/vistas/VistaPrincipalConsejeroAcademico.xhtml?id="+persona.getSeqPersona().toString());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
    }

    public void redireccionCordinadorPracticas() {
        try {
//                        session.setAttribute("formularioPersona", "frmFormularioCoordiandorPracticas");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/faces/vistas/VistaPrincipalCordinadorPracticas.xhtml?id="+persona.getSeqPersona().toString());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
    }

    public void redireccionSubEntidad() {
        try {
//                        session.setAttribute("formularioPersona", "frmFormularioSupervisorEntidad");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/faces/vistas/VistaPrincipalSupervisorEntidad.xhtml?id="+persona.getSeqPersona().toString());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
    }

    public void redireccionSubPractica() {
        try {
//                        session.setAttribute("formularioPersona", "frmFormularioSupPractica");
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(ec.getRequestContextPath() + "/faces/vistas/VistaPrincipalSupPractica.xhtml?id="+persona.getSeqPersona().toString());
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<RolPersona> getRolPersona() {
        return rolPersona;
    }

    public void setRolPersona(List<RolPersona> rolPersona) {
        this.rolPersona = rolPersona;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    public boolean isAsesorJuridico() {
        return asesorJuridico;
    }

    public void setAsesorJuridico(boolean asesorJuridico) {
        this.asesorJuridico = asesorJuridico;
    }

    public boolean isConsejeroAcademico() {
        return consejeroAcademico;
    }

    public void setConsejeroAcademico(boolean consejeroAcademico) {
        this.consejeroAcademico = consejeroAcademico;
    }

    public boolean isCordinadorPracticas() {
        return cordinadorPracticas;
    }

    public void setCordinadorPracticas(boolean cordinadorPracticas) {
        this.cordinadorPracticas = cordinadorPracticas;
    }

    public boolean isPracticante() {
        return practicante;
    }

    public void setPracticante(boolean practicante) {
        this.practicante = practicante;
    }

    public boolean isSubEntidad() {
        return subEntidad;
    }

    public void setSubEntidad(boolean subEntidad) {
        this.subEntidad = subEntidad;
    }

    public boolean isSubPractica() {
        return subPractica;
    }

    public void setSubPractica(boolean subPractica) {
        this.subPractica = subPractica;
    }

    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }

}
