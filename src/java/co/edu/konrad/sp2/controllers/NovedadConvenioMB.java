package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.NovedadConvenio;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosNovedadConvenio;
import co.edu.konrad.sp2.servicios.ServiciosNovedadConvenioImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "novedadConvenioMB")
@ViewScoped
public class NovedadConvenioMB implements Serializable {

    private final static Logger log = Logger.getLogger(NovedadConvenioMB.class);
    private List<NovedadConvenio> ListnovedadConvenio;
    private List<NovedadConvenio> novedadConvenioFiltradas;
    private NovedadConvenio novedadConvenio;
    private String operacion;
    private ServiciosNovedadConvenio servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosNovedadConvenioImp();
        this.ListnovedadConvenio = new ArrayList();
        this.novedadConvenio = new NovedadConvenio();
        this.ListnovedadConvenio = servicios.consultarNovedadConvenioJoin();

    }

    public List<NovedadConvenio> getListNovedadConvenio() {
        return ListnovedadConvenio;
    }

    public void setListNovedadConvenio(List<NovedadConvenio> ListnovedadConvenio) {
        this.ListnovedadConvenio = ListnovedadConvenio;
    }

    public List<NovedadConvenio> getNovedadConvenioFiltradas() {
        return novedadConvenioFiltradas;
    }

    public void setNovedadConvenioFiltradas(List<NovedadConvenio> novedadConvenioFiltradas) {
        this.novedadConvenioFiltradas = novedadConvenioFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public NovedadConvenio getNovedadConvenio() {
        return novedadConvenio;
    }

    public void setNovedadConvenio(NovedadConvenio novedadConvenio) {
        this.novedadConvenio = novedadConvenio;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.novedadConvenio = new NovedadConvenio();
        this.operacion = Constantes.CREAR;

//        this.novedadConvenio.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.novedadConvenio.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.novedadConvenio != null) {
            this.operacion = Constantes.ACTUALIZAR;
            novedadConvenio.setConvenio(novedadConvenio.getConvenio());
            novedadConvenio.setDatFecha(novedadConvenio.getDatFecha());
            novedadConvenio.setNovedad(novedadConvenio.getNovedad());
            novedadConvenio.setDescripcionNovedad(novedadConvenio.getDescripcionNovedad());
            novedadConvenio.setEstadoNovedad(novedadConvenio.getEstadoNovedad());
            novedadConvenio.setDatCreacion(novedadConvenio.getDatCreacion());
            novedadConvenio.setDatModificacion(novedadConvenio.getDatModificacion());
            novedadConvenio.setUsrCreacion(novedadConvenio.getUsrCreacion());
//            novedadConvenio.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearNovedadConvenio(novedadConvenio);
        } else {
            this.servicios.actualizarNovedadConvenio(novedadConvenio);
        }
        this.lista = true;
        this.crear = false;
        this.ListnovedadConvenio = servicios.consultarNovedadConvenio();
    }

    public void onBorrar() {
        if (this.novedadConvenio != null) {
            servicios.borrarNovedadConvenio(novedadConvenio);
            this.ListnovedadConvenio = servicios.consultarNovedadConvenio();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
