package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.DetallePracticante;
import co.edu.konrad.sp2.bean.Persona;
//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.SesionConsejeria;
import co.edu.konrad.sp2.constant.Constantes;
//import co.edu.konrad.sp2.helper.EventoAsistente;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticante;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticanteImp;
import co.edu.konrad.sp2.servicios.ServiciosPersona;
import co.edu.konrad.sp2.servicios.ServiciosPersonaImp;
import co.edu.konrad.sp2.servicios.ServiciosSesionConsejeria;
import co.edu.konrad.sp2.servicios.ServiciosSesionConsejeriaImp;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "sesionConsejeriaMB")
@ViewScoped
public class SesionConsejeriaMB implements Serializable {

    private final static Logger log = Logger.getLogger(SesionConsejeriaMB.class);
    private List<SesionConsejeria> ListsesionConsejeria;
    private List<SesionConsejeria> sesionConsejeriaFiltradas;
    private List<SesionConsejeria> ListPracticanteEstudiante;
    private SesionConsejeria sesionConsejeria;
    private String operacion;
    private ServiciosSesionConsejeria servicios;
    public String codigo = "";
    boolean crear;
    boolean lista;
    boolean varPanelInfoPersonal, Calendario;
    boolean pnCalendarioSesionConsejeria, pnConsultarSesionConsejeria, pnFormularioSesionConsejeria;
    boolean pnDesavilitarFunciones;
    private Persona practicante;
    private ServiciosPersona serviciosPersona;
    private ServiciosDetallePracticante serviciosDetallePracticante;

    
    @PostConstruct
    public void init() {
        this.serviciosPersona = new ServiciosPersonaImp();
        this.servicios = new ServiciosSesionConsejeriaImp();
        this.serviciosDetallePracticante = new ServiciosDetallePracticanteImp();

        this.practicante = new Persona();
//        this.sesionConsejeria = EventoAsistente.getInstance().getSesionConsejeria();
        

        this.ListsesionConsejeria = new ArrayList<>();
        this.ListPracticanteEstudiante = new ArrayList<>();

        this.ListsesionConsejeria = this.grillaListarIformacion();
//        this.ListPracticanteEstudiante = servicios.consultarSesionConsejeriaJoinPorCCA(AsistenteSesion.getInstance().getPersona().getSeqPersona());
        
        this.lista = true;
        this.crear = false;
        this.varPanelInfoPersonal = true;

    }

    public Persona getPracticante() {
        return practicante;
    }

    public void setPracticante(Persona practicante) {
        this.practicante = practicante;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public boolean isPnDesavilitarFunciones() {
        return pnDesavilitarFunciones;
    }

    public void setPnDesavilitarFunciones(boolean pnDesavilitarFunciones) {
        this.pnDesavilitarFunciones = pnDesavilitarFunciones;
    }

    public boolean isCalendario() {
        return Calendario;
    }

    public void setCalendario(boolean Calendario) {
        this.Calendario = Calendario;
    }

    public boolean isPnFormularioSesionConsejeria() {
        return pnFormularioSesionConsejeria;
    }

    public void setPnFormularioSesionConsejeria(boolean pnFormularioSesionConsejeria) {
        this.pnFormularioSesionConsejeria = pnFormularioSesionConsejeria;
    }

    public boolean isVarPanelInfoPersonal() {
        return varPanelInfoPersonal;
    }

    public void setVarPanelInfoPersonal(boolean varPanelInfoPersonal) {
        this.varPanelInfoPersonal = varPanelInfoPersonal;
    }

    public boolean isPnConsultarSesionConsejeria() {
        return pnConsultarSesionConsejeria;
    }

    public void setPnConsultarSesionConsejeria(boolean pnConsultarSesionConsejeria) {
        this.pnConsultarSesionConsejeria = pnConsultarSesionConsejeria;
    }

    public boolean isPnCalendarioSesionConsejeria() {
        return pnCalendarioSesionConsejeria;
    }

    public void setPnCalendarioSesionConsejeria(boolean pnCalendarioSesionConsejeria) {
        this.pnCalendarioSesionConsejeria = pnCalendarioSesionConsejeria;
    }

    public List<SesionConsejeria> getListSesionConsejeria() {
        return ListsesionConsejeria;
    }

    public void setListSesionConsejeria(List<SesionConsejeria> ListsesionConsejeria) {
        this.ListsesionConsejeria = ListsesionConsejeria;
    }

    public List<SesionConsejeria> getSesionConsejeriaFiltradas() {
        return sesionConsejeriaFiltradas;
    }

    public void setSesionConsejeriaFiltradas(List<SesionConsejeria> sesionConsejeriaFiltradas) {
        this.sesionConsejeriaFiltradas = sesionConsejeriaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public SesionConsejeria getSesionConsejeria() {
        return sesionConsejeria;
    }

    public void setSesionConsejeria(SesionConsejeria sesionConsejeria) {
        this.sesionConsejeria = sesionConsejeria;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public List<SesionConsejeria> getListPracticanteEstudiante() {
        return ListPracticanteEstudiante;
    }

    public void setListPracticanteEstudiante(List<SesionConsejeria> ListPracticanteEstudiante) {
        this.ListPracticanteEstudiante = ListPracticanteEstudiante;
    }

    public void onCancelar() {
//        this.lista = true;
//        this.crear = false;
        this.pnFormularioSesionConsejeria = false;
    }

    public void onCrearRegistro() throws ParseException {
//        this.lista = false;
//        this.crear = true;
        this.operacion = Constantes.CREAR;
//        this.sesionConsejeria = EventoAsistente.getInstance().getSesionConsejeria();
//        this.sesionConsejeria.setUsrCreacion(AsistenteSesion.getInstance().getUsuario()); //AsistenteSesion.getInstance().getUsuarioSesion();
//        this.sesionConsejeria.setUsrModificacion(AsistenteSesion.getInstance().getUsuario());
        onGuardarRegistro();

    }

    public void onActulizarRegistro() throws ParseException {

        if (this.sesionConsejeria != null) {
            this.operacion = Constantes.ACTUALIZAR;
            sesionConsejeria.setPersona(sesionConsejeria.getPersona());
            sesionConsejeria.setDetallePracticante(sesionConsejeria.getDetallePracticante());
            sesionConsejeria.setNombreSesion(sesionConsejeria.getNombreSesion());
            sesionConsejeria.setTipoConsulta(sesionConsejeria.getTipoConsulta());
            sesionConsejeria.setDatSesion(sesionConsejeria.getDatSesion());
            sesionConsejeria.setDescripcionSesion(sesionConsejeria.getDescripcionSesion());
            sesionConsejeria.setAsistencia(sesionConsejeria.getAsistencia());
            sesionConsejeria.setDatCreacion(sesionConsejeria.getDatCreacion());
            sesionConsejeria.setDatModificacion(sesionConsejeria.getDatModificacion());
            sesionConsejeria.setUsrCreacion(sesionConsejeria.getUsrCreacion());
//            sesionConsejeria.setUsrModificacion(AsistenteSesion.getInstance().getUsuario());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() throws ParseException {
        if ((this.sesionConsejeria.getNombreSesion() == null) || (sesionConsejeria.getTipoConsulta() == null) || (sesionConsejeria.getDescripcionSesion() == null) || (sesionConsejeria.getAsistencia() == null)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Falta completar información"));
        } else if ("".equals(this.codigo)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Falta codigo de estudiante"));
        } else if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            List<DetallePracticante> detallePracticante = serviciosDetallePracticante.consultarDetallePracticanteJoin();
            for (DetallePracticante detallePracticanteEvaluar : detallePracticante) {
                if (Objects.equals(detallePracticanteEvaluar.getPersona().getSeqPersona(), practicante.getSeqPersona())) {
                    sesionConsejeria.setPersonaInvitada(detallePracticanteEvaluar.getSeqDetallePracticante().intValue());
                    sesionConsejeria.setPersonaSesion(detallePracticanteEvaluar.getSeqDetallePracticante().intValue());
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se encontro codigo del estudiante"));
                }
            }
            sesionConsejeria = this.servicios.crearSesionConsejeria(sesionConsejeria);
            if (sesionConsejeria.getConstante().equalsIgnoreCase(Constantes.OK)) {
                this.ListsesionConsejeria = this.grillaListarIformacion();
                this.codigo = "";
                this.practicante = new Persona();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Se registro correctamente"));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "No se puede registrar la sesión"));
            }
        } else {
            this.servicios.actualizarSesionConsejeria(sesionConsejeria);
        }
    }

    public void onBorrar() {
        if (this.sesionConsejeria != null) {
            servicios.borrarSesionConsejeria(sesionConsejeria);
            this.ListsesionConsejeria = this.grillaListarIformacion();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }



    public List<SesionConsejeria> grillaListarIformacion() {
//        List<SesionConsejeria> novedades = servicios.consultarSesionConsejeriaJoinPorRol(AsistenteSesion.getInstance().getUsuario());
//        return novedades;
return null;
    }

}
