/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.constant.Constantes;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.model.UploadedFile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author cesard.chacond
 */
@ManagedBean
@ViewScoped
@WebServlet(name = "adjuntoArchivosMB")
public class AdjuntoArchivosMB {

    private UploadedFile archivo;
    private String url = Constantes.SERVIDORARCHIVOS + "identificacion=1030691234&nombreArchivo=";
    private final static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AdjuntoArchivosMB.class);
    private HttpServletRequest request;

    @PostConstruct
    public void init() {
        this.inicializarParametros();
        this.sessionPersona();
    }

    public void sessionPersona() {
        HttpSession sesion = request.getSession(true);
        String nombreUsuario
                = (String) (sesion.getAttribute("nombre"));
        sesion.setAttribute("password", "secreto");
    }

    private void inicializarParametros() {
        request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    }

    public String verificarFormato() {
        return "#";
    }

    public void upload(String extencion) {
        if (archivo != null) {
            if (archivo.getFileName().endsWith(extencion) || archivo.getFileName().endsWith(extencion.toUpperCase())) {
                String nomarchivo = new File(archivo.getFileName()).getAbsolutePath();
                File file = new File(nomarchivo);
                send(archivo.getFileName(), file);
                FacesMessage message = new FacesMessage("Successful", archivo.getFileName() + " is uploaded.");
                FacesContext.getCurrentInstance().addMessage(null, message);
            } else {
                FacesMessage message = new FacesMessage("mal", archivo.getFileName() + "formato incorrecto");
                FacesContext.getCurrentInstance().addMessage(null, message);
            }
        }
    }

    public boolean send(String nombreArchivo, File file) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            InputStream inputStream = null;
            OutputStream outputStreams = null;
            inputStream = archivo.getInputstream();
            outputStreams = new FileOutputStream(new File(file.getAbsolutePath()));
            int read = 0;
            byte[] bytes = new byte[1024];
            while ((read = inputStream.read(bytes)) != -1) {
                outputStreams.write(bytes, 0, read);
            }
            outputStreams.close();
            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("hv", new FileSystemResource(file));

            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.postForEntity(url + nombreArchivo, requestEntity, String.class);
            file.delete();
            return response.getStatusCode() == HttpStatus.ACCEPTED;
        } catch (IOException ex) {
            log.info(ex.getMessage());
        }
        return false;
    }

    public UploadedFile getArchivo() {
        return archivo;
    }

    public void setArchivo(UploadedFile archivo) {
        this.archivo = archivo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
