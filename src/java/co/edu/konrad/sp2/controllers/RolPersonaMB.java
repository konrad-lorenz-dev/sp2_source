package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.RolPersona;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosRolPersona;
import co.edu.konrad.sp2.servicios.ServiciosRolPersonaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "rolPersonaMB")
@ViewScoped
public class RolPersonaMB implements Serializable {

    private final static Logger log = Logger.getLogger(RolPersonaMB.class);
    private List<RolPersona> ListrolPersona;
    private List<RolPersona> rolPersonaFiltradas;
    private RolPersona rolPersona;
    private String operacion;
    private ServiciosRolPersona servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosRolPersonaImp();
        this.ListrolPersona = new ArrayList();
        this.rolPersona = new RolPersona();
        this.ListrolPersona = servicios.consultarRolPersonaJoin();

    }

    public List<RolPersona> getListRolPersona() {
        return ListrolPersona;
    }

    public void setListRolPersona(List<RolPersona> ListrolPersona) {
        this.ListrolPersona = ListrolPersona;
    }

    public List<RolPersona> getRolPersonaFiltradas() {
        return rolPersonaFiltradas;
    }

    public void setRolPersonaFiltradas(List<RolPersona> rolPersonaFiltradas) {
        this.rolPersonaFiltradas = rolPersonaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public RolPersona getRolPersona() {
        return rolPersona;
    }

    public void setRolPersona(RolPersona rolPersona) {
        this.rolPersona = rolPersona;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.rolPersona = new RolPersona();
        this.operacion = Constantes.CREAR;

//        this.rolPersona.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.rolPersona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.rolPersona != null) {
            this.operacion = Constantes.ACTUALIZAR;
            rolPersona.setRol(rolPersona.getRol());
            rolPersona.setPersona(rolPersona.getPersona());
            rolPersona.setObservacion(rolPersona.getObservacion());
            rolPersona.setDatCreacion(rolPersona.getDatCreacion());
            rolPersona.setDatModificacion(rolPersona.getDatModificacion());
            rolPersona.setUsrCreacion(rolPersona.getUsrCreacion());
//            rolPersona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearRolPersona(rolPersona);
        } else {
            this.servicios.actualizarRolPersona(rolPersona);
        }
        this.lista = true;
        this.crear = false;
        this.ListrolPersona = servicios.consultarRolPersona();
    }

    public void onBorrar() {
        if (this.rolPersona != null) {
            servicios.borrarRolPersona(rolPersona);
            this.ListrolPersona = servicios.consultarRolPersona();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
