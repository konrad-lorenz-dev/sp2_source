/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.constant.Constantes;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;

/**
 *
 * @author cesard.chacond
 */
@ManagedBean(name = "loginMB")
@ViewScoped
public class LoginMB implements Serializable {

    private final static Logger log = Logger.getLogger(LoginMB.class);
    private HttpServletRequest request;
    private  String URL;
    
    @PostConstruct
    public void init() {
        this.URL = Constantes.INICIOAPLICACION;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
    
}
