package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.DetallePracticante;
//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.DetalleSupEntidad;
import co.edu.konrad.sp2.bean.Persona;
import co.edu.konrad.sp2.bean.Practica;
import co.edu.konrad.sp2.bean.PracticanteConvenio;
import co.edu.konrad.sp2.bean.RegistroSemanalEntidad;
import co.edu.konrad.sp2.constant.Constantes;
//import co.edu.konrad.sp2.helper.SesionContexto;
import co.edu.konrad.sp2.servicios.ServiciosAreaEscolaris;
import co.edu.konrad.sp2.servicios.ServiciosAreaEscolarisImp;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticante;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticanteImp;
import co.edu.konrad.sp2.servicios.ServiciosDetalleSupEntidad;
import co.edu.konrad.sp2.servicios.ServiciosDetalleSupEntidadImp;
import co.edu.konrad.sp2.servicios.ServiciosEps;
import co.edu.konrad.sp2.servicios.ServiciosEpsImp;
import co.edu.konrad.sp2.servicios.ServiciosPersona;
import co.edu.konrad.sp2.servicios.ServiciosPersonaImp;
import co.edu.konrad.sp2.servicios.ServiciosPractica;
import co.edu.konrad.sp2.servicios.ServiciosPracticaImp;
import co.edu.konrad.sp2.servicios.ServiciosPracticanteConvenio;
import co.edu.konrad.sp2.servicios.ServiciosPracticanteConvenioImp;
import co.edu.konrad.sp2.servicios.ServiciosProgramaEscolaris;
import co.edu.konrad.sp2.servicios.ServiciosProgramaEscolarisImp;
import co.edu.konrad.sp2.servicios.ServiciosRegistroSemanalEntidad;
import co.edu.konrad.sp2.servicios.ServiciosRegistroSemanalEntidadImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;

@ManagedBean(name = "detalleSupEntidadMB")
@ViewScoped
public class DetalleSupEntidadMB implements Serializable {

    private final static Logger log = Logger.getLogger(DetalleSupEntidadMB.class);
    private List<DetalleSupEntidad> ListdetalleSupEntidad;
    private List<DetalleSupEntidad> detalleSupEntidadFiltradas;
    private List<DetallePracticante> ListdetallePracticante;
    private ServiciosDetallePracticante serviciosDetallePracticante;

    /*
    Para registro semanal
     */
    private RegistroSemanalEntidad registroSemanalEntidad;
    private List<RegistroSemanalEntidad> ListregistroSemanalEntidad;
    private List<RegistroSemanalEntidad> registroSemanalEntidadListHistorial;
    private ServiciosRegistroSemanalEntidad serviciosRegistroSemanaEntidad;

    private DetalleSupEntidad detalleSupEntidad;
    private DetalleSupEntidad detallePrincipalEntidadInf;
    private Practica practica;
    private ServiciosPractica servicioPractica;
    private PracticanteConvenio practicanteConvenio;
    private ServiciosPracticanteConvenio servicioPracticaConvenio;
    private String operacion;
    private ServiciosDetalleSupEntidad servicios;
    private ServiciosDetallePracticante serviciosPersonaPracticante;
    private ServiciosEps servicioEps;
    private ServiciosAreaEscolaris servicioAreaEscolaris;
    private ServiciosProgramaEscolaris servicioProgramaEscolaris;
    private DetallePracticante detallePracticante;
    boolean crear;
    boolean lista;

    /*
    Menu principal de Detalle Sup entidad
     */
    private boolean menuPrincipalDetalleSupEntidad;
    private boolean submenuPrincipalDetalleSupEntidad = true;
    private String NombreGrilla;
    /*
    boolean de menus principales
     */
    private boolean menuPostualdos, menuAsiganados, menuRetiroEstudiante, menuNotificaciones;

    /*
    boolean de submenus 
     */
    private boolean subMenuGrillaPostulados = false, subMenuGrillaRetirados = false, panDetalleSupEntidad = false, menuPostuladosPerfil = false, menuPostuladosProcesos = false, menuPostuladosDesempeño = false;
    private boolean menuAsignadosProceso = false, menuAsignadosRegistro = false, menuAsignadosReferenciacion = false;
    protected boolean formularioPersonaOcultar = false;
    /*
    boolean de botones de detalles
     */
    private boolean btnDetallePostulados = false, btnDetalleAsiganados = false;
    /*
    Actualizacion del perfil entidad
     */
    private Persona personaEntidad;
    private ServiciosPersona servicioPersona;
    protected Long idPersonaPracticante;
    private boolean menuSupervisorEntidadPerfil = false;
    private boolean ocultarMenuPerfilEntidad = true;

    @PostConstruct
    public void init() {
//        if (SesionContexto.getInstance().getAttribute(Constantes.USUARIO) == null) {
//            SesionContexto.getInstance().setAttribute(Constantes.PERSONA, AsistenteSesion.getInstance().getPersona());
//            SesionContexto.getInstance().setAttribute(Constantes.USUARIO, AsistenteSesion.getInstance().getUsuario());
//            SesionContexto.getInstance().setAttribute(Constantes.CODIGO_PERSONA, AsistenteSesion.getInstance().getPersona().getCodigo());
//            SesionContexto.getInstance().setAttribute("CARGO", AsistenteSesion.getInstance().getPersona().getCargo());
//            String cargo = (String) SesionContexto.getInstance().getAttribute("CARGO");
//            if (!"Practicante".equals(cargo)) {
//                SesionContexto.getInstance().setAttribute(Constantes.CEDULA, AsistenteSesion.getInstance().getPersona().getCedula());
//            }
//
//            //###########################################################################################################################
//        }
        this.registroSemanalEntidad = new RegistroSemanalEntidad();
        this.personaEntidad = new Persona();
        this.detallePracticante = new DetallePracticante();
        this.practica = new Practica();
        this.practicanteConvenio = new PracticanteConvenio();
        this.detalleSupEntidad = new DetalleSupEntidad();

        this.servicios = new ServiciosDetalleSupEntidadImp();
        this.serviciosPersonaPracticante = new ServiciosDetallePracticanteImp();
        this.servicioPersona = new ServiciosPersonaImp();
        this.servicioEps = new ServiciosEpsImp();
        this.servicioAreaEscolaris = new ServiciosAreaEscolarisImp();
        this.servicioProgramaEscolaris = new ServiciosProgramaEscolarisImp();
        this.servicioPractica = new ServiciosPracticaImp();
        this.servicioPracticaConvenio = new ServiciosPracticanteConvenioImp();
        this.serviciosDetallePracticante = new ServiciosDetallePracticanteImp();
        this.serviciosRegistroSemanaEntidad = new ServiciosRegistroSemanalEntidadImp();

        this.ListdetallePracticante = new ArrayList();
        this.ListregistroSemanalEntidad = new ArrayList();
        this.registroSemanalEntidadListHistorial = new ArrayList();
        this.ListdetalleSupEntidad = new ArrayList();
        
        this.ListdetalleSupEntidad = servicios.consultarDetalleSupEntidadJoin();
        
        CargueDatos();

        this.menuPrincipalDetalleSupEntidad = true;
        this.lista = true;
        this.crear = false;
    }

    public List<DetalleSupEntidad> getListDetalleSupEntidad() {
        return ListdetalleSupEntidad;
    }

    public void setListDetalleSupEntidad(List<DetalleSupEntidad> ListdetalleSupEntidad) {
        this.ListdetalleSupEntidad = ListdetalleSupEntidad;
    }

    public List<DetalleSupEntidad> getDetalleSupEntidadFiltradas() {
        return detalleSupEntidadFiltradas;
    }

    public void setDetalleSupEntidadFiltradas(List<DetalleSupEntidad> detalleSupEntidadFiltradas) {
        this.detalleSupEntidadFiltradas = detalleSupEntidadFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public DetalleSupEntidad getDetalleSupEntidad() {
        return detalleSupEntidad;
    }

    public void setDetalleSupEntidad(DetalleSupEntidad detalleSupEntidad) {
        this.detalleSupEntidad = detalleSupEntidad;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public boolean isMenuPostualdos() {
        return menuPostualdos;
    }

    public void setMenuPostualdos(boolean menuPostualdos) {
        this.menuPostualdos = menuPostualdos;
    }

    public boolean isMenuAsiganados() {
        return menuAsiganados;
    }

    public void setMenuAsiganados(boolean menuAsiganados) {
        this.menuAsiganados = menuAsiganados;
    }

    public boolean isMenuRetiroEstudiante() {
        return menuRetiroEstudiante;
    }

    public void setMenuRetiroEstudiante(boolean menuRetiroEstudiante) {
        this.menuRetiroEstudiante = menuRetiroEstudiante;
    }

    public boolean isMenuNotificaciones() {
        return menuNotificaciones;
    }

    public void setMenuNotificaciones(boolean menuNotificaciones) {
        this.menuNotificaciones = menuNotificaciones;
    }

    public boolean isSubMenuGrillaPostulados() {
        return subMenuGrillaPostulados;
    }

    public void setSubMenuGrillaPostulados(boolean subMenuGrillaPostulados) {
        this.subMenuGrillaPostulados = subMenuGrillaPostulados;
    }

    public boolean isPanDetalleSupEntidad() {
        return panDetalleSupEntidad;
    }

    public void setPanDetalleSupEntidad(boolean panDetalleSupEntidad) {
        this.panDetalleSupEntidad = panDetalleSupEntidad;
    }

    public boolean isMenuPostuladosPerfil() {
        return menuPostuladosPerfil;
    }

    public void setMenuPostuladosPerfil(boolean MenuPostuladosPerfil) {
        this.menuPostuladosPerfil = MenuPostuladosPerfil;
    }

    public boolean isFormularioPersonaOcultar() {
        return formularioPersonaOcultar;
    }

    public void setFormularioPersonaOcultar(boolean formularioPersonaOcultar) {
        this.formularioPersonaOcultar = formularioPersonaOcultar;
    }

    public DetallePracticante getDetallePracticante() {
        return detallePracticante;
    }

    public void setDetallePracticante(DetallePracticante detallePracticante) {
        this.detallePracticante = detallePracticante;
    }

    public boolean isBtnDetallePostulados() {
        return btnDetallePostulados;
    }

    public void setBtnDetallePostulados(boolean btnDetallePostulados) {
        this.btnDetallePostulados = btnDetallePostulados;
    }

    public boolean isBtnDetalleAsiganados() {
        return btnDetalleAsiganados;
    }

    public void setBtnDetalleAsiganados(boolean btnDetalleAsiganados) {
        this.btnDetalleAsiganados = btnDetalleAsiganados;
    }

    public boolean isMenuPostuladosProcesos() {
        return menuPostuladosProcesos;
    }

    public void setMenuPostuladosProcesos(boolean menuPostuladosProcesos) {
        this.menuPostuladosProcesos = menuPostuladosProcesos;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public PracticanteConvenio getPracticanteConvenio() {
        return practicanteConvenio;
    }

    public void setPracticanteConvenio(PracticanteConvenio practicanteConvenio) {
        this.practicanteConvenio = practicanteConvenio;
    }

    public boolean isMenuPostuladosDesempeño() {
        return menuPostuladosDesempeño;
    }

    public void setMenuPostuladosDesempeño(boolean menuPostuladosDesempeño) {
        this.menuPostuladosDesempeño = menuPostuladosDesempeño;
    }

    public boolean isMenuAsignadosProceso() {
        return menuAsignadosProceso;
    }

    public void setMenuAsignadosProceso(boolean menuAsignadosProceso) {
        this.menuAsignadosProceso = menuAsignadosProceso;
    }

    public boolean isMenuAsignadosRegistro() {
        return menuAsignadosRegistro;
    }

    public void setMenuAsignadosRegistro(boolean menuAsignadosRegistro) {
        this.menuAsignadosRegistro = menuAsignadosRegistro;
    }

    public boolean isMenuAsignadosReferenciacion() {
        return menuAsignadosReferenciacion;
    }

    public void setMenuAsignadosReferenciacion(boolean menuAsignadosReferenciacion) {
        this.menuAsignadosReferenciacion = menuAsignadosReferenciacion;
    }

    public List<DetallePracticante> getListdetallePracticante() {
        return ListdetallePracticante;
    }

    public void setListdetallePracticante(List<DetallePracticante> ListdetallePracticante) {
        this.ListdetallePracticante = ListdetallePracticante;
    }

    public List<RegistroSemanalEntidad> getListregistroSemanalEntidad() {
        return ListregistroSemanalEntidad;
    }

    public void setListregistroSemanalEntidad(List<RegistroSemanalEntidad> ListregistroSemanalEntidad) {
        this.ListregistroSemanalEntidad = ListregistroSemanalEntidad;
    }

    public List<RegistroSemanalEntidad> getRegistroSemanalEntidadListHistorial() {
        return registroSemanalEntidadListHistorial;
    }

    public void setRegistroSemanalEntidadListHistorial(List<RegistroSemanalEntidad> registroSemanalEntidadListHistorial) {
        this.registroSemanalEntidadListHistorial = registroSemanalEntidadListHistorial;
    }

    public DetalleSupEntidad getDetallePrincipalEntidadInf() {
        return detallePrincipalEntidadInf;
    }

    public void setDetallePrincipalEntidadInf(DetalleSupEntidad detallePrincipalEntidadInf) {
        this.detallePrincipalEntidadInf = detallePrincipalEntidadInf;
    }

    public boolean isMenuPrincipalDetalleSupEntidad() {
        return menuPrincipalDetalleSupEntidad;
    }

    public void setMenuPrincipalDetalleSupEntidad(boolean menuPrincipalDetalleSupEntidad) {
        this.menuPrincipalDetalleSupEntidad = menuPrincipalDetalleSupEntidad;
    }

    public String getNombreGrilla() {
        return NombreGrilla;
    }

    public void setNombreGrilla(String NombreGrilla) {
        this.NombreGrilla = NombreGrilla;
    }

    public boolean ismenuSupervisorEntidadPerfil() {
        return menuSupervisorEntidadPerfil;
    }

    public void setmenuSupervisorEntidadPerfil(boolean MenuSupervisorEntidadPerfil) {
        this.menuSupervisorEntidadPerfil = MenuSupervisorEntidadPerfil;
    }

    public boolean isSubmenuPrincipalDetalleSupEntidad() {
        return submenuPrincipalDetalleSupEntidad;
    }

    public void setSubmenuPrincipalDetalleSupEntidad(boolean submenuPrincipalDetalleSupEntidad) {
        this.submenuPrincipalDetalleSupEntidad = submenuPrincipalDetalleSupEntidad;
    }

    public boolean isSubMenuGrillaRetirados() {
        return subMenuGrillaRetirados;
    }

    public void setSubMenuGrillaRetirados(boolean subMenuGrillaRetirados) {
        this.subMenuGrillaRetirados = subMenuGrillaRetirados;
    }

    public boolean isOcultarMenuPerfilEntidad() {
        return ocultarMenuPerfilEntidad;
    }

    public void setOcultarMenuPerfilEntidad(boolean ocultarMenuPerfilEntidad) {
        this.ocultarMenuPerfilEntidad = ocultarMenuPerfilEntidad;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.detalleSupEntidad = new DetalleSupEntidad();
        this.operacion = Constantes.CREAR;

//        this.detalleSupEntidad.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.detalleSupEntidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
    }

    public void onActulizarRegistro() {

        if (this.detalleSupEntidad != null) {
            this.operacion = Constantes.ACTUALIZAR;
            detalleSupEntidad.setPersona(detalleSupEntidad.getPersona());
            detalleSupEntidad.setEntidad(detalleSupEntidad.getEntidad());
            detalleSupEntidad.setDatVigenciaDesde(detalleSupEntidad.getDatVigenciaDesde());
            detalleSupEntidad.setDatVigenciaHasta(detalleSupEntidad.getDatVigenciaHasta());
            detalleSupEntidad.setEstado(detalleSupEntidad.getEstado());
            detalleSupEntidad.setDescripcion(detalleSupEntidad.getDescripcion());
            detalleSupEntidad.setObservacion(detalleSupEntidad.getObservacion());
            detalleSupEntidad.setDatCreacion(detalleSupEntidad.getDatCreacion());
            detalleSupEntidad.setDatModificacion(detalleSupEntidad.getDatModificacion());
            detalleSupEntidad.setUsrCreacion(detalleSupEntidad.getUsrCreacion());
//            detalleSupEntidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearDetalleSupEntidad(detalleSupEntidad);
        } else {
            this.servicios.actualizarDetalleSupEntidad(detalleSupEntidad);
        }
        this.lista = true;
        this.crear = false;
        this.ListdetalleSupEntidad = servicios.consultarDetalleSupEntidad();
    }

    public void onBorrar() {
        if (this.detalleSupEntidad != null) {
            servicios.borrarDetalleSupEntidad(detalleSupEntidad);
            this.ListdetalleSupEntidad = servicios.consultarDetalleSupEntidad();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

    /*
    MENU DE DIRECCIONAMIENTO PRINCIPAL  
     */
    public void Menu(int seleccion) {
        if (this.panDetalleSupEntidad == true) {
            this.panDetalleSupEntidad = false;
            MenuPostuladosDetalle(0);
        }
        if (this.submenuPrincipalDetalleSupEntidad == false) {
            menuPostualdos = false;
            menuAsiganados = false;
            menuRetiroEstudiante = false;
            menuNotificaciones = false;
        }

        this.menuPrincipalDetalleSupEntidad = false;
        this.submenuPrincipalDetalleSupEntidad = true;
        this.ocultarMenuPerfilEntidad = false;
        switch (seleccion) {
            case 0: {
                menuPostualdos = false;
                menuAsiganados = false;
                menuRetiroEstudiante = false;
                menuNotificaciones = false;
                subMenuGrillaPostulados = false;
                subMenuGrillaRetirados = false;
                break;
            }
            case 1: {
                if (menuPostualdos == true) {
                    menuPostualdos = false;
                    subMenuGrillaPostulados = false;
                    subMenuGrillaRetirados = false;
                    btnDetallePostulados = false;
                    btnDetalleAsiganados = false;
                } else {
                    menuPostualdos = true;
                    this.NombreGrilla = "Postulados";
                    btnDetallePostulados = true;
                    subMenuGrillaPostulados = true;
                    subMenuGrillaRetirados = false;
                    EstadoPractica(Constantes.POSTULADOS);
                    btnDetalleAsiganados = false;
                    menuRetiroEstudiante = false;
                    menuNotificaciones = false;
                    menuAsiganados = false;
                }
                break;
            }

            case 2: {
                if (menuAsiganados == true) {
                    menuAsiganados = false;
                    menuPostualdos = false;
                    subMenuGrillaPostulados = false;
                    subMenuGrillaRetirados = false;
                    btnDetallePostulados = false;
                    btnDetalleAsiganados = false;
                } else {
                    menuAsiganados = true;
                    subMenuGrillaPostulados = true;
                    subMenuGrillaRetirados = false;
                    this.NombreGrilla = "Asignados";
                    EstadoPractica(Constantes.ASIGANDOS);
                    btnDetalleAsiganados = true;
                    btnDetallePostulados = false;
                    menuRetiroEstudiante = false;
                    menuNotificaciones = false;
                    menuPostualdos = false;
                }
                break;
            }
            case 3: {
                if (menuRetiroEstudiante == true) {
                    menuRetiroEstudiante = false;
                    subMenuGrillaPostulados = false;
                    subMenuGrillaRetirados = false;
                    btnDetallePostulados = false;
                    btnDetalleAsiganados = false;

                } else {
                    menuRetiroEstudiante = true;
                    this.NombreGrilla = "Retiro Estudiantes";
                    subMenuGrillaPostulados = false;
                    subMenuGrillaRetirados = true;
                    btnDetalleAsiganados = false;
                    btnDetallePostulados = false;
                    menuNotificaciones = false;
                    menuPostualdos = false;
                    menuAsiganados = false;
                }
                break;
            }
            case 4: {
                if (menuNotificaciones == true) {
                    menuNotificaciones = false;
                    menuPostualdos = false;
                    menuAsiganados = false;
                    menuRetiroEstudiante = false;
                    subMenuGrillaPostulados = false;
                    subMenuGrillaRetirados = false;
                    btnDetallePostulados = false;
                    btnDetalleAsiganados = false;
                    panDetalleSupEntidad = false;
                } else {
                    menuNotificaciones = true;
                    this.NombreGrilla = "Notificaciones";
                    btnDetalleAsiganados = false;
                    btnDetallePostulados = false;
                    subMenuGrillaPostulados = false;
                    subMenuGrillaRetirados = false;
                    menuRetiroEstudiante = false;
                    menuPostualdos = false;
                    menuAsiganados = false;
                    panDetalleSupEntidad = false;
                }
                break;
            }
        }

    }

    /*
    Menu Detalle Postulados (+)
     */
    public void DetallePostulados(Long idDetallePracticante, Long idPersonaPraticante) {
        Menu(0);
        this.idPersonaPracticante = idPersonaPraticante;
        this.panDetalleSupEntidad = true;
        if (this.btnDetallePostulados == true) {
            this.menuPostuladosPerfil = true;
        } else if (this.btnDetalleAsiganados == true) {
            this.menuPostuladosPerfil = true;
        }
        this.detallePracticante = this.serviciosPersonaPracticante.consultarDetallePracticanteJoinById(idDetallePracticante);
        /*
        Servicio para procesos de Postulados
         */
        if (btnDetallePostulados) {
            this.practica = this.servicioPractica.consultarEstadoPractica(this.detallePracticante.getSeqDetallePracticante());
            if (!this.practica.getConstantes().equals(Constantes.OK)) {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "No tiene Proceso Registrado");
                PrimeFaces.current().dialog().showMessageDynamic(message);
            }
        } else if (btnDetalleAsiganados) {
            this.practica = this.servicioPractica.informacionPractica(this.detallePracticante.getSeqDetallePracticante());
//            if (this.practica.getConstantes().equals(Constantes.OK)) {
            this.ListregistroSemanalEntidad = this.serviciosRegistroSemanaEntidad.consultarRegistroSemanalEntidadEstadoValido(idPersonaPraticante);
            this.registroSemanalEntidadListHistorial = this.serviciosRegistroSemanaEntidad.consultarRegistroSemanalEntidadHistorial(idPersonaPraticante);
//            } else {
//                this.ListregistroSemanalEntidad = new ArrayList();
//                this.registroSemanalEntidadListHistorial = new ArrayList();
//                //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "No tiene Proceso"));
//            }

        }

    }

    /*
    Menu interno
     */
    public void MenuInterno(int seleccion) {
        Menu(0);
        switch (seleccion) {
            case 0: {
                /*
    Volviendo al menu principal
                 */
                this.menuPrincipalDetalleSupEntidad = true;
                this.submenuPrincipalDetalleSupEntidad = false;
                this.ocultarMenuPerfilEntidad = true;
                break;
            }
            case 1: {
                Menu(1);
                break;
            }
            case 2: {
                Menu(2);
                break;
            }
            case 3: {
                Menu(3);
                break;
            }
            case 4: {
                Menu(4);
                break;
            }
        }
    }

    /*
    Mostrar estado practica List
     */
    public void EstadoPractica(String Estado) {
        this.ListdetallePracticante = this.serviciosDetallePracticante.consultarDetallePracticanteEstadoPracticanteJoin(Estado);
    }


    /*
    Guardar Registro Semanal
     */
    public void GuardarRegistroSemanal(Long idRegistroSemanal, String estadoValido) {
        System.out.println("");
        String constante = null;
        if (!estadoValido.equals(Constantes.SINREVISAR)) {
            this.registroSemanalEntidad = this.serviciosRegistroSemanaEntidad.consultarRegistroSemanalEntidadById(idRegistroSemanal);
            if (this.registroSemanalEntidad.getConstante().equals(Constantes.OK)) {
                this.registroSemanalEntidad.setEstadoValido(estadoValido);
                constante = this.serviciosRegistroSemanaEntidad.actualizarRegistroSemanalEntidad(registroSemanalEntidad);
                if (constante.equals(Constantes.OK)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Información Guardada Corectamente."));
                    this.ListregistroSemanalEntidad = this.serviciosRegistroSemanaEntidad.consultarRegistroSemanalEntidadEstadoValido(this.idPersonaPracticante);
                    this.registroSemanalEntidadListHistorial = this.serviciosRegistroSemanaEntidad.consultarRegistroSemanalEntidadHistorial(this.idPersonaPracticante);
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Hay un problema no se puede guardar la información."));
                }
            }
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "No hay ningún estado seleccionado."));
        }
    }

    /*
    Submenus Panel
     */
    public void MenuPostuladosDetalle(int seleccion) {
        switch (seleccion) {
            case 0: {
                menuPostuladosPerfil = false;
                menuPostuladosProcesos = false;
                menuPostuladosDesempeño = false;
                menuAsignadosProceso = false;
                menuAsignadosRegistro = false;
                menuAsignadosReferenciacion = false;
                btnDetallePostulados = false;
                btnDetalleAsiganados = false;
                break;
            }
            case 1: {
                if (btnDetallePostulados) {
                    if (menuPostuladosPerfil == true) {
                        menuPostuladosPerfil = false;
                        menuPostuladosProcesos = false;
                        menuPostuladosDesempeño = false;
                    } else {
                        menuPostuladosPerfil = true;
                        menuPostuladosProcesos = false;
                        menuPostuladosDesempeño = false;
                    }
                    break;
                } else if (btnDetalleAsiganados) {
                    if (menuPostuladosPerfil == true) {
                        menuPostuladosPerfil = false;
                        menuAsignadosProceso = false;
                        menuAsignadosRegistro = false;
                        menuAsignadosReferenciacion = false;
                    } else {
                        menuPostuladosPerfil = true;
                        menuAsignadosProceso = false;
                        menuAsignadosRegistro = false;
                        menuAsignadosReferenciacion = false;
                    }
                    break;
                }

            }
            case 2: {
                if (menuPostuladosProcesos == true) {
                    menuPostuladosProcesos = false;
                    menuPostuladosPerfil = false;
                    menuPostuladosDesempeño = false;
                } else {
                    menuPostuladosProcesos = true;
                    menuPostuladosPerfil = false;
                    menuPostuladosDesempeño = false;
                }
                break;
            }
            case 3: {
                if (menuPostuladosDesempeño == true) {
                    menuPostuladosDesempeño = false;
                    menuPostuladosProcesos = false;
                    menuPostuladosPerfil = false;
                } else {
                    menuPostuladosDesempeño = true;
                    menuPostuladosPerfil = false;
                    menuPostuladosProcesos = false;
                }
                break;
            }
            case 4: {
                if (menuAsignadosProceso == true) {
                    menuAsignadosProceso = false;
                    menuAsignadosRegistro = false;
                    menuAsignadosReferenciacion = false;
                    menuPostuladosPerfil = false;
                } else {
                    menuAsignadosProceso = true;
                    menuAsignadosRegistro = false;
                    menuAsignadosReferenciacion = false;
                    menuPostuladosPerfil = false;
                }
                break;
            }
            case 5: {
                if (menuAsignadosRegistro == true) {
                    menuAsignadosProceso = false;
                    menuAsignadosRegistro = false;
                    menuAsignadosReferenciacion = false;
                    menuPostuladosPerfil = false;
                } else {
                    menuAsignadosProceso = false;
                    menuAsignadosRegistro = true;
                    menuAsignadosReferenciacion = false;
                    menuPostuladosPerfil = false;
                }
                break;
            }

            case 6: {
                if (menuAsignadosReferenciacion == true) {
                    menuAsignadosProceso = false;
                    menuAsignadosRegistro = false;
                    menuAsignadosReferenciacion = false;
                    menuPostuladosPerfil = false;
                } else {
                    menuAsignadosProceso = false;
                    menuAsignadosRegistro = false;
                    menuAsignadosReferenciacion = true;
                    menuPostuladosPerfil = false;
                }
                break;
            }
        }
    }

    /*
    Cargue De datos Persona entidad
     */
    public void CargueDatos() {
        this.detallePrincipalEntidadInf = new DetalleSupEntidad();
//        this.detallePrincipalEntidadInf.setPersona(AsistenteSesion.getInstance().getPersona());
    }

    /*
    ACTUALIZACION DEL SUPERVISOR INFORMACION
     */
    public void ActualizarDatosPerfil() {
        if (this.detallePrincipalEntidadInf != null) {
            this.operacion = Constantes.ACTUALIZAR;
            this.personaEntidad = detallePrincipalEntidadInf.getPersona();
            this.servicioPersona.actualizarPersona(personaEntidad);
//            this.detallePrincipalEntidadInf.setPersona(this.servicioPersona.consultarPersonaById(AsistenteSesion.getInstance().getPersona().getSeqPersona()));
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Información Guardada."));
        }
    }

    /*
    MENU DE SUPERVISOR
     */
    public void MenuPerfilSupervisor(int valor) {

        switch (valor) {
            case 0: { // cerrar perfil supervisor
                menuPrincipalDetalleSupEntidad = true;
                menuSupervisorEntidadPerfil = false;
                submenuPrincipalDetalleSupEntidad = false;
                break;
            }
            case 1: { // abrir perfil supervisor
                menuPrincipalDetalleSupEntidad = false;
                menuSupervisorEntidadPerfil = true;
                submenuPrincipalDetalleSupEntidad = false;
                break;
            }

        }
    }

}
