package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.OfertaLaboral;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosOfertaLaboral;
import co.edu.konrad.sp2.servicios.ServiciosOfertaLaboralImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "ofertaLaboralMB")
@ViewScoped
public class OfertaLaboralMB implements Serializable {

    private final static Logger log = Logger.getLogger(OfertaLaboralMB.class);
    private List<OfertaLaboral> ListofertaLaboral;
    private List<OfertaLaboral> ofertaLaboralFiltradas;
    private OfertaLaboral ofertaLaboral;
    private String operacion;
    private ServiciosOfertaLaboral servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosOfertaLaboralImp();
        this.ListofertaLaboral = new ArrayList();
        this.ofertaLaboral = new OfertaLaboral();
        this.ListofertaLaboral = servicios.consultarOfertaLaboralJoin();

    }

    public List<OfertaLaboral> getListOfertaLaboral() {
        return ListofertaLaboral;
    }

    public void setListOfertaLaboral(List<OfertaLaboral> ListofertaLaboral) {
        this.ListofertaLaboral = ListofertaLaboral;
    }

    public List<OfertaLaboral> getOfertaLaboralFiltradas() {
        return ofertaLaboralFiltradas;
    }

    public void setOfertaLaboralFiltradas(List<OfertaLaboral> ofertaLaboralFiltradas) {
        this.ofertaLaboralFiltradas = ofertaLaboralFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public OfertaLaboral getOfertaLaboral() {
        return ofertaLaboral;
    }

    public void setOfertaLaboral(OfertaLaboral ofertaLaboral) {
        this.ofertaLaboral = ofertaLaboral;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.ofertaLaboral = new OfertaLaboral();
        this.operacion = Constantes.CREAR;

//        this.ofertaLaboral.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.ofertaLaboral.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.ofertaLaboral != null) {
            this.operacion = Constantes.ACTUALIZAR;
            ofertaLaboral.setConvenio(ofertaLaboral.getConvenio());
            ofertaLaboral.setOferta(ofertaLaboral.getOferta());
            ofertaLaboral.setDescripcion(ofertaLaboral.getDescripcion());
            ofertaLaboral.setObservacion(ofertaLaboral.getObservacion());
            ofertaLaboral.setEstado(ofertaLaboral.getEstado());
            ofertaLaboral.setDatVigenciaDesde(ofertaLaboral.getDatVigenciaDesde());
            ofertaLaboral.setDatVigenciaHasta(ofertaLaboral.getDatVigenciaHasta());
            ofertaLaboral.setDatCreacion(ofertaLaboral.getDatCreacion());
            ofertaLaboral.setDatModificacion(ofertaLaboral.getDatModificacion());
            ofertaLaboral.setUsrCreacion(ofertaLaboral.getUsrCreacion());
//            ofertaLaboral.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearOfertaLaboral(ofertaLaboral);
        } else {
            this.servicios.actualizarOfertaLaboral(ofertaLaboral);
        }
        this.lista = true;
        this.crear = false;
        this.ListofertaLaboral = servicios.consultarOfertaLaboral();
    }

    public void onBorrar() {
        if (this.ofertaLaboral != null) {
            servicios.borrarOfertaLaboral(ofertaLaboral);
            this.ListofertaLaboral = servicios.consultarOfertaLaboral();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
