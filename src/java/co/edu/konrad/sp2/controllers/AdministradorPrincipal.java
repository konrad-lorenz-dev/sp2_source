/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.Persona;
import co.edu.konrad.sp2.servicios.ServiciosPersona;
import co.edu.konrad.sp2.servicios.ServiciosPersonaImp;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;

/**
 *
 * @author cesard.chacond
 */
@ManagedBean
@ViewScoped
@WebServlet(name = "administradorPrincipalMB")
@Controller
public class AdministradorPrincipal implements Serializable {

    private final static Logger log = Logger.getLogger(AdministradorPrincipal.class);
    private HttpServletRequest request;
    private String nombrePersona;
    private String idRequest;
    private ServiciosPersona servicioPersona;
    private Persona persona;
    private HttpSession session;

    @PostConstruct
    public void init() {
        this.inicializarParametros();
        this.obtenerParametrosRequest();
        this.funcionalidad();
        
    }
    
    private void inicializarParametros() {
        servicioPersona = new ServiciosPersonaImp();
        request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        session = request.getSession();
//      this.formulario = (Formulario) session.getAttribute("formularioPersona");
//      session.setAttribute("formularioPersona", this.formulario);
    }
    
    private void obtenerParametrosRequest(){
        this.idRequest = request.getParameter("id");
    }

    private void funcionalidad(){
        this.obtenerPersona();
    }
    
    private void obtenerPersona(){
        persona = this.servicioPersona.consultarPersonaById(Long.parseLong(idRequest));
        nombrePersona = persona.getNombres() + " " + persona.getApellidos();
    }
    
    public String getNombrePersona() {
        return nombrePersona;
    }

    public void setNombrePersona(String nombrePersona) {
        this.nombrePersona = nombrePersona;
    }
    
    
}
