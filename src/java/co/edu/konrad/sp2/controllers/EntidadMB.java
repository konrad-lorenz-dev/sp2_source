package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Entidad;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosEntidad;
import co.edu.konrad.sp2.servicios.ServiciosEntidadImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "entidadMB")
@ViewScoped
public class EntidadMB implements Serializable {

    private final static Logger log = Logger.getLogger(EntidadMB.class);
    private List<Entidad> Listentidad;
    private List<Entidad> entidadFiltradas;
    private Entidad entidad;
    private String operacion;
    private ServiciosEntidad servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosEntidadImp();
        this.Listentidad = new ArrayList();
        this.entidad = new Entidad();
        this.Listentidad = servicios.consultarEntidadJoin();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<Entidad> getListEntidad() {
        return Listentidad;
    }

    public void setListEntidad(List<Entidad> Listentidad) {
        this.Listentidad = Listentidad;
    }

    public List<Entidad> getEntidadFiltradas() {
        return entidadFiltradas;
    }

    public void setEntidadFiltradas(List<Entidad> entidadFiltradas) {
        this.entidadFiltradas = entidadFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Entidad getEntidad() {
        return entidad;
    }

    public void setEntidad(Entidad entidad) {
        this.entidad = entidad;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.entidad = new Entidad();
        this.operacion = Constantes.CREAR;

//        this.entidad.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.entidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onVistaActualizar() {
        if (this.entidad != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onActulizarRegistro() {

        if (this.entidad != null) {
            this.operacion = Constantes.ACTUALIZAR;
            entidad.setFichaTecnica(entidad.getFichaTecnica());
            entidad.setNit(entidad.getNit());
            entidad.setNombre(entidad.getNombre());
            entidad.setEstadoEntidad(entidad.getEstadoEntidad());
            entidad.setCorreoElectronico(entidad.getCorreoElectronico());
            entidad.setDireccion(entidad.getDireccion());
            entidad.setTelefono(entidad.getTelefono());
            entidad.setRepresentante(entidad.getRepresentante());
            entidad.setEstrato(entidad.getEstrato());
            entidad.setObjetoSocial(entidad.getObjetoSocial());
            entidad.setObservacion(entidad.getObservacion());
            entidad.setEstadoMostrar(entidad.getEstadoMostrar());
            entidad.setDatCreacion(entidad.getDatCreacion());
            entidad.setDatModificacion(entidad.getDatModificacion());
            entidad.setUsrCreacion(entidad.getUsrCreacion());
//            entidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarEntidad(entidad);

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearEntidad(entidad);
        } else {
            this.servicios.actualizarEntidad(entidad);
        }
        this.lista = true;
        this.crear = false;
        this.Listentidad = servicios.consultarEntidad();
    }

    public void onBorrar() {
        if (this.entidad != null) {
            servicios.borrarEntidad(entidad);
            this.Listentidad = servicios.consultarEntidad();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
