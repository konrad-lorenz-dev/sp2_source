package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Novedad;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosNovedad;
import co.edu.konrad.sp2.servicios.ServiciosNovedadImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "novedadMB")
@ViewScoped
public class NovedadMB implements Serializable {

    private final static Logger log = Logger.getLogger(NovedadMB.class);
    private List<Novedad> Listnovedad;
    private List<Novedad> novedadFiltradas;
    private Novedad novedad;
    private String operacion;
    private ServiciosNovedad servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosNovedadImp();
        this.Listnovedad = new ArrayList();
        this.novedad = new Novedad();
        this.Listnovedad = servicios.consultarNovedadJoin();

    }

    public List<Novedad> getListNovedad() {
        return Listnovedad;
    }

    public void setListNovedad(List<Novedad> Listnovedad) {
        this.Listnovedad = Listnovedad;
    }

    public List<Novedad> getNovedadFiltradas() {
        return novedadFiltradas;
    }

    public void setNovedadFiltradas(List<Novedad> novedadFiltradas) {
        this.novedadFiltradas = novedadFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Novedad getNovedad() {
        return novedad;
    }

    public void setNovedad(Novedad novedad) {
        this.novedad = novedad;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.novedad = new Novedad();
        this.operacion = Constantes.CREAR;
//
//        this.novedad.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.novedad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.novedad != null) {
            this.operacion = Constantes.ACTUALIZAR;
            novedad.setTipoNovedad(novedad.getTipoNovedad());
            novedad.setPersona(novedad.getPersona());
            novedad.setNovedadPersona(novedad.getNovedadPersona());
            novedad.setEstado(novedad.getEstado());
            novedad.setObservacion(novedad.getObservacion());
            novedad.setDatCreacion(novedad.getDatCreacion());
            novedad.setDatModificacion(novedad.getDatModificacion());
            novedad.setUsrCreacion(novedad.getUsrCreacion());
//            novedad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            try {
                this.servicios.crearNovedad(novedad);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            this.servicios.actualizarNovedad(novedad);
        }
        this.lista = true;
        this.crear = false;
        this.Listnovedad = servicios.consultarNovedad();
    }

    public void onBorrar() {
        if (this.novedad != null) {
            servicios.borrarNovedad(novedad);
            this.Listnovedad = servicios.consultarNovedad();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

    /*
    Consultar novedad por id para el detalle de la entidad
    */
    public void DetalleNovedadEntidad(Long idNovedad){
        this.novedad = this.servicios.consultarNovedadJoinById(idNovedad);
    }
}
