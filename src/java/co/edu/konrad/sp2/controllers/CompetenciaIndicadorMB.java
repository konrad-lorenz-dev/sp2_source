package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.CompetenciaIndicador;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosCompetenciaIndicador;
import co.edu.konrad.sp2.servicios.ServiciosCompetenciaIndicadorImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "competenciaIndicadorMB")
@ViewScoped
public class CompetenciaIndicadorMB implements Serializable {

    private final static Logger log = Logger.getLogger(CompetenciaIndicadorMB.class);
    private List<CompetenciaIndicador> ListcompetenciaIndicador;
    private List<CompetenciaIndicador> competenciaIndicadorFiltradas;
    private CompetenciaIndicador competenciaIndicador;
    private String operacion;
    private ServiciosCompetenciaIndicador servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosCompetenciaIndicadorImp();
        this.ListcompetenciaIndicador = new ArrayList();
        this.competenciaIndicador = new CompetenciaIndicador();
        this.ListcompetenciaIndicador = servicios.consultarCompetenciaIndicador();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<CompetenciaIndicador> getListCompetenciaIndicador() {
        return ListcompetenciaIndicador;
    }

    public void setListCompetenciaIndicador(List<CompetenciaIndicador> ListcompetenciaIndicador) {
        this.ListcompetenciaIndicador = ListcompetenciaIndicador;
    }

    public List<CompetenciaIndicador> getCompetenciaIndicadorFiltradas() {
        return competenciaIndicadorFiltradas;
    }

    public void setCompetenciaIndicadorFiltradas(List<CompetenciaIndicador> competenciaIndicadorFiltradas) {
        this.competenciaIndicadorFiltradas = competenciaIndicadorFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public CompetenciaIndicador getCompetenciaIndicador() {
        return competenciaIndicador;
    }

    public void setCompetenciaIndicador(CompetenciaIndicador competenciaIndicador) {
        this.competenciaIndicador = competenciaIndicador;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.competenciaIndicador = new CompetenciaIndicador();
        this.operacion = Constantes.CREAR;

//        this.competenciaIndicador.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.competenciaIndicador.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onVistaActualizar() {
        if (this.competenciaIndicador != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onActulizarRegistro() {

        if (this.competenciaIndicador != null) {
            this.operacion = Constantes.ACTUALIZAR;
            competenciaIndicador.setCodigo(competenciaIndicador.getCodigo());
            competenciaIndicador.setCompetencia(competenciaIndicador.getCompetencia());
            competenciaIndicador.setDescripcion(competenciaIndicador.getDescripcion());
            competenciaIndicador.setEstadoVisible(competenciaIndicador.getEstadoVisible());
            competenciaIndicador.setDatCreacion(competenciaIndicador.getDatCreacion());
            competenciaIndicador.setDatModificacion(competenciaIndicador.getDatModificacion());
            competenciaIndicador.setUsrCreacion(competenciaIndicador.getUsrCreacion());
//            competenciaIndicador.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarCompetenciaIndicador(competenciaIndicador);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearCompetenciaIndicador(competenciaIndicador);
        } else {
            this.servicios.actualizarCompetenciaIndicador(competenciaIndicador);
        }
        this.lista = true;
        this.crear = false;
        this.ListcompetenciaIndicador = servicios.consultarCompetenciaIndicador();
    }

    public void onBorrar() {
        if (this.competenciaIndicador != null) {
            servicios.borrarCompetenciaIndicador(competenciaIndicador);
            this.ListcompetenciaIndicador = servicios.consultarCompetenciaIndicador();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
