package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.EstadoPersona;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosEstadoPersona;
import co.edu.konrad.sp2.servicios.ServiciosEstadoPersonaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "estadoPersonaMB")
@ViewScoped
public class EstadoPersonaMB implements Serializable {

    private final static Logger log = Logger.getLogger(EstadoPersonaMB.class);
    private List<EstadoPersona> ListestadoPersona;
    private List<EstadoPersona> estadoPersonaFiltradas;
    private EstadoPersona estadoPersona;
    private String operacion;
    private ServiciosEstadoPersona servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosEstadoPersonaImp();
        this.ListestadoPersona = new ArrayList();
        this.estadoPersona = new EstadoPersona();
        this.ListestadoPersona = servicios.consultarEstadoPersona();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<EstadoPersona> getListEstadoPersona() {
        return ListestadoPersona;
    }

    public void setListEstadoPersona(List<EstadoPersona> ListestadoPersona) {
        this.ListestadoPersona = ListestadoPersona;
    }

    public List<EstadoPersona> getEstadoPersonaFiltradas() {
        return estadoPersonaFiltradas;
    }

    public void setEstadoPersonaFiltradas(List<EstadoPersona> estadoPersonaFiltradas) {
        this.estadoPersonaFiltradas = estadoPersonaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public EstadoPersona getEstadoPersona() {
        return estadoPersona;
    }

    public void setEstadoPersona(EstadoPersona estadoPersona) {
        this.estadoPersona = estadoPersona;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.estadoPersona = new EstadoPersona();
        this.operacion = Constantes.CREAR;

//        this.estadoPersona.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.estadoPersona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onVistaActualizar() {
        if (this.estadoPersona != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onActulizarRegistro() {

        if (this.estadoPersona != null) {
            this.operacion = Constantes.ACTUALIZAR;
            estadoPersona.setCodigo(estadoPersona.getCodigo());
            estadoPersona.setEstado(estadoPersona.getEstado());
            estadoPersona.setDescripcion(estadoPersona.getDescripcion());
            estadoPersona.setDatCreacion(estadoPersona.getDatCreacion());
            estadoPersona.setDatModificacion(estadoPersona.getDatModificacion());
            estadoPersona.setUsrCreacion(estadoPersona.getUsrCreacion());
//            estadoPersona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarEstadoPersona(estadoPersona);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearEstadoPersona(estadoPersona);
        } else {
            this.servicios.actualizarEstadoPersona(estadoPersona);
        }
        this.lista = true;
        this.crear = false;
        this.ListestadoPersona = servicios.consultarEstadoPersona();
    }

    public void onBorrar() {
        if (this.estadoPersona != null) {
            servicios.borrarEstadoPersona(estadoPersona);
            this.ListestadoPersona = servicios.consultarEstadoPersona();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
