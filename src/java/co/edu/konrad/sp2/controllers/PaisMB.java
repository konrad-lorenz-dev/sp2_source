package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Pais;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosPais;
import co.edu.konrad.sp2.servicios.ServiciosPaisImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "paisMB")
@ViewScoped
public class PaisMB implements Serializable {

    private final static Logger log = Logger.getLogger(PaisMB.class);
    private List<Pais> Listpais;
    private List<Pais> paisFiltradas;
    private Pais pais;
    private String operacion;
    private ServiciosPais servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosPaisImp();
        this.Listpais = new ArrayList();
        this.pais = new Pais();
        this.Listpais = servicios.consultarPais();
//        SesionContexto.getInstance().setAttribute(Constantes.USUARIO, "pruebas");
    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<Pais> getListPais() {
        return Listpais;
    }

    public void setListPais(List<Pais> Listpais) {
        this.Listpais = Listpais;
    }

    public List<Pais> getPaisFiltradas() {
        return paisFiltradas;
    }

    public void setPaisFiltradas(List<Pais> paisFiltradas) {
        this.paisFiltradas = paisFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.pais = new Pais();
        this.operacion = Constantes.CREAR;

//        this.pais.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.pais.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onVistaActualizar() {
        if (this.pais != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));

        }
    }

    public void onActulizarRegistro() {

        if (this.pais != null) {
            this.operacion = Constantes.ACTUALIZAR;
            pais.setCodigo(pais.getCodigo());
            pais.setPais(pais.getPais());
            pais.setCodigoPostal(pais.getCodigoPostal());
            pais.setDescripcion(pais.getDescripcion());
            pais.setDatCreacion(pais.getDatCreacion());
            pais.setDatModificacion(pais.getDatModificacion());
            pais.setUsrCreacion(pais.getUsrCreacion());
//            pais.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarPais(pais);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearPais(pais);
        } else {
            this.servicios.actualizarPais(pais);
        }
        this.lista = true;
        this.crear = false;
        this.Listpais = servicios.consultarPais();
    }

    public void onBorrar() {
        if (this.pais != null) {
            servicios.borrarPais(pais);
            this.Listpais = servicios.consultarPais();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));
        }
    }

}
