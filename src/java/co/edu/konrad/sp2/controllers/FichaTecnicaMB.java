package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.FichaTecnica;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosFichaTecnica;
import co.edu.konrad.sp2.servicios.ServiciosFichaTecnicaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "fichaTecnicaMB")
@ViewScoped
public class FichaTecnicaMB implements Serializable {

    private final static Logger log = Logger.getLogger(FichaTecnicaMB.class);
    private List<FichaTecnica> ListfichaTecnica;
    private List<FichaTecnica> fichaTecnicaFiltradas;
    private FichaTecnica fichaTecnica;
    private String operacion;
    private ServiciosFichaTecnica servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosFichaTecnicaImp();
        this.ListfichaTecnica = new ArrayList();
        this.fichaTecnica = new FichaTecnica();
        this.ListfichaTecnica = servicios.consultarFichaTecnica();

    }

    public List<FichaTecnica> getListFichaTecnica() {
        return ListfichaTecnica;
    }

    public void setListFichaTecnica(List<FichaTecnica> ListfichaTecnica) {
        this.ListfichaTecnica = ListfichaTecnica;
    }

    public List<FichaTecnica> getFichaTecnicaFiltradas() {
        return fichaTecnicaFiltradas;
    }

    public void setFichaTecnicaFiltradas(List<FichaTecnica> fichaTecnicaFiltradas) {
        this.fichaTecnicaFiltradas = fichaTecnicaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public FichaTecnica getFichaTecnica() {
        return fichaTecnica;
    }

    public void setFichaTecnica(FichaTecnica fichaTecnica) {
        this.fichaTecnica = fichaTecnica;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.fichaTecnica = new FichaTecnica();
        this.operacion = Constantes.CREAR;

//        this.fichaTecnica.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.fichaTecnica.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.fichaTecnica != null) {
            this.operacion = Constantes.ACTUALIZAR;
            fichaTecnica.setNombre(fichaTecnica.getNombre());
            fichaTecnica.setCodigo(fichaTecnica.getCodigo());
            fichaTecnica.setEstado(fichaTecnica.getEstado());
            fichaTecnica.setDatCreacion(fichaTecnica.getDatCreacion());
            fichaTecnica.setDatModificacion(fichaTecnica.getDatModificacion());
            fichaTecnica.setUsrCreacion(fichaTecnica.getUsrCreacion());
//            fichaTecnica.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearFichaTecnica(fichaTecnica);
        } else {
            this.servicios.actualizarFichaTecnica(fichaTecnica);
        }
        this.lista = true;
        this.crear = false;
        this.ListfichaTecnica = servicios.consultarFichaTecnica();
    }

    public void onBorrar() {
        if (this.fichaTecnica != null) {
            servicios.borrarFichaTecnica(fichaTecnica);
            this.ListfichaTecnica = servicios.consultarFichaTecnica();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
