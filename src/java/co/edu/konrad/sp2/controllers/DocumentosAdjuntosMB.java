package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.DocumentosAdjuntos;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosDocumentosAdjuntos;
import co.edu.konrad.sp2.servicios.ServiciosDocumentosAdjuntosImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "documentosAdjuntosMB")
@ViewScoped
public class DocumentosAdjuntosMB implements Serializable {

    private final static Logger log = Logger.getLogger(DocumentosAdjuntosMB.class);
    private List<DocumentosAdjuntos> ListdocumentosAdjuntos;
    private List<DocumentosAdjuntos> documentosAdjuntosFiltradas;
    private DocumentosAdjuntos documentosAdjuntos;
    private String operacion;
    private ServiciosDocumentosAdjuntos servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosDocumentosAdjuntosImp();
        this.ListdocumentosAdjuntos = new ArrayList();
        this.documentosAdjuntos = new DocumentosAdjuntos();
        this.ListdocumentosAdjuntos = servicios.consultarDocumentosAdjuntosJoin();

    }

    public List<DocumentosAdjuntos> getListDocumentosAdjuntos() {
        return ListdocumentosAdjuntos;
    }

    public void setListDocumentosAdjuntos(List<DocumentosAdjuntos> ListdocumentosAdjuntos) {
        this.ListdocumentosAdjuntos = ListdocumentosAdjuntos;
    }

    public List<DocumentosAdjuntos> getDocumentosAdjuntosFiltradas() {
        return documentosAdjuntosFiltradas;
    }

    public void setDocumentosAdjuntosFiltradas(List<DocumentosAdjuntos> documentosAdjuntosFiltradas) {
        this.documentosAdjuntosFiltradas = documentosAdjuntosFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public DocumentosAdjuntos getDocumentosAdjuntos() {
        return documentosAdjuntos;
    }

    public void setDocumentosAdjuntos(DocumentosAdjuntos documentosAdjuntos) {
        this.documentosAdjuntos = documentosAdjuntos;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.documentosAdjuntos = new DocumentosAdjuntos();
        this.operacion = Constantes.CREAR;

//        this.documentosAdjuntos.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.documentosAdjuntos.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.documentosAdjuntos != null) {
            this.operacion = Constantes.ACTUALIZAR;
            documentosAdjuntos.setTipoDocumento(documentosAdjuntos.getTipoDocumento());
            documentosAdjuntos.setPersona(documentosAdjuntos.getPersona());
            documentosAdjuntos.setCodigo(documentosAdjuntos.getCodigo());
            documentosAdjuntos.setNombre(documentosAdjuntos.getNombre());
            documentosAdjuntos.setDescripcion(documentosAdjuntos.getDescripcion());
            documentosAdjuntos.setValido(documentosAdjuntos.getValido());
            documentosAdjuntos.setEstado(documentosAdjuntos.getEstado());
            documentosAdjuntos.setDatCreacion(documentosAdjuntos.getDatCreacion());
            documentosAdjuntos.setDatModificacion(documentosAdjuntos.getDatModificacion());
            documentosAdjuntos.setUsrCreacion(documentosAdjuntos.getUsrCreacion());
//            documentosAdjuntos.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearDocumentosAdjuntos(documentosAdjuntos);
        } else {
            this.servicios.actualizarDocumentosAdjuntos(documentosAdjuntos);
        }
        this.lista = true;
        this.crear = false;
        this.ListdocumentosAdjuntos = servicios.consultarDocumentosAdjuntos();
    }

    public void onBorrar() {
        if (this.documentosAdjuntos != null) {
            servicios.borrarDocumentosAdjuntos(documentosAdjuntos);
            this.ListdocumentosAdjuntos = servicios.consultarDocumentosAdjuntos();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
