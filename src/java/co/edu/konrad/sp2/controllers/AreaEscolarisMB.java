package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.AreaEscolaris;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosAreaEscolaris;
import co.edu.konrad.sp2.servicios.ServiciosAreaEscolarisImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "areaEscolarisMB")
@ViewScoped
public class AreaEscolarisMB implements Serializable {

    private final static Logger log = Logger.getLogger(AreaEscolarisMB.class);
    private List<AreaEscolaris> ListareaEscolaris;
    private List<AreaEscolaris> areaEscolarisFiltradas;
    private AreaEscolaris areaEscolaris;
    private String operacion;
    private ServiciosAreaEscolaris servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosAreaEscolarisImp();
        this.ListareaEscolaris = new ArrayList();
        this.areaEscolaris = new AreaEscolaris();
        this.ListareaEscolaris = servicios.consultarAreaEscolarisJoin();

    }

    public List<AreaEscolaris> getListAreaEscolaris() {
        return ListareaEscolaris;
    }

    public void setListAreaEscolaris(List<AreaEscolaris> ListareaEscolaris) {
        this.ListareaEscolaris = ListareaEscolaris;
    }

    public List<AreaEscolaris> getAreaEscolarisFiltradas() {
        return areaEscolarisFiltradas;
    }

    public void setAreaEscolarisFiltradas(List<AreaEscolaris> areaEscolarisFiltradas) {
        this.areaEscolarisFiltradas = areaEscolarisFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public AreaEscolaris getAreaEscolaris() {
        return areaEscolaris;
    }

    public void setAreaEscolaris(AreaEscolaris areaEscolaris) {
        this.areaEscolaris = areaEscolaris;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.areaEscolaris = new AreaEscolaris();
        this.operacion = Constantes.CREAR;

    }

    public void onActulizarRegistro() {

        if (this.areaEscolaris != null) {
            this.operacion = Constantes.ACTUALIZAR;
            areaEscolaris.setProgramaEscolaris(areaEscolaris.getProgramaEscolaris());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearAreaEscolaris(areaEscolaris);
        } else {
            this.servicios.actualizarAreaEscolaris(areaEscolaris);
        }
        this.lista = true;
        this.crear = false;
        this.ListareaEscolaris = servicios.consultarAreaEscolaris();
    }

    public void onBorrar() {
        if (this.areaEscolaris != null) {
            servicios.borrarAreaEscolaris(areaEscolaris);
            this.ListareaEscolaris = servicios.consultarAreaEscolaris();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
