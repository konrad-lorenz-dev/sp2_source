package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.TipoEvento;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosTipoEvento;
import co.edu.konrad.sp2.servicios.ServiciosTipoEventoImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "tipoEventoMB")
@ViewScoped
public class TipoEventoMB implements Serializable {

    private final static Logger log = Logger.getLogger(TipoEventoMB.class);
    private List<TipoEvento> ListtipoEvento;
    private List<TipoEvento> tipoEventoFiltradas;
    private TipoEvento tipoEvento;
    private String operacion;
    private ServiciosTipoEvento servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosTipoEventoImp();
        this.ListtipoEvento = new ArrayList();
        this.tipoEvento = new TipoEvento();
        this.ListtipoEvento = servicios.consultarTipoEvento();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<TipoEvento> getListTipoEvento() {
        return ListtipoEvento;
    }

    public void setListTipoEvento(List<TipoEvento> ListtipoEvento) {
        this.ListtipoEvento = ListtipoEvento;
    }

    public List<TipoEvento> getTipoEventoFiltradas() {
        return tipoEventoFiltradas;
    }

    public void setTipoEventoFiltradas(List<TipoEvento> tipoEventoFiltradas) {
        this.tipoEventoFiltradas = tipoEventoFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public TipoEvento getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(TipoEvento tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.tipoEvento = new TipoEvento();
        this.operacion = Constantes.CREAR;
//        this.tipoEvento.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.tipoEvento.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onVistaActualizar() {
        if (this.tipoEvento != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }

    }

    public void onActulizarRegistro() {
        if (this.tipoEvento != null) {
            this.operacion = Constantes.ACTUALIZAR;
            tipoEvento.setCodigo(tipoEvento.getCodigo());
            tipoEvento.setNombre(tipoEvento.getNombre());
            tipoEvento.setDesTipoEvento(tipoEvento.getDesTipoEvento());
            tipoEvento.setDatCreacion(tipoEvento.getDatCreacion());
            tipoEvento.setDatModificacion(tipoEvento.getDatModificacion());
            tipoEvento.setUsrCreacion(tipoEvento.getUsrCreacion());
//            tipoEvento.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarTipoEvento(tipoEvento);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearTipoEvento(tipoEvento);
        } else {
            this.servicios.actualizarTipoEvento(tipoEvento);
        }
        this.lista = true;
        this.crear = false;
        this.ListtipoEvento = servicios.consultarTipoEvento();
    }

    public void onBorrar() {
        if (this.tipoEvento != null) {
            servicios.borrarTipoEvento(tipoEvento);
            this.ListtipoEvento = servicios.consultarTipoEvento();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
