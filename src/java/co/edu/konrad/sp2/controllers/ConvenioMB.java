package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Convenio;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosConvenio;
import co.edu.konrad.sp2.servicios.ServiciosConvenioImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "convenioMB")
@ViewScoped
public class ConvenioMB implements Serializable {

    private final static Logger log = Logger.getLogger(ConvenioMB.class);
    private List<Convenio> Listconvenio;
    private List<Convenio> convenioFiltradas;
    private Convenio convenio;
    private String operacion;
    private ServiciosConvenio servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosConvenioImp();
        this.Listconvenio = new ArrayList();
        this.convenio = new Convenio();
        this.Listconvenio = servicios.consultarConvenioJoin();

    }

    public List<Convenio> getListConvenio() {
        return Listconvenio;
    }

    public void setListConvenio(List<Convenio> Listconvenio) {
        this.Listconvenio = Listconvenio;
    }

    public List<Convenio> getConvenioFiltradas() {
        return convenioFiltradas;
    }

    public void setConvenioFiltradas(List<Convenio> convenioFiltradas) {
        this.convenioFiltradas = convenioFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Convenio getConvenio() {
        return convenio;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.convenio = new Convenio();
        this.operacion = Constantes.CREAR;

//        this.convenio.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.convenio.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.convenio != null) {
            this.operacion = Constantes.ACTUALIZAR;
            convenio.setEntidad(convenio.getEntidad());
            convenio.setNombre(convenio.getNombre());
            convenio.setDatInicial(convenio.getDatInicial());
            convenio.setDatFinal(convenio.getDatFinal());
            convenio.setCuposOfrecidos(convenio.getCuposOfrecidos());
            convenio.setCuposAsignados(convenio.getCuposAsignados());
            convenio.setEstadoVisible(convenio.getEstadoVisible());
            convenio.setFirmado(convenio.getFirmado());
            convenio.setObservaciones(convenio.getObservaciones());
            convenio.setPeriodo(convenio.getPeriodo());
            convenio.setDatCreacion(convenio.getDatCreacion());
            convenio.setDatModificacion(convenio.getDatModificacion());
            convenio.setUsrCreacion(convenio.getUsrCreacion());
//            convenio.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearConvenio(convenio);
        } else {
            this.servicios.actualizarConvenio(convenio);
        }
        this.lista = true;
        this.crear = false;
        this.Listconvenio = servicios.consultarConvenio();
    }

    public void onBorrar() {
        if (this.convenio != null) {
            servicios.borrarConvenio(convenio);
            this.Listconvenio = servicios.consultarConvenio();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
