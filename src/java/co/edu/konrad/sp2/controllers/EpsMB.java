package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Eps;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosEps;
import co.edu.konrad.sp2.servicios.ServiciosEpsImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "epsMB")
@ViewScoped
public class EpsMB implements Serializable {

    private final static Logger log = Logger.getLogger(EpsMB.class);
    private List<Eps> Listeps;
    private List<Eps> epsFiltradas;
    private Eps eps;
    private String operacion;
    private ServiciosEps servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosEpsImp();
        this.Listeps = new ArrayList();
        this.eps = new Eps();
        this.Listeps = servicios.consultarEps();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<Eps> getListEps() {
        return Listeps;
    }

    public void setListEps(List<Eps> Listeps) {
        this.Listeps = Listeps;
    }

    public List<Eps> getEpsFiltradas() {
        return epsFiltradas;
    }

    public void setEpsFiltradas(List<Eps> epsFiltradas) {
        this.epsFiltradas = epsFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Eps getEps() {
        return eps;
    }

    public void setEps(Eps eps) {
        this.eps = eps;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.eps = new Eps();
        this.operacion = Constantes.CREAR;

//        this.eps.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.eps.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onVistaActualizar() {
        if (this.eps != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onActulizarRegistro() {

        if (this.eps != null) {
            this.operacion = Constantes.ACTUALIZAR;
            eps.setCodigo(eps.getCodigo());
            eps.setEps(eps.getEps());
            eps.setDescripcion(eps.getDescripcion());
            eps.setDatCreacion(eps.getDatCreacion());
            eps.setDatModificacion(eps.getDatModificacion());
            eps.setUsrCreacion(eps.getUsrCreacion());
//            eps.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarEps(eps);

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearEps(eps);
        } else {
            this.servicios.actualizarEps(eps);
        }
        this.lista = true;
        this.crear = false;
        this.Listeps = servicios.consultarEps();
    }

    public void onBorrar() {
        if (this.eps != null) {
            servicios.borrarEps(eps);
            this.Listeps = servicios.consultarEps();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
