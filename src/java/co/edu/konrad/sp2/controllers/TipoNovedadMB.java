package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.TipoNovedad;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosTipoNovedad;
import co.edu.konrad.sp2.servicios.ServiciosTipoNovedadImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "tipoNovedadMB")
@ViewScoped
public class TipoNovedadMB implements Serializable {

    private final static Logger log = Logger.getLogger(TipoNovedadMB.class);
    private List<TipoNovedad> ListtipoNovedad;
    private List<TipoNovedad> tipoNovedadFiltradas;
    private TipoNovedad tipoNovedad;
    private String operacion;
    private ServiciosTipoNovedad servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosTipoNovedadImp();
        this.ListtipoNovedad = new ArrayList();
        this.tipoNovedad = new TipoNovedad();
        this.ListtipoNovedad = servicios.consultarTipoNovedad();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<TipoNovedad> getListTipoNovedad() {
        return ListtipoNovedad;
    }

    public void setListTipoNovedad(List<TipoNovedad> ListtipoNovedad) {
        this.ListtipoNovedad = ListtipoNovedad;
    }

    public List<TipoNovedad> getTipoNovedadFiltradas() {
        return tipoNovedadFiltradas;
    }

    public void setTipoNovedadFiltradas(List<TipoNovedad> tipoNovedadFiltradas) {
        this.tipoNovedadFiltradas = tipoNovedadFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public TipoNovedad getTipoNovedad() {
        return tipoNovedad;
    }

    public void setTipoNovedad(TipoNovedad tipoNovedad) {
        this.tipoNovedad = tipoNovedad;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.tipoNovedad = new TipoNovedad();
        this.operacion = Constantes.CREAR;

//        this.tipoNovedad.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.tipoNovedad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onVistaActualizar() {
        if (this.tipoNovedad != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onActulizarRegistro() {

        if (this.tipoNovedad != null) {
            this.operacion = Constantes.ACTUALIZAR;
            tipoNovedad.setNovedad(tipoNovedad.getNovedad());
            tipoNovedad.setDescripcion(tipoNovedad.getDescripcion());
            tipoNovedad.setDatCreacion(tipoNovedad.getDatCreacion());
            tipoNovedad.setDatModificacion(tipoNovedad.getDatModificacion());
            tipoNovedad.setUsrCreacion(tipoNovedad.getUsrCreacion());
//            tipoNovedad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarTipoNovedad(tipoNovedad);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearTipoNovedad(tipoNovedad);
        } else {
            this.servicios.actualizarTipoNovedad(tipoNovedad);
        }
        this.lista = true;
        this.crear = false;
        this.ListtipoNovedad = servicios.consultarTipoNovedad();
    }

    public void onBorrar() {
        if (this.tipoNovedad != null) {
            servicios.borrarTipoNovedad(tipoNovedad);
            this.ListtipoNovedad = servicios.consultarTipoNovedad();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
