package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.TipoIdentificacion;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosTipoIdentificacion;
import co.edu.konrad.sp2.servicios.ServiciosTipoIdentificacionImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "tipoIdentificacionMB")
@ViewScoped
public class TipoIdentificacionMB implements Serializable {

    private final static Logger log = Logger.getLogger(TipoIdentificacionMB.class);
    private List<TipoIdentificacion> ListtipoIdentificacion;
    private List<TipoIdentificacion> tipoIdentificacionFiltradas;
    private TipoIdentificacion tipoIdentificacion;
    private String operacion;
    private ServiciosTipoIdentificacion servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosTipoIdentificacionImp();
        this.ListtipoIdentificacion = new ArrayList();
        this.tipoIdentificacion = new TipoIdentificacion();
        this.ListtipoIdentificacion = servicios.consultarTipoIdentificacion();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<TipoIdentificacion> getListTipoIdentificacion() {
        return ListtipoIdentificacion;
    }

    public void setListTipoIdentificacion(List<TipoIdentificacion> ListtipoIdentificacion) {
        this.ListtipoIdentificacion = ListtipoIdentificacion;
    }

    public List<TipoIdentificacion> getTipoIdentificacionFiltradas() {
        return tipoIdentificacionFiltradas;
    }

    public void setTipoIdentificacionFiltradas(List<TipoIdentificacion> tipoIdentificacionFiltradas) {
        this.tipoIdentificacionFiltradas = tipoIdentificacionFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public TipoIdentificacion getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.tipoIdentificacion = new TipoIdentificacion();
        this.operacion = Constantes.CREAR;

//        this.tipoIdentificacion.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.tipoIdentificacion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onVistaActualizar() {
        if (this.tipoIdentificacion != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onActulizarRegistro() {

        if (this.tipoIdentificacion != null) {
            this.operacion = Constantes.ACTUALIZAR;
            tipoIdentificacion.setCodigo(tipoIdentificacion.getCodigo());
            tipoIdentificacion.setIdentificacion(tipoIdentificacion.getIdentificacion());
            tipoIdentificacion.setDescripcion(tipoIdentificacion.getDescripcion());
            tipoIdentificacion.setDatCreacion(tipoIdentificacion.getDatCreacion());
            tipoIdentificacion.setDatModificacion(tipoIdentificacion.getDatModificacion());
            tipoIdentificacion.setUsrCreacion(tipoIdentificacion.getUsrCreacion());
//            tipoIdentificacion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarTipoIdentificacion(tipoIdentificacion);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));

        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearTipoIdentificacion(tipoIdentificacion);
        } else {
            this.servicios.actualizarTipoIdentificacion(tipoIdentificacion);
        }
        this.lista = true;
        this.crear = false;
        this.ListtipoIdentificacion = servicios.consultarTipoIdentificacion();
    }

    public void onBorrar() {
        if (this.tipoIdentificacion != null) {
            servicios.borrarTipoIdentificacion(tipoIdentificacion);
            this.ListtipoIdentificacion = servicios.consultarTipoIdentificacion();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
