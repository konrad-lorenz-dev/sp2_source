package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.TipoDocumento;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosTipoDocumento;
import co.edu.konrad.sp2.servicios.ServiciosTipoDocumentoImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "tipoDocumentoMB")
@ViewScoped
public class TipoDocumentoMB implements Serializable {

    private final static Logger log = Logger.getLogger(TipoDocumentoMB.class);
    private List<TipoDocumento> ListtipoDocumento;
    private List<TipoDocumento> tipoDocumentoFiltradas;
    private TipoDocumento tipoDocumento;
    private String operacion;
    private ServiciosTipoDocumento servicios;
    boolean crear;
    boolean lista;

    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosTipoDocumentoImp();
        this.ListtipoDocumento = new ArrayList();
        this.tipoDocumento = new TipoDocumento();
        this.ListtipoDocumento = servicios.consultarTipoDocumento();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<TipoDocumento> getListTipoDocumento() {
        return ListtipoDocumento;
    }

    public void setListTipoDocumento(List<TipoDocumento> ListtipoDocumento) {
        this.ListtipoDocumento = ListtipoDocumento;
    }

    public List<TipoDocumento> getTipoDocumentoFiltradas() {
        return tipoDocumentoFiltradas;
    }

    public void setTipoDocumentoFiltradas(List<TipoDocumento> tipoDocumentoFiltradas) {
        this.tipoDocumentoFiltradas = tipoDocumentoFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public TipoDocumento getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(TipoDocumento tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.tipoDocumento = new TipoDocumento();
        this.operacion = Constantes.CREAR;

        this.tipoDocumento.setUsrCreacion("mio");
        this.tipoDocumento.setUsrModificacion("mio");

    }

    public void onVistaActualizar() {
        if (this.tipoDocumento != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onActulizarRegistro() {

        if (this.tipoDocumento != null) {
            this.operacion = Constantes.ACTUALIZAR;
            tipoDocumento.setCodigo(tipoDocumento.getCodigo());
            tipoDocumento.setDocumento(tipoDocumento.getDocumento());
            tipoDocumento.setDescripcion(tipoDocumento.getDescripcion());
            tipoDocumento.setDatCreacion(tipoDocumento.getDatCreacion());
            tipoDocumento.setDatModificacion(tipoDocumento.getDatModificacion());
            tipoDocumento.setUsrCreacion(tipoDocumento.getUsrCreacion());
            tipoDocumento.setUsrModificacion("mio");
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarTipoDocumento(tipoDocumento);

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearTipoDocumento(tipoDocumento);
        } else {
            this.servicios.actualizarTipoDocumento(tipoDocumento);
        }
        this.lista = true;
        this.crear = false;
        this.ListtipoDocumento = servicios.consultarTipoDocumento();
    }

    public void onBorrar() {
        if (this.tipoDocumento != null) {
            servicios.borrarTipoDocumento(tipoDocumento);
            this.ListtipoDocumento = servicios.consultarTipoDocumento();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
