package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.CalificacionEntidad;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosCalificacionEntidad;
import co.edu.konrad.sp2.servicios.ServiciosCalificacionEntidadImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "calificacionEntidadMB")
@ViewScoped
public class CalificacionEntidadMB implements Serializable {

    private final static Logger log = Logger.getLogger(CalificacionEntidadMB.class);
    private List<CalificacionEntidad> ListcalificacionEntidad;
    private List<CalificacionEntidad> calificacionEntidadFiltradas;
    private CalificacionEntidad calificacionEntidad;
    private String operacion;
    private ServiciosCalificacionEntidad servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosCalificacionEntidadImp();
        this.ListcalificacionEntidad = new ArrayList();
        this.calificacionEntidad = new CalificacionEntidad();
        this.ListcalificacionEntidad = servicios.consultarCalificacionEntidadJoin();

    }

    public List<CalificacionEntidad> getListCalificacionEntidad() {
        return ListcalificacionEntidad;
    }

    public void setListCalificacionEntidad(List<CalificacionEntidad> ListcalificacionEntidad) {
        this.ListcalificacionEntidad = ListcalificacionEntidad;
    }

    public List<CalificacionEntidad> getCalificacionEntidadFiltradas() {
        return calificacionEntidadFiltradas;
    }

    public void setCalificacionEntidadFiltradas(List<CalificacionEntidad> calificacionEntidadFiltradas) {
        this.calificacionEntidadFiltradas = calificacionEntidadFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public CalificacionEntidad getCalificacionEntidad() {
        return calificacionEntidad;
    }

    public void setCalificacionEntidad(CalificacionEntidad calificacionEntidad) {
        this.calificacionEntidad = calificacionEntidad;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.calificacionEntidad = new CalificacionEntidad();
        this.operacion = Constantes.CREAR;

//        this.calificacionEntidad.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.calificacionEntidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.calificacionEntidad != null) {
            this.operacion = Constantes.ACTUALIZAR;
            calificacionEntidad.setEntidad(calificacionEntidad.getEntidad());
            calificacionEntidad.setDetalleSupPractica(calificacionEntidad.getDetalleSupPractica());
            calificacionEntidad.setCalificacion(calificacionEntidad.getCalificacion());
            calificacionEntidad.setDescripcion(calificacionEntidad.getDescripcion());
            calificacionEntidad.setDatCreacion(calificacionEntidad.getDatCreacion());
            calificacionEntidad.setDatModificacion(calificacionEntidad.getDatModificacion());
            calificacionEntidad.setUsrCreacion(calificacionEntidad.getUsrCreacion());
//            calificacionEntidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearCalificacionEntidad(calificacionEntidad);
        } else {
            this.servicios.actualizarCalificacionEntidad(calificacionEntidad);
        }
        this.lista = true;
        this.crear = false;
        this.ListcalificacionEntidad = servicios.consultarCalificacionEntidad();
    }

    public void onBorrar() {
        if (this.calificacionEntidad != null) {
            servicios.borrarCalificacionEntidad(calificacionEntidad);
            this.ListcalificacionEntidad = servicios.consultarCalificacionEntidad();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
