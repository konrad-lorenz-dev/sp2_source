package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.DetallePracticante;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticante;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticanteImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "detallePracticanteMB")
@ViewScoped
public class DetallePracticanteMB implements Serializable {

    private final static Logger log = Logger.getLogger(DetallePracticanteMB.class);
    private List<DetallePracticante> ListdetallePracticante;
    private List<DetallePracticante> detallePracticanteFiltradas;
    private DetallePracticante detallePracticante;
    private String operacion;
    private ServiciosDetallePracticante servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosDetallePracticanteImp();
        this.ListdetallePracticante = new ArrayList();
        this.detallePracticante = new DetallePracticante();
        this.ListdetallePracticante = servicios.consultarDetallePracticanteJoin();

    }

    public List<DetallePracticante> getListDetallePracticante() {
        return ListdetallePracticante;
    }

    public void setListDetallePracticante(List<DetallePracticante> ListdetallePracticante) {
        this.ListdetallePracticante = ListdetallePracticante;
    }

    public List<DetallePracticante> getDetallePracticanteFiltradas() {
        return detallePracticanteFiltradas;
    }

    public void setDetallePracticanteFiltradas(List<DetallePracticante> detallePracticanteFiltradas) {
        this.detallePracticanteFiltradas = detallePracticanteFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public DetallePracticante getDetallePracticante() {
        return detallePracticante;
    }

    public void setDetallePracticante(DetallePracticante detallePracticante) {
        this.detallePracticante = detallePracticante;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.detallePracticante = new DetallePracticante();
        this.operacion = Constantes.CREAR;

//        this.detallePracticante.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.detallePracticante.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.detallePracticante != null) {
            this.operacion = Constantes.ACTUALIZAR;
            detallePracticante.setPersona(detallePracticante.getPersona());
            detallePracticante.setEps(detallePracticante.getEps());
            detallePracticante.setAreaEscolaris(detallePracticante.getAreaEscolaris());
            detallePracticante.setSemestre(detallePracticante.getSemestre());
            detallePracticante.setEstadoPractica(detallePracticante.getEstadoPractica());
            detallePracticante.setEstadoHabilitar(detallePracticante.getEstadoHabilitar());
            detallePracticante.setEstadoBloquear(detallePracticante.getEstadoBloquear());
            detallePracticante.setObservaciones(detallePracticante.getObservaciones());
            detallePracticante.setDatPracticaDesde(detallePracticante.getDatPracticaDesde());
            detallePracticante.setDatPracticaHasta(detallePracticante.getDatPracticaHasta());
            detallePracticante.setDatCreacion(detallePracticante.getDatCreacion());
            detallePracticante.setDatModificacion(detallePracticante.getDatModificacion());
            detallePracticante.setUsrCreacion(detallePracticante.getUsrCreacion());
//            detallePracticante.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearDetallePracticante(detallePracticante);
        } else {
            this.servicios.actualizarDetallePracticante(detallePracticante);
        }
        this.lista = true;
        this.crear = false;
        this.ListdetallePracticante = servicios.consultarDetallePracticante();
    }

    public void onBorrar() {
        if (this.detallePracticante != null) {
            servicios.borrarDetallePracticante(detallePracticante);
            this.ListdetallePracticante = servicios.consultarDetallePracticante();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
