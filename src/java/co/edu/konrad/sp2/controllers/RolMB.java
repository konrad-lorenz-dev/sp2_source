package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Rol;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosRol;
import co.edu.konrad.sp2.servicios.ServiciosRolImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "rolMB")
@ViewScoped
public class RolMB implements Serializable {

    private final static Logger log = Logger.getLogger(RolMB.class);
    private List<Rol> Listrol;
    private List<Rol> rolFiltradas;
    private Rol rol;
    private String operacion;
    private ServiciosRol servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosRolImp();
        this.Listrol = new ArrayList();
        this.rol = new Rol();
        this.Listrol = servicios.consultarRol();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<Rol> getListRol() {
        return Listrol;
    }

    public void setListRol(List<Rol> Listrol) {
        this.Listrol = Listrol;
    }

    public List<Rol> getRolFiltradas() {
        return rolFiltradas;
    }

    public void setRolFiltradas(List<Rol> rolFiltradas) {
        this.rolFiltradas = rolFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.rol = new Rol();
        this.operacion = Constantes.CREAR;

//        this.rol.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.rol.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onVistaActualizar() {
        if (this.rol != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onActulizarRegistro() {

        if (this.rol != null) {
            this.operacion = Constantes.ACTUALIZAR;
            rol.setCodigo(rol.getCodigo());
            rol.setRol(rol.getRol());
            rol.setDescripcion(rol.getDescripcion());
            rol.setDatCreacion(rol.getDatCreacion());
            rol.setDatModificacion(rol.getDatModificacion());
            rol.setUsrCreacion(rol.getUsrCreacion());
//            rol.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarRol(rol);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearRol(rol);
        } else {
            this.servicios.actualizarRol(rol);
        }
        this.lista = true;
        this.crear = false;
        this.Listrol = servicios.consultarRol();
    }

    public void onBorrar() {
        if (this.rol != null) {
            servicios.borrarRol(rol);
            this.Listrol = servicios.consultarRol();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
