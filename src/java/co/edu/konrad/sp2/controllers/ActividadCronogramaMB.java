package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.ActividadCronograma;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosActividadCronograma;
import co.edu.konrad.sp2.servicios.ServiciosActividadCronogramaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "actividadCronogramaMB")
@ViewScoped
public class ActividadCronogramaMB implements Serializable {

    private final static Logger log = Logger.getLogger(ActividadCronogramaMB.class);
    private List<ActividadCronograma> ListactividadCronograma;
    private List<ActividadCronograma> actividadCronogramaFiltradas;
    private ActividadCronograma actividadCronograma;
    private String operacion;
    private ServiciosActividadCronograma servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosActividadCronogramaImp();
        this.ListactividadCronograma = new ArrayList();
        this.actividadCronograma = new ActividadCronograma();
        this.ListactividadCronograma = servicios.consultarActividadCronogramaJoin();

    }

    public List<ActividadCronograma> getListActividadCronograma() {
        return ListactividadCronograma;
    }

    public void setListActividadCronograma(List<ActividadCronograma> ListactividadCronograma) {
        this.ListactividadCronograma = ListactividadCronograma;
    }

    public List<ActividadCronograma> getActividadCronogramaFiltradas() {
        return actividadCronogramaFiltradas;
    }

    public void setActividadCronogramaFiltradas(List<ActividadCronograma> actividadCronogramaFiltradas) {
        this.actividadCronogramaFiltradas = actividadCronogramaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public ActividadCronograma getActividadCronograma() {
        return actividadCronograma;
    }

    public void setActividadCronograma(ActividadCronograma actividadCronograma) {
        this.actividadCronograma = actividadCronograma;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.actividadCronograma = new ActividadCronograma();
        this.operacion = Constantes.CREAR;

//        this.actividadCronograma.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.actividadCronograma.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.actividadCronograma != null) {
            this.operacion = Constantes.ACTUALIZAR;
            actividadCronograma.setCronograma(actividadCronograma.getCronograma());
            actividadCronograma.setObservacion(actividadCronograma.getObservacion());
            actividadCronograma.setEstado(actividadCronograma.getEstado());
            actividadCronograma.setDatCreacion(actividadCronograma.getDatCreacion());
            actividadCronograma.setDatModificacion(actividadCronograma.getDatModificacion());
            actividadCronograma.setUsrCreacion(actividadCronograma.getUsrCreacion());
//            actividadCronograma.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearActividadCronograma(actividadCronograma);
        } else {
            this.servicios.actualizarActividadCronograma(actividadCronograma);
        }
        this.lista = true;
        this.crear = false;
        this.ListactividadCronograma = servicios.consultarActividadCronograma();
    }

    public void onBorrar() {
        if (this.actividadCronograma != null) {
            servicios.borrarActividadCronograma(actividadCronograma);
            this.ListactividadCronograma = servicios.consultarActividadCronograma();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
