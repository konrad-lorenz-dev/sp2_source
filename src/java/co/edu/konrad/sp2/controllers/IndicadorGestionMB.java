package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.IndicadorGestion;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosIndicadorGestion;
import co.edu.konrad.sp2.servicios.ServiciosIndicadorGestionImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "indicadorGestionMB")
@ViewScoped
public class IndicadorGestionMB implements Serializable {

    private final static Logger log = Logger.getLogger(IndicadorGestionMB.class);
    private List<IndicadorGestion> ListindicadorGestion;
    private List<IndicadorGestion> indicadorGestionFiltradas;
    private IndicadorGestion indicadorGestion;
    private String operacion;
    private ServiciosIndicadorGestion servicios;
    boolean crear;
    boolean lista;
    // habilita funcion actualizar
    boolean botonHabilitar;
    // desabilita funcion guardar
    boolean botonDesabilidar;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.botonHabilitar = false;
        this.botonDesabilidar = true;
        this.servicios = new ServiciosIndicadorGestionImp();
        this.ListindicadorGestion = new ArrayList();
        this.indicadorGestion = new IndicadorGestion();
        this.ListindicadorGestion = servicios.consultarIndicadorGestion();

    }

    public boolean isBotonHabilitar() {
        return botonHabilitar;
    }

    public void setBotonHabilitar(boolean botonHabilitar) {
        this.botonHabilitar = botonHabilitar;
    }

    public boolean isBotonDesabilidar() {
        return botonDesabilidar;
    }

    public void setBotonDesabilidar(boolean botonDesabilidar) {
        this.botonDesabilidar = botonDesabilidar;
    }

    public List<IndicadorGestion> getListIndicadorGestion() {
        return ListindicadorGestion;
    }

    public void setListIndicadorGestion(List<IndicadorGestion> ListindicadorGestion) {
        this.ListindicadorGestion = ListindicadorGestion;
    }

    public List<IndicadorGestion> getIndicadorGestionFiltradas() {
        return indicadorGestionFiltradas;
    }

    public void setIndicadorGestionFiltradas(List<IndicadorGestion> indicadorGestionFiltradas) {
        this.indicadorGestionFiltradas = indicadorGestionFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public IndicadorGestion getIndicadorGestion() {
        return indicadorGestion;
    }

    public void setIndicadorGestion(IndicadorGestion indicadorGestion) {
        this.indicadorGestion = indicadorGestion;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.indicadorGestion = new IndicadorGestion();
        this.operacion = Constantes.CREAR;

        this.indicadorGestion.setUsrCreacion("mio");
        this.indicadorGestion.setUsrModificacion("mio");

    }

    public void onVistaActualizar() {
        if (this.indicadorGestion != null) {
            this.lista = false;
            this.crear = true;
            this.botonHabilitar = true;
            this.botonDesabilidar = false;
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onActulizarRegistro() {

        if (this.indicadorGestion != null) {
            this.operacion = Constantes.ACTUALIZAR;
            indicadorGestion.setCodigo(indicadorGestion.getCodigo());
            indicadorGestion.setIndicador(indicadorGestion.getIndicador());
            indicadorGestion.setDescripcion(indicadorGestion.getDescripcion());
            indicadorGestion.setEstadoVisible(indicadorGestion.getEstadoVisible());
            indicadorGestion.setDatCreacion(indicadorGestion.getDatCreacion());
            indicadorGestion.setDatModificacion(indicadorGestion.getDatModificacion());
            indicadorGestion.setUsrCreacion(indicadorGestion.getUsrCreacion());
            indicadorGestion.setUsrModificacion("mio");
            this.lista = true;
            this.crear = false;
            this.botonHabilitar = false;
            this.botonDesabilidar = true;
            this.servicios.actualizarIndicadorGestion(indicadorGestion);
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearIndicadorGestion(indicadorGestion);
        } else {
            this.servicios.actualizarIndicadorGestion(indicadorGestion);
        }
        this.lista = true;
        this.crear = false;
        this.ListindicadorGestion = servicios.consultarIndicadorGestion();
    }

    public void onBorrar() {
        if (this.indicadorGestion != null) {
            servicios.borrarIndicadorGestion(indicadorGestion);
            this.ListindicadorGestion = servicios.consultarIndicadorGestion();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
