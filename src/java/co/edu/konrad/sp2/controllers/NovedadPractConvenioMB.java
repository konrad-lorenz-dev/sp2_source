package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.NovedadPractConvenio;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosNovedadPractConvenio;
import co.edu.konrad.sp2.servicios.ServiciosNovedadPractConvenioImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "novedadPractConvenioMB")
@ViewScoped
public class NovedadPractConvenioMB implements Serializable {

    private final static Logger log = Logger.getLogger(NovedadPractConvenioMB.class);
    private List<NovedadPractConvenio> ListnovedadPractConvenio;
    private List<NovedadPractConvenio> novedadPractConvenioFiltradas;
    private NovedadPractConvenio novedadPractConvenio;
    private String operacion;
    private ServiciosNovedadPractConvenio servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosNovedadPractConvenioImp();
        this.ListnovedadPractConvenio = new ArrayList();
        this.novedadPractConvenio = new NovedadPractConvenio();
        this.ListnovedadPractConvenio = servicios.consultarNovedadPractConvenioJoin();

    }

    public List<NovedadPractConvenio> getListNovedadPractConvenio() {
        return ListnovedadPractConvenio;
    }

    public void setListNovedadPractConvenio(List<NovedadPractConvenio> ListnovedadPractConvenio) {
        this.ListnovedadPractConvenio = ListnovedadPractConvenio;
    }

    public List<NovedadPractConvenio> getNovedadPractConvenioFiltradas() {
        return novedadPractConvenioFiltradas;
    }

    public void setNovedadPractConvenioFiltradas(List<NovedadPractConvenio> novedadPractConvenioFiltradas) {
        this.novedadPractConvenioFiltradas = novedadPractConvenioFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public NovedadPractConvenio getNovedadPractConvenio() {
        return novedadPractConvenio;
    }

    public void setNovedadPractConvenio(NovedadPractConvenio novedadPractConvenio) {
        this.novedadPractConvenio = novedadPractConvenio;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.novedadPractConvenio = new NovedadPractConvenio();
        this.operacion = Constantes.CREAR;

//        this.novedadPractConvenio.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.novedadPractConvenio.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.novedadPractConvenio != null) {
            this.operacion = Constantes.ACTUALIZAR;
            novedadPractConvenio.setPracticanteConvenio(novedadPractConvenio.getPracticanteConvenio());
            novedadPractConvenio.setDatNovedad(novedadPractConvenio.getDatNovedad());
            novedadPractConvenio.setNovedad(novedadPractConvenio.getNovedad());
            novedadPractConvenio.setObservacion(novedadPractConvenio.getObservacion());
            novedadPractConvenio.setEstado(novedadPractConvenio.getEstado());
            novedadPractConvenio.setDatCreacion(novedadPractConvenio.getDatCreacion());
            novedadPractConvenio.setDatModificacion(novedadPractConvenio.getDatModificacion());
            novedadPractConvenio.setUsrCreacion(novedadPractConvenio.getUsrCreacion());
//            novedadPractConvenio.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearNovedadPractConvenio(novedadPractConvenio);
        } else {
            this.servicios.actualizarNovedadPractConvenio(novedadPractConvenio);
        }
        this.lista = true;
        this.crear = false;
        this.ListnovedadPractConvenio = servicios.consultarNovedadPractConvenio();
    }

    public void onBorrar() {
        if (this.novedadPractConvenio != null) {
            servicios.borrarNovedadPractConvenio(novedadPractConvenio);
            this.ListnovedadPractConvenio = servicios.consultarNovedadPractConvenio();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
