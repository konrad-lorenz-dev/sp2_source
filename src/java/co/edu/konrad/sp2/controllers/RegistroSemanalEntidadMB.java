package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.RegistroSemanalEntidad;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosRegistroSemanalEntidad;
import co.edu.konrad.sp2.servicios.ServiciosRegistroSemanalEntidadImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

@ManagedBean(name = "registroSemanalEntidadMB")
@ViewScoped
public class RegistroSemanalEntidadMB implements Serializable {

    private final static Logger log = Logger.getLogger(RegistroSemanalEntidadMB.class);
    private List<RegistroSemanalEntidad> ListregistroSemanalEntidad;
    private List<RegistroSemanalEntidad> registroSemanalEntidadFiltradas;
    private RegistroSemanalEntidad registroSemanalEntidad;
    private List<RegistroSemanalEntidad> registroSemanalEntidadListHistorial;
    private String operacion;
    private ServiciosRegistroSemanalEntidad servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosRegistroSemanalEntidadImp();
        this.ListregistroSemanalEntidad = new ArrayList();
        this.registroSemanalEntidadListHistorial = new ArrayList();
        this.registroSemanalEntidad = new RegistroSemanalEntidad();
        //this.ListregistroSemanalEntidad = servicios.consultarRegistroSemanalEntidadEstadoValido();
        //this.registroSemanalEntidadListHistorial = servicios.consultarRegistroSemanalEntidadHistorial();

    }

    public List<RegistroSemanalEntidad> getListRegistroSemanalEntidad() {
        return ListregistroSemanalEntidad;
    }

    public void setListRegistroSemanalEntidad(List<RegistroSemanalEntidad> ListregistroSemanalEntidad) {
        this.ListregistroSemanalEntidad = ListregistroSemanalEntidad;
    }

    public List<RegistroSemanalEntidad> getRegistroSemanalEntidadFiltradas() {
        return registroSemanalEntidadFiltradas;
    }

    public void setRegistroSemanalEntidadFiltradas(List<RegistroSemanalEntidad> registroSemanalEntidadFiltradas) {
        this.registroSemanalEntidadFiltradas = registroSemanalEntidadFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public RegistroSemanalEntidad getRegistroSemanalEntidad() {
        return registroSemanalEntidad;
    }

    public void setRegistroSemanalEntidad(RegistroSemanalEntidad registroSemanalEntidad) {
        this.registroSemanalEntidad = registroSemanalEntidad;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public List<RegistroSemanalEntidad> getRegistroSemanalEntidadListHistorial() {
        return registroSemanalEntidadListHistorial;
    }

    public void setRegistroSemanalEntidadListHistorial(List<RegistroSemanalEntidad> registroSemanalEntidadListHistorial) {
        this.registroSemanalEntidadListHistorial = registroSemanalEntidadListHistorial;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.registroSemanalEntidad = new RegistroSemanalEntidad();
        this.operacion = Constantes.CREAR;

    }

    public void onActulizarRegistro() {

        if (this.registroSemanalEntidad != null) {
            this.operacion = Constantes.ACTUALIZAR;
            registroSemanalEntidad.setPractica(registroSemanalEntidad.getPractica());
            registroSemanalEntidad.setDescripcion(registroSemanalEntidad.getDescripcion());
            registroSemanalEntidad.setEstadoValido(registroSemanalEntidad.getEstadoValido());
            registroSemanalEntidad.setDatRegistro(registroSemanalEntidad.getDatRegistro());
            registroSemanalEntidad.setDatCreacion(registroSemanalEntidad.getDatCreacion());
            registroSemanalEntidad.setDatModificacion(registroSemanalEntidad.getDatModificacion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearRegistroSemanalEntidad(registroSemanalEntidad);
        } else {
            this.servicios.actualizarRegistroSemanalEntidad(registroSemanalEntidad);
        }
        this.lista = true;
        this.crear = false;
        this.ListregistroSemanalEntidad = servicios.consultarRegistroSemanalEntidad();
    }

    public void onBorrar() {
        if (this.registroSemanalEntidad != null) {
            servicios.borrarRegistroSemanalEntidad(registroSemanalEntidad);
            this.ListregistroSemanalEntidad = servicios.consultarRegistroSemanalEntidad();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
