package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Ciudad;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosCiudad;
import co.edu.konrad.sp2.servicios.ServiciosCiudadImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "ciudadMB")
@ViewScoped
public class CiudadMB implements Serializable {

    private final static Logger log = Logger.getLogger(CiudadMB.class);
    private List<Ciudad> Listciudad;
    private List<Ciudad> ciudadFiltradas;
    private Ciudad ciudad;
    private String operacion;
    private ServiciosCiudad servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosCiudadImp();
        this.Listciudad = new ArrayList();
        this.ciudad = new Ciudad();
        this.Listciudad = servicios.consultarCiudadJoin();

    }

    public List<Ciudad> getListCiudad() {
        return Listciudad;
    }

    public void setListCiudad(List<Ciudad> Listciudad) {
        this.Listciudad = Listciudad;
    }

    public List<Ciudad> getCiudadFiltradas() {
        return ciudadFiltradas;
    }

    public void setCiudadFiltradas(List<Ciudad> ciudadFiltradas) {
        this.ciudadFiltradas = ciudadFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.ciudad = new Ciudad();
        this.operacion = Constantes.CREAR;

//        this.ciudad.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.ciudad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.ciudad != null) {
            this.operacion = Constantes.ACTUALIZAR;
            ciudad.setDepartamento(ciudad.getDepartamento());
            ciudad.setCodigo(ciudad.getCodigo());
            ciudad.setCiudad(ciudad.getCiudad());
            ciudad.setCodigoPostal(ciudad.getCodigoPostal());
            ciudad.setDescripcion(ciudad.getDescripcion());
            ciudad.setDatCreacion(ciudad.getDatCreacion());
            ciudad.setDatModificacion(ciudad.getDatModificacion());
            ciudad.setUsrCreacion(ciudad.getUsrCreacion());
//            ciudad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearCiudad(ciudad);
        } else {
            this.servicios.actualizarCiudad(ciudad);
        }
        this.lista = true;
        this.crear = false;
        this.Listciudad = servicios.consultarCiudad();
    }

    public void onBorrar() {
        if (this.ciudad != null) {
            servicios.borrarCiudad(ciudad);
            this.Listciudad = servicios.consultarCiudad();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
