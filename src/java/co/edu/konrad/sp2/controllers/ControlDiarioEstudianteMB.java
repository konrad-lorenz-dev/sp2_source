package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.ControlDiarioEstudiante;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosControlDiarioEstudiante;
import co.edu.konrad.sp2.servicios.ServiciosControlDiarioEstudianteImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "controlDiarioEstudianteMB")
@ViewScoped
public class ControlDiarioEstudianteMB implements Serializable {

    private final static Logger log = Logger.getLogger(ControlDiarioEstudianteMB.class);
    private List<ControlDiarioEstudiante> ListcontrolDiarioEstudiante;
    private List<ControlDiarioEstudiante> controlDiarioEstudianteFiltradas;
    private ControlDiarioEstudiante controlDiarioEstudiante;
    private String operacion;
    private ServiciosControlDiarioEstudiante servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosControlDiarioEstudianteImp();
        this.ListcontrolDiarioEstudiante = new ArrayList();
        this.controlDiarioEstudiante = new ControlDiarioEstudiante();
        this.ListcontrolDiarioEstudiante = servicios.consultarControlDiarioEstudianteJoin();

    }

    public List<ControlDiarioEstudiante> getListControlDiarioEstudiante() {
        return ListcontrolDiarioEstudiante;
    }

    public void setListControlDiarioEstudiante(List<ControlDiarioEstudiante> ListcontrolDiarioEstudiante) {
        this.ListcontrolDiarioEstudiante = ListcontrolDiarioEstudiante;
    }

    public List<ControlDiarioEstudiante> getControlDiarioEstudianteFiltradas() {
        return controlDiarioEstudianteFiltradas;
    }

    public void setControlDiarioEstudianteFiltradas(List<ControlDiarioEstudiante> controlDiarioEstudianteFiltradas) {
        this.controlDiarioEstudianteFiltradas = controlDiarioEstudianteFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public ControlDiarioEstudiante getControlDiarioEstudiante() {
        return controlDiarioEstudiante;
    }

    public void setControlDiarioEstudiante(ControlDiarioEstudiante controlDiarioEstudiante) {
        this.controlDiarioEstudiante = controlDiarioEstudiante;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.controlDiarioEstudiante = new ControlDiarioEstudiante();
        this.operacion = Constantes.CREAR;

//        this.controlDiarioEstudiante.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.controlDiarioEstudiante.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.controlDiarioEstudiante != null) {
            this.operacion = Constantes.ACTUALIZAR;
            controlDiarioEstudiante.setPractica(controlDiarioEstudiante.getPractica());
            controlDiarioEstudiante.setSesionControl(controlDiarioEstudiante.getSesionControl());
            controlDiarioEstudiante.setDatControl(controlDiarioEstudiante.getDatControl());
            controlDiarioEstudiante.setHoraEntrada(controlDiarioEstudiante.getHoraEntrada());
            controlDiarioEstudiante.setHoraSalida(controlDiarioEstudiante.getHoraSalida());
            controlDiarioEstudiante.setActividad(controlDiarioEstudiante.getActividad());
            controlDiarioEstudiante.setDatCreacion(controlDiarioEstudiante.getDatCreacion());
            controlDiarioEstudiante.setDatModificacion(controlDiarioEstudiante.getDatModificacion());
            controlDiarioEstudiante.setUsrCreacion(controlDiarioEstudiante.getUsrCreacion());
//            controlDiarioEstudiante.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearControlDiarioEstudiante(controlDiarioEstudiante);
        } else {
            this.servicios.actualizarControlDiarioEstudiante(controlDiarioEstudiante);
        }
        this.lista = true;
        this.crear = false;
        this.ListcontrolDiarioEstudiante = servicios.consultarControlDiarioEstudiante();
    }

    public void onBorrar() {
        if (this.controlDiarioEstudiante != null) {
            servicios.borrarControlDiarioEstudiante(controlDiarioEstudiante);
            this.ListcontrolDiarioEstudiante = servicios.consultarControlDiarioEstudiante();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
