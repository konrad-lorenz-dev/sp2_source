package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.UsuarioSesion;
import co.edu.konrad.sp2.bean.Persona;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosPersona;
import co.edu.konrad.sp2.servicios.ServiciosPersonaImp;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@ManagedBean(name = "inicioMB")
@SessionScoped
public class InicioMB implements Serializable {

    private final static Logger log = Logger.getLogger(InicioMB.class);
    private boolean mostrarSalir;
    private Persona user;
    private UsuarioSesion usuarioSesion;
    private ServiciosPersona servicioPersona;

    @PostConstruct
    public void init() {
        mostrarSalir = false;
        this.usuarioSesion = new UsuarioSesion();
        servicioPersona = new ServiciosPersonaImp();
    }

    public Persona getUser() {
        return user;
    }

    public void setUser(Persona user) {
        this.user = user;
    }

    public UsuarioSesion getUsuarioSesion() {
        return usuarioSesion;
    }

    public void setUsuarioSesion(UsuarioSesion usuarioSesion) {
        this.usuarioSesion = usuarioSesion;
    }

    public boolean isMostrarSalir() {
        return mostrarSalir;
    }

    public void setMostrarSalir(boolean mostrarSalir) {
        this.mostrarSalir = mostrarSalir;
    }

    public void logout() {

    }

    public void onIngresar() {
        String usuarioValido;
        System.out.println("Usuario: " + usuarioSesion.getUsuario());
        System.out.println("Contraseña: " + usuarioSesion.getContrasenia());
        usuarioValido = servicioPersona.validaUsuario(usuarioSesion);
        if (!usuarioValido.equalsIgnoreCase(Constantes.ZERO)) {
            // Long seqPersona;
            // seqPersona = servicioPersona.consultaSeqUsuario(usuarioSesion);
            //user = servicioPersona.consultarPersonaJoinById(seqPersona);                   

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuario registrado", "Usuario existente"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Usuario no registrado en el sistema"));
        }
    }
}
