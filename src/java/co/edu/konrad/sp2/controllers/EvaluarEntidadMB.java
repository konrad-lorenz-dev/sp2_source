package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.EvaluarEntidad;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosEvaluarEntidad;
import co.edu.konrad.sp2.servicios.ServiciosEvaluarEntidadImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "evaluarEntidadMB")
@ViewScoped
public class EvaluarEntidadMB implements Serializable {

    private final static Logger log = Logger.getLogger(EvaluarEntidadMB.class);
    private List<EvaluarEntidad> ListevaluarEntidad;
    private List<EvaluarEntidad> evaluarEntidadFiltradas;
    private EvaluarEntidad evaluarEntidad;
    private String operacion;
    private ServiciosEvaluarEntidad servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosEvaluarEntidadImp();
        this.ListevaluarEntidad = new ArrayList();
        this.evaluarEntidad = new EvaluarEntidad();
        this.ListevaluarEntidad = servicios.consultarEvaluarEntidadJoin();

    }

    public List<EvaluarEntidad> getListEvaluarEntidad() {
        return ListevaluarEntidad;
    }

    public void setListEvaluarEntidad(List<EvaluarEntidad> ListevaluarEntidad) {
        this.ListevaluarEntidad = ListevaluarEntidad;
    }

    public List<EvaluarEntidad> getEvaluarEntidadFiltradas() {
        return evaluarEntidadFiltradas;
    }

    public void setEvaluarEntidadFiltradas(List<EvaluarEntidad> evaluarEntidadFiltradas) {
        this.evaluarEntidadFiltradas = evaluarEntidadFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public EvaluarEntidad getEvaluarEntidad() {
        return evaluarEntidad;
    }

    public void setEvaluarEntidad(EvaluarEntidad evaluarEntidad) {
        this.evaluarEntidad = evaluarEntidad;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.evaluarEntidad = new EvaluarEntidad();
        this.operacion = Constantes.CREAR;

//        this.evaluarEntidad.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.evaluarEntidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.evaluarEntidad != null) {
            this.operacion = Constantes.ACTUALIZAR;
            evaluarEntidad.setDetalleSupPractica(evaluarEntidad.getDetalleSupPractica());
            evaluarEntidad.setPracticanteConvenio(evaluarEntidad.getPracticanteConvenio());
            evaluarEntidad.setEvaluacion(evaluarEntidad.getEvaluacion());
            evaluarEntidad.setObservaciones(evaluarEntidad.getObservaciones());
            evaluarEntidad.setDatCreacion(evaluarEntidad.getDatCreacion());
            evaluarEntidad.setDatModificacion(evaluarEntidad.getDatModificacion());
            evaluarEntidad.setUsrCreacion(evaluarEntidad.getUsrCreacion());
//            evaluarEntidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearEvaluarEntidad(evaluarEntidad);
        } else {
            this.servicios.actualizarEvaluarEntidad(evaluarEntidad);
        }
        this.lista = true;
        this.crear = false;
        this.ListevaluarEntidad = servicios.consultarEvaluarEntidad();
    }

    public void onBorrar() {
        if (this.evaluarEntidad != null) {
            servicios.borrarEvaluarEntidad(evaluarEntidad);
            this.ListevaluarEntidad = servicios.consultarEvaluarEntidad();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
