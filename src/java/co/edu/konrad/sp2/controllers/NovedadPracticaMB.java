package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.NovedadPractica;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosNovedadPractica;
import co.edu.konrad.sp2.servicios.ServiciosNovedadPracticaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "novedadPracticaMB")
@ViewScoped
public class NovedadPracticaMB implements Serializable {

    private final static Logger log = Logger.getLogger(NovedadPracticaMB.class);
    private List<NovedadPractica> ListnovedadPractica;
    private List<NovedadPractica> novedadPracticaFiltradas;
    private NovedadPractica novedadPractica;
    private String operacion;
    private ServiciosNovedadPractica servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosNovedadPracticaImp();
        this.ListnovedadPractica = new ArrayList();
        this.novedadPractica = new NovedadPractica();
        this.ListnovedadPractica = servicios.consultarNovedadPracticaJoin();

    }

    public List<NovedadPractica> getListNovedadPractica() {
        return ListnovedadPractica;
    }

    public void setListNovedadPractica(List<NovedadPractica> ListnovedadPractica) {
        this.ListnovedadPractica = ListnovedadPractica;
    }

    public List<NovedadPractica> getNovedadPracticaFiltradas() {
        return novedadPracticaFiltradas;
    }

    public void setNovedadPracticaFiltradas(List<NovedadPractica> novedadPracticaFiltradas) {
        this.novedadPracticaFiltradas = novedadPracticaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public NovedadPractica getNovedadPractica() {
        return novedadPractica;
    }

    public void setNovedadPractica(NovedadPractica novedadPractica) {
        this.novedadPractica = novedadPractica;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.novedadPractica = new NovedadPractica();
        this.operacion = Constantes.CREAR;

//        this.novedadPractica.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.novedadPractica.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.novedadPractica != null) {
            this.operacion = Constantes.ACTUALIZAR;
            novedadPractica.setPractica(novedadPractica.getPractica());
            novedadPractica.setDatNovedad(novedadPractica.getDatNovedad());
            novedadPractica.setNovedad(novedadPractica.getNovedad());
            novedadPractica.setEstado(novedadPractica.getEstado());
            novedadPractica.setDatCreacion(novedadPractica.getDatCreacion());
            novedadPractica.setDatModificacion(novedadPractica.getDatModificacion());
            novedadPractica.setUsrCreacion(novedadPractica.getUsrCreacion());
//            novedadPractica.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearNovedadPractica(novedadPractica);
        } else {
            this.servicios.actualizarNovedadPractica(novedadPractica);
        }
        this.lista = true;
        this.crear = false;
        this.ListnovedadPractica = servicios.consultarNovedadPractica();
    }

    public void onBorrar() {
        if (this.novedadPractica != null) {
            servicios.borrarNovedadPractica(novedadPractica);
            this.ListnovedadPractica = servicios.consultarNovedadPractica();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
