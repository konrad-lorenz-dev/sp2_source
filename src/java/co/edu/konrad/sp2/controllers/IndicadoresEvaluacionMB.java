package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.IndicadoresEvaluacion;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosIndicadoresEvaluacion;
import co.edu.konrad.sp2.servicios.ServiciosIndicadoresEvaluacionImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "indicadoresEvaluacionMB")
@ViewScoped
public class IndicadoresEvaluacionMB implements Serializable {

    private final static Logger log = Logger.getLogger(IndicadoresEvaluacionMB.class);
    private List<IndicadoresEvaluacion> ListindicadoresEvaluacion;
    private List<IndicadoresEvaluacion> indicadoresEvaluacionFiltradas;
    private IndicadoresEvaluacion indicadoresEvaluacion;
    private String operacion;
    private ServiciosIndicadoresEvaluacion servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosIndicadoresEvaluacionImp();
        this.ListindicadoresEvaluacion = new ArrayList();
        this.indicadoresEvaluacion = new IndicadoresEvaluacion();
        this.ListindicadoresEvaluacion = servicios.consultarIndicadoresEvaluacionJoin();

    }

    public List<IndicadoresEvaluacion> getListIndicadoresEvaluacion() {
        return ListindicadoresEvaluacion;
    }

    public void setListIndicadoresEvaluacion(List<IndicadoresEvaluacion> ListindicadoresEvaluacion) {
        this.ListindicadoresEvaluacion = ListindicadoresEvaluacion;
    }

    public List<IndicadoresEvaluacion> getIndicadoresEvaluacionFiltradas() {
        return indicadoresEvaluacionFiltradas;
    }

    public void setIndicadoresEvaluacionFiltradas(List<IndicadoresEvaluacion> indicadoresEvaluacionFiltradas) {
        this.indicadoresEvaluacionFiltradas = indicadoresEvaluacionFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public IndicadoresEvaluacion getIndicadoresEvaluacion() {
        return indicadoresEvaluacion;
    }

    public void setIndicadoresEvaluacion(IndicadoresEvaluacion indicadoresEvaluacion) {
        this.indicadoresEvaluacion = indicadoresEvaluacion;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.indicadoresEvaluacion = new IndicadoresEvaluacion();
        this.operacion = Constantes.CREAR;

//        this.indicadoresEvaluacion.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.indicadoresEvaluacion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.indicadoresEvaluacion != null) {
            this.operacion = Constantes.ACTUALIZAR;
            indicadoresEvaluacion.setCompetenciaIndicador(indicadoresEvaluacion.getCompetenciaIndicador());
            indicadoresEvaluacion.setAreaEscolaris(indicadoresEvaluacion.getAreaEscolaris());
            indicadoresEvaluacion.setIndicadorGestion(indicadoresEvaluacion.getIndicadorGestion());
            indicadoresEvaluacion.setEstadoVisible(indicadoresEvaluacion.getEstadoVisible());
            indicadoresEvaluacion.setDatCreacion(indicadoresEvaluacion.getDatCreacion());
            indicadoresEvaluacion.setDatModificacion(indicadoresEvaluacion.getDatModificacion());
            indicadoresEvaluacion.setUsrCreacion(indicadoresEvaluacion.getUsrCreacion());
//            indicadoresEvaluacion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearIndicadoresEvaluacion(indicadoresEvaluacion);
        } else {
            this.servicios.actualizarIndicadoresEvaluacion(indicadoresEvaluacion);
        }
        this.lista = true;
        this.crear = false;
        this.ListindicadoresEvaluacion = servicios.consultarIndicadoresEvaluacion();
    }

    public void onBorrar() {
        if (this.indicadoresEvaluacion != null) {
            servicios.borrarIndicadoresEvaluacion(indicadoresEvaluacion);
            this.ListindicadoresEvaluacion = servicios.consultarIndicadoresEvaluacion();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
