  package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.Ciudad;
//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.DetalleCoorPracticas;
import co.edu.konrad.sp2.bean.DetallePracticante;
import co.edu.konrad.sp2.bean.Eps;
import co.edu.konrad.sp2.bean.Persona;
import co.edu.konrad.sp2.bean.ProgramaEscolaris;
import co.edu.konrad.sp2.bean.RolPersona;
import co.edu.konrad.sp2.bean.TipoIdentificacion;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosCiudad;
import co.edu.konrad.sp2.servicios.ServiciosCiudadImp;
import co.edu.konrad.sp2.servicios.ServiciosDetalleCoorPracticas;
import co.edu.konrad.sp2.servicios.ServiciosDetalleCoorPracticasImp;
import co.edu.konrad.sp2.servicios.ServiciosEps;
import co.edu.konrad.sp2.servicios.ServiciosEpsImp;
import co.edu.konrad.sp2.servicios.ServiciosPersona;
import co.edu.konrad.sp2.servicios.ServiciosPersonaImp;
import co.edu.konrad.sp2.servicios.ServiciosProgramaEscolaris;
import co.edu.konrad.sp2.servicios.ServiciosProgramaEscolarisImp;
import co.edu.konrad.sp2.servicios.ServiciosRolPersona;
import co.edu.konrad.sp2.servicios.ServiciosRolPersonaImp;
import co.edu.konrad.sp2.servicios.ServiciosTipoIdentificacion;
import co.edu.konrad.sp2.servicios.ServiciosTipoIdentificacionImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "detalleCoorPracticasMB")
@ViewScoped
public class DetalleCoorPracticasMB implements Serializable {

    private final static Logger log = Logger.getLogger(DetalleCoorPracticasMB.class);
    private List<DetalleCoorPracticas> listdetalleCoorPracticas;
    private List<DetalleCoorPracticas> detalleCoorPracticasFiltradas;
    private List<Ciudad> listaCiudad;
    private List<Eps> listaEPS;
    private List<TipoIdentificacion> listaTipoIdenticacion;
    private List<ProgramaEscolaris> listProgramaEscolaris;
    private DetalleCoorPracticas detalleCoorPracticas;
    private String operacion;
    private ServiciosDetalleCoorPracticas servicios;
    private ServiciosCiudad servicioCiudad;
    private ServiciosEps servicioEps;
    private ServiciosPersona serviciosPersona;
    private ServiciosProgramaEscolaris servicioPrograma;
    private ServiciosRolPersona servicioRolPersona;
    private ServiciosTipoIdentificacion servicioTipoIdentificacion;
    private boolean crear;
    private boolean lista;
    private boolean varPanelEditar, varPanelInfoPersonal;
    private boolean menuestudiante, menuSupervisor, menuEntidad, menuConvenio, menuCompetencias, menuAgenda, menuReportes, menuNotificaciones;
    private boolean pnEstudianteRegistro, pnEstudianteConsultar;
    private boolean pnSupervisorRegistro, pnSupervisorConsultar, pnPerfilSesion;
    private Persona practicante;
    private RolPersona rolPersona;
    private String codigo;
    private DetallePracticante detallePracticante;
    private boolean varPanPracticaEstudiante;
    private Persona persona;
    private List<String> opciones;

    @PostConstruct
    public void init() {
        
        opciones = new ArrayList<String>();

        opciones.add("imgCargue.png");
        opciones.add("imgCompetencia.png");
        opciones.add("imgConvenio.png");
        opciones.add("imgEntidad.png");
        opciones.add("imgEstudiante.png");
        opciones.add("imgSupervisor.png");
        opciones.add("imgUsers.png");
        opciones.add("imgHome.png");
        
        this.pnPerfilSesion = true;
        this.lista = true;
        this.crear = false;
        this.varPanelEditar = false;
        this.varPanelInfoPersonal = true;
        this.servicios = new ServiciosDetalleCoorPracticasImp();
        this.servicioCiudad = new ServiciosCiudadImp();
        this.servicioEps = new ServiciosEpsImp();
        this.serviciosPersona = new ServiciosPersonaImp();
        this.servicioPrograma = new ServiciosProgramaEscolarisImp();
        this.servicioTipoIdentificacion = new ServiciosTipoIdentificacionImp();
        this.servicioRolPersona = new ServiciosRolPersonaImp();
        this.detalleCoorPracticas = new DetalleCoorPracticas();
        this.listdetalleCoorPracticas = new ArrayList();
        this.listdetalleCoorPracticas = servicios.consultarDetalleCoorPracticasJoin();
        this.listaCiudad = new ArrayList();
        this.listaCiudad = servicioCiudad.consultarCiudad();
        this.listaEPS = new ArrayList();
        this.listaEPS = servicioEps.consultarEps();
        this.listProgramaEscolaris = new ArrayList();
        this.listProgramaEscolaris = servicioPrograma.consultarProgramaEscolaris();
        this.listaTipoIdenticacion = new ArrayList();
        this.listaTipoIdenticacion = servicioTipoIdentificacion.consultarTipoIdentificacion();
        this.practicante = new Persona();
        this.rolPersona = new RolPersona();
        this.detallePracticante = new DetallePracticante();
        this.varPanPracticaEstudiante = false;
        //Conocer la persona si esta logeada o no
//        if (SesionContexto.getInstance().isInicioSesion()) {
//            SesionContexto.getInstance().setAttribute(Constantes.PERSONA, AsistenteSesion.getInstance().getPersona());
//            SesionContexto.getInstance().setAttribute(Constantes.USUARIO, AsistenteSesion.getInstance().getUsuario());
//            SesionContexto.getInstance().setInicioSesion(false);
//        }
        this.detalleCoorPracticas = servicios.consultarDetalleCoorPracticasJoinById(Long.MIN_VALUE);
    }

    public List<String> getOpciones() {
        return opciones;
    }

    public void setOpciones(List<String> opciones) {
        this.opciones = opciones;
    }
    

    public boolean isPnPerfilSesion() {
        return pnPerfilSesion;
    }

    public void setPnPerfilSesion(boolean pnPerfilSesion) {
        this.pnPerfilSesion = pnPerfilSesion;
    }  
    
    public boolean isPnSupervisorRegistro() {
        return pnSupervisorRegistro;
    }

    public void setPnSupervisorRegistro(boolean pnSupervisorRegistro) {
        this.pnSupervisorRegistro = pnSupervisorRegistro;
    }

    public boolean isPnSupervisorConsultar() {
        return pnSupervisorConsultar;
    }

    public void setPnSupervisorConsultar(boolean pnSupervisorConsultar) {
        this.pnSupervisorConsultar = pnSupervisorConsultar;
    }

    public List<TipoIdentificacion> getListaTipoIdenticacion() {
        return listaTipoIdenticacion;
    }

    public void setListaTipoIdenticacion(List<TipoIdentificacion> listaTipoIdenticacion) {
        this.listaTipoIdenticacion = listaTipoIdenticacion;
    }

    public List<DetalleCoorPracticas> getListdetalleCoorPracticas() {
        return listdetalleCoorPracticas;
    }

    public void setListdetalleCoorPracticas(List<DetalleCoorPracticas> ListdetalleCoorPracticas) {
        this.listdetalleCoorPracticas = ListdetalleCoorPracticas;
    }

    public List<ProgramaEscolaris> getListProgramaEscolaris() {
        return listProgramaEscolaris;
    }

    public void setListProgramaEscolaris(List<ProgramaEscolaris> ListProgramaEscolaris) {
        this.listProgramaEscolaris = ListProgramaEscolaris;
    }

    public List<DetalleCoorPracticas> getListDetalleCoorPracticas() {
        return listdetalleCoorPracticas;
    }

    public void setListDetalleCoorPracticas(List<DetalleCoorPracticas> ListdetalleCoorPracticas) {
        this.listdetalleCoorPracticas = ListdetalleCoorPracticas;
    }

    public List<DetalleCoorPracticas> getDetalleCoorPracticasFiltradas() {
        return detalleCoorPracticasFiltradas;
    }

    public void setDetalleCoorPracticasFiltradas(List<DetalleCoorPracticas> detalleCoorPracticasFiltradas) {
        this.detalleCoorPracticasFiltradas = detalleCoorPracticasFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public DetalleCoorPracticas getDetalleCoorPracticas() {
        return detalleCoorPracticas;
    }

    public void setDetalleCoorPracticas(DetalleCoorPracticas detalleCoorPracticas) {
        this.detalleCoorPracticas = detalleCoorPracticas;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public boolean isVarPanelEditar() {
        return varPanelEditar;
    }

    public void setVarPanelEditar(boolean varPanelEditar) {
        this.varPanelEditar = varPanelEditar;
    }

    public boolean isVarPanelInfoPersonal() {
        return varPanelInfoPersonal;
    }

    public void setVarPanelInfoPersonal(boolean varPanelInfoPersonal) {
        this.varPanelInfoPersonal = varPanelInfoPersonal;
    }

    public boolean isMenuestudiante() {
        return menuestudiante;
    }

    public void setMenuestudiante(boolean menuestudiante) {
        this.menuestudiante = menuestudiante;
    }

    public boolean isMenuSupervisor() {
        return menuSupervisor;
    }

    public void setMenuSupervisor(boolean menuSupervisor) {
        this.menuSupervisor = menuSupervisor;
    }

    public boolean isMenuEntidad() {
        return menuEntidad;
    }

    public void setMenuEntidad(boolean menuEntidad) {
        this.menuEntidad = menuEntidad;
    }

    public boolean isMenuConvenio() {
        return menuConvenio;
    }

    public void setMenuConvenio(boolean menuConvenio) {
        this.menuConvenio = menuConvenio;
    }

    public boolean isMenuCompetencias() {
        return menuCompetencias;
    }

    public void setMenuCompetencias(boolean menuCompetencias) {
        this.menuCompetencias = menuCompetencias;
    }

    public boolean isMenuAgenda() {
        return menuAgenda;
    }

    public void setMenuAgenda(boolean menuAgenda) {
        this.menuAgenda = menuAgenda;
    }

    public boolean isMenuReportes() {
        return menuReportes;
    }

    public void setMenuReportes(boolean menuReportes) {
        this.menuReportes = menuReportes;
    }

    public boolean isMenuNotificaciones() {
        return menuNotificaciones;
    }

    public void setMenuNotificaciones(boolean menuNotificaciones) {
        this.menuNotificaciones = menuNotificaciones;
    }

    public Persona getPracticante() {
        return practicante;
    }

    public void setPracticante(Persona practicante) {
        this.practicante = practicante;
    }

    public DetallePracticante getDetallePracticante() {
        return detallePracticante;
    }

    public void setDetallePracticante(DetallePracticante detallePracticante) {
        this.detallePracticante = detallePracticante;
    }

    public boolean isPnEstudianteRegistro() {
        return pnEstudianteRegistro;
    }

    public void setPnEstudianteRegistro(boolean pnEstudianteRegistro) {
        this.pnEstudianteRegistro = pnEstudianteRegistro;
    }

    public boolean isPnEstudianteConsultar() {
        return pnEstudianteConsultar;
    }

    public void setPnEstudianteConsultar(boolean pnEstudianteConsultar) {
        this.pnEstudianteConsultar = pnEstudianteConsultar;
    }

    public List<Ciudad> getListaCiudad() {
        return listaCiudad;
    }

    public void setListaCiudad(List<Ciudad> listaCiudad) {
        this.listaCiudad = listaCiudad;
    }

    public List<Eps> getListaEPS() {
        return listaEPS;
    }

    public void setListaEPS(List<Eps> listaEPS) {
        this.listaEPS = listaEPS;
    }

    public boolean isVarPanPracticaEstudiante() {
        return varPanPracticaEstudiante;
    }

    public ServiciosCiudad getServicioCiudad() {
        return servicioCiudad;
    }

    public void setServicioCiudad(ServiciosCiudad servicioCiudad) {
        this.servicioCiudad = servicioCiudad;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public void setVarPanPracticaEstudiante(boolean varPanPracticaEstudiante) {
        this.varPanPracticaEstudiante = varPanPracticaEstudiante;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void onCancelar() {
        this.varPanelEditar = false;
        this.varPanelInfoPersonal = true;
        // FacesMessage message = new FacesMessage("Succesful", " is uploaded.");
        // FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.detalleCoorPracticas = new DetalleCoorPracticas();
        this.operacion = Constantes.CREAR;

//        this.detalleCoorPracticas.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.detalleCoorPracticas.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {
        if (this.detalleCoorPracticas != null) {
            this.operacion = Constantes.ACTUALIZAR;
            System.out.println("id PERSONA: " + detalleCoorPracticas.getPersona().getNombres());
            persona.setNombres(detalleCoorPracticas.getPersona().getNombres());
            persona.setApellidos(detalleCoorPracticas.getPersona().getApellidos());
            persona.setCedula(detalleCoorPracticas.getPersona().getCedula());
            persona.setLugarExpedicion(detalleCoorPracticas.getPersona().getLugarExpedicion());
            persona.setCargo(detalleCoorPracticas.getPersona().getCargo());
            persona.setCodigo(detalleCoorPracticas.getPersona().getCodigo());
            persona.setCorreoInstiCorp(detalleCoorPracticas.getPersona().getCorreoInstiCorp());
            persona.setCorreoAlternativo(detalleCoorPracticas.getPersona().getCorreoAlternativo());
            persona.setTelefono(detalleCoorPracticas.getPersona().getTelefono());
            persona.setCelular(detalleCoorPracticas.getPersona().getCelular());
            persona.setDireccion(detalleCoorPracticas.getPersona().getDireccion());
            persona.setDatVigenciaDesde(detalleCoorPracticas.getPersona().getDatVigenciaDesde());
            persona.setDatVigenciaHasta(detalleCoorPracticas.getPersona().getDatVigenciaHasta());
            persona.setSeqPersona(detalleCoorPracticas.getPersona().getSeqPersona());
            persona.setUsrModificacion(detalleCoorPracticas.getPersona().getNombres() + detalleCoorPracticas.getPersona().getApellidos());
            persona.getEstadoPersona().setSeqEstadoPersona(detalleCoorPracticas.getPersona().getEstadoPersona().getSeqEstadoPersona());
            persona.getTipoIdentificacion().setSeqTipoIdentificacion(detalleCoorPracticas.getPersona().getTipoIdentificacion().getSeqTipoIdentificacion());
            persona.getCiudad().setSeqCiudad(detalleCoorPracticas.getPersona().getCiudad().getSeqCiudad());
            //detalleCoorPracticas.getPersona().setSeqPersona(persona.getSeqPersona());
            detalleCoorPracticas.setOficina(detalleCoorPracticas.getOficina());
            detalleCoorPracticas.setNombreSecretaria(detalleCoorPracticas.getNombreSecretaria());
            detalleCoorPracticas.setOficinaSecretaria(detalleCoorPracticas.getOficinaSecretaria());
            detalleCoorPracticas.setExtensionOficina(detalleCoorPracticas.getExtensionOficina());
            detalleCoorPracticas.setExtensionSecretaria(detalleCoorPracticas.getExtensionSecretaria());
            detalleCoorPracticas.setDatModificacion(detalleCoorPracticas.getDatModificacion());
            detalleCoorPracticas.setUsrModificacion(detalleCoorPracticas.getPersona().getNombres() + detalleCoorPracticas.getPersona().getApellidos());
//          detalleCoorPracticas.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
            onGuardarRegistro();
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;

        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearDetalleCoorPracticas(detalleCoorPracticas);

        } else {
            this.serviciosPersona.actualizarPersona(persona);
            this.servicios.actualizarDetalleCoorPracticas(detalleCoorPracticas);
            System.out.println("Entró a guardar registro");
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Información", "Datos actualizados correctamente");
            PrimeFaces.current().dialog().showMessageDynamic(message);
            this.varPanelEditar = false;
            this.varPanelInfoPersonal = true;

        }
        this.lista = true;
        this.crear = false;
        this.listdetalleCoorPracticas = servicios.consultarDetalleCoorPracticas();
    }

    public void onBorrar() {
        if (this.detalleCoorPracticas != null) {
            servicios.borrarDetalleCoorPracticas(detalleCoorPracticas);
            this.listdetalleCoorPracticas = servicios.consultarDetalleCoorPracticas();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));
        }
    }

    public void panelEditar() {
        this.varPanelEditar = true;
        this.varPanelInfoPersonal = false;
        this.pnEstudianteRegistro = false;
    }

    public void opcionMenu(int seleccion) {
        switch (seleccion) {
            case 1:
                if (menuestudiante == true) {
                    menuestudiante = false;
                } else {
                    menuestudiante = true;
                }
                break;
            case 2:
                if (menuSupervisor == true) {
                    menuSupervisor = false;
                } else {
                    menuSupervisor = true;
                }
                break;

            case 3:
                if (menuEntidad == true) {
                    menuEntidad = false;
                } else {
                    menuEntidad = true;
                }
                break;

            case 4:
                if (menuConvenio == true) {
                    menuConvenio = false;
                } else {
                    menuConvenio = true;
                }
                break;

            case 5:
                if (menuCompetencias == true) {
                    menuCompetencias = false;
                } else {
                    menuCompetencias = true;
                }
                break;

            case 6:
                if (menuAgenda == true) {
                    menuAgenda = false;
                } else {
                    menuAgenda = true;
                }
                break;

            case 7:
                if (menuReportes == true) {
                    menuReportes = false;
                } else {
                    menuReportes = true;
                }
                break;

            case 8:
                if (menuNotificaciones == true) {
                    menuNotificaciones = false;
                } else {
                    menuNotificaciones = true;
                }
                break;
        }
    }

    public void opcionSubMenu(int seleccion) {

        switch (seleccion) {
            case 1:
                if (pnEstudianteRegistro == true) {
                    pnEstudianteRegistro = false;
                    pnEstudianteConsultar = false;
                } else {
                    pnEstudianteRegistro = true;
                    pnEstudianteConsultar = false;
                }
                break;

            case 2:
                pnEstudianteConsultar = true;
                varPanelInfoPersonal = false;
                pnPerfilSesion = false;
                System.out.println("panel : *** " + varPanelInfoPersonal);
                 System.out.println("panel2 : *** " + pnEstudianteConsultar);
                break;

            case 3:
                pnEstudianteConsultar = false;
                pnEstudianteRegistro = false;
                if (pnSupervisorConsultar == true) {
                    pnSupervisorConsultar = false;
                    pnSupervisorRegistro = false;
                } else {
                    pnSupervisorConsultar = true;
                    pnSupervisorRegistro = false;
                }
                break;

            case 4:
                break;

            case 5:
                break;

            case 6:
                break;

            case 7:
                break;
        }
    }

    public void consultarPracticante() {
        if (codigo == null) {
            RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_WARN, "Información", "Por favor ingrese un código"));
        } else {
            this.practicante = serviciosPersona.ConsultarEstEscolaris(codigo);
            if (practicante.getConstante().equalsIgnoreCase(Constantes.FAILED)) {
                RequestContext.getCurrentInstance().showMessageInDialog(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "El código ingresado no se encuentra registrado en el sistema"));
            }
        }
    }

    public void limpiarBusqueda() {
        codigo = null;
        this.practicante = new Persona();
        this.detallePracticante = new DetallePracticante();
    }

    public void registrarPracticante() {

        System.out.println("usuario: " + detalleCoorPracticas.getPersona().getCorreoInstiCorp());

        Long textoId = Long.parseLong(Constantes.ESTADO_ACTIVO);
        practicante.getEstadoPersona().setSeqEstadoPersona(textoId);
        practicante.setUsrCreacion(detalleCoorPracticas.getPersona().getCorreoInstiCorp());
        practicante.setUsrModificacion(detalleCoorPracticas.getPersona().getCorreoInstiCorp());
        practicante.getCiudad().setSeqCiudad(Long.parseLong(practicante.getLugarExpedicion()));
        this.serviciosPersona.crearPersona(practicante);
        if (practicante.getConstante().equalsIgnoreCase(Constantes.OK)) {
            Persona seqPersona;
            seqPersona = this.serviciosPersona.consultarPersonaCedula(practicante.getCedula());
            if (seqPersona.getConstante().equalsIgnoreCase(Constantes.OK)) {
                rolPersona.getPersona().setSeqPersona(seqPersona.getSeqPersona());
                rolPersona.getRol().setSeqRolPersona(Long.parseLong(Constantes.ROL_PRACTICANTE));
                rolPersona.setObservacion("Practicante");
                rolPersona.setUsrCreacion(detalleCoorPracticas.getPersona().getCorreoInstiCorp());
                rolPersona.setUsrModificacion(detalleCoorPracticas.getPersona().getCorreoInstiCorp());
                this.servicioRolPersona.crearRolPersona(rolPersona);
                if (rolPersona.getConstante().equalsIgnoreCase(Constantes.OK)) {
                    limpiarBusqueda();
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Practicante creado", "El practicante ha sido creado correctamente");
                    PrimeFaces.current().dialog().showMessageDynamic(message);
                } else {
                    FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Error al crear registro");
                    PrimeFaces.current().dialog().showMessageDynamic(message);
                    log.error("Error al crear rol persona practicante");
                }
            } else {
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Error al crear registro");
                PrimeFaces.current().dialog().showMessageDynamic(message);
                log.error("Error al asignar rol practicante");
            }
        } else {
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", "Error al crear registro");
            PrimeFaces.current().dialog().showMessageDynamic(message);
            log.error("Error al crear persona practicante");
        }
    }

    public String procesoPracticante() {
        this.varPanPracticaEstudiante = true;
        return "/vistas/VistaDetallePracticante.xhtml?faces-redirect=true";
    }

    public void RegistroDetallePracticante(String op) {
        System.out.println("PRUEBA CORRECTA");
        System.out.println("Entró a opcion: " + op);
        //this.varPanelInfoPersonal = false;
        //return "/vistas/VistaGrillaDetallePracticante.xhtml?faces-redirect=true";
    }

}
