package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.Cronograma;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosCronograma;
import co.edu.konrad.sp2.servicios.ServiciosCronogramaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "cronogramaMB")
@ViewScoped
public class CronogramaMB implements Serializable {

    private final static Logger log = Logger.getLogger(CronogramaMB.class);
    private List<Cronograma> Listcronograma;
    private List<Cronograma> cronogramaFiltradas;
    private Cronograma cronograma;
    private String operacion;
    private ServiciosCronograma servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosCronogramaImp();
        this.Listcronograma = new ArrayList();
        this.cronograma = new Cronograma();
        this.Listcronograma = servicios.consultarCronogramaJoin();

    }

    public List<Cronograma> getListCronograma() {
        return Listcronograma;
    }

    public void setListCronograma(List<Cronograma> Listcronograma) {
        this.Listcronograma = Listcronograma;
    }

    public List<Cronograma> getCronogramaFiltradas() {
        return cronogramaFiltradas;
    }

    public void setCronogramaFiltradas(List<Cronograma> cronogramaFiltradas) {
        this.cronogramaFiltradas = cronogramaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public Cronograma getCronograma() {
        return cronograma;
    }

    public void setCronograma(Cronograma cronograma) {
        this.cronograma = cronograma;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.cronograma = new Cronograma();
        this.operacion = Constantes.CREAR;

//        this.cronograma.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.cronograma.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.cronograma != null) {
            this.operacion = Constantes.ACTUALIZAR;
            cronograma.setPractica(cronograma.getPractica());
            cronograma.setSeqPrograma(cronograma.getSeqPrograma());
            cronograma.setSeqEntidad(cronograma.getSeqEntidad());
            cronograma.setProcesoPrograma(cronograma.getProcesoPrograma());
            cronograma.setObjetivoEntidad(cronograma.getObjetivoEntidad());
            cronograma.setObjetivoFormativo(cronograma.getObjetivoFormativo());
            cronograma.setActividades(cronograma.getActividades());
            cronograma.setRecursos(cronograma.getRecursos());
            cronograma.setObservaciones(cronograma.getObservaciones());
            cronograma.setDatCreacion(cronograma.getDatCreacion());
            cronograma.setDatModificacion(cronograma.getDatModificacion());
            cronograma.setUsrCreacion(cronograma.getUsrCreacion());
//            cronograma.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearCronograma(cronograma);
        } else {
            this.servicios.actualizarCronograma(cronograma);
        }
        this.lista = true;
        this.crear = false;
        this.Listcronograma = servicios.consultarCronograma();
    }

    public void onBorrar() {
        if (this.cronograma != null) {
            servicios.borrarCronograma(cronograma);
            this.Listcronograma = servicios.consultarCronograma();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
