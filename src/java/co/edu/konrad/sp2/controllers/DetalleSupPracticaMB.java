package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.DetalleSupPractica;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosDetalleSupPractica;
import co.edu.konrad.sp2.servicios.ServiciosDetalleSupPracticaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "detalleSupPracticaMB")
@ViewScoped
public class DetalleSupPracticaMB implements Serializable {

    private final static Logger log = Logger.getLogger(DetalleSupPracticaMB.class);
    private List<DetalleSupPractica> ListdetalleSupPractica;
    private List<DetalleSupPractica> detalleSupPracticaFiltradas;
    private DetalleSupPractica detalleSupPractica;
    private String operacion;
    private ServiciosDetalleSupPractica servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosDetalleSupPracticaImp();
        this.ListdetalleSupPractica = new ArrayList();
        this.detalleSupPractica = new DetalleSupPractica();
        this.ListdetalleSupPractica = servicios.consultarDetalleSupPracticaJoin();

    }

    public List<DetalleSupPractica> getListDetalleSupPractica() {
        return ListdetalleSupPractica;
    }

    public void setListDetalleSupPractica(List<DetalleSupPractica> ListdetalleSupPractica) {
        this.ListdetalleSupPractica = ListdetalleSupPractica;
    }

    public List<DetalleSupPractica> getDetalleSupPracticaFiltradas() {
        return detalleSupPracticaFiltradas;
    }

    public void setDetalleSupPracticaFiltradas(List<DetalleSupPractica> detalleSupPracticaFiltradas) {
        this.detalleSupPracticaFiltradas = detalleSupPracticaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public DetalleSupPractica getDetalleSupPractica() {
        return detalleSupPractica;
    }

    public void setDetalleSupPractica(DetalleSupPractica detalleSupPractica) {
        this.detalleSupPractica = detalleSupPractica;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.detalleSupPractica = new DetalleSupPractica();
        this.operacion = Constantes.CREAR;

//        this.detalleSupPractica.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.detalleSupPractica.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.detalleSupPractica != null) {
            this.operacion = Constantes.ACTUALIZAR;
            detalleSupPractica.setPersona(detalleSupPractica.getPersona());
            detalleSupPractica.setAreaEscolaris(detalleSupPractica.getAreaEscolaris());
            detalleSupPractica.setEstadoPractica(detalleSupPractica.getEstadoPractica());
            detalleSupPractica.setBloqueado(detalleSupPractica.getBloqueado());
            detalleSupPractica.setDatVigenciaDesde(detalleSupPractica.getDatVigenciaDesde());
            detalleSupPractica.setDatVigenciaHasta(detalleSupPractica.getDatVigenciaHasta());
            detalleSupPractica.setDatCreacion(detalleSupPractica.getDatCreacion());
            detalleSupPractica.setDatModificacion(detalleSupPractica.getDatModificacion());
            detalleSupPractica.setUsrCreacion(detalleSupPractica.getUsrCreacion());
//            detalleSupPractica.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearDetalleSupPractica(detalleSupPractica);
        } else {
            this.servicios.actualizarDetalleSupPractica(detalleSupPractica);
        }
        this.lista = true;
        this.crear = false;
        this.ListdetalleSupPractica = servicios.consultarDetalleSupPractica();
    }

    public void onBorrar() {
        if (this.detalleSupPractica != null) {
            servicios.borrarDetalleSupPractica(detalleSupPractica);
            this.ListdetalleSupPractica = servicios.consultarDetalleSupPractica();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
