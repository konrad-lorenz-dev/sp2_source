package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.CriteriosEvaluacion;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosCriteriosEvaluacion;
import co.edu.konrad.sp2.servicios.ServiciosCriteriosEvaluacionImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "criteriosEvaluacionMB")
@ViewScoped
public class CriteriosEvaluacionMB implements Serializable {

    private final static Logger log = Logger.getLogger(CriteriosEvaluacionMB.class);
    private List<CriteriosEvaluacion> ListcriteriosEvaluacion;
    private List<CriteriosEvaluacion> criteriosEvaluacionFiltradas;
    private CriteriosEvaluacion criteriosEvaluacion;
    private String operacion;
    private ServiciosCriteriosEvaluacion servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosCriteriosEvaluacionImp();
        this.ListcriteriosEvaluacion = new ArrayList();
        this.criteriosEvaluacion = new CriteriosEvaluacion();
        this.ListcriteriosEvaluacion = servicios.consultarCriteriosEvaluacionJoin();

    }

    public List<CriteriosEvaluacion> getListCriteriosEvaluacion() {
        return ListcriteriosEvaluacion;
    }

    public void setListCriteriosEvaluacion(List<CriteriosEvaluacion> ListcriteriosEvaluacion) {
        this.ListcriteriosEvaluacion = ListcriteriosEvaluacion;
    }

    public List<CriteriosEvaluacion> getCriteriosEvaluacionFiltradas() {
        return criteriosEvaluacionFiltradas;
    }

    public void setCriteriosEvaluacionFiltradas(List<CriteriosEvaluacion> criteriosEvaluacionFiltradas) {
        this.criteriosEvaluacionFiltradas = criteriosEvaluacionFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public CriteriosEvaluacion getCriteriosEvaluacion() {
        return criteriosEvaluacion;
    }

    public void setCriteriosEvaluacion(CriteriosEvaluacion criteriosEvaluacion) {
        this.criteriosEvaluacion = criteriosEvaluacion;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.criteriosEvaluacion = new CriteriosEvaluacion();
        this.operacion = Constantes.CREAR;

//        this.criteriosEvaluacion.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.criteriosEvaluacion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.criteriosEvaluacion != null) {
            this.operacion = Constantes.ACTUALIZAR;
            criteriosEvaluacion.setSesionSupervsion(criteriosEvaluacion.getSesionSupervsion());
            criteriosEvaluacion.setIndicadoresEvaluacion(criteriosEvaluacion.getIndicadoresEvaluacion());
            criteriosEvaluacion.setCalificacionIndicador(criteriosEvaluacion.getCalificacionIndicador());
            criteriosEvaluacion.setDescripcion(criteriosEvaluacion.getDescripcion());
            criteriosEvaluacion.setDatCreacion(criteriosEvaluacion.getDatCreacion());
            criteriosEvaluacion.setDatModificacion(criteriosEvaluacion.getDatModificacion());
            criteriosEvaluacion.setUsrCreacion(criteriosEvaluacion.getUsrCreacion());
//            criteriosEvaluacion.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearCriteriosEvaluacion(criteriosEvaluacion);
        } else {
            this.servicios.actualizarCriteriosEvaluacion(criteriosEvaluacion);
        }
        this.lista = true;
        this.crear = false;
        this.ListcriteriosEvaluacion = servicios.consultarCriteriosEvaluacion();
    }

    public void onBorrar() {
        if (this.criteriosEvaluacion != null) {
            servicios.borrarCriteriosEvaluacion(criteriosEvaluacion);
            this.ListcriteriosEvaluacion = servicios.consultarCriteriosEvaluacion();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
