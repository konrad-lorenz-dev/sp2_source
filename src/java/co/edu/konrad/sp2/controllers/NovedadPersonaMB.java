package co.edu.konrad.sp2.controllers;

import co.edu.konrad.sp2.bean.DetallePracticante;
import co.edu.konrad.sp2.bean.Novedad;
//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.NovedadPersona;
import co.edu.konrad.sp2.bean.RolPersona;
import co.edu.konrad.sp2.constant.Constantes;
import static co.edu.konrad.sp2.constant.Constantes.TIPO_NOVEDAD_RETIRO;
import static co.edu.konrad.sp2.constant.Constantes.TIPO_NOVEDAD_RETROALIMENTACION;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticante;
import co.edu.konrad.sp2.servicios.ServiciosDetallePracticanteImp;
import co.edu.konrad.sp2.servicios.ServiciosNovedad;
import co.edu.konrad.sp2.servicios.ServiciosNovedadImp;
import co.edu.konrad.sp2.servicios.ServiciosNovedadPersona;
import co.edu.konrad.sp2.servicios.ServiciosNovedadPersonaImp;
import co.edu.konrad.sp2.servicios.ServiciosRolPersona;
import co.edu.konrad.sp2.servicios.ServiciosRolPersonaImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "novedadPersonaMB")
@ViewScoped
public class NovedadPersonaMB implements Serializable {

    private final static Logger log = Logger.getLogger(NovedadPersonaMB.class);
    private List<NovedadPersona> ListnovedadPersona;
    private List<Novedad> ListNovedad;
    private List<Novedad> ListNovedadesGenerales;
    private List<NovedadPersona> novedadPersonaFiltradas;
    private NovedadPersona novedadPersona;
    private Novedad novedad;
    private String operacion;
    private ServiciosNovedadPersona servicios;
    private ServiciosRolPersona servicioRolPersona;
    boolean crear;
    boolean lista;
    private int NumeroTipoNovedad;

    /*
    Ingresar Comentario
     */
    private String observacionesAdicionales;
    private ServiciosNovedad serviciosNovedad;

    /*
    Retiro estudiante
     */
    private ServiciosDetallePracticante serviciosPersonaPracticante;
    private DetallePracticante detallePracticante;
    private List<DetallePracticante> ListdetallePracticante;
    private boolean btnConfrimacionRetiro = false;
    /*
    Actualizar estado Practicante
     */
    private ServiciosDetallePracticante serviciosDetallePracticante;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicioRolPersona = new ServiciosRolPersonaImp();
        this.servicios = new ServiciosNovedadPersonaImp();
        this.serviciosNovedad = new ServiciosNovedadImp();
        this.serviciosDetallePracticante = new ServiciosDetallePracticanteImp();
        this.ListnovedadPersona = new ArrayList();
        this.novedadPersona = new NovedadPersona();
        this.novedad = new Novedad();
        this.setObservacionesAdicionales("");
        this.ListnovedadPersona = servicios.consultarNovedadPersonaJoin();
        this.detallePracticante = new DetallePracticante();
        this.serviciosPersonaPracticante = new ServiciosDetallePracticanteImp();
        this.ListNovedadesGenerales = new ArrayList();
//        this.ListNovedadesGenerales = serviciosNovedad.consultarNovedadesPorPersonaJoin(AsistenteSesion.getInstance().getPersona().getSeqPersona());
        this.ListdetallePracticante = new ArrayList();
        this.ListdetallePracticante = this.serviciosDetallePracticante.consultarDetallePracticanteEstadoPracticanteJoin(Constantes.ASIGANDOS);

    }

    public List<NovedadPersona> getListNovedadPersona() {
        return ListnovedadPersona;
    }

    public void setListNovedadPersona(List<NovedadPersona> ListnovedadPersona) {
        this.ListnovedadPersona = ListnovedadPersona;
    }

    public List<NovedadPersona> getNovedadPersonaFiltradas() {
        return novedadPersonaFiltradas;
    }

    public void setNovedadPersonaFiltradas(List<NovedadPersona> novedadPersonaFiltradas) {
        this.novedadPersonaFiltradas = novedadPersonaFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public NovedadPersona getNovedadPersona() {
        return novedadPersona;
    }

    public void setNovedadPersona(NovedadPersona novedadPersona) {
        this.novedadPersona = novedadPersona;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public String getObservacionesAdicionales() {
        return observacionesAdicionales;
    }

    public void setObservacionesAdicionales(String observacionesAdicionales) {
        this.observacionesAdicionales = observacionesAdicionales;
    }

    public List<Novedad> getListNovedad() {
        return ListNovedad;
    }

    public void setListNovedad(List<Novedad> ListNovedad) {
        this.ListNovedad = ListNovedad;
    }

    public List<Novedad> getListNovedadesGenerales() {
        return ListNovedadesGenerales;
    }

    public void setListNovedadesGenerales(List<Novedad> ListNovedadesGenerales) {
        this.ListNovedadesGenerales = ListNovedadesGenerales;
    }

    public List<DetallePracticante> getListdetallePracticante() {
        return ListdetallePracticante;
    }

    public void setListdetallePracticante(List<DetallePracticante> ListdetallePracticante) {
        this.ListdetallePracticante = ListdetallePracticante;
    }

    public DetallePracticante getDetallePracticante() {
        return detallePracticante;
    }

    public void setDetallePracticante(DetallePracticante detallePracticante) {
        this.detallePracticante = detallePracticante;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public boolean isBtnConfrimacionRetiro() {
        return btnConfrimacionRetiro;
    }

    public void setBtnConfrimacionRetiro(boolean btnConfrimacionRetiro) {
        this.btnConfrimacionRetiro = btnConfrimacionRetiro;
    }

    public int getNumeroTipoNovedad() {
        return NumeroTipoNovedad;
    }

    public void setNumeroTipoNovedad(int NumeroTipoNovedad) {
        this.NumeroTipoNovedad = NumeroTipoNovedad;
    }

    
    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.novedadPersona = new NovedadPersona();
        this.operacion = Constantes.CREAR;

//        this.novedadPersona.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.novedadPersona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.novedadPersona != null) {
            this.operacion = Constantes.ACTUALIZAR;
            novedadPersona.setRolPersona(novedadPersona.getRolPersona());
            novedadPersona.setNovedad(novedadPersona.getNovedad());
            novedadPersona.setDescripcion(novedadPersona.getDescripcion());
            novedadPersona.setDatCreacion(novedadPersona.getDatCreacion());
            novedadPersona.setDatModificacion(novedadPersona.getDatModificacion());
            novedadPersona.setUsrCreacion(novedadPersona.getUsrCreacion());
//            novedadPersona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearNovedadPersona(novedadPersona);
        } else {
            this.servicios.actualizarNovedadPersona(novedadPersona);
        }
        this.lista = true;
        this.crear = false;
        this.ListnovedadPersona = servicios.consultarNovedadPersona();
    }

    public void onBorrar() {
        if (this.novedadPersona != null) {
            servicios.borrarNovedadPersona(novedadPersona);
            this.ListnovedadPersona = servicios.consultarNovedadPersona();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));
        }
    }

    /*
     Funcion donde guardar Informacion Retroalimentación.
     */
//    public void GuardarRetroalimentacionProceso(Long seqPersonaPracticante) {
//        String constantes = "";
//        try {
//            this.novedadPersona.setRolPersona(new RolPersona());
//            if (!("".equals(this.novedadPersona.getNovedad()) || "".equals(this.novedadPersona.getDescripcion()))) {
//                int seqRolPersona = this.servicioRolPersona.buscarPersonaid(seqPersonaPracticante);
//                if (!(seqRolPersona == 0)) {
//                    this.novedadPersona.getRolPersona().setSeqRolPersona(Long.valueOf(seqRolPersona));
//                    this.novedadPersona.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//                    this.novedadPersona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
//                    this.servicios.crearNovedadPersona(novedadPersona);
//                    int seqNovedadPersona = this.servicios.consultarNovedadPersonaId();
//                    if(NumeroTipoNovedad == 0){
//                       this.novedad = new Novedad(TIPO_NOVEDAD_RETROALIMENTACION, AsistenteSesion.getInstance().getPersona(), Long.valueOf(seqNovedadPersona), this.getObservacionesAdicionales(), AsistenteSesion.getInstance().getUsuarioSesion(), AsistenteSesion.getInstance().getUsuarioSesion());   
//                    } else {
//                       this.novedad = new Novedad(Long.valueOf(NumeroTipoNovedad), AsistenteSesion.getInstance().getPersona(), Long.valueOf(seqNovedadPersona), this.getObservacionesAdicionales(), AsistenteSesion.getInstance().getUsuarioSesion(), AsistenteSesion.getInstance().getUsuarioSesion());
//                    }
//                    
//                    constantes = this.serviciosNovedad.crearNovedad(novedad);
//                    this.setObservacionesAdicionales("");
//                    if (constantes.equalsIgnoreCase(Constantes.OK) && NumeroTipoNovedad == 0) {
//                        this.ListNovedad.clear();
//                        this.ListNovedadesGenerales.clear();
//                        this.ListNovedadesGenerales = serviciosNovedad.consultarNovedadesPorPersonaJoin(AsistenteSesion.getInstance().getPersona().getSeqPersona());
//                        CrearListaNovedad(seqPersonaPracticante);
//                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Actualizado"));
//                    } else if(constantes.equalsIgnoreCase(Constantes.OK) && NumeroTipoNovedad != 0){
//                        this.novedadPersona = new NovedadPersona();
//                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Actualizado"));
//                    }
//                    NumeroTipoNovedad = 0;
//                    System.out.println(this.novedadPersona.getNovedad() + " " + this.novedadPersona.getDescripcion() + " " + this.getObservacionesAdicionales() + " " + this.novedadPersona.getRolPersona().getSeqRolPersona());
//                } else {
//                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "No se puede guardar la información"));
//                }
//            } else {
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "", "Complete las Observaciones"));
//            }
//        } catch (Exception e) {
//            if (constantes.equalsIgnoreCase(Constantes.FAILED)) {
//                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", e.getMessage()));
//            }
//        }
//
//    }

    /*
    GrillaNovedad Retoralimentacion
     */
    public void CrearListaNovedad(Long logPersonaPostualante) {
//        this.novedadPersona = new NovedadPersona(logPersonaPostualante, AsistenteSesion.getInstance().getPersona().getSeqPersona());
        ListNovedad = serviciosNovedad.consultarNovedadGrillaPostulados(this.novedadPersona);
    }

    /*
    Guardar Retiro persona
     */
    public void GuardarRetiroPersona(Long idDetallePracticante, boolean guardar) {
//        String constantes = "";
//        PrimeFaces current = PrimeFaces.current();
//        if (guardar == false && idDetallePracticante == -1) {
//            if (!("".equals(this.novedadPersona.getNovedad()) || "".equals(this.novedadPersona.getDescripcion()))) {
//                current.executeScript("PF('ConfirmacionRetiro').show();PF('RetiroEstDialog').hide()");
//            } else {
//                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Falta Información", " Falta Completar todos los campos");
//                FacesContext.getCurrentInstance().addMessage(null, message);
//            }
//        }
//        try {
//            if (guardar == true && idDetallePracticante == null) {
//                if (!("".equals(this.novedadPersona.getNovedad()) || "".equals(this.novedadPersona.getDescripcion()))) {
//                    int seqRolPersona = this.servicioRolPersona.buscarPersonaid(this.detallePracticante.getPersona().getSeqPersona());
//                    if (!(seqRolPersona == 0)) {
//                        System.out.println("LLave del rol " + seqRolPersona);
//                        this.novedadPersona.getRolPersona().setSeqRolPersona(Long.valueOf(seqRolPersona));
//                        this.novedadPersona.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//                        this.novedadPersona.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());
//                        this.servicios.crearNovedadPersona(novedadPersona);
//                        int seqNovedadPersona = this.servicios.consultarNovedadPersonaId();
//                        this.novedad = new Novedad(TIPO_NOVEDAD_RETIRO, AsistenteSesion.getInstance().getPersona(), Long.valueOf(seqNovedadPersona), this.getObservacionesAdicionales(), AsistenteSesion.getInstance().getUsuarioSesion(), AsistenteSesion.getInstance().getUsuarioSesion());
//                        constantes = this.serviciosNovedad.crearNovedad(novedad);
//                        this.setObservacionesAdicionales("");
//                        if (constantes.equalsIgnoreCase(Constantes.OK)) {
//                            this.detallePracticante.setEstadoPractica("RET");
//                            this.novedadPersona = new NovedadPersona();
//                            this.serviciosDetallePracticante.actualizarDetallePracticanteEstadoPractica(detallePracticante);
//                            this.ListNovedadesGenerales.clear();
//                            this.ListNovedadesGenerales = serviciosNovedad.consultarNovedadesPorPersonaJoin(AsistenteSesion.getInstance().getPersona().getSeqPersona());
//                            this.ListdetallePracticante.clear();
//                            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Retiro estudiante " + this.detallePracticante.getPersona().getNombres() + " " + this.detallePracticante.getPersona().getApellidos(), "Éxito");
//                            FacesContext.getCurrentInstance().addMessage(null, message);
//                            init();
//                        }
//                    }
//                }
//            } else if (guardar == false && idDetallePracticante != null && idDetallePracticante != -1) {
//                this.novedadPersona.setDescripcion("");
//                this.novedadPersona.setNovedad("");
//                this.setObservacionesAdicionales("");
//                this.detallePracticante = this.serviciosPersonaPracticante.consultarDetallePracticanteJoinById(idDetallePracticante);
//                this.novedadPersona.setRolPersona(new RolPersona());
//                current.executeScript("PF('RetiroEstDialog').show()");
//                System.out.println("SOy Retiro Estudiante " + this.detallePracticante.getSeqDetallePracticante());
//            }
//        } catch (Exception e) {
//
//        }

    }

}
