package co.edu.konrad.sp2.controllers;

//import co.edu.konrad.sp2.helper.AsistenteSesion;
import co.edu.konrad.sp2.bean.SeguimientoEntidad;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.servicios.ServiciosSeguimientoEntidad;
import co.edu.konrad.sp2.servicios.ServiciosSeguimientoEntidadImp;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

@ManagedBean(name = "seguimientoEntidadMB")
@ViewScoped
public class SeguimientoEntidadMB implements Serializable {

    private final static Logger log = Logger.getLogger(SeguimientoEntidadMB.class);
    private List<SeguimientoEntidad> ListseguimientoEntidad;
    private List<SeguimientoEntidad> seguimientoEntidadFiltradas;
    private SeguimientoEntidad seguimientoEntidad;
    private String operacion;
    private ServiciosSeguimientoEntidad servicios;
    boolean crear;
    boolean lista;

    @PostConstruct
    public void init() {
        this.lista = true;
        this.crear = false;
        this.servicios = new ServiciosSeguimientoEntidadImp();
        this.ListseguimientoEntidad = new ArrayList();
        this.seguimientoEntidad = new SeguimientoEntidad();
        this.ListseguimientoEntidad = servicios.consultarSeguimientoEntidadJoin();

    }

    public List<SeguimientoEntidad> getListSeguimientoEntidad() {
        return ListseguimientoEntidad;
    }

    public void setListSeguimientoEntidad(List<SeguimientoEntidad> ListseguimientoEntidad) {
        this.ListseguimientoEntidad = ListseguimientoEntidad;
    }

    public List<SeguimientoEntidad> getSeguimientoEntidadFiltradas() {
        return seguimientoEntidadFiltradas;
    }

    public void setSeguimientoEntidadFiltradas(List<SeguimientoEntidad> seguimientoEntidadFiltradas) {
        this.seguimientoEntidadFiltradas = seguimientoEntidadFiltradas;
    }

    public String getOperacion() {
        return operacion;
    }

    public void setOperacion(String operacion) {
        this.operacion = operacion;
    }

    public SeguimientoEntidad getSeguimientoEntidad() {
        return seguimientoEntidad;
    }

    public void setSeguimientoEntidad(SeguimientoEntidad seguimientoEntidad) {
        this.seguimientoEntidad = seguimientoEntidad;
    }

    public boolean isLista() {
        return lista;
    }

    public void setLista(boolean lista) {
        this.lista = lista;
    }

    public boolean isCrear() {
        return crear;
    }

    public void setCrear(boolean crear) {
        this.crear = crear;
    }

    public void onCancelar() {
        this.lista = true;
        this.crear = false;
    }

    public void onCrearRegistro() {
        this.lista = false;
        this.crear = true;
        this.seguimientoEntidad = new SeguimientoEntidad();
        this.operacion = Constantes.CREAR;

//        this.seguimientoEntidad.setUsrCreacion(AsistenteSesion.getInstance().getUsuarioSesion());
//        this.seguimientoEntidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

    }

    public void onActulizarRegistro() {

        if (this.seguimientoEntidad != null) {
            this.operacion = Constantes.ACTUALIZAR;
            seguimientoEntidad.setPractica(seguimientoEntidad.getPractica());
            seguimientoEntidad.setAsistencia(seguimientoEntidad.getAsistencia());
            seguimientoEntidad.setActividad(seguimientoEntidad.getActividad());
            seguimientoEntidad.setCalificacion(seguimientoEntidad.getCalificacion());
            seguimientoEntidad.setDescripcion(seguimientoEntidad.getDescripcion());
            seguimientoEntidad.setDatRegistro(seguimientoEntidad.getDatRegistro());
            seguimientoEntidad.setDatCreacion(seguimientoEntidad.getDatCreacion());
            seguimientoEntidad.setDatModificacion(seguimientoEntidad.getDatModificacion());
            seguimientoEntidad.setUsrCreacion(seguimientoEntidad.getUsrCreacion());
//            seguimientoEntidad.setUsrModificacion(AsistenteSesion.getInstance().getUsuarioSesion());

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a actualizar."));
        }
    }

    public void onGuardarRegistro() {
        // this.operacion = Constantes.CREAR;
        if (this.operacion.equalsIgnoreCase(Constantes.CREAR)) {
            this.servicios.crearSeguimientoEntidad(seguimientoEntidad);
        } else {
            this.servicios.actualizarSeguimientoEntidad(seguimientoEntidad);
        }
        this.lista = true;
        this.crear = false;
        this.ListseguimientoEntidad = servicios.consultarSeguimientoEntidad();
    }

    public void onBorrar() {
        if (this.seguimientoEntidad != null) {
            servicios.borrarSeguimientoEntidad(seguimientoEntidad);
            this.ListseguimientoEntidad = servicios.consultarSeguimientoEntidad();

        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Debe seleccionar el registro a borrar."));

        }
    }

}
