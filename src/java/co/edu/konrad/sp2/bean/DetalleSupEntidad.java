package co.edu.konrad.sp2.bean;

import java.util.Date;

public class DetalleSupEntidad {

    private Long seqSupEntidad;
    private Persona persona;
    private Entidad entidad;
    private Date datVigenciaDesde;
    private Date datVigenciaHasta;
    private String estado;
    private String descripcion;
    private String observacion;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqSupEntidad() {
        return seqSupEntidad;
    }

    public void setSeqSupEntidad(Long seqSupEntidad) {
        this.seqSupEntidad = seqSupEntidad;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Entidad getEntidad() {
        return entidad;
    }

    public void setEntidad(Entidad entidad) {
        this.entidad = entidad;
    }

    public Date getDatVigenciaDesde() {
        return datVigenciaDesde;
    }

    public void setDatVigenciaDesde(Date datVigenciaDesde) {
        this.datVigenciaDesde = datVigenciaDesde;
    }

    public Date getDatVigenciaHasta() {
        return datVigenciaHasta;
    }

    public void setDatVigenciaHasta(Date datVigenciaHasta) {
        this.datVigenciaHasta = datVigenciaHasta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
