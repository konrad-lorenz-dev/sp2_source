package co.edu.konrad.sp2.bean;

import java.util.Date;

public class NovedadPractConvenio {

    private Long seqNovedadPractConvenio;
    private PracticanteConvenio practicanteConvenio;
    private Date datNovedad;
    private String novedad;
    private String observacion;
    private String estado;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqNovedadPractConvenio() {
        return seqNovedadPractConvenio;
    }

    public void setSeqNovedadPractConvenio(Long seqNovedadPractConvenio) {
        this.seqNovedadPractConvenio = seqNovedadPractConvenio;
    }

    public PracticanteConvenio getPracticanteConvenio() {
        return practicanteConvenio;
    }

    public void setPracticanteConvenio(PracticanteConvenio practicanteConvenio) {
        this.practicanteConvenio = practicanteConvenio;
    }

    public Date getDatNovedad() {
        return datNovedad;
    }

    public void setDatNovedad(Date datNovedad) {
        this.datNovedad = datNovedad;
    }

    public String getNovedad() {
        return novedad;
    }

    public void setNovedad(String novedad) {
        this.novedad = novedad;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
