/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.konrad.sp2.bean;

import java.util.Date;

/**
 *
 * @author cesard.chacond
 */
public class TipoPractica {
    private Long seqTipoPractica;
    private String namTipoPractica;
    private String codigo;
    private String descripcion;
    private Date datCreacion;
    private Date datModificacion;

    public Long getSeqTipoPractica() {
        return seqTipoPractica;
    }

    public void setSeqTipoPractica(Long seqTipoPractica) {
        this.seqTipoPractica = seqTipoPractica;
    }

    public String getNamTipoPractica() {
        return namTipoPractica;
    }

    public void setNamTipoPractica(String namTipoPractica) {
        this.namTipoPractica = namTipoPractica;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }
    
    
}
