package co.edu.konrad.sp2.bean;

import java.util.Date;

public class DetalleCronograma {

    private Long seqDetalleCronograma;
    private ActividadCronograma actividadCronograma;
    private Date datInicio;
    private Date datTerminacion;
    private String entregable;
    private String cumplimiento;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqDetalleCronograma() {
        return seqDetalleCronograma;
    }

    public void setSeqDetalleCronograma(Long seqDetalleCronograma) {
        this.seqDetalleCronograma = seqDetalleCronograma;
    }

    public ActividadCronograma getActividadCronograma() {
        return actividadCronograma;
    }

    public void setActividadCronograma(ActividadCronograma actividadCronograma) {
        this.actividadCronograma = actividadCronograma;
    }

    public Date getDatInicio() {
        return datInicio;
    }

    public void setDatInicio(Date datInicio) {
        this.datInicio = datInicio;
    }

    public Date getDatTerminacion() {
        return datTerminacion;
    }

    public void setDatTerminacion(Date datTerminacion) {
        this.datTerminacion = datTerminacion;
    }

    public String getEntregable() {
        return entregable;
    }

    public void setEntregable(String entregable) {
        this.entregable = entregable;
    }

    public String getCumplimiento() {
        return cumplimiento;
    }

    public void setCumplimiento(String cumplimiento) {
        this.cumplimiento = cumplimiento;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
