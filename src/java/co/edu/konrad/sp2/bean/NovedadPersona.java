package co.edu.konrad.sp2.bean;

import java.util.Date;

public class NovedadPersona {

    private Long seqNovedadPersona;
    private RolPersona rolPersona;
    private String novedad;
    private String descripcion;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    /*
    Parametros consultar id
     */
    private Long idPersonaPracticante;
    private Long idPersonaEntidad;

    public NovedadPersona(Long idPersonaPracticante, Long idPersonaEntidad) {
        this.idPersonaPracticante = idPersonaPracticante;
        this.idPersonaEntidad = idPersonaEntidad;
    }
    
    public NovedadPersona() {
        this.rolPersona = new RolPersona();
    }

    public Long getSeqNovedadPersona() {
        return seqNovedadPersona;
    }

    public void setSeqNovedadPersona(Long seqNovedadPersona) {
        this.seqNovedadPersona = seqNovedadPersona;
    }

    public RolPersona getRolPersona() {
        return rolPersona;
    }

    public void setRolPersona(RolPersona rolPersona) {
        this.rolPersona = rolPersona;
    }

    public String getNovedad() {
        return novedad;
    }

    public void setNovedad(String novedad) {
        this.novedad = novedad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

    public Long getIdPersonaPracticante() {
        return idPersonaPracticante;
    }

    public void setIdPersonaPracticante(Long idPersonaPracticante) {
        this.idPersonaPracticante = idPersonaPracticante;
    }

    public Long getIdPersonaEntidad() {
        return idPersonaEntidad;
    }

    public void setIdPersonaEntidad(Long idPersonaEntidad) {
        this.idPersonaEntidad = idPersonaEntidad;
    }

}
