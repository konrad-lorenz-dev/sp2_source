package co.edu.konrad.sp2.bean;

import java.util.Date;

public class TipoEvento {

    private Long seqTipoEvento;
    private String codigo;
    private String nombre;
    private String desTipoEvento;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqTipoEvento() {
        return seqTipoEvento;
    }

    public void setSeqTipoEvento(Long seqTipoEvento) {
        this.seqTipoEvento = seqTipoEvento;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDesTipoEvento() {
        return desTipoEvento;
    }

    public void setDesTipoEvento(String desTipoEvento) {
        this.desTipoEvento = desTipoEvento;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
