package co.edu.konrad.sp2.bean;

import java.util.Date;

public class PracticanteConvenio {

    private Long seqPracticanteConvenio;
    private DetalleSupEntidad detalleSupEntidad;
    private DetallePracticante detallePracticante;
    private Practica practica;
    private Convenio convenio;
    private AreaEscolaris areaEscolaris;
    private Cronograma seqCronograma;
    private OfertaLaboral seqOfertaLaboral;
    private String estadoAprobado;
    private String estadoConvenio;
    private String estadoPracticante;
    private String aprobado;
    private String observacion;
    private Date datVigPeriodoDesde;
    private Date datVigPeriodoHasta;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    private String constante;

    public PracticanteConvenio() {
    }

    public PracticanteConvenio(PracticanteConvenio consultarPracticantePorSeqPracticante) {
        this.detallePracticante = consultarPracticantePorSeqPracticante.getDetallePracticante();
        this.constante = consultarPracticantePorSeqPracticante.getConstante();
    }

    
    public Long getSeqPracticanteConvenio() {
        return seqPracticanteConvenio;
    }

    public void setSeqPracticanteConvenio(Long seqPracticanteConvenio) {
        this.seqPracticanteConvenio = seqPracticanteConvenio;
    }

    public DetalleSupEntidad getDetalleSupEntidad() {
        return detalleSupEntidad;
    }

    public void setDetalleSupEntidad(DetalleSupEntidad detalleSupEntidad) {
        this.detalleSupEntidad = detalleSupEntidad;
    }

    public DetallePracticante getDetallePracticante() {
        return detallePracticante;
    }

    public void setDetallePracticante(DetallePracticante detallePracticante) {
        this.detallePracticante = detallePracticante;
    }

    public Convenio getConvenio() {
        return convenio;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }

    public AreaEscolaris getAreaEscolaris() {
        return areaEscolaris;
    }

    public void setAreaEscolaris(AreaEscolaris areaEscolaris) {
        this.areaEscolaris = areaEscolaris;
    }

    public Cronograma getSeqCronograma() {
        return seqCronograma;
    }

    public void setSeqCronograma(Cronograma seqCronograma) {
        this.seqCronograma = seqCronograma;
    }

    public OfertaLaboral getSeqOfertaLaboral() {
        return seqOfertaLaboral;
    }

    public void setSeqOfertaLaboral(OfertaLaboral seqOfertaLaboral) {
        this.seqOfertaLaboral = seqOfertaLaboral;
    }

    public String getEstadoAprobado() {
        return estadoAprobado;
    }

    public void setEstadoAprobado(String estadoAprobado) {
        this.estadoAprobado = estadoAprobado;
    }

    public String getEstadoConvenio() {
        return estadoConvenio;
    }

    public void setEstadoConvenio(String estadoConvenio) {
        this.estadoConvenio = estadoConvenio;
    }

    public String getEstadoPracticante() {
        return estadoPracticante;
    }

    public void setEstadoPracticante(String estadoPracticante) {
        this.estadoPracticante = estadoPracticante;
    }

    public String getAprobado() {
        return aprobado;
    }

    public void setAprobado(String aprobado) {
        this.aprobado = aprobado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getDatVigPeriodoDesde() {
        return datVigPeriodoDesde;
    }

    public void setDatVigPeriodoDesde(Date datVigPeriodoDesde) {
        this.datVigPeriodoDesde = datVigPeriodoDesde;
    }

    public Date getDatVigPeriodoHasta() {
        return datVigPeriodoHasta;
    }

    public void setDatVigPeriodoHasta(Date datVigPeriodoHasta) {
        this.datVigPeriodoHasta = datVigPeriodoHasta;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

    public String getConstante() {
        return constante;
    }

    public void setConstante(String constante) {
        this.constante = constante;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    
}
