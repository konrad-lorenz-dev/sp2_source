package co.edu.konrad.sp2.bean;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AgendaEvento{

    private Long seqAgendaEvento;
    private Persona persona = new Persona();

    private Persona personaProgramadorEvento = new Persona();
    private Evento evento = new Evento();
    private String observacion;
    private String estadoEvento;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;
    private String constante;
    private Object data;

    /*Identifiacion de id para agregar a tabla*/
    private int personaSesion;
    private int personaInvitada;
    private int eventos;

    public AgendaEvento() {
        this.evento = new Evento();
        this.persona = new Persona();
        this.personaProgramadorEvento = new Persona();
    }

    public AgendaEvento(int evento, int personaProgramadorEvento, int practicante, String observacion, String estadoEvento, String usrCreacion, String usrModificacion) {
        this.eventos = evento;
        this.personaSesion = personaProgramadorEvento;
        this.personaInvitada = practicante;
        this.observacion = observacion;
        this.estadoEvento = estadoEvento;
        this.usrCreacion = usrCreacion;
        this.usrModificacion = usrModificacion;
    }

    public AgendaEvento(Long seqAgendaEvento, String observacion, String estadoEvento, Date datCreacion, Persona personaInvitada, String usrCreacion) {
        this.seqAgendaEvento = seqAgendaEvento;
        this.observacion = observacion;
        this.estadoEvento = estadoEvento;
        this.datCreacion = datCreacion;
        this.persona = personaInvitada;
        this.usrCreacion = usrCreacion;
    }

    public int getPersonaSesion() {
        return personaSesion;
    }

    public void setPersonaSesion(int personaSesion) {
        this.personaSesion = personaSesion;
    }

    public int getPersonaInvitada() {
        return personaInvitada;
    }

    public void setPersonaInvitada(int personaInvitada) {
        this.personaInvitada = personaInvitada;
    }

    public int getEventos() {
        return eventos;
    }

    public void setEventos(int eventos) {
        this.eventos = eventos;
    }

    public String getConstante() {
        return constante;
    }

    public void setConstante(String constante) {
        this.constante = constante;
    }

    public Long getSeqAgendaEvento() {
        return seqAgendaEvento;
    }

    public void setSeqAgendaEvento(Long seqAgendaEvento) {
        this.seqAgendaEvento = seqAgendaEvento;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Persona getPersonaProgramadorEvento() {
        return personaProgramadorEvento;
    }

    public void setPersonaProgramadorEvento(Persona personaProgramadorEvento) {
        this.personaProgramadorEvento = personaProgramadorEvento;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getEstadoEvento() {
//        if ("Activo".equals(estadoEvento)) {
//            return "Act";
//        } else if ("Inactivo".equals(estadoEvento)) {
//            return "Inc";
//        }
        return estadoEvento;
    }

    public void setEstadoEvento(String estadoEvento) {
        this.estadoEvento = estadoEvento;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
