package co.edu.konrad.sp2.bean;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import net.sourceforge.jtds.jdbc.DateTime;

public class RegistroSemanalEntidad {

    private Long seqRegistroSemanal;
    private Practica practica;
    private String descripcion;
    private String estadoValido;
    private Date datRegistro;
    private Date datCreacion;
    private Date datModificacion;
    private String constante;
    protected ZonedDateTime fecha;
    
    @Override
    public String toString() {
        return "RegistroSemanalEntidad{" + "seqRegistroSemanal=" + seqRegistroSemanal + ", practica=" + practica + ", descripcion=" + descripcion + ", estadoValido=" + estadoValido + ", datRegistro=" + datRegistro + ", datCreacion=" + datCreacion + ", datModificacion=" + datModificacion + '}';
    }

    public RegistroSemanalEntidad() {
        practica = new Practica();
    }

    public Long getSeqRegistroSemanal() {
        return seqRegistroSemanal;
    }

    public void setSeqRegistroSemanal(Long seqRegistroSemanal) {
        this.seqRegistroSemanal = seqRegistroSemanal;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstadoValido() {
        return estadoValido;
    }

    public void setEstadoValido(String estadoValido) {
        this.estadoValido = estadoValido;
    }

    public Date getDatRegistro() {
        return datRegistro;
    }

    public void setDatRegistro(Date datRegistro) {
        this.datRegistro = datRegistro;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }
    
    public String getConstante() {
        return constante;
    }

    public void setConstante(String constante) {
        this.constante = constante;
    }

    
}
