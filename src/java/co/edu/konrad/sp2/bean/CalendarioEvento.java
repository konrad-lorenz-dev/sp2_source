/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.konrad.sp2.bean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleRenderingMode;

/**
 *
 * @author cesard.chacond
 */
public class CalendarioEvento implements ScheduleEvent {

    private String id;
    private String title;
    private String ubication;
    private Date startDate = new Date();
    private Date endDate = new Date();
    private String styleClass;
    private Object data;
    private String url;
    private String description;
    private Long claseTipoEvento;
    private boolean allDay;
    private boolean editable;
    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    SimpleDateFormat formatoHora = new SimpleDateFormat(" dd/MM/yyyy HH:mm");

//    agenda evento
    private String codigoEstudiante;
    private String asistio;
    private String observaciones;

    private Evento evento;
    private Persona personaSesion;
    private Persona personaInvitada;
    private TipoEvento tipoevento;

    public CalendarioEvento() {
    }

    public CalendarioEvento(String title, String ubicacion, Date startDate, Date endDate, Long claseTipoEvento, Object data) {
        this.title = title;
        this.ubication = ubicacion;
        this.startDate = startDate;
        this.endDate = endDate;
        this.claseTipoEvento = claseTipoEvento;
        this.data = data;
    }

    public CalendarioEvento(String title, String ubicacion, Date startDate, Date endDate, TipoEvento claseTipoEvento, String codigoEstudiante, String asistio, String observaciones, Object data) {
        this.title = title;
        this.ubication = ubicacion;
        this.startDate = startDate;
        this.endDate = endDate;
        this.tipoevento = claseTipoEvento;
        this.codigoEstudiante = codigoEstudiante;
        this.asistio = asistio;
        this.observaciones = observaciones;
        this.data = data;
    }

    public CalendarioEvento(String title, Date startDate, Date endDate) {
        this.title = title;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    public Persona getPersonaSesion() {
        return personaSesion;
    }

    public void setPersonaSesion(Persona personaSesion) {
        this.personaSesion = personaSesion;
    }

    public Persona getPersonaInvitada() {
        return personaInvitada;
    }

    public void setPersonaInvitada(Persona personaInvitada) {
        this.personaInvitada = personaInvitada;
    }

    public String getCodigoEstudiante() {
        return codigoEstudiante;
    }

    public void setCodigoEstudiante(String codigoEstudiante) {
        this.codigoEstudiante = codigoEstudiante;
    }

    public String getAsistio() {
        return asistio;
    }

    public void setAsistio(String asistio) {
        this.asistio = asistio;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getUbication() {
        return ubication;
    }

    public void setUbication(String ubication) {
        this.ubication = ubication;
    }

    public Long getClaseTipoEvento() {
        return claseTipoEvento;
    }

    public void setClaseTipoEvento(Long claseTipoEvento) {
        this.claseTipoEvento = claseTipoEvento;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Override
    public Date getEndDate() {

        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public boolean isAllDay() {
        return allDay;
    }

    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }

    @Override
    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }

    @Override
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    @Override
    public ScheduleRenderingMode getRenderingMode() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map<String, Object> getDynamicProperties() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
