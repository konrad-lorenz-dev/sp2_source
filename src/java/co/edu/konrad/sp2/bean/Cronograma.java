package co.edu.konrad.sp2.bean;

import java.util.Date;

public class Cronograma {

    private Long seqCronograma;
    private Practica practica;
    private Integer seqPrograma;
    private Integer seqEntidad;
    private String procesoPrograma;
    private String objetivoEntidad;
    private String objetivoFormativo;
    private String actividades;
    private String recursos;
    private String observaciones;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqCronograma() {
        return seqCronograma;
    }

    public void setSeqCronograma(Long seqCronograma) {
        this.seqCronograma = seqCronograma;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public Integer getSeqPrograma() {
        return seqPrograma;
    }

    public void setSeqPrograma(Integer seqPrograma) {
        this.seqPrograma = seqPrograma;
    }

    public Integer getSeqEntidad() {
        return seqEntidad;
    }

    public void setSeqEntidad(Integer seqEntidad) {
        this.seqEntidad = seqEntidad;
    }

    public String getProcesoPrograma() {
        return procesoPrograma;
    }

    public void setProcesoPrograma(String procesoPrograma) {
        this.procesoPrograma = procesoPrograma;
    }

    public String getObjetivoEntidad() {
        return objetivoEntidad;
    }

    public void setObjetivoEntidad(String objetivoEntidad) {
        this.objetivoEntidad = objetivoEntidad;
    }

    public String getObjetivoFormativo() {
        return objetivoFormativo;
    }

    public void setObjetivoFormativo(String objetivoFormativo) {
        this.objetivoFormativo = objetivoFormativo;
    }

    public String getActividades() {
        return actividades;
    }

    public void setActividades(String actividades) {
        this.actividades = actividades;
    }

    public String getRecursos() {
        return recursos;
    }

    public void setRecursos(String recursos) {
        this.recursos = recursos;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
