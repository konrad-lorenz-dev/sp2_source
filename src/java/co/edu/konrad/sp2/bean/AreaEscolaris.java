package co.edu.konrad.sp2.bean;

public class AreaEscolaris {

    private Long seqArea;
    private ProgramaEscolaris programaEscolaris;

    public AreaEscolaris() {
        this.programaEscolaris = new ProgramaEscolaris();
    }

    public Long getSeqArea() {
        return seqArea;
    }

    public void setSeqArea(Long seqArea) {
        this.seqArea = seqArea;
    }

    public ProgramaEscolaris getProgramaEscolaris() {
        return programaEscolaris;
    }

    public void setProgramaEscolaris(ProgramaEscolaris programaEscolaris) {
        this.programaEscolaris = programaEscolaris;
    }

}
