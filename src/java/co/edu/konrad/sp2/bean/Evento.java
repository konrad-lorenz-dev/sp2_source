package co.edu.konrad.sp2.bean;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Evento {

    private Long seqDetalleEvento;
    private TipoEvento tipoEvento = new TipoEvento();
    private String asunto;
    private String ubicacion;
    private Date fechaInicio;
    private Date fechaFin;
    private Date horaInicio;
    private Date horaFin;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;
    private String constantes;
    
    private String fechaInicioEs;
    private String fechaFinEs;
    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    SimpleDateFormat formatoHora = new SimpleDateFormat("dd/MM/yyyy HH:mm");

    public Evento() {
        this.tipoEvento = new TipoEvento();
    }

    public Evento(Long seqDetalleEvento, String asunto, String ubicacion, Date fechaInicio, Date fechaFin, TipoEvento evento, Date datCreacion, String usrCreacion) {
        this.seqDetalleEvento = seqDetalleEvento;
        this.asunto = asunto;
        this.ubicacion = ubicacion;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.tipoEvento = evento;
        this.datCreacion = datCreacion;
        this.usrCreacion = usrCreacion;
    }

    public String getConstantes() {
        return constantes;
    }

    public void setConstantes(String constantes) {
        this.constantes = constantes;
    }

    public Long getSeqDetalleEvento() {
        return seqDetalleEvento;
    }

    public void setSeqDetalleEvento(Long seqDetalleEvento) {
        this.seqDetalleEvento = seqDetalleEvento;
    }

    public TipoEvento getTipoEvento() {
        return tipoEvento;
    }

    public void setTipoEvento(TipoEvento tipoEvento) {
        this.tipoEvento = tipoEvento;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Date getHoraInicio() throws ParseException {
        return this.formatoHora.parse(this.formatoHora.format(horaInicio));
    }

    public void setHoraInicio(Date horaInicio) throws ParseException {
        this.horaInicio = horaInicio;
    }

    public Date getFechaInicio() throws ParseException {
        return this.formatoFecha.parse(this.formatoFecha.format(fechaInicio));
    }

    public void setFechaInicio(Date fechaInicio) throws ParseException {
        this.fechaInicio = this.formatoFecha.parse(this.formatoFecha.format(fechaInicio));
    }

    public Date getFechaFin() throws ParseException {
        return this.formatoFecha.parse(this.formatoFecha.format(fechaFin));
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getHoraFin() throws ParseException {
        return this.formatoHora.parse(this.formatoHora.format(horaFin));
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModifcacion() {
        return datModificacion;
    }

    public void setDatModifcacion(Date datModifcacion) {
        this.datModificacion = datModifcacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

    public String getFechaInicioEs() {
        return this.formatoFecha.format(fechaInicio);
    }

    public void setFechaInicioEs(String fechaInicioEs) {
        this.fechaInicioEs = fechaInicioEs;
    }

    public String getFechaFinEs() {
        return this.formatoFecha.format(fechaFin);
    }

    public void setFechaFinEs(String fechaFinEs) {
        this.fechaFinEs = fechaFinEs;
    }

}
