package co.edu.konrad.sp2.bean;

import java.util.Date;

public class IndicadoresEvaluacion {

    private Long seqIndicadorEvaluacion;
    private CompetenciaIndicador competenciaIndicador;
    private AreaEscolaris areaEscolaris;
    private IndicadorGestion indicadorGestion;
    private String estadoVisible;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqIndicadorEvaluacion() {
        return seqIndicadorEvaluacion;
    }

    public void setSeqIndicadorEvaluacion(Long seqIndicadorEvaluacion) {
        this.seqIndicadorEvaluacion = seqIndicadorEvaluacion;
    }

    public CompetenciaIndicador getCompetenciaIndicador() {
        return competenciaIndicador;
    }

    public void setCompetenciaIndicador(CompetenciaIndicador competenciaIndicador) {
        this.competenciaIndicador = competenciaIndicador;
    }

    public AreaEscolaris getAreaEscolaris() {
        return areaEscolaris;
    }

    public void setAreaEscolaris(AreaEscolaris areaEscolaris) {
        this.areaEscolaris = areaEscolaris;
    }

    public IndicadorGestion getIndicadorGestion() {
        return indicadorGestion;
    }

    public void setIndicadorGestion(IndicadorGestion indicadorGestion) {
        this.indicadorGestion = indicadorGestion;
    }

    public String getEstadoVisible() {
        return estadoVisible;
    }

    public void setEstadoVisible(String estadoVisible) {
        this.estadoVisible = estadoVisible;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
