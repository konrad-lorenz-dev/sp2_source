package co.edu.konrad.sp2.bean;

import java.util.Date;

public class ControlDiarioEstudiante {

    private Long seqControlDiario;
    private Practica practica;
    private Integer sesionControl;
    private Date datControl;
    private Date horaEntrada;
    private Date horaSalida;
    private String actividad;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqControlDiario() {
        return seqControlDiario;
    }

    public void setSeqControlDiario(Long seqControlDiario) {
        this.seqControlDiario = seqControlDiario;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public Integer getSesionControl() {
        return sesionControl;
    }

    public void setSesionControl(Integer sesionControl) {
        this.sesionControl = sesionControl;
    }

    public Date getDatControl() {
        return datControl;
    }

    public void setDatControl(Date datControl) {
        this.datControl = datControl;
    }

    public Date getHoraEntrada() {
        return horaEntrada;
    }

    public void setHoraEntrada(Date horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }

    public String getActividad() {
        return actividad;
    }

    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
