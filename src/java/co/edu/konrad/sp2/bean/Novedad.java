package co.edu.konrad.sp2.bean;

import java.util.Date;

public class Novedad {

    private Long seqNovedad;
    private TipoNovedad tipoNovedad;
    private Persona persona;
    private NovedadPersona novedadPersona;
    private String estado;
    private String observacion;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;
    private String constantes;
    public Novedad() {
    }
    
    public Novedad(Long tipoNovedad, Persona persona, Long novedadPersona, String observacion, String usrCreacion, String usrModificacion) {
        this.tipoNovedad = new TipoNovedad();
        this.novedadPersona = new NovedadPersona();
        this.persona = new Persona();
        this.tipoNovedad.setSeqTipoNovedad(tipoNovedad);
        this.persona = persona;
        this.novedadPersona.setSeqNovedadPersona(novedadPersona);
        this.estado = "nul";
        this.observacion = observacion;
        this.usrModificacion = usrModificacion;
        this.usrCreacion = usrCreacion;
    }
    public Long getSeqNovedad() {
        return seqNovedad;
    }

    public void setSeqNovedad(Long seqNovedad) {
        this.seqNovedad = seqNovedad;
    }

    public TipoNovedad getTipoNovedad() {
        return tipoNovedad;
    }

    public void setTipoNovedad(TipoNovedad tipoNovedad) {
        this.tipoNovedad = tipoNovedad;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public NovedadPersona getNovedadPersona() {
        return novedadPersona;
    }

    public void setNovedadPersona(NovedadPersona novedadPersona) {
        this.novedadPersona = novedadPersona;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

    public String getConstantes() {
        return constantes;
    }

    public void setConstantes(String constantes) {
        this.constantes = constantes;
    }

    
}
