package co.edu.konrad.sp2.bean;

import co.edu.konrad.sp2.constant.Constantes;
import java.util.Date;

public class DetallePracticante {

    private Long seqDetallePracticante;
    private Persona persona;
    private Eps eps;
    private AreaEscolaris areaEscolaris;
    private String semestre;
    private String estadoPractica;
    private String estadoHabilitar;
    private String estadoBloquear;
    private String observaciones;
    private Date datPracticaDesde;
    private Date datPracticaHasta;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;
    private String constantes;  
    private String estadoPersona;

    public DetallePracticante() {
        this.eps = new Eps();
        this.areaEscolaris = new AreaEscolaris();
        this.persona = new Persona();
    }

    public DetallePracticante(DetallePracticante consultarDetallePracticanteById) {
        this.seqDetallePracticante = consultarDetallePracticanteById.seqDetallePracticante;
        this.persona = consultarDetallePracticanteById.persona;
        this.eps = consultarDetallePracticanteById.eps;
        this.areaEscolaris = consultarDetallePracticanteById.areaEscolaris;
        this.semestre = consultarDetallePracticanteById.semestre;
        this.estadoPractica = consultarDetallePracticanteById.estadoPractica;
        this.estadoHabilitar = consultarDetallePracticanteById.estadoHabilitar;
        this.estadoBloquear = consultarDetallePracticanteById.estadoBloquear;
    }

    public DetallePracticante(Persona persona, Eps eps, AreaEscolaris areaEscolaris, String semestre, String estadoPractica, String estadoHabilitar, String estadoBloquear) {

    }

    public Long getSeqDetallePracticante() {
        return seqDetallePracticante;
    }

    public void setSeqDetallePracticante(Long seqDetallePracticante) {
        this.seqDetallePracticante = seqDetallePracticante;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Eps getEps() {
        return eps;
    }

    public void setEps(Eps eps) {
        this.eps = eps;
    }

    public AreaEscolaris getAreaEscolaris() {
        return areaEscolaris;
    }

    public void setAreaEscolaris(AreaEscolaris areaEscolaris) {
        this.areaEscolaris = areaEscolaris;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getEstadoPractica() {
        return this.estadoPractica;        
//        if (null == estadoPractica) {
//            return estadoPractica;
//        } else {
//            switch (estadoPractica) {
//                case Constantes.POSTULADOS:
//                    return "Postulado";
//                case Constantes.ASIGANDOS:
//                    return "Asigando";
//                default:
//                    return estadoPractica;
//            }
//        }
    }

    public void setEstadoPractica(String estadoPractica) {
        this.estadoPractica = estadoPractica;
    }

    public String getEstadoHabilitar() {
        return estadoHabilitar;
    }

    public void setEstadoHabilitar(String estadoHabilitar) {
        this.estadoHabilitar = estadoHabilitar;
    }

    public String getEstadoBloquear() {
        return estadoBloquear;
    }

    public void setEstadoBloquear(String estadoBloquear) {
        this.estadoBloquear = estadoBloquear;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getDatPracticaDesde() {
        return datPracticaDesde;
    }

    public void setDatPracticaDesde(Date datPracticaDesde) {
        this.datPracticaDesde = datPracticaDesde;
    }

    public Date getDatPracticaHasta() {
        return datPracticaHasta;
    }

    public void setDatPracticaHasta(Date datPracticaHasta) {
        this.datPracticaHasta = datPracticaHasta;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }
    public String getConstantes() {
        return constantes;
    }

    public void setConstantes(String constantes) {
        this.constantes = constantes;
    }

    public String getEstadoPersona() {
        return estadoPersona;
    }

    public void setEstadoPersona(String estadoPersona) {
        this.estadoPersona = estadoPersona;
    }

}
