package co.edu.konrad.sp2.bean;

import java.util.Date;

public class DetalleCoorPracticas {

    private Long seqCoordinadorPractica;
    private Persona persona;
    private String oficina;
    private String extensionOficina;
    private String nombreSecretaria;
    private String oficinaSecretaria;
    private String extensionSecretaria;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqCoordinadorPractica() {
        return seqCoordinadorPractica;
    }

    public void setSeqCoordinadorPractica(Long seqCoordinadorPractica) {
        this.seqCoordinadorPractica = seqCoordinadorPractica;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public String getNombreSecretaria() {
        return nombreSecretaria;
    }

    public void setNombreSecretaria(String nombreSecretaria) {
        this.nombreSecretaria = nombreSecretaria;
    }

    public String getOficinaSecretaria() {
        return oficinaSecretaria;
    }

    public void setOficinaSecretaria(String oficinaSecretaria) {
        this.oficinaSecretaria = oficinaSecretaria;
    }

    public String getExtensionSecretaria() {
        return extensionSecretaria;
    }

    public void setExtensionSecretaria(String extensionSecretaria) {
        this.extensionSecretaria = extensionSecretaria;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

    public String getExtensionOficina() {
        return extensionOficina;
    }

    public void setExtensionOficina(String extensionOficina) {
        this.extensionOficina = extensionOficina;
    }

}
