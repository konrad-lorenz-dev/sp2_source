package co.edu.konrad.sp2.bean;

import java.util.Date;

public class SeguimientoQincenal {

    private Long seqSeguimientoQuincenal;
    private Practica practica;
    private Date datRegistro;
    private Date horaRegistro;
    private String tema;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqSeguimientoQuincenal() {
        return seqSeguimientoQuincenal;
    }

    public void setSeqSeguimientoQuincenal(Long seqSeguimientoQuincenal) {
        this.seqSeguimientoQuincenal = seqSeguimientoQuincenal;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public Date getDatRegistro() {
        return datRegistro;
    }

    public void setDatRegistro(Date datRegistro) {
        this.datRegistro = datRegistro;
    }

    public Date getHoraRegistro() {
        return horaRegistro;
    }

    public void setHoraRegistro(Date horaRegistro) {
        this.horaRegistro = horaRegistro;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
