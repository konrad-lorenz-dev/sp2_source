package co.edu.konrad.sp2.bean;

import java.util.Date;

public class EvaluarEntidad {

    private Long seqEvaluarEntidad;
    private DetalleSupPractica detalleSupPractica;
    private PracticanteConvenio practicanteConvenio;
    private Double evaluacion;
    private String observaciones;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqEvaluarEntidad() {
        return seqEvaluarEntidad;
    }

    public void setSeqEvaluarEntidad(Long seqEvaluarEntidad) {
        this.seqEvaluarEntidad = seqEvaluarEntidad;
    }

    public DetalleSupPractica getDetalleSupPractica() {
        return detalleSupPractica;
    }

    public void setDetalleSupPractica(DetalleSupPractica detalleSupPractica) {
        this.detalleSupPractica = detalleSupPractica;
    }

    public PracticanteConvenio getPracticanteConvenio() {
        return practicanteConvenio;
    }

    public void setPracticanteConvenio(PracticanteConvenio practicanteConvenio) {
        this.practicanteConvenio = practicanteConvenio;
    }

    public Double getEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(Double evaluacion) {
        this.evaluacion = evaluacion;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
