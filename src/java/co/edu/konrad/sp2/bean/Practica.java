package co.edu.konrad.sp2.bean;

import java.util.Date;

public class Practica {

    private Long seqPractica;
    private PracticanteConvenio practicanteConvenio;
    private DetalleSupPractica detalleSupPractica;
    private TipoPractica tipoPractica;
    private Date datVigenciaDesde;
    private Date datVigenciaHasta;
    private String estadoPractica;
    private String pazYSalvo;
    private String cartaReferenciacion;
    private Date datCreacion;
    private Date datModificacion;
    private String usrModificacion;
    private String usrCreacion;
    private String constantes;
    
    public Practica() {
        this.practicanteConvenio = new PracticanteConvenio();
        this.detalleSupPractica = new DetalleSupPractica();
        this.tipoPractica = new TipoPractica();
    }
    
    public Practica(Practica consultarPracticaById) {
        this.estadoPractica = consultarPracticaById.estadoPractica;
        this.pazYSalvo = consultarPracticaById.pazYSalvo;
        this.cartaReferenciacion = consultarPracticaById.cartaReferenciacion;
    }

    @Override
    public String toString() {
        return "Practica{" + "seqPractica=" + seqPractica + ", practicanteConvenio=" + practicanteConvenio + ", detalleSupPractica=" + detalleSupPractica + ", datVigenciaDesde=" + datVigenciaDesde + ", datVigenciaHasta=" + datVigenciaHasta + ", estadoPractica=" + estadoPractica + ", pazYSalvo=" + pazYSalvo + ", cartaReferenciacion=" + cartaReferenciacion + ", datCreacion=" + datCreacion + ", datModificacion=" + datModificacion + ", usrModificacion=" + usrModificacion + ", usrCreacion=" + usrCreacion + '}';
    }



    public Long getSeqPractica() {
        return seqPractica;
    }

    public void setSeqPractica(Long seqPractica) {
        this.seqPractica = seqPractica;
    }

    public PracticanteConvenio getPracticanteConvenio() {
        return practicanteConvenio;
    }

    public void setPracticanteConvenio(PracticanteConvenio practicanteConvenio) {
        this.practicanteConvenio = practicanteConvenio;
    }

    public DetalleSupPractica getDetalleSupPractica() {
        return detalleSupPractica;
    }

    public void setDetalleSupPractica(DetalleSupPractica detalleSupPractica) {
        this.detalleSupPractica = detalleSupPractica;
    }

    public TipoPractica getTipoPractica() {
        return tipoPractica;
    }

    public void setTipoPractica(TipoPractica tipoPractica) {
        this.tipoPractica = tipoPractica;
    }

    public Date getDatVigenciaDesde() {
        return datVigenciaDesde;
    }

    public void setDatVigenciaDesde(Date datVigenciaDesde) {
        this.datVigenciaDesde = datVigenciaDesde;
    }

    public Date getDatVigenciaHasta() {
        return datVigenciaHasta;
    }

    public void setDatVigenciaHasta(Date datVigenciaHasta) {
        this.datVigenciaHasta = datVigenciaHasta;
    }

    public String getEstadoPractica() {
        return estadoPractica;
    }

    public void setEstadoPractica(String estadoPractica) {
        this.estadoPractica = estadoPractica;
    }

    public String getPazYSalvo() {
        return pazYSalvo;
    }

    public void setPazYSalvo(String pazYSalvo) {
        this.pazYSalvo = pazYSalvo;
    }

    public String getCartaReferenciacion() {
        return cartaReferenciacion;
    }

    public void setCartaReferenciacion(String cartaReferenciacion) {
        this.cartaReferenciacion = cartaReferenciacion;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getConstantes() {
        return constantes;
    }

    public void setConstantes(String constantes) {
        this.constantes = constantes;
    }

    
}
