package co.edu.konrad.sp2.bean;

import java.util.Date;

public class PersonaSesion {

    private Long seqPersonaSesion;
    private Persona persona;
    private Date datUltimaSesion;
    private String estado;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqPersonaSesion() {
        return seqPersonaSesion;
    }

    public void setSeqPersonaSesion(Long seqPersonaSesion) {
        this.seqPersonaSesion = seqPersonaSesion;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Date getDatUltimaSesion() {
        return datUltimaSesion;
    }

    public void setDatUltimaSesion(Date datUltimaSesion) {
        this.datUltimaSesion = datUltimaSesion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
