package co.edu.konrad.sp2.bean;

import java.util.Date;

public class IndicadorGestion {

    private Long seqIndicadorEstion;
    private Integer codigo;
    private String indicador;
    private String descripcion;
    private String estadoVisible;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqIndicadorEstion() {
        return seqIndicadorEstion;
    }

    public void setSeqIndicadorEstion(Long seqIndicadorEstion) {
        this.seqIndicadorEstion = seqIndicadorEstion;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getIndicador() {
        return indicador;
    }

    public void setIndicador(String indicador) {
        this.indicador = indicador;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstadoVisible() {
        return estadoVisible;
    }

    public void setEstadoVisible(String estadoVisible) {
        this.estadoVisible = estadoVisible;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
