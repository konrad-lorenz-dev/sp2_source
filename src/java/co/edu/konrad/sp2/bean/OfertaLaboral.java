package co.edu.konrad.sp2.bean;

import java.util.Date;

public class OfertaLaboral {

    private Long seqOfertaLaboral;
    private Convenio convenio;
    private String oferta;
    private String descripcion;
    private String observacion;
    private String estado;
    private Date datVigenciaDesde;
    private Date datVigenciaHasta;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqOfertaLaboral() {
        return seqOfertaLaboral;
    }

    public void setSeqOfertaLaboral(Long seqOfertaLaboral) {
        this.seqOfertaLaboral = seqOfertaLaboral;
    }

    public Convenio getConvenio() {
        return convenio;
    }

    public void setConvenio(Convenio convenio) {
        this.convenio = convenio;
    }

    public String getOferta() {
        return oferta;
    }

    public void setOferta(String oferta) {
        this.oferta = oferta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getDatVigenciaDesde() {
        return datVigenciaDesde;
    }

    public void setDatVigenciaDesde(Date datVigenciaDesde) {
        this.datVigenciaDesde = datVigenciaDesde;
    }

    public Date getDatVigenciaHasta() {
        return datVigenciaHasta;
    }

    public void setDatVigenciaHasta(Date datVigenciaHasta) {
        this.datVigenciaHasta = datVigenciaHasta;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
