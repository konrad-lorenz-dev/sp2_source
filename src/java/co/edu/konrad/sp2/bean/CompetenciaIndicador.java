package co.edu.konrad.sp2.bean;

import java.util.Date;

public class CompetenciaIndicador {

    private Long seqCompetenciaIndicador;
    private String codigo;
    private String competencia;
    private String descripcion;
    private String estadoVisible;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqCompetenciaIndicador() {
        return seqCompetenciaIndicador;
    }

    public void setSeqCompetenciaIndicador(Long seqCompetenciaIndicador) {
        this.seqCompetenciaIndicador = seqCompetenciaIndicador;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCompetencia() {
        return competencia;
    }

    public void setCompetencia(String competencia) {
        this.competencia = competencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstadoVisible() {
        return estadoVisible;
    }

    public void setEstadoVisible(String estadoVisible) {
        this.estadoVisible = estadoVisible;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
