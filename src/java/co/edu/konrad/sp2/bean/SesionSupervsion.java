package co.edu.konrad.sp2.bean;

import java.util.Date;

public class SesionSupervsion {

    private Long seqSesionSupervision;
    private Practica practica;
    private Integer numeroSesion;
    private Date datSesion;
    private Date horaInicio;
    private Date horaFinal;
    private String objetivos;
    private String resumen;
    private String compromisos;
    private Double calificacionCompromiso;
    private String observaciones;
    private String asistencia;
    private String puntualidad;
    private String observacionInasistencia;
    private String aspectos;
    private Double calificacionAspectos;
    private String bitacora;
    private String planMejoramiento;
    private Double calificacionSesion;
    private Double promedio40;
    private Double promedio60;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqSesionSupervision() {
        return seqSesionSupervision;
    }

    public void setSeqSesionSupervision(Long seqSesionSupervision) {
        this.seqSesionSupervision = seqSesionSupervision;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public Integer getNumeroSesion() {
        return numeroSesion;
    }

    public void setNumeroSesion(Integer numeroSesion) {
        this.numeroSesion = numeroSesion;
    }

    public Date getDatSesion() {
        return datSesion;
    }

    public void setDatSesion(Date datSesion) {
        this.datSesion = datSesion;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFinal() {
        return horaFinal;
    }

    public void setHoraFinal(Date horaFinal) {
        this.horaFinal = horaFinal;
    }

    public String getObjetivos() {
        return objetivos;
    }

    public void setObjetivos(String objetivos) {
        this.objetivos = objetivos;
    }

    public String getResumen() {
        return resumen;
    }

    public void setResumen(String resumen) {
        this.resumen = resumen;
    }

    public String getCompromisos() {
        return compromisos;
    }

    public void setCompromisos(String compromisos) {
        this.compromisos = compromisos;
    }

    public Double getCalificacionCompromiso() {
        return calificacionCompromiso;
    }

    public void setCalificacionCompromiso(Double calificacionCompromiso) {
        this.calificacionCompromiso = calificacionCompromiso;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(String asistencia) {
        this.asistencia = asistencia;
    }

    public String getPuntualidad() {
        return puntualidad;
    }

    public void setPuntualidad(String puntualidad) {
        this.puntualidad = puntualidad;
    }

    public String getObservacionInasistencia() {
        return observacionInasistencia;
    }

    public void setObservacionInasistencia(String observacionInasistencia) {
        this.observacionInasistencia = observacionInasistencia;
    }

    public String getAspectos() {
        return aspectos;
    }

    public void setAspectos(String aspectos) {
        this.aspectos = aspectos;
    }

    public Double getCalificacionAspectos() {
        return calificacionAspectos;
    }

    public void setCalificacionAspectos(Double calificacionAspectos) {
        this.calificacionAspectos = calificacionAspectos;
    }

    public String getBitacora() {
        return bitacora;
    }

    public void setBitacora(String bitacora) {
        this.bitacora = bitacora;
    }

    public String getPlanMejoramiento() {
        return planMejoramiento;
    }

    public void setPlanMejoramiento(String planMejoramiento) {
        this.planMejoramiento = planMejoramiento;
    }

    public Double getCalificacionSesion() {
        return calificacionSesion;
    }

    public void setCalificacionSesion(Double calificacionSesion) {
        this.calificacionSesion = calificacionSesion;
    }

    public Double getPromedio40() {
        return promedio40;
    }

    public void setPromedio40(Double promedio40) {
        this.promedio40 = promedio40;
    }

    public Double getPromedio60() {
        return promedio60;
    }

    public void setPromedio60(Double promedio60) {
        this.promedio60 = promedio60;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
