package co.edu.konrad.sp2.bean;

import java.util.Date;

public class CriteriosEvaluacion {

    private Long seqDetalleCriterioEvaluacion;
    private SesionSupervsion sesionSupervsion;
    private IndicadoresEvaluacion indicadoresEvaluacion;
    private Double calificacionIndicador;
    private String descripcion;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqDetalleCriterioEvaluacion() {
        return seqDetalleCriterioEvaluacion;
    }

    public void setSeqDetalleCriterioEvaluacion(Long seqDetalleCriterioEvaluacion) {
        this.seqDetalleCriterioEvaluacion = seqDetalleCriterioEvaluacion;
    }

    public SesionSupervsion getSesionSupervsion() {
        return sesionSupervsion;
    }

    public void setSesionSupervsion(SesionSupervsion sesionSupervsion) {
        this.sesionSupervsion = sesionSupervsion;
    }

    public IndicadoresEvaluacion getIndicadoresEvaluacion() {
        return indicadoresEvaluacion;
    }

    public void setIndicadoresEvaluacion(IndicadoresEvaluacion indicadoresEvaluacion) {
        this.indicadoresEvaluacion = indicadoresEvaluacion;
    }

    public Double getCalificacionIndicador() {
        return calificacionIndicador;
    }

    public void setCalificacionIndicador(Double calificacionIndicador) {
        this.calificacionIndicador = calificacionIndicador;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
