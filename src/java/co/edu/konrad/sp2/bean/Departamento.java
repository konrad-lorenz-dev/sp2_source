package co.edu.konrad.sp2.bean;

import java.util.Date;

public class Departamento {

    private Long seqDepartamento;
    private Pais pais;
    private String codigo;
    private String departamento;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqDepartamento() {
        return seqDepartamento;
    }

    public void setSeqDepartamento(Long seqDepartamento) {
        this.seqDepartamento = seqDepartamento;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
