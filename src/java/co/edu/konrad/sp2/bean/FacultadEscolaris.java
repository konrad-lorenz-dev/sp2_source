package co.edu.konrad.sp2.bean;

public class FacultadEscolaris {

    private Long seqFacultad;
    private String nomFacultad;

    public Long getSeqFacultad() {
        return seqFacultad;
    }

    public void setSeqFacultad(Long seqFacultad) {
        this.seqFacultad = seqFacultad;
    }

    public String getNomFacultad() {
        return nomFacultad;
    }

    public void setNomFacultad(String nomFacultad) {
        this.nomFacultad = nomFacultad;
    }

}
