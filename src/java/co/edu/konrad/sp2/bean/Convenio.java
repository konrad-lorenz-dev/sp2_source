package co.edu.konrad.sp2.bean;

import java.util.Date;

public class Convenio {

    private Long seqConvenio;
    private Entidad entidad;
    private String nombre;
    private Date datInicial;
    private Date datFinal;
    private Integer cuposOfrecidos;
    private Integer cuposAsignados;
    private String estadoVisible;
    private String firmado;
    private String observaciones;
    private String periodo;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqConvenio() {
        return seqConvenio;
    }

    public void setSeqConvenio(Long seqConvenio) {
        this.seqConvenio = seqConvenio;
    }

    public Entidad getEntidad() {
        return entidad;
    }

    public void setEntidad(Entidad entidad) {
        this.entidad = entidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getDatInicial() {
        return datInicial;
    }

    public void setDatInicial(Date datInicial) {
        this.datInicial = datInicial;
    }

    public Date getDatFinal() {
        return datFinal;
    }

    public void setDatFinal(Date datFinal) {
        this.datFinal = datFinal;
    }

    public Integer getCuposOfrecidos() {
        return cuposOfrecidos;
    }

    public void setCuposOfrecidos(Integer cuposOfrecidos) {
        this.cuposOfrecidos = cuposOfrecidos;
    }

    public Integer getCuposAsignados() {
        return cuposAsignados;
    }

    public void setCuposAsignados(Integer cuposAsignados) {
        this.cuposAsignados = cuposAsignados;
    }

    public String getEstadoVisible() {
        return estadoVisible;
    }

    public void setEstadoVisible(String estadoVisible) {
        this.estadoVisible = estadoVisible;
    }

    public String getFirmado() {
        return firmado;
    }

    public void setFirmado(String firmado) {
        this.firmado = firmado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
