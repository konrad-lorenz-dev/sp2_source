package co.edu.konrad.sp2.bean;

import java.util.Date;

public class DetalleSupPractica {

    private Long seqSupervisorPractica;
    private Persona persona;
    private AreaEscolaris areaEscolaris;
    private String estadoPractica;
    private String bloqueado;
    private Date datVigenciaDesde;
    private Date datVigenciaHasta;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqSupervisorPractica() {
        return seqSupervisorPractica;
    }

    public void setSeqSupervisorPractica(Long seqSupervisorPractica) {
        this.seqSupervisorPractica = seqSupervisorPractica;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public AreaEscolaris getAreaEscolaris() {
        return areaEscolaris;
    }

    public void setAreaEscolaris(AreaEscolaris areaEscolaris) {
        this.areaEscolaris = areaEscolaris;
    }

    public String getEstadoPractica() {
        return estadoPractica;
    }

    public void setEstadoPractica(String estadoPractica) {
        this.estadoPractica = estadoPractica;
    }

    public String getBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(String bloqueado) {
        this.bloqueado = bloqueado;
    }

    public Date getDatVigenciaDesde() {
        return datVigenciaDesde;
    }

    public void setDatVigenciaDesde(Date datVigenciaDesde) {
        this.datVigenciaDesde = datVigenciaDesde;
    }

    public Date getDatVigenciaHasta() {
        return datVigenciaHasta;
    }

    public void setDatVigenciaHasta(Date datVigenciaHasta) {
        this.datVigenciaHasta = datVigenciaHasta;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
