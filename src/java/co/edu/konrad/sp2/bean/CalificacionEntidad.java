package co.edu.konrad.sp2.bean;

import java.util.Date;

public class CalificacionEntidad {

    private Long seqCalificacionEntidad;
    private Entidad entidad;
    private DetalleSupPractica detalleSupPractica;
    private Double calificacion;
    private String descripcion;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqCalificacionEntidad() {
        return seqCalificacionEntidad;
    }

    public void setSeqCalificacionEntidad(Long seqCalificacionEntidad) {
        this.seqCalificacionEntidad = seqCalificacionEntidad;
    }

    public Entidad getEntidad() {
        return entidad;
    }

    public void setEntidad(Entidad entidad) {
        this.entidad = entidad;
    }

    public DetalleSupPractica getDetalleSupPractica() {
        return detalleSupPractica;
    }

    public void setDetalleSupPractica(DetalleSupPractica detalleSupPractica) {
        this.detalleSupPractica = detalleSupPractica;
    }

    public Double getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(Double calificacion) {
        this.calificacion = calificacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
