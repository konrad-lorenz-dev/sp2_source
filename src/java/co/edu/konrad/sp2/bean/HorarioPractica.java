package co.edu.konrad.sp2.bean;

import java.util.Date;

public class HorarioPractica {

    private Long seqHorarioPractica;
    private Practica practica;
    private String dia;
    private Date horaInicio;
    private Date horaFin;
    private String tipoPractica;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModifcacion;

    public Long getSeqHorarioPractica() {
        return seqHorarioPractica;
    }

    public void setSeqHorarioPractica(Long seqHorarioPractica) {
        this.seqHorarioPractica = seqHorarioPractica;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public Date getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Date horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Date getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Date horaFin) {
        this.horaFin = horaFin;
    }

    public String getTipoPractica() {
        return tipoPractica;
    }

    public void setTipoPractica(String tipoPractica) {
        this.tipoPractica = tipoPractica;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModifcacion() {
        return usrModifcacion;
    }

    public void setUsrModifcacion(String usrModifcacion) {
        this.usrModifcacion = usrModifcacion;
    }

}
