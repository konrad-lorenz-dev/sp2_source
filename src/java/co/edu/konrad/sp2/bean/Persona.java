package co.edu.konrad.sp2.bean;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Persona {

    private Long seqPersona;
    private TipoIdentificacion tipoIdentificacion;
    private String nombres;
    private String apellidos;
    private Ciudad ciudad;
    private String cedula;
    private String lugarExpedicion;
    private String lugarNacimiento;
    private String codigo;
    private String cargo;
    private String correoInstiCorp;
    private String correoAlternativo;
    private String telefono;
    private String celular;
    private String direccion;
    private EstadoPersona estadoPersona;
    private Date datVigenciaDesde;
    private Date datVigenciaHasta;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    private String constante;
    private String strVigenciaDesde;

    /*Escolaris*/
    private String seqPrograma;
    private String seqFacultad;
    private String semestrePracticante;
    private Date fechaNacimiento;
    private String semestre;

    public Persona() {
        this.estadoPersona = new EstadoPersona();
        this.tipoIdentificacion = new TipoIdentificacion();
        this.ciudad = new Ciudad();
    }

    public Long getSeqPersona() {
        return seqPersona;
    }

    public void setSeqPersona(Long seqPersona) {
        this.seqPersona = seqPersona;
    }

    public TipoIdentificacion getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getLugarExpedicion() {
        return lugarExpedicion;
    }

    public void setLugarExpedicion(String lugarExpedicion) {
        this.lugarExpedicion = lugarExpedicion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCorreoInstiCorp() {
        return correoInstiCorp;
    }

    public void setCorreoInstiCorp(String correoInstiCorp) {
        this.correoInstiCorp = correoInstiCorp;
    }

    public String getCorreoAlternativo() {
        return correoAlternativo;
    }

    public void setCorreoAlternativo(String correoAlternativo) {
        this.correoAlternativo = correoAlternativo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public EstadoPersona getEstadoPersona() {
        return estadoPersona;
    }

    public void setEstadoPersona(EstadoPersona estadoPersona) {
        this.estadoPersona = estadoPersona;
    }

    public Date getDatVigenciaDesde() {
        return datVigenciaDesde;
    }

    public void setDatVigenciaDesde(Date datVigenciaDesde) {
        this.datVigenciaDesde = datVigenciaDesde;
    }

    public Date getDatVigenciaHasta() {
        return datVigenciaHasta;
    }

    public void setDatVigenciaHasta(Date datVigenciaHasta) {
        this.datVigenciaHasta = datVigenciaHasta;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

    public String getSemestrePracticante() {
        return semestrePracticante;
    }

    public void setSemestrePracticante(String semestrePracticante) {
        this.semestrePracticante = semestrePracticante;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getSeqPrograma() {
        return seqPrograma;
    }

    public void setSeqPrograma(String seqPrograma) {
        this.seqPrograma = seqPrograma;
    }

    public String getSeqFacultad() {
        return seqFacultad;
    }

    public void setSeqFacultad(String seqFacultad) {
        this.seqFacultad = seqFacultad;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getConstante() {
        return constante;
    }

    public void setConstante(String constante) {
        this.constante = constante;
    }

    public String getStrVigenciaDesde() {
        return strVigenciaDesde;
    }

    public void setStrVigenciaDesde(String strVigenciaDesde) {
        this.strVigenciaDesde = strVigenciaDesde;
    }
    
}
