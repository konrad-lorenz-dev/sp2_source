package co.edu.konrad.sp2.bean;

import java.util.Date;

public class NovedadPractica {

    private Long seqNovedadPractica;
    private Practica practica;
    private Date datNovedad;
    private String novedad;
    private String estado;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;

    public Long getSeqNovedadPractica() {
        return seqNovedadPractica;
    }

    public void setSeqNovedadPractica(Long seqNovedadPractica) {
        this.seqNovedadPractica = seqNovedadPractica;
    }

    public Practica getPractica() {
        return practica;
    }

    public void setPractica(Practica practica) {
        this.practica = practica;
    }

    public Date getDatNovedad() {
        return datNovedad;
    }

    public void setDatNovedad(Date datNovedad) {
        this.datNovedad = datNovedad;
    }

    public String getNovedad() {
        return novedad;
    }

    public void setNovedad(String novedad) {
        this.novedad = novedad;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
