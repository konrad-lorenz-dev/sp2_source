package co.edu.konrad.sp2.bean;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SesionConsejeria{

    private Long seqSesionConsejeria;
    private Persona persona;
    private DetallePracticante detallePracticante;
    private String nombreSesion;
    private TipoEvento tipoConsulta;
    private Date datSesion = new Date();
    private String descripcionSesion;
    private String asistencia;
    private Date datCreacion;
    private Date datModificacion;
    private String usrCreacion;
    private String usrModificacion;
    private String constante;
    SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
    /*Identifiacion de id para agregar a tabla*/
    private int personaSesion;
    private int personaInvitada;
    

    public SesionConsejeria(){
        this.persona = new Persona();
        this.detallePracticante = new DetallePracticante();
        this.tipoConsulta = new TipoEvento();
    }
    public String getConstante() {
        return constante;
    }

    public void setConstante(String constante) {
        this.constante = constante;
    }

    public int getPersonaSesion() {
        return personaSesion;
    }

    public void setPersonaSesion(int personaSesion) {
        this.personaSesion = personaSesion;
    }

    public int getPersonaInvitada() {
        return personaInvitada;
    }

    public void setPersonaInvitada(int personaInvitada) {
        this.personaInvitada = personaInvitada;
    }

    public Long getSeqSesionConsejeria() {
        return seqSesionConsejeria;
    }

    public void setSeqSesionConsejeria(Long seqSesionConsejeria) {
        this.seqSesionConsejeria = seqSesionConsejeria;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public DetallePracticante getDetallePracticante() {
        return detallePracticante;
    }

    public void setDetallePracticante(DetallePracticante detallePracticante) {
        this.detallePracticante = detallePracticante;
    }

    public String getNombreSesion() {
        return nombreSesion;
    }

    public void setNombreSesion(String nombreSesion) {
        this.nombreSesion = nombreSesion;
    }

    public TipoEvento getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(TipoEvento tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }


    public Date getDatSesion() throws ParseException {
        return this.formatoFecha.parse(this.formatoFecha.format(datSesion));
    }

    public void setDatSesion(Date datSesion) throws ParseException {
        this.datSesion = this.formatoFecha.parse(this.formatoFecha.format(datSesion));
    }

    public String getDescripcionSesion() {
        return descripcionSesion;
    }

    public void setDescripcionSesion(String descripcionSesion) {
        this.descripcionSesion = descripcionSesion;
    }

    public String getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(String asistencia) {
        this.asistencia = asistencia;
    }

    public Date getDatCreacion() {
        return datCreacion;
    }

    public void setDatCreacion(Date datCreacion) {
        this.datCreacion = datCreacion;
    }

    public Date getDatModificacion() {
        return datModificacion;
    }

    public void setDatModificacion(Date datModificacion) {
        this.datModificacion = datModificacion;
    }

    public String getUsrCreacion() {
        return usrCreacion;
    }

    public void setUsrCreacion(String usrCreacion) {
        this.usrCreacion = usrCreacion;
    }

    public String getUsrModificacion() {
        return usrModificacion;
    }

    public void setUsrModificacion(String usrModificacion) {
        this.usrModificacion = usrModificacion;
    }

}
