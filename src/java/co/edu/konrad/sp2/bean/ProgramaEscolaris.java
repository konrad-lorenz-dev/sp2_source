package co.edu.konrad.sp2.bean;

public class ProgramaEscolaris {

    private Long seqPrograma;
    private String programa;
    private String tipoPrograma;
    private FacultadEscolaris facultadEscolaris;

    public ProgramaEscolaris() {
        this.facultadEscolaris = new FacultadEscolaris();
    }

    public Long getSeqPrograma() {
        return seqPrograma;
    }

    public void setSeqPrograma(Long seqPrograma) {
        this.seqPrograma = seqPrograma;
    }

    public FacultadEscolaris getFacultadEscolaris() {
        return facultadEscolaris;
    }

    public void setFacultadEscolaris(FacultadEscolaris facultadEscolaris) {
        this.facultadEscolaris = facultadEscolaris;
    }

    public String getPrograma() {
        return programa;
    }

    public void setPrograma(String programa) {
        this.programa = programa;
    }

    public String getTipoPrograma() {
        return tipoPrograma;
    }

    public void setTipoPrograma(String tipoPrograma) {
        this.tipoPrograma = tipoPrograma;
    }

}
