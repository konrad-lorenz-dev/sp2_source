package co.edu.konrad.sp2.framework.manager;

import co.edu.konrad.sp2.framework.manager.ManagerStandard;

import co.edu.konrad.sp2.framework.dao.DaoConcrete;
import java.util.List;

public class ManagerConcrete extends ManagerStandard {

    private static final ManagerConcrete managerConcrete = new ManagerConcrete();

    private ManagerConcrete() {
        super();
    }

    public static ManagerConcrete getManager() {
        return managerConcrete;
    }

    public List obtenerListado(String sqlName, Object objeto) throws Exception {
        DaoConcrete serviciosDao = new DaoConcrete();
        return (List) serviciosDao.obtenerListado(sqlName, objeto);
    }

    public List obtenerListado(String sqlName) throws Exception {
        DaoConcrete serviciosDao = new DaoConcrete();
        return (List) serviciosDao.obtenerListado(sqlName);
    }

    public Object ejecutarProcedimiento(String sqlName) throws Exception {
        DaoConcrete serviciosDao = new DaoConcrete();
        return serviciosDao.ejecutarProcedimiento(sqlName);
    }

    public Object obtenerRegistro(String sqlName, Object object) throws Exception {
        DaoConcrete serviciosDao = new DaoConcrete();
        return serviciosDao.obtenerRegistro(sqlName, object);
    }

    public Object obtenerRegistro(String sqlName) throws Exception {
        DaoConcrete serviciosDao = new DaoConcrete();
        return serviciosDao.obtenerRegistro(sqlName);
    }

    public Object insertarRegistro(String qryName, Object objeto)
            throws Exception {
        DaoConcrete serviciosDao = new DaoConcrete();
        return serviciosDao.insertarRegistro(qryName, objeto);
    }

    public Object actualizarRegistro(String qryName, Object objeto)
            throws Exception {
        DaoConcrete serviciosDao = new DaoConcrete();
        return serviciosDao.actualizarRegistro(qryName, objeto);
    }

    public void borrarRegistro(String sqlName, Object object) throws Exception {
        DaoConcrete serviciosDao = new DaoConcrete();
        serviciosDao.borrarRegistro(sqlName, object);
    }

}
