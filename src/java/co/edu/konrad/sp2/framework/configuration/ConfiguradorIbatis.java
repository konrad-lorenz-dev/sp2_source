package co.edu.konrad.sp2.framework.configuration;

import java.io.IOException;
import java.io.Reader;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

public class ConfiguradorIbatis {

    public static final ConfiguradorIbatis configuradorIbatis = new ConfiguradorIbatis();
    protected static Logger log = Logger.getLogger(ConfiguradorIbatis.class);
    private SqlSessionFactory sqlSessionFactory;

    private ConfiguradorIbatis() {
        super();
    }

    public static ConfiguradorIbatis getInstance() {
        return configuradorIbatis;
    }

    public void configurar(String ambiente) {

        //log.info("Inicia Configuracion ");
        try {
            String resource = "configuration.xml";
            Reader reader = Resources.getResourceAsReader(resource);

            this.sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, ambiente);
            log.info("SqlSession sp2 configurado OK!! ");
        } catch (IOException e) {
            log.info(e.getMessage());
        }
    }

    public SqlSessionFactory getSqlSessionFactory() {

        return sqlSessionFactory;
    }
}
