/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.edu.konrad.sp2.constant;

public interface Constantes {

    
    
    public static final String ZERO = "0";
    public static final String OK = "OK";
    public static final String FAILED = "FAILED";
    public static final String ROL_BASE = "1";
    
    public static final String ROL_ADMINISTRADOR = "1";
    public static final String ROL_CONSEJERO_ACADEMICO = "2";
    public static final String ROL_COORDINADOR_PRACTICAS = "3";
    public static final String ROL_PRACTICANTE = "4";
    public static final String ROL_SUP_ENTIDAD = "5";
    public static final String ROL_SUP_PRACTICA = "6";
    public static final String ROL_ASESOR_JURIDICO = "10";
    
    public static final String ESTADO_BASE = "A";
    public static final String CREAR = "C";
    public static final String ACTUALIZAR = "A";
    public static final String USUARIO = "USUARIO";
    public static final String PERSONA = "PERSONA";
    public static final String CODIGO_PERSONA = "CODIGO_PERSONA";
    public static final String CORREO = "CORREO";
    public static final String CEDULA = "CEDULA";
    public static final String CLAVE = "123456";
    public static final String TIPODA = "DA";
    public static final String TIPO_ENTIDAD = "AS";
    public static final String CURSANDO = "N";
    public static final String TAB_DATOS_GENERALES = "tabDatosGenerales";
    public static final String CODIGO_OTRA_CIUDAD = "OT";
    public static final String ESTADO_ACTIVO = "1";
    public static final String ESTADO_INACTIVO = "2";
    public static final String SINREVISAR ="SRV";
    public static final String POSTULADOS ="POS";
    public static final String ASIGANDOS="ASG";
    public static final Long TIPO_NOVEDAD_RETROALIMENTACION = Long.valueOf(3);
    public static final Long TIPO_NOVEDAD_RETIRO = Long.valueOf(4);
    
    //MS
    public static final String CLIEND_ID = "1325093b-bdea-4fd4-bea0-fe666920103a";
    public static final String CLAVE_SECRET = "A.rg67iiCTjyp8IN8xu3IYor.0B-_uXn.N";
    public static final String TENANT = "konradlorenz.edu.co";
    public static final String AUTHORITY = "https://login.windows.net/";
    
    public static final String SALIRAPLICACOIN = "https://login.microsoftonline.com/common/oauth2/v2.0/logout?post_logout_redirect_uri=http://localhost:8080/sp2/faces/vistas/VistaLogin.xhtml";
    public static final String SALIRAPLICACIONEXTERNO = "http://localhost:8080/sp2/faces/vistas/VistaLogin.xhtml";
    public static final String INICIOAPLICACION = "https://login.windows.net/konradlorenz.edu.co/oauth2/authorize?response_type=code&scope=directory.read.all&response_mode=form_post&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fsp2%2Ffaces%2Fvistas%2FVistaDireccionamientoURL.xhtml&client_id="+CLIEND_ID+"&resource=https%3a%2f%2fgraph.microsoft.com&state=a52653ba-8509-49e9-a16b-c29ee74f8bbb&nonce=f65bcded-11b7-4f9c-beab-6d1df5de3112";
    public static final String INICIOPRINCIPAL = "http://localhost:8080/sp2/faces/vistas/VistaDireccionamientoURL.xhtml";

    public static String SERVIDORARCHIVOS = "http://localhost:8888/cargarArchivos/cargar?directorio=imagenesPerfil&aplicacionPrincipal=sp2&";
    
    //parametros url
    public static final String TIPOPERSONA = "tipoPersona";
    public static final String EXTERNO = "externo";
}
