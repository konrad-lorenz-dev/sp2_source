package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosHorarioPractica;
import co.edu.konrad.sp2.bean.HorarioPractica;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosHorarioPracticaImp implements ServiciosHorarioPractica {

    private final static Logger log = Logger.getLogger(ServiciosHorarioPracticaImp.class);

    @Override
    public List<HorarioPractica> consultarHorarioPractica() {
        List<HorarioPractica> horariopracticas = new ArrayList();
        try {
            horariopracticas = (List<HorarioPractica>) ManagerConcrete.getManager().obtenerListado("consultarHorarioPracticaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return horariopracticas;
    }

    @Override
    public HorarioPractica consultarHorarioPracticaById(Long idHorarioPractica) {
        HorarioPractica horariopractica = new HorarioPractica();
        try {
            horariopractica = (HorarioPractica) ManagerConcrete.getManager().obtenerRegistro("consultarHorarioPractica", idHorarioPractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return horariopractica;
    }

    @Override
    public void crearHorarioPractica(HorarioPractica horariopractica) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearHorarioPractica", horariopractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarHorarioPractica(HorarioPractica horariopractica) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarHorarioPractica", horariopractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarHorarioPractica(HorarioPractica horariopractica) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarHorarioPractica", horariopractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<HorarioPractica> consultarHorarioPracticaJoin() {
        List<HorarioPractica> horariopracticas = new ArrayList();
        try {
            horariopracticas = (List<HorarioPractica>) ManagerConcrete.getManager().obtenerListado("consultarHorarioPracticaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return horariopracticas;
    }

    @Override
    public HorarioPractica consultarHorarioPracticaJoinById(Long idHorarioPractica) {
        HorarioPractica horariopractica = new HorarioPractica();
        try {
            horariopractica = (HorarioPractica) ManagerConcrete.getManager().obtenerRegistro("consultarHorarioPracticaJoin", idHorarioPractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return horariopractica;
    }

}
