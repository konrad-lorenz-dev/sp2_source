package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.DetalleCoorPracticas;
import java.util.List;
import java.util.Map;

public interface ServiciosDetalleCoorPracticas {

    public List<DetalleCoorPracticas> consultarDetalleCoorPracticas();

    public DetalleCoorPracticas consultarDetalleCoorPracticasById(Long idDetalleCoorPracticas);

    public void crearDetalleCoorPracticas(DetalleCoorPracticas detallecoorpracticas);

    public void actualizarDetalleCoorPracticas(DetalleCoorPracticas detallecoorpracticas);

    public void borrarDetalleCoorPracticas(DetalleCoorPracticas detallecoorpracticas);

    public List<DetalleCoorPracticas> consultarDetalleCoorPracticasJoin();

    public DetalleCoorPracticas consultarDetalleCoorPracticasJoinById(Long idDetalleCoorPracticas);

}
