package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.TipoIdentificacion;
import java.util.List;
import java.util.Map;

public interface ServiciosTipoIdentificacion {

    public List<TipoIdentificacion> consultarTipoIdentificacion();

    public TipoIdentificacion consultarTipoIdentificacionById(Long idTipoIdentificacion);

    public void crearTipoIdentificacion(TipoIdentificacion tipoidentificacion);

    public void actualizarTipoIdentificacion(TipoIdentificacion tipoidentificacion);

    public void borrarTipoIdentificacion(TipoIdentificacion tipoidentificacion);

}
