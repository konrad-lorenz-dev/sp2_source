package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosDetalleSupEntidad;
import co.edu.konrad.sp2.bean.DetalleSupEntidad;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosDetalleSupEntidadImp implements ServiciosDetalleSupEntidad {

    private final static Logger log = Logger.getLogger(ServiciosDetalleSupEntidadImp.class);

    @Override
    public List<DetalleSupEntidad> consultarDetalleSupEntidad() {
        List<DetalleSupEntidad> detallesupentidads = new ArrayList();
        try {
            detallesupentidads = (List<DetalleSupEntidad>) ManagerConcrete.getManager().obtenerListado("consultarDetalleSupEntidadList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallesupentidads;
    }

    @Override
    public DetalleSupEntidad consultarDetalleSupEntidadById(Long idDetalleSupEntidad) {
        DetalleSupEntidad detallesupentidad = new DetalleSupEntidad();
        try {
            detallesupentidad = (DetalleSupEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarDetalleSupEntidad", idDetalleSupEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallesupentidad;
    }

    @Override
    public void crearDetalleSupEntidad(DetalleSupEntidad detallesupentidad) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearDetalleSupEntidad", detallesupentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarDetalleSupEntidad(DetalleSupEntidad detallesupentidad) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarDetalleSupEntidad", detallesupentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarDetalleSupEntidad(DetalleSupEntidad detallesupentidad) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarDetalleSupEntidad", detallesupentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<DetalleSupEntidad> consultarDetalleSupEntidadJoin() {
        List<DetalleSupEntidad> detallesupentidads = new ArrayList();
        try {
            detallesupentidads = (List<DetalleSupEntidad>) ManagerConcrete.getManager().obtenerListado("consultarDetalleSupEntidadListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallesupentidads;
    }

    @Override
    public DetalleSupEntidad consultarDetalleSupEntidadJoinById(Long idDetalleSupEntidad) {
        DetalleSupEntidad detallesupentidad = new DetalleSupEntidad();
        try {
            detallesupentidad = (DetalleSupEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarDetalleSupEntidadJoin", idDetalleSupEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallesupentidad;
    }

}
