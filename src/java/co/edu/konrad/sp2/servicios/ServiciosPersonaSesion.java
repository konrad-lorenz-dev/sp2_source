package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.PersonaSesion;
import java.util.List;
import java.util.Map;

public interface ServiciosPersonaSesion {

    public List<PersonaSesion> consultarPersonaSesion();

    public PersonaSesion consultarPersonaSesionById(Long idPersonaSesion);

    public void crearPersonaSesion(PersonaSesion personasesion);

    public void actualizarPersonaSesion(PersonaSesion personasesion);

    public void borrarPersonaSesion(PersonaSesion personasesion);

    public List<PersonaSesion> consultarPersonaSesionJoin();

    public PersonaSesion consultarPersonaSesionJoinById(Long idPersonaSesion);

}
