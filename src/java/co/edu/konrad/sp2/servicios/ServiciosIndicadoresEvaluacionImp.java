package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosIndicadoresEvaluacion;
import co.edu.konrad.sp2.bean.IndicadoresEvaluacion;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosIndicadoresEvaluacionImp implements ServiciosIndicadoresEvaluacion {

    private final static Logger log = Logger.getLogger(ServiciosIndicadoresEvaluacionImp.class);

    @Override
    public List<IndicadoresEvaluacion> consultarIndicadoresEvaluacion() {
        List<IndicadoresEvaluacion> indicadoresevaluacions = new ArrayList();
        try {
            indicadoresevaluacions = (List<IndicadoresEvaluacion>) ManagerConcrete.getManager().obtenerListado("consultarIndicadoresEvaluacionList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return indicadoresevaluacions;
    }

    @Override
    public IndicadoresEvaluacion consultarIndicadoresEvaluacionById(Long idIndicadoresEvaluacion) {
        IndicadoresEvaluacion indicadoresevaluacion = new IndicadoresEvaluacion();
        try {
            indicadoresevaluacion = (IndicadoresEvaluacion) ManagerConcrete.getManager().obtenerRegistro("consultarIndicadoresEvaluacion", idIndicadoresEvaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return indicadoresevaluacion;
    }

    @Override
    public void crearIndicadoresEvaluacion(IndicadoresEvaluacion indicadoresevaluacion) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearIndicadoresEvaluacion", indicadoresevaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarIndicadoresEvaluacion(IndicadoresEvaluacion indicadoresevaluacion) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarIndicadoresEvaluacion", indicadoresevaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarIndicadoresEvaluacion(IndicadoresEvaluacion indicadoresevaluacion) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarIndicadoresEvaluacion", indicadoresevaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<IndicadoresEvaluacion> consultarIndicadoresEvaluacionJoin() {
        List<IndicadoresEvaluacion> indicadoresevaluacions = new ArrayList();
        try {
            indicadoresevaluacions = (List<IndicadoresEvaluacion>) ManagerConcrete.getManager().obtenerListado("consultarIndicadoresEvaluacionListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return indicadoresevaluacions;
    }

    @Override
    public IndicadoresEvaluacion consultarIndicadoresEvaluacionJoinById(Long idIndicadoresEvaluacion) {
        IndicadoresEvaluacion indicadoresevaluacion = new IndicadoresEvaluacion();
        try {
            indicadoresevaluacion = (IndicadoresEvaluacion) ManagerConcrete.getManager().obtenerRegistro("consultarIndicadoresEvaluacionJoin", idIndicadoresEvaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return indicadoresevaluacion;
    }

}
