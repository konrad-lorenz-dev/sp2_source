package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosSesionSupervsion;
import co.edu.konrad.sp2.bean.SesionSupervsion;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosSesionSupervsionImp implements ServiciosSesionSupervsion {

    private final static Logger log = Logger.getLogger(ServiciosSesionSupervsionImp.class);

    @Override
    public List<SesionSupervsion> consultarSesionSupervsion() {
        List<SesionSupervsion> sesionsupervsions = new ArrayList();
        try {
            sesionsupervsions = (List<SesionSupervsion>) ManagerConcrete.getManager().obtenerListado("consultarSesionSupervsionList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionsupervsions;
    }

    @Override
    public SesionSupervsion consultarSesionSupervsionById(Long idSesionSupervsion) {
        SesionSupervsion sesionsupervsion = new SesionSupervsion();
        try {
            sesionsupervsion = (SesionSupervsion) ManagerConcrete.getManager().obtenerRegistro("consultarSesionSupervsion", idSesionSupervsion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionsupervsion;
    }

    @Override
    public void crearSesionSupervsion(SesionSupervsion sesionsupervsion) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearSesionSupervsion", sesionsupervsion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarSesionSupervsion(SesionSupervsion sesionsupervsion) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarSesionSupervsion", sesionsupervsion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarSesionSupervsion(SesionSupervsion sesionsupervsion) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarSesionSupervsion", sesionsupervsion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<SesionSupervsion> consultarSesionSupervsionJoin() {
        List<SesionSupervsion> sesionsupervsions = new ArrayList();
        try {
            sesionsupervsions = (List<SesionSupervsion>) ManagerConcrete.getManager().obtenerListado("consultarSesionSupervsionListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionsupervsions;
    }

    @Override
    public SesionSupervsion consultarSesionSupervsionJoinById(Long idSesionSupervsion) {
        SesionSupervsion sesionsupervsion = new SesionSupervsion();
        try {
            sesionsupervsion = (SesionSupervsion) ManagerConcrete.getManager().obtenerRegistro("consultarSesionSupervsionJoin", idSesionSupervsion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionsupervsion;
    }

}
