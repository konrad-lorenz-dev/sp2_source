package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosTipoNovedad;
import co.edu.konrad.sp2.bean.TipoNovedad;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosTipoNovedadImp implements ServiciosTipoNovedad {

    private final static Logger log = Logger.getLogger(ServiciosTipoNovedadImp.class);

    @Override
    public List<TipoNovedad> consultarTipoNovedad() {
        List<TipoNovedad> tiponovedads = new ArrayList();
        try {
            tiponovedads = (List<TipoNovedad>) ManagerConcrete.getManager().obtenerListado("consultarTipoNovedadList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tiponovedads;
    }

    @Override
    public TipoNovedad consultarTipoNovedadById(Long idTipoNovedad) {
        TipoNovedad tiponovedad = new TipoNovedad();
        try {
            tiponovedad = (TipoNovedad) ManagerConcrete.getManager().obtenerRegistro("consultarTipoNovedad", idTipoNovedad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tiponovedad;
    }

    @Override
    public void crearTipoNovedad(TipoNovedad tiponovedad) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearTipoNovedad", tiponovedad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarTipoNovedad(TipoNovedad tiponovedad) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarTipoNovedad", tiponovedad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarTipoNovedad(TipoNovedad tiponovedad) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarTipoNovedad", tiponovedad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
