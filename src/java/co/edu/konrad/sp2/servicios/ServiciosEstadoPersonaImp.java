package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosEstadoPersona;
import co.edu.konrad.sp2.bean.EstadoPersona;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosEstadoPersonaImp implements ServiciosEstadoPersona {

    private final static Logger log = Logger.getLogger(ServiciosEstadoPersonaImp.class);

    @Override
    public List<EstadoPersona> consultarEstadoPersona() {
        List<EstadoPersona> estadopersonas = new ArrayList();
        try {
            estadopersonas = (List<EstadoPersona>) ManagerConcrete.getManager().obtenerListado("consultarEstadoPersonaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return estadopersonas;
    }

    @Override
    public EstadoPersona consultarEstadoPersonaById(Long idEstadoPersona) {
        EstadoPersona estadopersona = new EstadoPersona();
        try {
            estadopersona = (EstadoPersona) ManagerConcrete.getManager().obtenerRegistro("consultarEstadoPersona", idEstadoPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return estadopersona;
    }

    @Override
    public void crearEstadoPersona(EstadoPersona estadopersona) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearEstadoPersona", estadopersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarEstadoPersona(EstadoPersona estadopersona) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarEstadoPersona", estadopersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarEstadoPersona(EstadoPersona estadopersona) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarEstadoPersona", estadopersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
