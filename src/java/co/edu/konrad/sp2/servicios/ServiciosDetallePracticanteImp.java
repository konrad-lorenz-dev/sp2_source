package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosDetallePracticante;
import co.edu.konrad.sp2.bean.DetallePracticante;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosDetallePracticanteImp implements ServiciosDetallePracticante {

    private final static Logger log = Logger.getLogger(ServiciosDetallePracticanteImp.class);

    @Override
    public List<DetallePracticante> consultarDetallePracticante() {
        List<DetallePracticante> detallepracticantes = new ArrayList();
        try {
            detallepracticantes = (List<DetallePracticante>) ManagerConcrete.getManager().obtenerListado("consultarDetallePracticanteList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallepracticantes;
    }

    @Override
    public DetallePracticante consultarDetallePracticanteById(Long idDetallePracticante) {
        DetallePracticante detallepracticante = new DetallePracticante();
        try {
            detallepracticante = (DetallePracticante) ManagerConcrete.getManager().obtenerRegistro("consultarDetallePracticante", idDetallePracticante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallepracticante;
    }

    @Override
    public void crearDetallePracticante(DetallePracticante detallepracticante) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearDetallePracticante", detallepracticante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarDetallePracticante(DetallePracticante detallepracticante) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarDetallePracticante", detallepracticante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarDetallePracticante(DetallePracticante detallepracticante) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarDetallePracticante", detallepracticante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<DetallePracticante> consultarDetallePracticanteJoin() {
        List<DetallePracticante> detallepracticantes = new ArrayList();
        try {
            detallepracticantes = (List<DetallePracticante>) ManagerConcrete.getManager().obtenerListado("consultarDetallePracticanteListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallepracticantes;
    }

    @Override
    public DetallePracticante consultarDetallePracticanteJoinById(Long idDetallePracticante) {
        DetallePracticante detallepracticante = new DetallePracticante();
        try {
            detallepracticante = (DetallePracticante) ManagerConcrete.getManager().obtenerRegistro("consultarDetallePracticanteJoin", idDetallePracticante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallepracticante;
    }

    @Override
    public List<DetallePracticante> consultarDetallePracticanteEstadoPracticanteJoin(String estadoPracticante) {
        List<DetallePracticante> detallepracticantes = new ArrayList();
        try {
            detallepracticantes = (List<DetallePracticante>) ManagerConcrete.getManager().obtenerListado("consultarDetallePracticanteListEstadoPracticaJoin", estadoPracticante);
        } catch (Exception e) {
            log.error(e.getMessage() + "No se puede traer la lista");
        }
        return detallepracticantes;
    }

    @Override
    public void actualizarDetallePracticanteEstadoPractica(DetallePracticante detallePracticante) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarEstadoPractica", detallePracticante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
