package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.NovedadPractica;
import java.util.List;
import java.util.Map;

public interface ServiciosNovedadPractica {

    public List<NovedadPractica> consultarNovedadPractica();

    public NovedadPractica consultarNovedadPracticaById(Long idNovedadPractica);

    public void crearNovedadPractica(NovedadPractica novedadpractica);

    public void actualizarNovedadPractica(NovedadPractica novedadpractica);

    public void borrarNovedadPractica(NovedadPractica novedadpractica);

    public List<NovedadPractica> consultarNovedadPracticaJoin();

    public NovedadPractica consultarNovedadPracticaJoinById(Long idNovedadPractica);

}
