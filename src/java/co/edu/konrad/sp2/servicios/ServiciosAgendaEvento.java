package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.AgendaEvento;
import java.util.List;
import java.util.Map;

public interface ServiciosAgendaEvento {

    public List<AgendaEvento> consultarAgendaEvento();

    public AgendaEvento consultarAgendaEventoById(Long idAgendaEvento);

    public AgendaEvento crearAgendaEvento(AgendaEvento agendaevento);

    public AgendaEvento actualizarAgendaEvento(AgendaEvento agendaevento);

    public AgendaEvento borrarAgendaEvento(AgendaEvento agendaevento);

    public List<AgendaEvento> consultarAgendaEventoJoin();

    public AgendaEvento consultarAgendaEventoJoinById(Long idAgendaEvento);
    
    public List<AgendaEvento> consultarAgendaEventoJoinPorRol(String UsrCreacion);

}
