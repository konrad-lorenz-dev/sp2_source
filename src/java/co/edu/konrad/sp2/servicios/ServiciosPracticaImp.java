package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosPractica;
import co.edu.konrad.sp2.bean.Practica;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;

public class ServiciosPracticaImp implements ServiciosPractica {
    
    private final static Logger log = Logger.getLogger(ServiciosPracticaImp.class);
    
    @Override
    public List<Practica> consultarPractica() {
        List<Practica> practicas = new ArrayList();
        try {
            practicas = (List<Practica>) ManagerConcrete.getManager().obtenerListado("consultarPracticaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return practicas;
    }
    
    @Override
    public Practica consultarPracticaById(Long idPractica) {
        Practica practica = new Practica();
        try {
            practica = (Practica) ManagerConcrete.getManager().obtenerRegistro("consultarPractica", idPractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return practica;
    }
    
    @Override
    public void crearPractica(Practica practica) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearPractica", practica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
    
    @Override
    public void actualizarPractica(Practica practica) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarPractica", practica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
    
    @Override
    public void borrarPractica(Practica practica) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarPractica", practica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
    
    @Override
    public List<Practica> consultarPracticaJoin() {
        List<Practica> practicas = new ArrayList();
        try {
            practicas = (List<Practica>) ManagerConcrete.getManager().obtenerListado("consultarPracticaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return practicas;
    }
    
    @Override
    public Practica consultarPracticaJoinById(Long idPractica) {
        Practica practica = new Practica();
        try {
            practica = (Practica) ManagerConcrete.getManager().obtenerRegistro("consultarPracticaJoin", idPractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return practica;
    }
    
    @Override
    public Practica informacionPractica(Long idPracticante) {
        Practica practica = new Practica();
        try {
            practica = (Practica) ManagerConcrete.getManager().obtenerRegistro("informacionPracticanteJoin", idPracticante);
            if (practica == null) {
                practica = new Practica();
                practica.setConstantes(Constantes.FAILED);
            } else {
                practica.setConstantes(Constantes.OK);                
            }
        } catch (Exception e) {
            practica.setConstantes(Constantes.FAILED);
            log.error(e.getMessage());
        }
        return practica;
    }
    
    @Override
    public Practica consultarEstadoPractica(Long idDetallePracticante) {
        Practica practica = new Practica();
        try {
            practica = (Practica) ManagerConcrete.getManager().obtenerRegistro("consultarEstadoPractica", idDetallePracticante);
            if (practica == null) {
                practica = new Practica();
                practica.setConstantes(Constantes.FAILED);
            } else {
                practica.setConstantes(Constantes.OK);
            }
        } catch (Exception e) {
            practica.setConstantes(Constantes.FAILED);
            log.error(e.getMessage());
        }
        return practica;
    }
    
}
