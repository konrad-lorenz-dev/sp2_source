package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Rol;
import java.util.List;
import java.util.Map;

public interface ServiciosRol {

    public List<Rol> consultarRol();

    public Rol consultarRolById(Long idRol);

    public void crearRol(Rol rol);

    public void actualizarRol(Rol rol);

    public void borrarRol(Rol rol);

}
