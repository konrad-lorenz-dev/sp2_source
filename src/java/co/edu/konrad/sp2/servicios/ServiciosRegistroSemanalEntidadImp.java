package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosRegistroSemanalEntidad;
import co.edu.konrad.sp2.bean.RegistroSemanalEntidad;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosRegistroSemanalEntidadImp implements ServiciosRegistroSemanalEntidad {

    private final static Logger log = Logger.getLogger(ServiciosRegistroSemanalEntidadImp.class);

    @Override
    public List<RegistroSemanalEntidad> consultarRegistroSemanalEntidad() {
        List<RegistroSemanalEntidad> registrosemanalentidads = new ArrayList();
        try {
            registrosemanalentidads = (List<RegistroSemanalEntidad>) ManagerConcrete.getManager().obtenerListado("consultarRegistroSemanalEntidadList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return registrosemanalentidads;
    }

    @Override
    public RegistroSemanalEntidad consultarRegistroSemanalEntidadById(Long idRegistroSemanalEntidad) {
        RegistroSemanalEntidad registrosemanalentidad = new RegistroSemanalEntidad();
        try {
            registrosemanalentidad = (RegistroSemanalEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarRegistroSemanalEntidad", idRegistroSemanalEntidad);
            if (registrosemanalentidad == null) {
                registrosemanalentidad = new RegistroSemanalEntidad();
                registrosemanalentidad.setConstante(Constantes.FAILED);
            } else {
                registrosemanalentidad.setConstante(Constantes.OK);
            }

        } catch (Exception e) {
            registrosemanalentidad = new RegistroSemanalEntidad();
            registrosemanalentidad.setConstante(Constantes.FAILED);
            log.error(e.getMessage());
        }
        return registrosemanalentidad;
    }

    @Override
    public void crearRegistroSemanalEntidad(RegistroSemanalEntidad registrosemanalentidad) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearRegistroSemanalEntidad", registrosemanalentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public String actualizarRegistroSemanalEntidad(RegistroSemanalEntidad registrosemanalentidad) {
        String constante = null;
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarRegistroSemanalEntidad", registrosemanalentidad);
            constante = Constantes.OK;
        } catch (Exception e) {
            constante = Constantes.FAILED;
            log.error(e.getMessage());
        }
        return constante;
    }

    @Override
    public void borrarRegistroSemanalEntidad(RegistroSemanalEntidad registrosemanalentidad) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarRegistroSemanalEntidad", registrosemanalentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<RegistroSemanalEntidad> consultarRegistroSemanalEntidadJoin() {
        List<RegistroSemanalEntidad> registrosemanalentidads = new ArrayList();
        try {
            registrosemanalentidads = (List<RegistroSemanalEntidad>) ManagerConcrete.getManager().obtenerListado("consultarRegistroSemanalEntidadListJoin");
            System.out.println("ArrayLis" + registrosemanalentidads.size());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return registrosemanalentidads;
    }

    @Override
    public RegistroSemanalEntidad consultarRegistroSemanalEntidadJoinById(Long idRegistroSemanalEntidad) {
        RegistroSemanalEntidad registrosemanalentidad = new RegistroSemanalEntidad();
        try {
            registrosemanalentidad = (RegistroSemanalEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarRegistroSemanalEntidadJoin", idRegistroSemanalEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return registrosemanalentidad;
    }

    @Override
    public List<RegistroSemanalEntidad> consultarRegistroSemanalEntidadEstadoValido(Long idPersonaPracticante) {
        List<RegistroSemanalEntidad> registrosemanalentidads = new ArrayList();
        try {
            registrosemanalentidads = (List<RegistroSemanalEntidad>) ManagerConcrete.getManager().obtenerListado("BuscarEstadoRegistroEntidad", idPersonaPracticante);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return registrosemanalentidads;
    }

    @Override
    public List<RegistroSemanalEntidad> consultarRegistroSemanalEntidadHistorial(Long idPersonaPracticante) {
        List<RegistroSemanalEntidad> registrosemanalentidads = new ArrayList();
        try {
            registrosemanalentidads = (List<RegistroSemanalEntidad>) ManagerConcrete.getManager().obtenerListado("BuscarEstadoRegistroEntidadHistorial", idPersonaPracticante);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return registrosemanalentidads;
    }

}
