package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.CompetenciaIndicador;
import java.util.List;
import java.util.Map;

public interface ServiciosCompetenciaIndicador {

    public List<CompetenciaIndicador> consultarCompetenciaIndicador();

    public CompetenciaIndicador consultarCompetenciaIndicadorById(Long idCompetenciaIndicador);

    public void crearCompetenciaIndicador(CompetenciaIndicador competenciaindicador);

    public void actualizarCompetenciaIndicador(CompetenciaIndicador competenciaindicador);

    public void borrarCompetenciaIndicador(CompetenciaIndicador competenciaindicador);

}
