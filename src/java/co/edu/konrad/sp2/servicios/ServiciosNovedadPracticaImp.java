package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosNovedadPractica;
import co.edu.konrad.sp2.bean.NovedadPractica;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosNovedadPracticaImp implements ServiciosNovedadPractica {

    private final static Logger log = Logger.getLogger(ServiciosNovedadPracticaImp.class);

    @Override
    public List<NovedadPractica> consultarNovedadPractica() {
        List<NovedadPractica> novedadpracticas = new ArrayList();
        try {
            novedadpracticas = (List<NovedadPractica>) ManagerConcrete.getManager().obtenerListado("consultarNovedadPracticaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpracticas;
    }

    @Override
    public NovedadPractica consultarNovedadPracticaById(Long idNovedadPractica) {
        NovedadPractica novedadpractica = new NovedadPractica();
        try {
            novedadpractica = (NovedadPractica) ManagerConcrete.getManager().obtenerRegistro("consultarNovedadPractica", idNovedadPractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpractica;
    }

    @Override
    public void crearNovedadPractica(NovedadPractica novedadpractica) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearNovedadPractica", novedadpractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarNovedadPractica(NovedadPractica novedadpractica) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarNovedadPractica", novedadpractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarNovedadPractica(NovedadPractica novedadpractica) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarNovedadPractica", novedadpractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<NovedadPractica> consultarNovedadPracticaJoin() {
        List<NovedadPractica> novedadpracticas = new ArrayList();
        try {
            novedadpracticas = (List<NovedadPractica>) ManagerConcrete.getManager().obtenerListado("consultarNovedadPracticaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpracticas;
    }

    @Override
    public NovedadPractica consultarNovedadPracticaJoinById(Long idNovedadPractica) {
        NovedadPractica novedadpractica = new NovedadPractica();
        try {
            novedadpractica = (NovedadPractica) ManagerConcrete.getManager().obtenerRegistro("consultarNovedadPracticaJoin", idNovedadPractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpractica;
    }

}
