package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosEps;
import co.edu.konrad.sp2.bean.Eps;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosEpsImp implements ServiciosEps {

    private final static Logger log = Logger.getLogger(ServiciosEpsImp.class);

    @Override
    public List<Eps> consultarEps() {
        List<Eps> epss = new ArrayList();
        try {
            epss = (List<Eps>) ManagerConcrete.getManager().obtenerListado("consultarEpsList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return epss;
    }

    @Override
    public Eps consultarEpsById(Long idEps) {
        Eps eps = new Eps();
        try {
            eps = (Eps) ManagerConcrete.getManager().obtenerRegistro("consultarEps", idEps);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return eps;
    }

    @Override
    public void crearEps(Eps eps) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearEps", eps);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarEps(Eps eps) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarEps", eps);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarEps(Eps eps) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarEps", eps);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
