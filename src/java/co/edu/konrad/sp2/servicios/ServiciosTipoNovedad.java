package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.TipoNovedad;
import java.util.List;
import java.util.Map;

public interface ServiciosTipoNovedad {

    public List<TipoNovedad> consultarTipoNovedad();

    public TipoNovedad consultarTipoNovedadById(Long idTipoNovedad);

    public void crearTipoNovedad(TipoNovedad tiponovedad);

    public void actualizarTipoNovedad(TipoNovedad tiponovedad);

    public void borrarTipoNovedad(TipoNovedad tiponovedad);

}
