package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.NovedadPractConvenio;
import java.util.List;
import java.util.Map;

public interface ServiciosNovedadPractConvenio {

    public List<NovedadPractConvenio> consultarNovedadPractConvenio();

    public NovedadPractConvenio consultarNovedadPractConvenioById(Long idNovedadPractConvenio);

    public void crearNovedadPractConvenio(NovedadPractConvenio novedadpractconvenio);

    public void actualizarNovedadPractConvenio(NovedadPractConvenio novedadpractconvenio);

    public void borrarNovedadPractConvenio(NovedadPractConvenio novedadpractconvenio);

    public List<NovedadPractConvenio> consultarNovedadPractConvenioJoin();

    public NovedadPractConvenio consultarNovedadPractConvenioJoinById(Long idNovedadPractConvenio);

}
