package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosDetalleCoorPracticas;
import co.edu.konrad.sp2.bean.DetalleCoorPracticas;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosDetalleCoorPracticasImp implements ServiciosDetalleCoorPracticas {

    private final static Logger log = Logger.getLogger(ServiciosDetalleCoorPracticasImp.class);

    @Override
    public List<DetalleCoorPracticas> consultarDetalleCoorPracticas() {
        List<DetalleCoorPracticas> detallecoorpracticass = new ArrayList();
        try {
            detallecoorpracticass = (List<DetalleCoorPracticas>) ManagerConcrete.getManager().obtenerListado("consultarDetalleCoorPracticasList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallecoorpracticass;
    }

    @Override
    public DetalleCoorPracticas consultarDetalleCoorPracticasById(Long idDetalleCoorPracticas) {
        DetalleCoorPracticas detallecoorpracticas = new DetalleCoorPracticas();
        try {
            detallecoorpracticas = (DetalleCoorPracticas) ManagerConcrete.getManager().obtenerRegistro("consultarDetalleCoorPracticas", idDetalleCoorPracticas);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallecoorpracticas;
    }

    @Override
    public void crearDetalleCoorPracticas(DetalleCoorPracticas detallecoorpracticas) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearDetalleCoorPracticas", detallecoorpracticas);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarDetalleCoorPracticas(DetalleCoorPracticas detallecoorpracticas) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarDetalleCoorPracticas", detallecoorpracticas);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarDetalleCoorPracticas(DetalleCoorPracticas detallecoorpracticas) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarDetalleCoorPracticas", detallecoorpracticas);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<DetalleCoorPracticas> consultarDetalleCoorPracticasJoin() {
        List<DetalleCoorPracticas> detallecoorpracticass = new ArrayList();
        try {
            detallecoorpracticass = (List<DetalleCoorPracticas>) ManagerConcrete.getManager().obtenerListado("consultarDetalleCoorPracticasListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallecoorpracticass;
    }

    @Override
    public DetalleCoorPracticas consultarDetalleCoorPracticasJoinById(Long idDetalleCoorPracticas) {
        DetalleCoorPracticas detallecoorpracticas = new DetalleCoorPracticas();
        try {
            detallecoorpracticas = (DetalleCoorPracticas) ManagerConcrete.getManager().obtenerRegistro("consultarDetalleCoorPracticasJoin", idDetalleCoorPracticas);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallecoorpracticas;
    }

}
