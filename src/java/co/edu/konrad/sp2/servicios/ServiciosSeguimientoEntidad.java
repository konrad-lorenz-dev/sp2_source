package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.SeguimientoEntidad;
import java.util.List;
import java.util.Map;

public interface ServiciosSeguimientoEntidad {

    public List<SeguimientoEntidad> consultarSeguimientoEntidad();

    public SeguimientoEntidad consultarSeguimientoEntidadById(Long idSeguimientoEntidad);

    public void crearSeguimientoEntidad(SeguimientoEntidad seguimientoentidad);

    public void actualizarSeguimientoEntidad(SeguimientoEntidad seguimientoentidad);

    public void borrarSeguimientoEntidad(SeguimientoEntidad seguimientoentidad);

    public List<SeguimientoEntidad> consultarSeguimientoEntidadJoin();

    public SeguimientoEntidad consultarSeguimientoEntidadJoinById(Long idSeguimientoEntidad);

}
