package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosFichaTecnica;
import co.edu.konrad.sp2.bean.FichaTecnica;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosFichaTecnicaImp implements ServiciosFichaTecnica {

    private final static Logger log = Logger.getLogger(ServiciosFichaTecnicaImp.class);

    @Override
    public List<FichaTecnica> consultarFichaTecnica() {
        List<FichaTecnica> fichatecnicas = new ArrayList();
        try {
            fichatecnicas = (List<FichaTecnica>) ManagerConcrete.getManager().obtenerListado("consultarFichaTecnicaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return fichatecnicas;
    }

    @Override
    public FichaTecnica consultarFichaTecnicaById(Long idFichaTecnica) {
        FichaTecnica fichatecnica = new FichaTecnica();
        try {
            fichatecnica = (FichaTecnica) ManagerConcrete.getManager().obtenerRegistro("consultarFichaTecnica", idFichaTecnica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return fichatecnica;
    }

    @Override
    public void crearFichaTecnica(FichaTecnica fichatecnica) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearFichaTecnica", fichatecnica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarFichaTecnica(FichaTecnica fichatecnica) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarFichaTecnica", fichatecnica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarFichaTecnica(FichaTecnica fichatecnica) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarFichaTecnica", fichatecnica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
