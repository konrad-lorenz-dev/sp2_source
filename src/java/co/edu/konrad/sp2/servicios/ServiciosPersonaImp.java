package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.AgendaEvento;
import co.edu.konrad.sp2.servicios.ServiciosPersona;
import co.edu.konrad.sp2.bean.Persona;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.bean.UsuarioSesion;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosPersonaImp implements ServiciosPersona {

    private final static Logger log = Logger.getLogger(ServiciosPersonaImp.class);

    @Override
    public List<Persona> consultarPersona() {
        List<Persona> personas = new ArrayList();
        try {
            personas = (List<Persona>) ManagerConcrete.getManager().obtenerListado("consultarPersonaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return personas;
    }

    @Override
    public Persona consultarPersonaById(Long idPersona) {
        Persona persona = new Persona();
        try {
            persona = (Persona) ManagerConcrete.getManager().obtenerRegistro("consultarPersona", idPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }
    
    @Override
    public Persona consultarPersonaByCorreo(String correoPersona) {
        Persona persona = new Persona();
        try {
            persona = (Persona) ManagerConcrete.getManager().obtenerRegistro("consultarPersonaByCorreo", correoPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }

    @Override
    public void crearPersona(Persona persona) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearPersona", persona);
            persona.setConstante(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            persona.setConstante(Constantes.FAILED);
        }

    }

    @Override
    public void actualizarPersona(Persona persona) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarPersona", persona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarPersona(Persona persona) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarPersona", persona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<Persona> consultarPersonaJoin() {
        List<Persona> personas = new ArrayList();
        try {
            personas = (List<Persona>) ManagerConcrete.getManager().obtenerListado("consultarPersonaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return personas;
    }

    @Override
    public Persona consultarPersonaJoinById(Long idPersona) {
        Persona persona = new Persona();
        try {
            persona = (Persona) ManagerConcrete.getManager().obtenerRegistro("consultarPersonaJoin", idPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return persona;
    }

    @Override
    public Persona ConsultarEstEscolaris(String codPracticante) {
        Persona varCodPracticante = new Persona();
        try {
            varCodPracticante = (Persona) ManagerConcrete.getManager().obtenerRegistro("consultarEstEscolaris", codPracticante);
            varCodPracticante.setConstante(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            varCodPracticante.setConstante(Constantes.FAILED);
        }
        return varCodPracticante;
    }

    @Override
    public String validaUsuario(UsuarioSesion usuarioSesion) {
        String usuarioValido = "0";
        try {
            usuarioValido = (String) ManagerConcrete.getManager().obtenerRegistro("validarUsuario", usuarioSesion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return usuarioValido;
    }

    @Override
    public String validaUsuarioEntidad(UsuarioSesion usuarioSesion) {
        String usuarioValido = "0";
        try {
            usuarioValido = (String) ManagerConcrete.getManager().obtenerRegistro("validarUsuarioEntidad", usuarioSesion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return usuarioValido;
    }

    @Override
    public String consultaSeqUsuario(UsuarioSesion usuarioSesion) {
        String cedula = "0";
        try {
            cedula = (String) ManagerConcrete.getManager().obtenerRegistro("consultaSeqUsuario", usuarioSesion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return cedula;
    }

    @Override
    public Persona ConsultarCodigoPersona(String codPracticante) {
        Persona varCodPracticante = new Persona();
        try {
            varCodPracticante = (Persona) ManagerConcrete.getManager().obtenerRegistro("consultaCodigo", codPracticante);
            if (varCodPracticante == null) {
                varCodPracticante = new Persona();
                varCodPracticante.setConstante(Constantes.FAILED);
            } else {
                varCodPracticante.setConstante(Constantes.OK);
            }

        } catch (Exception e) {
            varCodPracticante.setConstante(Constantes.FAILED);
            log.error(e.getMessage());

        }
        return varCodPracticante;
    }

    @Override
    public Persona ConsultarPersonaId(String codPersona) {
        Persona idPersona = new Persona();
        try {
            idPersona = (Persona) ManagerConcrete.getManager().obtenerRegistro("consultarPersonaId", codPersona);
            if (idPersona == null) {
                idPersona = new Persona();
                idPersona.setConstante(Constantes.FAILED);
            } else {
                idPersona.setConstante(Constantes.OK);
            }
        } catch (Exception e) {
            idPersona.setConstante(Constantes.FAILED);
            log.error(e.getMessage());

        }
        return idPersona;
    }

    @Override
    public String consultarPrograma(Long idPersona) {
        String programa = "";
        try {
            programa = (String) ManagerConcrete.getManager().obtenerRegistro("consultarPrograma", idPersona);
        } catch (Exception e) {
            log.error(e.getMessage());

        }
        return programa;
    }

    @Override
    public Persona consultarCorreo(String correo) {
        Persona persona = new Persona();
        try {
            persona = (Persona) ManagerConcrete.getManager().obtenerRegistro("consultarPersonaCorreo", correo);
            if (persona == null) {
                persona = new Persona();
                persona.setConstante(Constantes.FAILED);
            } else {
                persona.setConstante(Constantes.OK);
            }
        } catch (Exception e) {
            persona.setConstante(Constantes.FAILED);
            log.error(e.getMessage());

        }
        return persona;
    }

    @Override
    public Persona consultarPersonaCedula(String cedula) {
        Persona persona = new Persona();
        try {
            persona = (Persona) ManagerConcrete.getManager().obtenerRegistro("consultarPersonaCedula", cedula);
            if (persona == null) {
                persona = new Persona();
                persona.setConstante(Constantes.FAILED);
            } else {
                persona.setConstante(Constantes.OK);
            }
        } catch (Exception e) {
            persona.setConstante(Constantes.FAILED);
            log.error(e.getMessage());

        }
        return persona;
    }

    @Override
    public Persona ConsultarSupPersonaId(String codPersona) {
        Persona idPersona = new Persona();
        try {
            idPersona = (Persona) ManagerConcrete.getManager().obtenerRegistro("consultarSupPersonaId", codPersona);
            if (idPersona == null) {
                idPersona = new Persona();
                idPersona.setConstante(Constantes.FAILED);
            } else {
                idPersona.setConstante(Constantes.OK);
            }
        } catch (Exception e) {
            idPersona.setConstante(Constantes.FAILED);
            log.error(e.getMessage());

        }
        return idPersona;
    }
}
