package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.NovedadConvenio;
import java.util.List;
import java.util.Map;

public interface ServiciosNovedadConvenio {

    public List<NovedadConvenio> consultarNovedadConvenio();

    public NovedadConvenio consultarNovedadConvenioById(Long idNovedadConvenio);

    public void crearNovedadConvenio(NovedadConvenio novedadconvenio);

    public void actualizarNovedadConvenio(NovedadConvenio novedadconvenio);

    public void borrarNovedadConvenio(NovedadConvenio novedadconvenio);

    public List<NovedadConvenio> consultarNovedadConvenioJoin();

    public NovedadConvenio consultarNovedadConvenioJoinById(Long idNovedadConvenio);

}
