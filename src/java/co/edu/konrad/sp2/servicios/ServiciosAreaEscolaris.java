package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.AreaEscolaris;
import java.util.List;
import java.util.Map;

public interface ServiciosAreaEscolaris {

    public List<AreaEscolaris> consultarAreaEscolaris();

    public AreaEscolaris consultarAreaEscolarisById(Long idAreaEscolaris);

    public void crearAreaEscolaris(AreaEscolaris areaescolaris);

    public void actualizarAreaEscolaris(AreaEscolaris areaescolaris);

    public void borrarAreaEscolaris(AreaEscolaris areaescolaris);

    public List<AreaEscolaris> consultarAreaEscolarisJoin();

    public AreaEscolaris consultarAreaEscolarisJoinById(Long idAreaEscolaris);

}
