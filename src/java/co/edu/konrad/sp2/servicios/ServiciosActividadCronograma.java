package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.ActividadCronograma;
import java.util.List;
import java.util.Map;

public interface ServiciosActividadCronograma {

    public List<ActividadCronograma> consultarActividadCronograma();

    public ActividadCronograma consultarActividadCronogramaById(Long idActividadCronograma);

    public void crearActividadCronograma(ActividadCronograma actividadcronograma);

    public void actualizarActividadCronograma(ActividadCronograma actividadcronograma);

    public void borrarActividadCronograma(ActividadCronograma actividadcronograma);

    public List<ActividadCronograma> consultarActividadCronogramaJoin();

    public ActividadCronograma consultarActividadCronogramaJoinById(Long idActividadCronograma);

}
