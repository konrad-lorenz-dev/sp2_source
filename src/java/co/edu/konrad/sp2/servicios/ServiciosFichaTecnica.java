package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.FichaTecnica;
import java.util.List;
import java.util.Map;

public interface ServiciosFichaTecnica {

    public List<FichaTecnica> consultarFichaTecnica();

    public FichaTecnica consultarFichaTecnicaById(Long idFichaTecnica);

    public void crearFichaTecnica(FichaTecnica fichatecnica);

    public void actualizarFichaTecnica(FichaTecnica fichatecnica);

    public void borrarFichaTecnica(FichaTecnica fichatecnica);

}
