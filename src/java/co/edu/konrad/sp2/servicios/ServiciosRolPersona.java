package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.RolPersona;
import java.util.List;
import java.util.Map;

public interface ServiciosRolPersona {

    public List<RolPersona> consultarRolPersona();

    public RolPersona consultarRolPersonaById(Long idRolPersona);

    public void crearRolPersona(RolPersona rolpersona);

    public void actualizarRolPersona(RolPersona rolpersona);

    public void borrarRolPersona(RolPersona rolpersona);

    public List<RolPersona> consultarRolPersonaJoin();

    public RolPersona consultarRolPersonaJoinById(Long idRolPersona);

    public int buscarPersonaCod(Long idPersona);
    
    public int buscarPersonaid(Long idPersona);
    
    public List<RolPersona> consultarRolPersonaJoinBySeqPersona(Long SeqPersona);

}
