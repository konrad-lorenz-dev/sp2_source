package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.OfertaLaboral;
import java.util.List;
import java.util.Map;

public interface ServiciosOfertaLaboral {

    public List<OfertaLaboral> consultarOfertaLaboral();

    public OfertaLaboral consultarOfertaLaboralById(Long idOfertaLaboral);

    public void crearOfertaLaboral(OfertaLaboral ofertalaboral);

    public void actualizarOfertaLaboral(OfertaLaboral ofertalaboral);

    public void borrarOfertaLaboral(OfertaLaboral ofertalaboral);

    public List<OfertaLaboral> consultarOfertaLaboralJoin();

    public OfertaLaboral consultarOfertaLaboralJoinById(Long idOfertaLaboral);

}
