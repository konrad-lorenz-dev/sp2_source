package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.EvaluarEntidad;
import java.util.List;
import java.util.Map;

public interface ServiciosEvaluarEntidad {

    public List<EvaluarEntidad> consultarEvaluarEntidad();

    public EvaluarEntidad consultarEvaluarEntidadById(Long idEvaluarEntidad);

    public void crearEvaluarEntidad(EvaluarEntidad evaluarentidad);

    public void actualizarEvaluarEntidad(EvaluarEntidad evaluarentidad);

    public void borrarEvaluarEntidad(EvaluarEntidad evaluarentidad);

    public List<EvaluarEntidad> consultarEvaluarEntidadJoin();

    public EvaluarEntidad consultarEvaluarEntidadJoinById(Long idEvaluarEntidad);

}
