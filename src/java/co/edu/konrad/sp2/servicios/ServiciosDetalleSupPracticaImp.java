package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosDetalleSupPractica;
import co.edu.konrad.sp2.bean.DetalleSupPractica;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosDetalleSupPracticaImp implements ServiciosDetalleSupPractica {

    private final static Logger log = Logger.getLogger(ServiciosDetalleSupPracticaImp.class);

    @Override
    public List<DetalleSupPractica> consultarDetalleSupPractica() {
        List<DetalleSupPractica> detallesuppracticas = new ArrayList();
        try {
            detallesuppracticas = (List<DetalleSupPractica>) ManagerConcrete.getManager().obtenerListado("consultarDetalleSupPracticaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallesuppracticas;
    }

    @Override
    public DetalleSupPractica consultarDetalleSupPracticaById(Long idDetalleSupPractica) {
        DetalleSupPractica detallesuppractica = new DetalleSupPractica();
        try {
            detallesuppractica = (DetalleSupPractica) ManagerConcrete.getManager().obtenerRegistro("consultarDetalleSupPractica", idDetalleSupPractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallesuppractica;
    }

    @Override
    public void crearDetalleSupPractica(DetalleSupPractica detallesuppractica) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearDetalleSupPractica", detallesuppractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarDetalleSupPractica(DetalleSupPractica detallesuppractica) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarDetalleSupPractica", detallesuppractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarDetalleSupPractica(DetalleSupPractica detallesuppractica) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarDetalleSupPractica", detallesuppractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<DetalleSupPractica> consultarDetalleSupPracticaJoin() {
        List<DetalleSupPractica> detallesuppracticas = new ArrayList();
        try {
            detallesuppracticas = (List<DetalleSupPractica>) ManagerConcrete.getManager().obtenerListado("consultarDetalleSupPracticaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallesuppracticas;
    }

    @Override
    public DetalleSupPractica consultarDetalleSupPracticaJoinById(Long idDetalleSupPractica) {
        DetalleSupPractica detallesuppractica = new DetalleSupPractica();
        try {
            detallesuppractica = (DetalleSupPractica) ManagerConcrete.getManager().obtenerRegistro("consultarDetalleSupPracticaJoin", idDetalleSupPractica);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallesuppractica;
    }

}
