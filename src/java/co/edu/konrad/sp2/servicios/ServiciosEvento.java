package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Evento;
import java.util.List;
import java.util.Map;

public interface ServiciosEvento {

    public List<Evento> consultarEvento();

    public Evento consultarEventoById(Long idEvento);

    public Evento crearEvento(Evento evento);

    public Evento actualizarEvento(Evento evento);

    public Evento borrarEvento(Evento evento);

    public List<Evento> consultarEventoJoin();

    public Evento consultarEventoJoinById(Long idEvento);

    public int ConsultarUltimoInsertado();
}
