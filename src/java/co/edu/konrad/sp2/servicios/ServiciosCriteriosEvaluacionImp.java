package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosCriteriosEvaluacion;
import co.edu.konrad.sp2.bean.CriteriosEvaluacion;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosCriteriosEvaluacionImp implements ServiciosCriteriosEvaluacion {

    private final static Logger log = Logger.getLogger(ServiciosCriteriosEvaluacionImp.class);

    @Override
    public List<CriteriosEvaluacion> consultarCriteriosEvaluacion() {
        List<CriteriosEvaluacion> criteriosevaluacions = new ArrayList();
        try {
            criteriosevaluacions = (List<CriteriosEvaluacion>) ManagerConcrete.getManager().obtenerListado("consultarCriteriosEvaluacionList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return criteriosevaluacions;
    }

    @Override
    public CriteriosEvaluacion consultarCriteriosEvaluacionById(Long idCriteriosEvaluacion) {
        CriteriosEvaluacion criteriosevaluacion = new CriteriosEvaluacion();
        try {
            criteriosevaluacion = (CriteriosEvaluacion) ManagerConcrete.getManager().obtenerRegistro("consultarCriteriosEvaluacion", idCriteriosEvaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return criteriosevaluacion;
    }

    @Override
    public void crearCriteriosEvaluacion(CriteriosEvaluacion criteriosevaluacion) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearCriteriosEvaluacion", criteriosevaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarCriteriosEvaluacion(CriteriosEvaluacion criteriosevaluacion) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarCriteriosEvaluacion", criteriosevaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarCriteriosEvaluacion(CriteriosEvaluacion criteriosevaluacion) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarCriteriosEvaluacion", criteriosevaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<CriteriosEvaluacion> consultarCriteriosEvaluacionJoin() {
        List<CriteriosEvaluacion> criteriosevaluacions = new ArrayList();
        try {
            criteriosevaluacions = (List<CriteriosEvaluacion>) ManagerConcrete.getManager().obtenerListado("consultarCriteriosEvaluacionListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return criteriosevaluacions;
    }

    @Override
    public CriteriosEvaluacion consultarCriteriosEvaluacionJoinById(Long idCriteriosEvaluacion) {
        CriteriosEvaluacion criteriosevaluacion = new CriteriosEvaluacion();
        try {
            criteriosevaluacion = (CriteriosEvaluacion) ManagerConcrete.getManager().obtenerRegistro("consultarCriteriosEvaluacionJoin", idCriteriosEvaluacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return criteriosevaluacion;
    }

}
