package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Ciudad;
import java.util.List;
import java.util.Map;

public interface ServiciosCiudad {

    public List<Ciudad> consultarCiudad();

    public Ciudad consultarCiudadById(Long idCiudad);

    public void crearCiudad(Ciudad ciudad);

    public void actualizarCiudad(Ciudad ciudad);

    public void borrarCiudad(Ciudad ciudad);

    public List<Ciudad> consultarCiudadJoin();

    public Ciudad consultarCiudadJoinById(Long idCiudad);

}
