package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Persona;
import co.edu.konrad.sp2.bean.UsuarioSesion;
import java.util.List;
import java.util.Map;

public interface ServiciosPersona {

    public List<Persona> consultarPersona();

    public Persona consultarPersonaById(Long idPersona);
    
    public Persona consultarPersonaByCorreo(String correoPersona);

    public void crearPersona(Persona persona);

    public void actualizarPersona(Persona persona);

    public void borrarPersona(Persona persona);

    public List<Persona> consultarPersonaJoin();

    public Persona consultarPersonaJoinById(Long idPersona);

    public Persona ConsultarEstEscolaris(String codPracticante);

    public String validaUsuario(UsuarioSesion usuarioSesion);

    public String consultaSeqUsuario(UsuarioSesion usuarioSesion);

    public Persona ConsultarCodigoPersona(String codPracticante);

    public Persona ConsultarPersonaId(String codPersona);
    
    public Persona ConsultarSupPersonaId(String codPersona);

    public String consultarPrograma(Long idPersona);
    
    public Persona consultarCorreo(String correo);
    
    public Persona consultarPersonaCedula(String cedula);

    public String validaUsuarioEntidad(UsuarioSesion usuarioSesion);
}
