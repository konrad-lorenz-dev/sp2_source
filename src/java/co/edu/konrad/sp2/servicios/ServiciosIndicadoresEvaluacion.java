package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.IndicadoresEvaluacion;
import java.util.List;
import java.util.Map;

public interface ServiciosIndicadoresEvaluacion {

    public List<IndicadoresEvaluacion> consultarIndicadoresEvaluacion();

    public IndicadoresEvaluacion consultarIndicadoresEvaluacionById(Long idIndicadoresEvaluacion);

    public void crearIndicadoresEvaluacion(IndicadoresEvaluacion indicadoresevaluacion);

    public void actualizarIndicadoresEvaluacion(IndicadoresEvaluacion indicadoresevaluacion);

    public void borrarIndicadoresEvaluacion(IndicadoresEvaluacion indicadoresevaluacion);

    public List<IndicadoresEvaluacion> consultarIndicadoresEvaluacionJoin();

    public IndicadoresEvaluacion consultarIndicadoresEvaluacionJoinById(Long idIndicadoresEvaluacion);

}
