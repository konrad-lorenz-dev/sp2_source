package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.CriteriosEvaluacion;
import java.util.List;
import java.util.Map;

public interface ServiciosCriteriosEvaluacion {

    public List<CriteriosEvaluacion> consultarCriteriosEvaluacion();

    public CriteriosEvaluacion consultarCriteriosEvaluacionById(Long idCriteriosEvaluacion);

    public void crearCriteriosEvaluacion(CriteriosEvaluacion criteriosevaluacion);

    public void actualizarCriteriosEvaluacion(CriteriosEvaluacion criteriosevaluacion);

    public void borrarCriteriosEvaluacion(CriteriosEvaluacion criteriosevaluacion);

    public List<CriteriosEvaluacion> consultarCriteriosEvaluacionJoin();

    public CriteriosEvaluacion consultarCriteriosEvaluacionJoinById(Long idCriteriosEvaluacion);

}
