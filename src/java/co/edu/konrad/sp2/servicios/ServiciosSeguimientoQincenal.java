package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.SeguimientoQincenal;
import java.util.List;
import java.util.Map;

public interface ServiciosSeguimientoQincenal {

    public List<SeguimientoQincenal> consultarSeguimientoQincenal();

    public SeguimientoQincenal consultarSeguimientoQincenalById(Long idSeguimientoQincenal);

    public void crearSeguimientoQincenal(SeguimientoQincenal seguimientoqincenal);

    public void actualizarSeguimientoQincenal(SeguimientoQincenal seguimientoqincenal);

    public void borrarSeguimientoQincenal(SeguimientoQincenal seguimientoqincenal);

    public List<SeguimientoQincenal> consultarSeguimientoQincenalJoin();

    public SeguimientoQincenal consultarSeguimientoQincenalJoinById(Long idSeguimientoQincenal);

}
