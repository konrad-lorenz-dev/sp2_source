package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosNovedad;
import co.edu.konrad.sp2.bean.Novedad;
import co.edu.konrad.sp2.bean.NovedadPersona;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosNovedadImp implements ServiciosNovedad {

    private final static Logger log = Logger.getLogger(ServiciosNovedadImp.class);

    @Override
    public List<Novedad> consultarNovedad() {
        List<Novedad> novedads = new ArrayList();
        try {
            novedads = (List<Novedad>) ManagerConcrete.getManager().obtenerListado("consultarNovedadList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedads;
    }

    @Override
    public Novedad consultarNovedadById(Long idNovedad) {
        Novedad novedad = new Novedad();
        try {
            novedad = (Novedad) ManagerConcrete.getManager().obtenerRegistro("consultarNovedad", idNovedad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedad;
    }

    @Override
    public String crearNovedad(Novedad novedad) throws Exception {
        String constantes = "";
        try {
            ManagerConcrete.getManager().insertarRegistro("crearNovedad", novedad);
            constantes = Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception("No se puede Crear Novedad");
        }
        return constantes;
    }

    @Override
    public void actualizarNovedad(Novedad novedad) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarNovedad", novedad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarNovedad(Novedad novedad) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarNovedad", novedad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<Novedad> consultarNovedadJoin() {
        List<Novedad> novedads = new ArrayList();
        try {
            novedads = (List<Novedad>) ManagerConcrete.getManager().obtenerListado("consultarNovedadListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedads;
    }

    @Override
    public List<Novedad> consultarNovedadesPorPersonaJoin(Long idPersona) { //se muestra las novedad que ha realizado cada persona
        List<Novedad> novedads = new ArrayList();
        try {
            novedads = (List<Novedad>) ManagerConcrete.getManager().obtenerListado("consultarNovedadListJoin", idPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedads;
    }

    @Override
    public Novedad consultarNovedadJoinById(Long idNovedad) {
        Novedad novedad = new Novedad();
        try {
            novedad = (Novedad) ManagerConcrete.getManager().obtenerRegistro("consultarNovedadJoin", idNovedad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedad;
    }

    @Override
    public List<Novedad> consultarNovedadGrillaPostulados(NovedadPersona novedades) {
        List<Novedad> novedad = new ArrayList();
        try {
            novedad = (List<Novedad>) ManagerConcrete.getManager().obtenerListado("consultarNovedadGrillaPostulados", novedades);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedad;
    }

}
