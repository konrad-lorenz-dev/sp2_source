package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosActividadCronograma;
import co.edu.konrad.sp2.bean.ActividadCronograma;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosActividadCronogramaImp implements ServiciosActividadCronograma {

    private final static Logger log = Logger.getLogger(ServiciosActividadCronogramaImp.class);

    @Override
    public List<ActividadCronograma> consultarActividadCronograma() {
        List<ActividadCronograma> actividadcronogramas = new ArrayList();
        try {
            actividadcronogramas = (List<ActividadCronograma>) ManagerConcrete.getManager().obtenerListado("consultarActividadCronogramaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return actividadcronogramas;
    }

    @Override
    public ActividadCronograma consultarActividadCronogramaById(Long idActividadCronograma) {
        ActividadCronograma actividadcronograma = new ActividadCronograma();
        try {
            actividadcronograma = (ActividadCronograma) ManagerConcrete.getManager().obtenerRegistro("consultarActividadCronograma", idActividadCronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return actividadcronograma;
    }

    @Override
    public void crearActividadCronograma(ActividadCronograma actividadcronograma) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearActividadCronograma", actividadcronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarActividadCronograma(ActividadCronograma actividadcronograma) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarActividadCronograma", actividadcronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarActividadCronograma(ActividadCronograma actividadcronograma) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarActividadCronograma", actividadcronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<ActividadCronograma> consultarActividadCronogramaJoin() {
        List<ActividadCronograma> actividadcronogramas = new ArrayList();
        try {
            actividadcronogramas = (List<ActividadCronograma>) ManagerConcrete.getManager().obtenerListado("consultarActividadCronogramaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return actividadcronogramas;
    }

    @Override
    public ActividadCronograma consultarActividadCronogramaJoinById(Long idActividadCronograma) {
        ActividadCronograma actividadcronograma = new ActividadCronograma();
        try {
            actividadcronograma = (ActividadCronograma) ManagerConcrete.getManager().obtenerRegistro("consultarActividadCronogramaJoin", idActividadCronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return actividadcronograma;
    }

}
