package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.HorarioPractica;
import java.util.List;
import java.util.Map;

public interface ServiciosHorarioPractica {

    public List<HorarioPractica> consultarHorarioPractica();

    public HorarioPractica consultarHorarioPracticaById(Long idHorarioPractica);

    public void crearHorarioPractica(HorarioPractica horariopractica);

    public void actualizarHorarioPractica(HorarioPractica horariopractica);

    public void borrarHorarioPractica(HorarioPractica horariopractica);

    public List<HorarioPractica> consultarHorarioPracticaJoin();

    public HorarioPractica consultarHorarioPracticaJoinById(Long idHorarioPractica);

}
