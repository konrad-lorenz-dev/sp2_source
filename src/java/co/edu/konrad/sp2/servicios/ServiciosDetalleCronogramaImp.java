package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosDetalleCronograma;
import co.edu.konrad.sp2.bean.DetalleCronograma;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosDetalleCronogramaImp implements ServiciosDetalleCronograma {

    private final static Logger log = Logger.getLogger(ServiciosDetalleCronogramaImp.class);

    @Override
    public List<DetalleCronograma> consultarDetalleCronograma() {
        List<DetalleCronograma> detallecronogramas = new ArrayList();
        try {
            detallecronogramas = (List<DetalleCronograma>) ManagerConcrete.getManager().obtenerListado("consultarDetalleCronogramaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallecronogramas;
    }

    @Override
    public DetalleCronograma consultarDetalleCronogramaById(Long idDetalleCronograma) {
        DetalleCronograma detallecronograma = new DetalleCronograma();
        try {
            detallecronograma = (DetalleCronograma) ManagerConcrete.getManager().obtenerRegistro("consultarDetalleCronograma", idDetalleCronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallecronograma;
    }

    @Override
    public void crearDetalleCronograma(DetalleCronograma detallecronograma) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearDetalleCronograma", detallecronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarDetalleCronograma(DetalleCronograma detallecronograma) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarDetalleCronograma", detallecronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarDetalleCronograma(DetalleCronograma detallecronograma) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarDetalleCronograma", detallecronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<DetalleCronograma> consultarDetalleCronogramaJoin() {
        List<DetalleCronograma> detallecronogramas = new ArrayList();
        try {
            detallecronogramas = (List<DetalleCronograma>) ManagerConcrete.getManager().obtenerListado("consultarDetalleCronogramaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallecronogramas;
    }

    @Override
    public DetalleCronograma consultarDetalleCronogramaJoinById(Long idDetalleCronograma) {
        DetalleCronograma detallecronograma = new DetalleCronograma();
        try {
            detallecronograma = (DetalleCronograma) ManagerConcrete.getManager().obtenerRegistro("consultarDetalleCronogramaJoin", idDetalleCronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return detallecronograma;
    }

}
