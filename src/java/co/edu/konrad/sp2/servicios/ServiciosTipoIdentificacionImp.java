package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosTipoIdentificacion;
import co.edu.konrad.sp2.bean.TipoIdentificacion;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosTipoIdentificacionImp implements ServiciosTipoIdentificacion {

    private final static Logger log = Logger.getLogger(ServiciosTipoIdentificacionImp.class);

    @Override
    public List<TipoIdentificacion> consultarTipoIdentificacion() {
        List<TipoIdentificacion> tipoidentificacions = new ArrayList();
        try {
            tipoidentificacions = (List<TipoIdentificacion>) ManagerConcrete.getManager().obtenerListado("consultarTipoIdentificacionList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tipoidentificacions;
    }

    @Override
    public TipoIdentificacion consultarTipoIdentificacionById(Long idTipoIdentificacion) {
        TipoIdentificacion tipoidentificacion = new TipoIdentificacion();
        try {
            tipoidentificacion = (TipoIdentificacion) ManagerConcrete.getManager().obtenerRegistro("consultarTipoIdentificacion", idTipoIdentificacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tipoidentificacion;
    }

    @Override
    public void crearTipoIdentificacion(TipoIdentificacion tipoidentificacion) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearTipoIdentificacion", tipoidentificacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarTipoIdentificacion(TipoIdentificacion tipoidentificacion) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarTipoIdentificacion", tipoidentificacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarTipoIdentificacion(TipoIdentificacion tipoidentificacion) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarTipoIdentificacion", tipoidentificacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
