package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.NovedadPersona;
import java.util.List;
import java.util.Map;

public interface ServiciosNovedadPersona {

    public List<NovedadPersona> consultarNovedadPersona();

    public NovedadPersona consultarNovedadPersonaById(Long idNovedadPersona);

    public void crearNovedadPersona(NovedadPersona novedadpersona);

    public void actualizarNovedadPersona(NovedadPersona novedadpersona);

    public void borrarNovedadPersona(NovedadPersona novedadpersona);

    public List<NovedadPersona> consultarNovedadPersonaJoin();

    public NovedadPersona consultarNovedadPersonaJoinById(Long idNovedadPersona);
    
    public int consultarNovedadPersonaId()throws Exception;

}
