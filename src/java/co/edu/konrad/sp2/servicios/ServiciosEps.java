package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Eps;
import java.util.List;
import java.util.Map;

public interface ServiciosEps {

    public List<Eps> consultarEps();

    public Eps consultarEpsById(Long idEps);

    public void crearEps(Eps eps);

    public void actualizarEps(Eps eps);

    public void borrarEps(Eps eps);

}
