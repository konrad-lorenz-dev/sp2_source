package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Cronograma;
import java.util.List;
import java.util.Map;

public interface ServiciosCronograma {

    public List<Cronograma> consultarCronograma();

    public Cronograma consultarCronogramaById(Long idCronograma);

    public void crearCronograma(Cronograma cronograma);

    public void actualizarCronograma(Cronograma cronograma);

    public void borrarCronograma(Cronograma cronograma);

    public List<Cronograma> consultarCronogramaJoin();

    public Cronograma consultarCronogramaJoinById(Long idCronograma);

}
