package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.ControlDiarioEstudiante;
import java.util.List;
import java.util.Map;

public interface ServiciosControlDiarioEstudiante {

    public List<ControlDiarioEstudiante> consultarControlDiarioEstudiante();

    public ControlDiarioEstudiante consultarControlDiarioEstudianteById(Long idControlDiarioEstudiante);

    public void crearControlDiarioEstudiante(ControlDiarioEstudiante controldiarioestudiante);

    public void actualizarControlDiarioEstudiante(ControlDiarioEstudiante controldiarioestudiante);

    public void borrarControlDiarioEstudiante(ControlDiarioEstudiante controldiarioestudiante);

    public List<ControlDiarioEstudiante> consultarControlDiarioEstudianteJoin();

    public ControlDiarioEstudiante consultarControlDiarioEstudianteJoinById(Long idControlDiarioEstudiante);

}
