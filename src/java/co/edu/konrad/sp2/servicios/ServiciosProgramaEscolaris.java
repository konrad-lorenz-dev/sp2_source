package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.ProgramaEscolaris;
import java.util.List;

public interface ServiciosProgramaEscolaris {

    public List<ProgramaEscolaris> consultarProgramaEscolaris();

    public ProgramaEscolaris consultarProgramaEscolarisById(Long idProgramaEscolaris);

    public void crearProgramaEscolaris(ProgramaEscolaris programaescolaris);

    public void actualizarProgramaEscolaris(ProgramaEscolaris programaescolaris);

    public void borrarProgramaEscolaris(ProgramaEscolaris programaescolaris);

    public List<ProgramaEscolaris> consultarProgramaEscolarisJoin();

    public ProgramaEscolaris consultarProgramaEscolarisJoinById(Long idProgramaEscolaris);

}
