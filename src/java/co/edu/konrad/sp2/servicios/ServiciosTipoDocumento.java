package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.TipoDocumento;
import java.util.List;
import java.util.Map;

public interface ServiciosTipoDocumento {

    public List<TipoDocumento> consultarTipoDocumento();

    public TipoDocumento consultarTipoDocumentoById(Long idTipoDocumento);

    public void crearTipoDocumento(TipoDocumento tipodocumento);

    public void actualizarTipoDocumento(TipoDocumento tipodocumento);

    public void borrarTipoDocumento(TipoDocumento tipodocumento);

}
