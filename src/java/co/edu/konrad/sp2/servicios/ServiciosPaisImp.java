package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosPais;
import co.edu.konrad.sp2.bean.Pais;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosPaisImp implements ServiciosPais {

    private final static Logger log = Logger.getLogger(ServiciosPaisImp.class);

    @Override
    public List<Pais> consultarPais() {
        List<Pais> paiss = new ArrayList();
        try {
            paiss = (List<Pais>) ManagerConcrete.getManager().obtenerListado("consultarPaisList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return paiss;
    }

    @Override
    public Pais consultarPaisById(Long idPais) {
        Pais pais = new Pais();
        try {
            pais = (Pais) ManagerConcrete.getManager().obtenerRegistro("consultarPais", idPais);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return pais;
    }

    @Override
    public void crearPais(Pais pais) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearPais", pais);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarPais(Pais pais) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarPais", pais);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarPais(Pais pais) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarPais", pais);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
