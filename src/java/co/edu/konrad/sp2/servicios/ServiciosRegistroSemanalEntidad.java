package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.RegistroSemanalEntidad;
import java.util.List;
import java.util.Map;

public interface ServiciosRegistroSemanalEntidad {

    public List<RegistroSemanalEntidad> consultarRegistroSemanalEntidad();

    public RegistroSemanalEntidad consultarRegistroSemanalEntidadById(Long idRegistroSemanalEntidad);

    public void crearRegistroSemanalEntidad(RegistroSemanalEntidad registrosemanalentidad);

    public String actualizarRegistroSemanalEntidad(RegistroSemanalEntidad registrosemanalentidad);

    public void borrarRegistroSemanalEntidad(RegistroSemanalEntidad registrosemanalentidad);

    public List<RegistroSemanalEntidad> consultarRegistroSemanalEntidadJoin();

    public RegistroSemanalEntidad consultarRegistroSemanalEntidadJoinById(Long idRegistroSemanalEntidad);
    
    public List<RegistroSemanalEntidad> consultarRegistroSemanalEntidadEstadoValido(Long idPersonaPracticante);
    
    public List<RegistroSemanalEntidad> consultarRegistroSemanalEntidadHistorial(Long idPersonaPracticante);
}
