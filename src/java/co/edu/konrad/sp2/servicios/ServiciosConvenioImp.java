package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosConvenio;
import co.edu.konrad.sp2.bean.Convenio;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosConvenioImp implements ServiciosConvenio {

    private final static Logger log = Logger.getLogger(ServiciosConvenioImp.class);

    @Override
    public List<Convenio> consultarConvenio() {
        List<Convenio> convenios = new ArrayList();
        try {
            convenios = (List<Convenio>) ManagerConcrete.getManager().obtenerListado("consultarConvenioList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return convenios;
    }

    @Override
    public Convenio consultarConvenioById(Long idConvenio) {
        Convenio convenio = new Convenio();
        try {
            convenio = (Convenio) ManagerConcrete.getManager().obtenerRegistro("consultarConvenio", idConvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return convenio;
    }

    @Override
    public void crearConvenio(Convenio convenio) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearConvenio", convenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarConvenio(Convenio convenio) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarConvenio", convenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarConvenio(Convenio convenio) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarConvenio", convenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<Convenio> consultarConvenioJoin() {
        List<Convenio> convenios = new ArrayList();
        try {
            convenios = (List<Convenio>) ManagerConcrete.getManager().obtenerListado("consultarConvenioListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return convenios;
    }

    @Override
    public Convenio consultarConvenioJoinById(Long idConvenio) {
        Convenio convenio = new Convenio();
        try {
            convenio = (Convenio) ManagerConcrete.getManager().obtenerRegistro("consultarConvenioJoin", idConvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return convenio;
    }

}
