package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosSesionConsejeria;
import co.edu.konrad.sp2.bean.SesionConsejeria;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import co.edu.konrad.sp2.constant.Constantes;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosSesionConsejeriaImp implements ServiciosSesionConsejeria {

    private final static Logger log = Logger.getLogger(ServiciosSesionConsejeriaImp.class);

    @Override
    public List<SesionConsejeria> consultarSesionConsejeria() {
        List<SesionConsejeria> sesionconsejerias = new ArrayList();
        try {
            sesionconsejerias = (List<SesionConsejeria>) ManagerConcrete.getManager().obtenerListado("consultarSesionConsejeriaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionconsejerias;
    }

    @Override
    public SesionConsejeria consultarSesionConsejeriaById(Long idSesionConsejeria) {
        SesionConsejeria sesionconsejeria = new SesionConsejeria();
        try {
            sesionconsejeria = (SesionConsejeria) ManagerConcrete.getManager().obtenerRegistro("consultarSesionConsejeria", idSesionConsejeria);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionconsejeria;
    }

    @Override
    public SesionConsejeria crearSesionConsejeria(SesionConsejeria sesionconsejeria) {
        SesionConsejeria constante = new SesionConsejeria();
        try {
            ManagerConcrete.getManager().insertarRegistro("crearSesionConsejeria", sesionconsejeria);
            constante.setConstante(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            constante.setConstante(Constantes.FAILED);
        }
        return constante;
    }

    @Override
    public void actualizarSesionConsejeria(SesionConsejeria sesionconsejeria) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarSesionConsejeria", sesionconsejeria);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarSesionConsejeria(SesionConsejeria sesionconsejeria) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarSesionConsejeria", sesionconsejeria);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<SesionConsejeria> consultarSesionConsejeriaJoin() {
        List<SesionConsejeria> sesionconsejerias = new ArrayList();
        try {
            sesionconsejerias = (List<SesionConsejeria>) ManagerConcrete.getManager().obtenerListado("consultarSesionConsejeriaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionconsejerias;
    }

    @Override
    public List<SesionConsejeria> consultarSesionConsejeriaJoinPorRol(String usrRol) {
        List<SesionConsejeria> sesionconsejerias = new ArrayList();
        try {
            sesionconsejerias = (List<SesionConsejeria>) ManagerConcrete.getManager().obtenerListado("consultarSesionConsejeriaListJoinPorRol", usrRol);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionconsejerias;
    }

    @Override
    public SesionConsejeria consultarSesionConsejeriaJoinById(Long idSesionConsejeria) {
        SesionConsejeria sesionconsejeria = new SesionConsejeria();
        try {
            sesionconsejeria = (SesionConsejeria) ManagerConcrete.getManager().obtenerRegistro("consultarSesionConsejeriaJoin", idSesionConsejeria);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionconsejeria;
    }

    @Override
    public SesionConsejeria consultarSesionConsejeriaJoinPorCcaYSeqPracticante(SesionConsejeria sesionConsejeria) {
        SesionConsejeria sesionconsejeri = new SesionConsejeria();
        try {
            sesionconsejeri = (SesionConsejeria) ManagerConcrete.getManager().obtenerRegistro("consultarSesionConsejeriaJoinPorCcaYSeqPracticanteDistinct", sesionConsejeria);
            sesionconsejeri = (SesionConsejeria) ManagerConcrete.getManager().obtenerRegistro("consultarSesionConsejeriaJoin", sesionconsejeri.getSeqSesionConsejeria());
            sesionconsejeri.setConstante(Constantes.OK);
        } catch (Exception e) {
            sesionconsejeri = new SesionConsejeria();
            sesionconsejeri.setConstante(Constantes.FAILED);
            log.error(e.getMessage());
        }
        return sesionconsejeri;
    }

    @Override
    public List<SesionConsejeria> consultarSesionConsejeriaJoinPorCCA(Long IdSeqCca) {
        List<SesionConsejeria> sesionconsejerias = new ArrayList();
        try {
        List<SesionConsejeria> lista = (List<SesionConsejeria>) ManagerConcrete.getManager().obtenerListado("consultarSesionConsejeriaJoinPorCCADistinct", IdSeqCca);
        for(SesionConsejeria l : lista){
          SesionConsejeria temp = (SesionConsejeria) ManagerConcrete.getManager().obtenerRegistro("consultarSesionConsejeriaJoin", l);  
          if (temp != null){
           sesionconsejerias.add(temp);   
          }     
        }  
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return sesionconsejerias;
    }

}
