package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.DetallePracticante;
import java.util.List;
import java.util.Map;

public interface ServiciosDetallePracticante {

    public List<DetallePracticante> consultarDetallePracticante();

    public DetallePracticante consultarDetallePracticanteById(Long idDetallePracticante);

    public void crearDetallePracticante(DetallePracticante detallepracticante);

    public void actualizarDetallePracticante(DetallePracticante detallepracticante);

    public void borrarDetallePracticante(DetallePracticante detallepracticante);

    public List<DetallePracticante> consultarDetallePracticanteJoin();

    public DetallePracticante consultarDetallePracticanteJoinById(Long idDetallePracticante);

    public List<DetallePracticante> consultarDetallePracticanteEstadoPracticanteJoin(String EstadoPracticante);
    
    public void actualizarDetallePracticanteEstadoPractica(DetallePracticante detallePracticante);
    
}
