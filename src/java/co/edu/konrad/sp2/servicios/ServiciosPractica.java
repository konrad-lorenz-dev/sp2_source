package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Practica;
import java.util.List;
import java.util.Map;

public interface ServiciosPractica {

    public List<Practica> consultarPractica();

    public Practica consultarPracticaById(Long idPractica);

    public void crearPractica(Practica practica);

    public void actualizarPractica(Practica practica);

    public void borrarPractica(Practica practica);

    public List<Practica> consultarPracticaJoin();

    public Practica consultarPracticaJoinById(Long idPractica);
    
    public Practica informacionPractica(Long idPracticante);
    
    public Practica consultarEstadoPractica(Long idDetallePracticante);

}
