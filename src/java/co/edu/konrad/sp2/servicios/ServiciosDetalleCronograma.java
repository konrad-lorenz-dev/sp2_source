package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.DetalleCronograma;
import java.util.List;
import java.util.Map;

public interface ServiciosDetalleCronograma {

    public List<DetalleCronograma> consultarDetalleCronograma();

    public DetalleCronograma consultarDetalleCronogramaById(Long idDetalleCronograma);

    public void crearDetalleCronograma(DetalleCronograma detallecronograma);

    public void actualizarDetalleCronograma(DetalleCronograma detallecronograma);

    public void borrarDetalleCronograma(DetalleCronograma detallecronograma);

    public List<DetalleCronograma> consultarDetalleCronogramaJoin();

    public DetalleCronograma consultarDetalleCronogramaJoinById(Long idDetalleCronograma);

}
