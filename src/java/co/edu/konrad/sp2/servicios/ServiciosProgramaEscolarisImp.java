package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosProgramaEscolaris;
import co.edu.konrad.sp2.bean.ProgramaEscolaris;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosProgramaEscolarisImp implements ServiciosProgramaEscolaris {

    private final static Logger log = Logger.getLogger(ServiciosProgramaEscolarisImp.class);

    @Override
    public List<ProgramaEscolaris> consultarProgramaEscolaris() {
        List<ProgramaEscolaris> programaescolariss = new ArrayList();
        try {
            programaescolariss = (List<ProgramaEscolaris>) ManagerConcrete.getManager().obtenerListado("consultarProgramaEscolarisList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return programaescolariss;
    }

    @Override
    public ProgramaEscolaris consultarProgramaEscolarisById(Long idProgramaEscolaris) {
        ProgramaEscolaris programaescolaris = new ProgramaEscolaris();
        try {
            programaescolaris = (ProgramaEscolaris) ManagerConcrete.getManager().obtenerRegistro("consultarProgramaEscolaris", idProgramaEscolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return programaescolaris;
    }

    @Override
    public void crearProgramaEscolaris(ProgramaEscolaris programaescolaris) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearProgramaEscolaris", programaescolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarProgramaEscolaris(ProgramaEscolaris programaescolaris) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarProgramaEscolaris", programaescolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarProgramaEscolaris(ProgramaEscolaris programaescolaris) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarProgramaEscolaris", programaescolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<ProgramaEscolaris> consultarProgramaEscolarisJoin() {
        List<ProgramaEscolaris> programaescolariss = new ArrayList();
        try {
            programaescolariss = (List<ProgramaEscolaris>) ManagerConcrete.getManager().obtenerListado("consultarProgramaEscolarisListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return programaescolariss;
    }

    @Override
    public ProgramaEscolaris consultarProgramaEscolarisJoinById(Long idProgramaEscolaris) {
        ProgramaEscolaris programaescolaris = new ProgramaEscolaris();
        try {
            programaescolaris = (ProgramaEscolaris) ManagerConcrete.getManager().obtenerRegistro("consultarProgramaEscolarisJoin", idProgramaEscolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return programaescolaris;
    }

}
