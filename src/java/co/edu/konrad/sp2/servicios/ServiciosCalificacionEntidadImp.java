package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosCalificacionEntidad;
import co.edu.konrad.sp2.bean.CalificacionEntidad;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosCalificacionEntidadImp implements ServiciosCalificacionEntidad {

    private final static Logger log = Logger.getLogger(ServiciosCalificacionEntidadImp.class);

    @Override
    public List<CalificacionEntidad> consultarCalificacionEntidad() {
        List<CalificacionEntidad> calificacionentidads = new ArrayList();
        try {
            calificacionentidads = (List<CalificacionEntidad>) ManagerConcrete.getManager().obtenerListado("consultarCalificacionEntidadList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return calificacionentidads;
    }

    @Override
    public CalificacionEntidad consultarCalificacionEntidadById(Long idCalificacionEntidad) {
        CalificacionEntidad calificacionentidad = new CalificacionEntidad();
        try {
            calificacionentidad = (CalificacionEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarCalificacionEntidad", idCalificacionEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return calificacionentidad;
    }

    @Override
    public void crearCalificacionEntidad(CalificacionEntidad calificacionentidad) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearCalificacionEntidad", calificacionentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarCalificacionEntidad(CalificacionEntidad calificacionentidad) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarCalificacionEntidad", calificacionentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarCalificacionEntidad(CalificacionEntidad calificacionentidad) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarCalificacionEntidad", calificacionentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<CalificacionEntidad> consultarCalificacionEntidadJoin() {
        List<CalificacionEntidad> calificacionentidads = new ArrayList();
        try {
            calificacionentidads = (List<CalificacionEntidad>) ManagerConcrete.getManager().obtenerListado("consultarCalificacionEntidadListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return calificacionentidads;
    }

    @Override
    public CalificacionEntidad consultarCalificacionEntidadJoinById(Long idCalificacionEntidad) {
        CalificacionEntidad calificacionentidad = new CalificacionEntidad();
        try {
            calificacionentidad = (CalificacionEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarCalificacionEntidadJoin", idCalificacionEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return calificacionentidad;
    }

}
