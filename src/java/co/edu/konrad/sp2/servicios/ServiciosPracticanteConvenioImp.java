package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosPracticanteConvenio;
import co.edu.konrad.sp2.bean.PracticanteConvenio;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosPracticanteConvenioImp implements ServiciosPracticanteConvenio {

    private final static Logger log = Logger.getLogger(ServiciosPracticanteConvenioImp.class);

    @Override
    public List<PracticanteConvenio> consultarPracticanteConvenio() {
        List<PracticanteConvenio> practicanteconvenios = new ArrayList();
        try {
            practicanteconvenios = (List<PracticanteConvenio>) ManagerConcrete.getManager().obtenerListado("consultarPracticanteConvenioList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return practicanteconvenios;
    }

    @Override
    public PracticanteConvenio consultarPracticanteConvenioById(Long idPracticanteConvenio) {
        PracticanteConvenio practicanteconvenio = new PracticanteConvenio();
        try {
            practicanteconvenio = (PracticanteConvenio) ManagerConcrete.getManager().obtenerRegistro("consultarPracticanteConvenio", idPracticanteConvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return practicanteconvenio;
    }

    @Override
    public void crearPracticanteConvenio(PracticanteConvenio practicanteconvenio) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearPracticanteConvenio", practicanteconvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarPracticanteConvenio(PracticanteConvenio practicanteconvenio) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarPracticanteConvenio", practicanteconvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarPracticanteConvenio(PracticanteConvenio practicanteconvenio) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarPracticanteConvenio", practicanteconvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<PracticanteConvenio> consultarPracticanteConvenioJoin() {
        List<PracticanteConvenio> practicanteconvenios = new ArrayList();
        try {
            practicanteconvenios = (List<PracticanteConvenio>) ManagerConcrete.getManager().obtenerListado("consultarPracticanteConvenioListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return practicanteconvenios;
    }

    @Override
    public PracticanteConvenio consultarPracticanteConvenioJoinById(Long idPracticanteConvenio) {
        PracticanteConvenio practicanteconvenio = new PracticanteConvenio();
        try {
            practicanteconvenio = (PracticanteConvenio) ManagerConcrete.getManager().obtenerRegistro("consultarPracticanteConvenioJoin", idPracticanteConvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return practicanteconvenio;
    }

    @Override
    public PracticanteConvenio consultarPracticantePorSeqPracticante(Long idSeqPracticante) {
        PracticanteConvenio practicanteconvenio = new PracticanteConvenio();
        try {
            practicanteconvenio = (PracticanteConvenio) ManagerConcrete.getManager().obtenerRegistro("consultarPracticantePorSeqPracticante", idSeqPracticante);
            if (practicanteconvenio == null) {
                practicanteconvenio = new PracticanteConvenio();
                practicanteconvenio.setConstante(Constantes.FAILED);
            } else {
                practicanteconvenio.setConstante(Constantes.OK);
            }
        } catch (Exception e) {
            practicanteconvenio.setConstante(Constantes.FAILED);
            log.error(e.getMessage());
        }
        return practicanteconvenio;
    }

}
