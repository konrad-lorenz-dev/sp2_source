package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosRolPersona;
import co.edu.konrad.sp2.bean.RolPersona;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosRolPersonaImp implements ServiciosRolPersona {

    private final static Logger log = Logger.getLogger(ServiciosRolPersonaImp.class);

    @Override
    public List<RolPersona> consultarRolPersona() {
        List<RolPersona> rolpersonas = new ArrayList();
        try {
            rolpersonas = (List<RolPersona>) ManagerConcrete.getManager().obtenerListado("consultarRolPersonaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return rolpersonas;
    }

    @Override
    public RolPersona consultarRolPersonaById(Long idRolPersona) {
        RolPersona rolpersona = new RolPersona();
        try {
            rolpersona = (RolPersona) ManagerConcrete.getManager().obtenerRegistro("consultarRolPersona", idRolPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return rolpersona;
    }

    @Override
    public void crearRolPersona(RolPersona rolpersona) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearRolPersona", rolpersona);
            rolpersona.setConstante(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            rolpersona.setConstante(Constantes.FAILED);
        }
    }

    @Override
    public void actualizarRolPersona(RolPersona rolpersona) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarRolPersona", rolpersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarRolPersona(RolPersona rolpersona) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarRolPersona", rolpersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<RolPersona> consultarRolPersonaJoin() {
        List<RolPersona> rolpersonas = new ArrayList();
        try {
            rolpersonas = (List<RolPersona>) ManagerConcrete.getManager().obtenerListado("consultarRolPersonaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return rolpersonas;
    }

    @Override
    public RolPersona consultarRolPersonaJoinById(Long idRolPersona) {
        RolPersona rolpersona = new RolPersona();
        try {
            rolpersona = (RolPersona) ManagerConcrete.getManager().obtenerRegistro("consultarRolPersonaJoin", idRolPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return rolpersona;
    }

    @Override
    public List<RolPersona> consultarRolPersonaJoinBySeqPersona(Long SeqPersona) {
        List<RolPersona> rolpersona = new ArrayList<>();
        try {
            rolpersona = (List<RolPersona>) ManagerConcrete.getManager().obtenerListado("consultarRolPersonaBySeqPersona", SeqPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return rolpersona;
    }

    @Override
    public int buscarPersonaCod(Long idPersona) {
        int idRolpersona = 0;
        try {
            idRolpersona = (int) ManagerConcrete.getManager().obtenerRegistro("consultarCodPersona", idPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return idRolpersona;
    }

    @Override
    public int buscarPersonaid(Long idPersona) {
        int idRolpersona = 0;
        try {
            idRolpersona = (int) ManagerConcrete.getManager().obtenerRegistro("consultarIdPersonaRol", idPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return idRolpersona;
    }

}
