package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.DetalleSupEntidad;
import java.util.List;
import java.util.Map;

public interface ServiciosDetalleSupEntidad {

    public List<DetalleSupEntidad> consultarDetalleSupEntidad();

    public DetalleSupEntidad consultarDetalleSupEntidadById(Long idDetalleSupEntidad);

    public void crearDetalleSupEntidad(DetalleSupEntidad detallesupentidad);

    public void actualizarDetalleSupEntidad(DetalleSupEntidad detallesupentidad);

    public void borrarDetalleSupEntidad(DetalleSupEntidad detallesupentidad);

    public List<DetalleSupEntidad> consultarDetalleSupEntidadJoin();

    public DetalleSupEntidad consultarDetalleSupEntidadJoinById(Long idDetalleSupEntidad);

}
