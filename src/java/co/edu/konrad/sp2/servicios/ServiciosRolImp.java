package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosRol;
import co.edu.konrad.sp2.bean.Rol;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosRolImp implements ServiciosRol {

    private final static Logger log = Logger.getLogger(ServiciosRolImp.class);

    @Override
    public List<Rol> consultarRol() {
        List<Rol> rols = new ArrayList();
        try {
            rols = (List<Rol>) ManagerConcrete.getManager().obtenerListado("consultarRolList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return rols;
    }

    @Override
    public Rol consultarRolById(Long idRol) {
        Rol rol = new Rol();
        try {
            rol = (Rol) ManagerConcrete.getManager().obtenerRegistro("consultarRol", idRol);
            if(rol == null){
                rol = new Rol();
                rol.setConstantes(Constantes.FAILED);
            }else {
                rol.setConstantes(Constantes.OK);
            }
        } catch (Exception e) {
            rol = new Rol();
            rol.setConstantes(Constantes.FAILED);
            log.error(e.getMessage());
        }
        return rol;
    }

    @Override
    public void crearRol(Rol rol) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearRol", rol);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarRol(Rol rol) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarRol", rol);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarRol(Rol rol) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarRol", rol);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
