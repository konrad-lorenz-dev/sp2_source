package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosControlDiarioEstudiante;
import co.edu.konrad.sp2.bean.ControlDiarioEstudiante;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosControlDiarioEstudianteImp implements ServiciosControlDiarioEstudiante {

    private final static Logger log = Logger.getLogger(ServiciosControlDiarioEstudianteImp.class);

    @Override
    public List<ControlDiarioEstudiante> consultarControlDiarioEstudiante() {
        List<ControlDiarioEstudiante> controldiarioestudiantes = new ArrayList();
        try {
            controldiarioestudiantes = (List<ControlDiarioEstudiante>) ManagerConcrete.getManager().obtenerListado("consultarControlDiarioEstudianteList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return controldiarioestudiantes;
    }

    @Override
    public ControlDiarioEstudiante consultarControlDiarioEstudianteById(Long idControlDiarioEstudiante) {
        ControlDiarioEstudiante controldiarioestudiante = new ControlDiarioEstudiante();
        try {
            controldiarioestudiante = (ControlDiarioEstudiante) ManagerConcrete.getManager().obtenerRegistro("consultarControlDiarioEstudiante", idControlDiarioEstudiante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return controldiarioestudiante;
    }

    @Override
    public void crearControlDiarioEstudiante(ControlDiarioEstudiante controldiarioestudiante) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearControlDiarioEstudiante", controldiarioestudiante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarControlDiarioEstudiante(ControlDiarioEstudiante controldiarioestudiante) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarControlDiarioEstudiante", controldiarioestudiante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarControlDiarioEstudiante(ControlDiarioEstudiante controldiarioestudiante) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarControlDiarioEstudiante", controldiarioestudiante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<ControlDiarioEstudiante> consultarControlDiarioEstudianteJoin() {
        List<ControlDiarioEstudiante> controldiarioestudiantes = new ArrayList();
        try {
            controldiarioestudiantes = (List<ControlDiarioEstudiante>) ManagerConcrete.getManager().obtenerListado("consultarControlDiarioEstudianteListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return controldiarioestudiantes;
    }

    @Override
    public ControlDiarioEstudiante consultarControlDiarioEstudianteJoinById(Long idControlDiarioEstudiante) {
        ControlDiarioEstudiante controldiarioestudiante = new ControlDiarioEstudiante();
        try {
            controldiarioestudiante = (ControlDiarioEstudiante) ManagerConcrete.getManager().obtenerRegistro("consultarControlDiarioEstudianteJoin", idControlDiarioEstudiante);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return controldiarioestudiante;
    }

}
