package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosNovedadPersona;
import co.edu.konrad.sp2.bean.NovedadPersona;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosNovedadPersonaImp implements ServiciosNovedadPersona {

    private final static Logger log = Logger.getLogger(ServiciosNovedadPersonaImp.class);

    @Override
    public List<NovedadPersona> consultarNovedadPersona() {
        List<NovedadPersona> novedadpersonas = new ArrayList();
        try {
            novedadpersonas = (List<NovedadPersona>) ManagerConcrete.getManager().obtenerListado("consultarNovedadPersonaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpersonas;
    }

    @Override
    public NovedadPersona consultarNovedadPersonaById(Long idNovedadPersona) {
        NovedadPersona novedadpersona = new NovedadPersona();
        try {
            novedadpersona = (NovedadPersona) ManagerConcrete.getManager().obtenerRegistro("consultarNovedadPersona", idNovedadPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpersona;
    }

    @Override
    public void crearNovedadPersona(NovedadPersona novedadpersona) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearNovedadPersona", novedadpersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarNovedadPersona(NovedadPersona novedadpersona) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarNovedadPersona", novedadpersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarNovedadPersona(NovedadPersona novedadpersona) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarNovedadPersona", novedadpersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<NovedadPersona> consultarNovedadPersonaJoin() {
        List<NovedadPersona> novedadpersonas = new ArrayList();
        try {
            novedadpersonas = (List<NovedadPersona>) ManagerConcrete.getManager().obtenerListado("consultarNovedadPersonaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpersonas;
    }

    @Override
    public NovedadPersona consultarNovedadPersonaJoinById(Long idNovedadPersona) {
        NovedadPersona novedadpersona = new NovedadPersona();
        try {
            novedadpersona = (NovedadPersona) ManagerConcrete.getManager().obtenerRegistro("consultarNovedadPersonaJoin", idNovedadPersona);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpersona;
    }

    @Override
    public int consultarNovedadPersonaId() throws Exception {
        int seqNovedadPersona = 0;
        try {
            seqNovedadPersona = (int) ManagerConcrete.getManager().obtenerRegistro("ConsultaUltimoInsertadoNovedadPersona");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new Exception("No se puede consultar");
        }
        return seqNovedadPersona;
    }

}
