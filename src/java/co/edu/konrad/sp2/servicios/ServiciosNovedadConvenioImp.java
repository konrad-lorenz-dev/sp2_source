package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosNovedadConvenio;
import co.edu.konrad.sp2.bean.NovedadConvenio;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosNovedadConvenioImp implements ServiciosNovedadConvenio {

    private final static Logger log = Logger.getLogger(ServiciosNovedadConvenioImp.class);

    @Override
    public List<NovedadConvenio> consultarNovedadConvenio() {
        List<NovedadConvenio> novedadconvenios = new ArrayList();
        try {
            novedadconvenios = (List<NovedadConvenio>) ManagerConcrete.getManager().obtenerListado("consultarNovedadConvenioList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadconvenios;
    }

    @Override
    public NovedadConvenio consultarNovedadConvenioById(Long idNovedadConvenio) {
        NovedadConvenio novedadconvenio = new NovedadConvenio();
        try {
            novedadconvenio = (NovedadConvenio) ManagerConcrete.getManager().obtenerRegistro("consultarNovedadConvenio", idNovedadConvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadconvenio;
    }

    @Override
    public void crearNovedadConvenio(NovedadConvenio novedadconvenio) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearNovedadConvenio", novedadconvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarNovedadConvenio(NovedadConvenio novedadconvenio) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarNovedadConvenio", novedadconvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarNovedadConvenio(NovedadConvenio novedadconvenio) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarNovedadConvenio", novedadconvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<NovedadConvenio> consultarNovedadConvenioJoin() {
        List<NovedadConvenio> novedadconvenios = new ArrayList();
        try {
            novedadconvenios = (List<NovedadConvenio>) ManagerConcrete.getManager().obtenerListado("consultarNovedadConvenioListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadconvenios;
    }

    @Override
    public NovedadConvenio consultarNovedadConvenioJoinById(Long idNovedadConvenio) {
        NovedadConvenio novedadconvenio = new NovedadConvenio();
        try {
            novedadconvenio = (NovedadConvenio) ManagerConcrete.getManager().obtenerRegistro("consultarNovedadConvenioJoin", idNovedadConvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadconvenio;
    }

}
