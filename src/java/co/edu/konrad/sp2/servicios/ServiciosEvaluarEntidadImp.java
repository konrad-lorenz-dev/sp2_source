package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosEvaluarEntidad;
import co.edu.konrad.sp2.bean.EvaluarEntidad;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosEvaluarEntidadImp implements ServiciosEvaluarEntidad {

    private final static Logger log = Logger.getLogger(ServiciosEvaluarEntidadImp.class);

    @Override
    public List<EvaluarEntidad> consultarEvaluarEntidad() {
        List<EvaluarEntidad> evaluarentidads = new ArrayList();
        try {
            evaluarentidads = (List<EvaluarEntidad>) ManagerConcrete.getManager().obtenerListado("consultarEvaluarEntidadList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return evaluarentidads;
    }

    @Override
    public EvaluarEntidad consultarEvaluarEntidadById(Long idEvaluarEntidad) {
        EvaluarEntidad evaluarentidad = new EvaluarEntidad();
        try {
            evaluarentidad = (EvaluarEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarEvaluarEntidad", idEvaluarEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return evaluarentidad;
    }

    @Override
    public void crearEvaluarEntidad(EvaluarEntidad evaluarentidad) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearEvaluarEntidad", evaluarentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarEvaluarEntidad(EvaluarEntidad evaluarentidad) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarEvaluarEntidad", evaluarentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarEvaluarEntidad(EvaluarEntidad evaluarentidad) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarEvaluarEntidad", evaluarentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<EvaluarEntidad> consultarEvaluarEntidadJoin() {
        List<EvaluarEntidad> evaluarentidads = new ArrayList();
        try {
            evaluarentidads = (List<EvaluarEntidad>) ManagerConcrete.getManager().obtenerListado("consultarEvaluarEntidadListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return evaluarentidads;
    }

    @Override
    public EvaluarEntidad consultarEvaluarEntidadJoinById(Long idEvaluarEntidad) {
        EvaluarEntidad evaluarentidad = new EvaluarEntidad();
        try {
            evaluarentidad = (EvaluarEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarEvaluarEntidadJoin", idEvaluarEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return evaluarentidad;
    }

}
