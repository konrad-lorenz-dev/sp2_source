package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosDepartamento;
import co.edu.konrad.sp2.bean.Departamento;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosDepartamentoImp implements ServiciosDepartamento {

    private final static Logger log = Logger.getLogger(ServiciosDepartamentoImp.class);

    @Override
    public List<Departamento> consultarDepartamento() {
        List<Departamento> departamentos = new ArrayList();
        try {
            departamentos = (List<Departamento>) ManagerConcrete.getManager().obtenerListado("consultarDepartamentoList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return departamentos;
    }

    @Override
    public Departamento consultarDepartamentoById(Long idDepartamento) {
        Departamento departamento = new Departamento();
        try {
            departamento = (Departamento) ManagerConcrete.getManager().obtenerRegistro("consultarDepartamento", idDepartamento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return departamento;
    }

    @Override
    public void crearDepartamento(Departamento departamento) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearDepartamento", departamento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarDepartamento(Departamento departamento) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarDepartamento", departamento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarDepartamento(Departamento departamento) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarDepartamento", departamento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<Departamento> consultarDepartamentoJoin() {
        List<Departamento> departamentos = new ArrayList();
        try {
            departamentos = (List<Departamento>) ManagerConcrete.getManager().obtenerListado("consultarDepartamentoListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return departamentos;
    }

    @Override
    public Departamento consultarDepartamentoJoinById(Long idDepartamento) {
        Departamento departamento = new Departamento();
        try {
            departamento = (Departamento) ManagerConcrete.getManager().obtenerRegistro("consultarDepartamentoJoin", idDepartamento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return departamento;
    }

}
