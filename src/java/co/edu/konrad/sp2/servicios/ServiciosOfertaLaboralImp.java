package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosOfertaLaboral;
import co.edu.konrad.sp2.bean.OfertaLaboral;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosOfertaLaboralImp implements ServiciosOfertaLaboral {

    private final static Logger log = Logger.getLogger(ServiciosOfertaLaboralImp.class);

    @Override
    public List<OfertaLaboral> consultarOfertaLaboral() {
        List<OfertaLaboral> ofertalaborals = new ArrayList();
        try {
            ofertalaborals = (List<OfertaLaboral>) ManagerConcrete.getManager().obtenerListado("consultarOfertaLaboralList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ofertalaborals;
    }

    @Override
    public OfertaLaboral consultarOfertaLaboralById(Long idOfertaLaboral) {
        OfertaLaboral ofertalaboral = new OfertaLaboral();
        try {
            ofertalaboral = (OfertaLaboral) ManagerConcrete.getManager().obtenerRegistro("consultarOfertaLaboral", idOfertaLaboral);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ofertalaboral;
    }

    @Override
    public void crearOfertaLaboral(OfertaLaboral ofertalaboral) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearOfertaLaboral", ofertalaboral);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarOfertaLaboral(OfertaLaboral ofertalaboral) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarOfertaLaboral", ofertalaboral);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarOfertaLaboral(OfertaLaboral ofertalaboral) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarOfertaLaboral", ofertalaboral);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<OfertaLaboral> consultarOfertaLaboralJoin() {
        List<OfertaLaboral> ofertalaborals = new ArrayList();
        try {
            ofertalaborals = (List<OfertaLaboral>) ManagerConcrete.getManager().obtenerListado("consultarOfertaLaboralListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ofertalaborals;
    }

    @Override
    public OfertaLaboral consultarOfertaLaboralJoinById(Long idOfertaLaboral) {
        OfertaLaboral ofertalaboral = new OfertaLaboral();
        try {
            ofertalaboral = (OfertaLaboral) ManagerConcrete.getManager().obtenerRegistro("consultarOfertaLaboralJoin", idOfertaLaboral);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ofertalaboral;
    }

}
