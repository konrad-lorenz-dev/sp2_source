package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.PracticanteConvenio;
import java.util.List;
import java.util.Map;

public interface ServiciosPracticanteConvenio {

    public List<PracticanteConvenio> consultarPracticanteConvenio();

    public PracticanteConvenio consultarPracticanteConvenioById(Long idPracticanteConvenio);

    public void crearPracticanteConvenio(PracticanteConvenio practicanteconvenio);

    public void actualizarPracticanteConvenio(PracticanteConvenio practicanteconvenio);

    public void borrarPracticanteConvenio(PracticanteConvenio practicanteconvenio);

    public List<PracticanteConvenio> consultarPracticanteConvenioJoin();

    public PracticanteConvenio consultarPracticanteConvenioJoinById(Long idPracticanteConvenio);
    
    public PracticanteConvenio consultarPracticantePorSeqPracticante(Long idSeqPracticante);

}
