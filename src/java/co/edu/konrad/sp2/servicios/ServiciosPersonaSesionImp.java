package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosPersonaSesion;
import co.edu.konrad.sp2.bean.PersonaSesion;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosPersonaSesionImp implements ServiciosPersonaSesion {

    private final static Logger log = Logger.getLogger(ServiciosPersonaSesionImp.class);

    @Override
    public List<PersonaSesion> consultarPersonaSesion() {
        List<PersonaSesion> personasesions = new ArrayList();
        try {
            personasesions = (List<PersonaSesion>) ManagerConcrete.getManager().obtenerListado("consultarPersonaSesionList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return personasesions;
    }

    @Override
    public PersonaSesion consultarPersonaSesionById(Long idPersonaSesion) {
        PersonaSesion personasesion = new PersonaSesion();
        try {
            personasesion = (PersonaSesion) ManagerConcrete.getManager().obtenerRegistro("consultarPersonaSesion", idPersonaSesion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return personasesion;
    }

    @Override
    public void crearPersonaSesion(PersonaSesion personasesion) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearPersonaSesion", personasesion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarPersonaSesion(PersonaSesion personasesion) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarPersonaSesion", personasesion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarPersonaSesion(PersonaSesion personasesion) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarPersonaSesion", personasesion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<PersonaSesion> consultarPersonaSesionJoin() {
        List<PersonaSesion> personasesions = new ArrayList();
        try {
            personasesions = (List<PersonaSesion>) ManagerConcrete.getManager().obtenerListado("consultarPersonaSesionListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return personasesions;
    }

    @Override
    public PersonaSesion consultarPersonaSesionJoinById(Long idPersonaSesion) {
        PersonaSesion personasesion = new PersonaSesion();
        try {
            personasesion = (PersonaSesion) ManagerConcrete.getManager().obtenerRegistro("consultarPersonaSesionJoin", idPersonaSesion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return personasesion;
    }

}
