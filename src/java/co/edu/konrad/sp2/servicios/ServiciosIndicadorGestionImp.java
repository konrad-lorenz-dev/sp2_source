package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosIndicadorGestion;
import co.edu.konrad.sp2.bean.IndicadorGestion;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosIndicadorGestionImp implements ServiciosIndicadorGestion {

    private final static Logger log = Logger.getLogger(ServiciosIndicadorGestionImp.class);

    @Override
    public List<IndicadorGestion> consultarIndicadorGestion() {
        List<IndicadorGestion> indicadorgestions = new ArrayList();
        try {
            indicadorgestions = (List<IndicadorGestion>) ManagerConcrete.getManager().obtenerListado("consultarIndicadorGestionList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return indicadorgestions;
    }

    @Override
    public IndicadorGestion consultarIndicadorGestionById(Long idIndicadorGestion) {
        IndicadorGestion indicadorgestion = new IndicadorGestion();
        try {
            indicadorgestion = (IndicadorGestion) ManagerConcrete.getManager().obtenerRegistro("consultarIndicadorGestion", idIndicadorGestion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return indicadorgestion;
    }

    @Override
    public void crearIndicadorGestion(IndicadorGestion indicadorgestion) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearIndicadorGestion", indicadorgestion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarIndicadorGestion(IndicadorGestion indicadorgestion) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarIndicadorGestion", indicadorgestion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarIndicadorGestion(IndicadorGestion indicadorgestion) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarIndicadorGestion", indicadorgestion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
