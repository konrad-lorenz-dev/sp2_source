package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Departamento;
import java.util.List;
import java.util.Map;

public interface ServiciosDepartamento {

    public List<Departamento> consultarDepartamento();

    public Departamento consultarDepartamentoById(Long idDepartamento);

    public void crearDepartamento(Departamento departamento);

    public void actualizarDepartamento(Departamento departamento);

    public void borrarDepartamento(Departamento departamento);

    public List<Departamento> consultarDepartamentoJoin();

    public Departamento consultarDepartamentoJoinById(Long idDepartamento);

}
