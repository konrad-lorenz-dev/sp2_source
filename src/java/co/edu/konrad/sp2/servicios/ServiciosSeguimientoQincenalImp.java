package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosSeguimientoQincenal;
import co.edu.konrad.sp2.bean.SeguimientoQincenal;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosSeguimientoQincenalImp implements ServiciosSeguimientoQincenal {

    private final static Logger log = Logger.getLogger(ServiciosSeguimientoQincenalImp.class);

    @Override
    public List<SeguimientoQincenal> consultarSeguimientoQincenal() {
        List<SeguimientoQincenal> seguimientoqincenals = new ArrayList();
        try {
            seguimientoqincenals = (List<SeguimientoQincenal>) ManagerConcrete.getManager().obtenerListado("consultarSeguimientoQincenalList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return seguimientoqincenals;
    }

    @Override
    public SeguimientoQincenal consultarSeguimientoQincenalById(Long idSeguimientoQincenal) {
        SeguimientoQincenal seguimientoqincenal = new SeguimientoQincenal();
        try {
            seguimientoqincenal = (SeguimientoQincenal) ManagerConcrete.getManager().obtenerRegistro("consultarSeguimientoQincenal", idSeguimientoQincenal);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return seguimientoqincenal;
    }

    @Override
    public void crearSeguimientoQincenal(SeguimientoQincenal seguimientoqincenal) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearSeguimientoQincenal", seguimientoqincenal);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarSeguimientoQincenal(SeguimientoQincenal seguimientoqincenal) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarSeguimientoQincenal", seguimientoqincenal);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarSeguimientoQincenal(SeguimientoQincenal seguimientoqincenal) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarSeguimientoQincenal", seguimientoqincenal);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<SeguimientoQincenal> consultarSeguimientoQincenalJoin() {
        List<SeguimientoQincenal> seguimientoqincenals = new ArrayList();
        try {
            seguimientoqincenals = (List<SeguimientoQincenal>) ManagerConcrete.getManager().obtenerListado("consultarSeguimientoQincenalListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return seguimientoqincenals;
    }

    @Override
    public SeguimientoQincenal consultarSeguimientoQincenalJoinById(Long idSeguimientoQincenal) {
        SeguimientoQincenal seguimientoqincenal = new SeguimientoQincenal();
        try {
            seguimientoqincenal = (SeguimientoQincenal) ManagerConcrete.getManager().obtenerRegistro("consultarSeguimientoQincenalJoin", idSeguimientoQincenal);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return seguimientoqincenal;
    }

}
