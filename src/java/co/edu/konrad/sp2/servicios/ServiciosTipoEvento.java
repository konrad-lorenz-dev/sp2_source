package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.TipoEvento;
import java.util.List;
import java.util.Map;

public interface ServiciosTipoEvento {

    public List<TipoEvento> consultarTipoEvento();

    public TipoEvento consultarTipoEventoById(Long idTipoEvento);

    public void crearTipoEvento(TipoEvento tipoevento);

    public void actualizarTipoEvento(TipoEvento tipoevento);

    public void borrarTipoEvento(TipoEvento tipoevento);

}
