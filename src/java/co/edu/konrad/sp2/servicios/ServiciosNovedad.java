package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Novedad;
import co.edu.konrad.sp2.bean.NovedadPersona;
import java.util.List;
import java.util.Map;

public interface ServiciosNovedad {

    public List<Novedad> consultarNovedad();

    public Novedad consultarNovedadById(Long idNovedad);

    public String crearNovedad(Novedad novedad)throws Exception;

    public void actualizarNovedad(Novedad novedad);

    public void borrarNovedad(Novedad novedad);

    public List<Novedad> consultarNovedadJoin();

    public Novedad consultarNovedadJoinById(Long idNovedad);
    
    public  List<Novedad>  consultarNovedadGrillaPostulados(NovedadPersona novedad);
    
    public List<Novedad> consultarNovedadesPorPersonaJoin(Long idPersona);

}
