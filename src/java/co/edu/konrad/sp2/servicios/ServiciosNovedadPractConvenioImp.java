package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosNovedadPractConvenio;
import co.edu.konrad.sp2.bean.NovedadPractConvenio;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosNovedadPractConvenioImp implements ServiciosNovedadPractConvenio {

    private final static Logger log = Logger.getLogger(ServiciosNovedadPractConvenioImp.class);

    @Override
    public List<NovedadPractConvenio> consultarNovedadPractConvenio() {
        List<NovedadPractConvenio> novedadpractconvenios = new ArrayList();
        try {
            novedadpractconvenios = (List<NovedadPractConvenio>) ManagerConcrete.getManager().obtenerListado("consultarNovedadPractConvenioList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpractconvenios;
    }

    @Override
    public NovedadPractConvenio consultarNovedadPractConvenioById(Long idNovedadPractConvenio) {
        NovedadPractConvenio novedadpractconvenio = new NovedadPractConvenio();
        try {
            novedadpractconvenio = (NovedadPractConvenio) ManagerConcrete.getManager().obtenerRegistro("consultarNovedadPractConvenio", idNovedadPractConvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpractconvenio;
    }

    @Override
    public void crearNovedadPractConvenio(NovedadPractConvenio novedadpractconvenio) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearNovedadPractConvenio", novedadpractconvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarNovedadPractConvenio(NovedadPractConvenio novedadpractconvenio) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarNovedadPractConvenio", novedadpractconvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarNovedadPractConvenio(NovedadPractConvenio novedadpractconvenio) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarNovedadPractConvenio", novedadpractconvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<NovedadPractConvenio> consultarNovedadPractConvenioJoin() {
        List<NovedadPractConvenio> novedadpractconvenios = new ArrayList();
        try {
            novedadpractconvenios = (List<NovedadPractConvenio>) ManagerConcrete.getManager().obtenerListado("consultarNovedadPractConvenioListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpractconvenios;
    }

    @Override
    public NovedadPractConvenio consultarNovedadPractConvenioJoinById(Long idNovedadPractConvenio) {
        NovedadPractConvenio novedadpractconvenio = new NovedadPractConvenio();
        try {
            novedadpractconvenio = (NovedadPractConvenio) ManagerConcrete.getManager().obtenerRegistro("consultarNovedadPractConvenioJoin", idNovedadPractConvenio);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return novedadpractconvenio;
    }

}
