package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.FacultadEscolaris;
import java.util.List;
import java.util.Map;

public interface ServiciosFacultadEscolaris {

    public List<FacultadEscolaris> consultarFacultadEscolaris();

    public FacultadEscolaris consultarFacultadEscolarisById(Long idFacultadEscolaris);

    public void crearFacultadEscolaris(FacultadEscolaris facultadescolaris);

    public void actualizarFacultadEscolaris(FacultadEscolaris facultadescolaris);

    public void borrarFacultadEscolaris(FacultadEscolaris facultadescolaris);

}
