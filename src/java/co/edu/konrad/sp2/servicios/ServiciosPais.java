package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Pais;
import java.util.List;
import java.util.Map;

public interface ServiciosPais {

    public List<Pais> consultarPais();

    public Pais consultarPaisById(Long idPais);

    public void crearPais(Pais pais);

    public void actualizarPais(Pais pais);

    public void borrarPais(Pais pais);

}
