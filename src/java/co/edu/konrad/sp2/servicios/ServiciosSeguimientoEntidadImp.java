package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosSeguimientoEntidad;
import co.edu.konrad.sp2.bean.SeguimientoEntidad;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosSeguimientoEntidadImp implements ServiciosSeguimientoEntidad {

    private final static Logger log = Logger.getLogger(ServiciosSeguimientoEntidadImp.class);

    @Override
    public List<SeguimientoEntidad> consultarSeguimientoEntidad() {
        List<SeguimientoEntidad> seguimientoentidads = new ArrayList();
        try {
            seguimientoentidads = (List<SeguimientoEntidad>) ManagerConcrete.getManager().obtenerListado("consultarSeguimientoEntidadList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return seguimientoentidads;
    }

    @Override
    public SeguimientoEntidad consultarSeguimientoEntidadById(Long idSeguimientoEntidad) {
        SeguimientoEntidad seguimientoentidad = new SeguimientoEntidad();
        try {
            seguimientoentidad = (SeguimientoEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarSeguimientoEntidad", idSeguimientoEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return seguimientoentidad;
    }

    @Override
    public void crearSeguimientoEntidad(SeguimientoEntidad seguimientoentidad) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearSeguimientoEntidad", seguimientoentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarSeguimientoEntidad(SeguimientoEntidad seguimientoentidad) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarSeguimientoEntidad", seguimientoentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarSeguimientoEntidad(SeguimientoEntidad seguimientoentidad) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarSeguimientoEntidad", seguimientoentidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<SeguimientoEntidad> consultarSeguimientoEntidadJoin() {
        List<SeguimientoEntidad> seguimientoentidads = new ArrayList();
        try {
            seguimientoentidads = (List<SeguimientoEntidad>) ManagerConcrete.getManager().obtenerListado("consultarSeguimientoEntidadListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return seguimientoentidads;
    }

    @Override
    public SeguimientoEntidad consultarSeguimientoEntidadJoinById(Long idSeguimientoEntidad) {
        SeguimientoEntidad seguimientoentidad = new SeguimientoEntidad();
        try {
            seguimientoentidad = (SeguimientoEntidad) ManagerConcrete.getManager().obtenerRegistro("consultarSeguimientoEntidadJoin", idSeguimientoEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return seguimientoentidad;
    }

}
