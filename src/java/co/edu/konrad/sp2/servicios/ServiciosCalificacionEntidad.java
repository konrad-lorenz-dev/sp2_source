package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.CalificacionEntidad;
import java.util.List;
import java.util.Map;

public interface ServiciosCalificacionEntidad {

    public List<CalificacionEntidad> consultarCalificacionEntidad();

    public CalificacionEntidad consultarCalificacionEntidadById(Long idCalificacionEntidad);

    public void crearCalificacionEntidad(CalificacionEntidad calificacionentidad);

    public void actualizarCalificacionEntidad(CalificacionEntidad calificacionentidad);

    public void borrarCalificacionEntidad(CalificacionEntidad calificacionentidad);

    public List<CalificacionEntidad> consultarCalificacionEntidadJoin();

    public CalificacionEntidad consultarCalificacionEntidadJoinById(Long idCalificacionEntidad);

}
