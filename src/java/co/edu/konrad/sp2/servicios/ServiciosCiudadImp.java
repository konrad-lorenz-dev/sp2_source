package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosCiudad;
import co.edu.konrad.sp2.bean.Ciudad;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosCiudadImp implements ServiciosCiudad {

    private final static Logger log = Logger.getLogger(ServiciosCiudadImp.class);

    @Override
    public List<Ciudad> consultarCiudad() {
        List<Ciudad> ciudads = new ArrayList();
        try {
            ciudads = (List<Ciudad>) ManagerConcrete.getManager().obtenerListado("consultarCiudadList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ciudads;
    }

    @Override
    public Ciudad consultarCiudadById(Long idCiudad) {
        Ciudad ciudad = new Ciudad();
        try {
            ciudad = (Ciudad) ManagerConcrete.getManager().obtenerRegistro("consultarCiudad", idCiudad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ciudad;
    }

    @Override
    public void crearCiudad(Ciudad ciudad) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearCiudad", ciudad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarCiudad(Ciudad ciudad) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarCiudad", ciudad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarCiudad(Ciudad ciudad) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarCiudad", ciudad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<Ciudad> consultarCiudadJoin() {
        List<Ciudad> ciudads = new ArrayList();
        try {
            ciudads = (List<Ciudad>) ManagerConcrete.getManager().obtenerListado("consultarCiudadListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ciudads;
    }

    @Override
    public Ciudad consultarCiudadJoinById(Long idCiudad) {
        Ciudad ciudad = new Ciudad();
        try {
            ciudad = (Ciudad) ManagerConcrete.getManager().obtenerRegistro("consultarCiudadJoin", idCiudad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ciudad;
    }

}
