package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosDocumentosAdjuntos;
import co.edu.konrad.sp2.bean.DocumentosAdjuntos;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosDocumentosAdjuntosImp implements ServiciosDocumentosAdjuntos {

    private final static Logger log = Logger.getLogger(ServiciosDocumentosAdjuntosImp.class);

    @Override
    public List<DocumentosAdjuntos> consultarDocumentosAdjuntos() {
        List<DocumentosAdjuntos> documentosadjuntoss = new ArrayList();
        try {
            documentosadjuntoss = (List<DocumentosAdjuntos>) ManagerConcrete.getManager().obtenerListado("consultarDocumentosAdjuntosList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return documentosadjuntoss;
    }

    @Override
    public DocumentosAdjuntos consultarDocumentosAdjuntosById(Long idDocumentosAdjuntos) {
        DocumentosAdjuntos documentosadjuntos = new DocumentosAdjuntos();
        try {
            documentosadjuntos = (DocumentosAdjuntos) ManagerConcrete.getManager().obtenerRegistro("consultarDocumentosAdjuntos", idDocumentosAdjuntos);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return documentosadjuntos;
    }

    @Override
    public void crearDocumentosAdjuntos(DocumentosAdjuntos documentosadjuntos) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearDocumentosAdjuntos", documentosadjuntos);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarDocumentosAdjuntos(DocumentosAdjuntos documentosadjuntos) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarDocumentosAdjuntos", documentosadjuntos);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarDocumentosAdjuntos(DocumentosAdjuntos documentosadjuntos) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarDocumentosAdjuntos", documentosadjuntos);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<DocumentosAdjuntos> consultarDocumentosAdjuntosJoin() {
        List<DocumentosAdjuntos> documentosadjuntoss = new ArrayList();
        try {
            documentosadjuntoss = (List<DocumentosAdjuntos>) ManagerConcrete.getManager().obtenerListado("consultarDocumentosAdjuntosListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return documentosadjuntoss;
    }

    @Override
    public DocumentosAdjuntos consultarDocumentosAdjuntosJoinById(Long idDocumentosAdjuntos) {
        DocumentosAdjuntos documentosadjuntos = new DocumentosAdjuntos();
        try {
            documentosadjuntos = (DocumentosAdjuntos) ManagerConcrete.getManager().obtenerRegistro("consultarDocumentosAdjuntosJoin", idDocumentosAdjuntos);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return documentosadjuntos;
    }

}
