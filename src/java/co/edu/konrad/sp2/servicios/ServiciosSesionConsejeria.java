package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.SesionConsejeria;
import java.util.List;

public interface ServiciosSesionConsejeria {

    public List<SesionConsejeria> consultarSesionConsejeria();

    public SesionConsejeria consultarSesionConsejeriaById(Long idSesionConsejeria);

    public SesionConsejeria crearSesionConsejeria(SesionConsejeria sesionconsejeria);

    public void actualizarSesionConsejeria(SesionConsejeria sesionconsejeria);

    public void borrarSesionConsejeria(SesionConsejeria sesionconsejeria);

    public List<SesionConsejeria> consultarSesionConsejeriaJoin();
    
    public List<SesionConsejeria> consultarSesionConsejeriaJoinPorRol(String usrRol);

    public SesionConsejeria consultarSesionConsejeriaJoinById(Long idSesionConsejeria);
    
    public List<SesionConsejeria> consultarSesionConsejeriaJoinPorCCA(Long IdSeqCca);
    
    public SesionConsejeria consultarSesionConsejeriaJoinPorCcaYSeqPracticante(SesionConsejeria sesionConsejeria);
}
