package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.EstadoPersona;
import java.util.List;
import java.util.Map;

public interface ServiciosEstadoPersona {

    public List<EstadoPersona> consultarEstadoPersona();

    public EstadoPersona consultarEstadoPersonaById(Long idEstadoPersona);

    public void crearEstadoPersona(EstadoPersona estadopersona);

    public void actualizarEstadoPersona(EstadoPersona estadopersona);

    public void borrarEstadoPersona(EstadoPersona estadopersona);

}
