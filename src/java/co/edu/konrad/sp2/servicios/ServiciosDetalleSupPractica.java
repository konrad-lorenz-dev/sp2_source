package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.DetalleSupPractica;
import java.util.List;
import java.util.Map;

public interface ServiciosDetalleSupPractica {

    public List<DetalleSupPractica> consultarDetalleSupPractica();

    public DetalleSupPractica consultarDetalleSupPracticaById(Long idDetalleSupPractica);

    public void crearDetalleSupPractica(DetalleSupPractica detallesuppractica);

    public void actualizarDetalleSupPractica(DetalleSupPractica detallesuppractica);

    public void borrarDetalleSupPractica(DetalleSupPractica detallesuppractica);

    public List<DetalleSupPractica> consultarDetalleSupPracticaJoin();

    public DetalleSupPractica consultarDetalleSupPracticaJoinById(Long idDetalleSupPractica);

}
