package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosEvento;
import co.edu.konrad.sp2.bean.Evento;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosEventoImp implements ServiciosEvento {

    private final static Logger log = Logger.getLogger(ServiciosEventoImp.class);

    @Override
    public List<Evento> consultarEvento() {
        List<Evento> eventos = new ArrayList();
        try {
            eventos = (List<Evento>) ManagerConcrete.getManager().obtenerListado("consultarEventoList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return eventos;
    }

    @Override
    public Evento consultarEventoById(Long idEvento) {
        Evento evento = new Evento();
        try {
            evento = (Evento) ManagerConcrete.getManager().obtenerRegistro("consultarEvento", idEvento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return evento;
    }

    @Override
    public Evento crearEvento(Evento evento) {
        Evento constantes = new Evento();
        try {
            ManagerConcrete.getManager().insertarRegistro("crearEvento", evento);
            constantes.setConstantes(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            constantes.setConstantes(Constantes.FAILED);
        }
        return constantes;
    }

    @Override
    public Evento actualizarEvento(Evento evento) {
        Evento constantes = new Evento();
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarEvento", evento);
            constantes.setConstantes(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            constantes.setConstantes(Constantes.FAILED);
        }
        return constantes;
    }

    @Override
    public Evento borrarEvento(Evento evento) {
        Evento constantes = new Evento();
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarEvento", evento);
            constantes.setConstantes(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            constantes.setConstantes(Constantes.FAILED);
        }

        return constantes;
    }

    @Override
    public List<Evento> consultarEventoJoin() {
        List<Evento> eventos = new ArrayList();
        try {
            eventos = (List<Evento>) ManagerConcrete.getManager().obtenerListado("consultarEventoListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return eventos;
    }

    @Override
    public Evento consultarEventoJoinById(Long idEvento) {
        Evento evento = new Evento();
        try {
            evento = (Evento) ManagerConcrete.getManager().obtenerRegistro("consultarEventoJoin", idEvento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return evento;
    }

    @Override
    public int ConsultarUltimoInsertado() {
        int idEvento = 0;
        try {
            idEvento = (int) ManagerConcrete.getManager().obtenerRegistro("ConsultaUltimoInsertado");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return idEvento;
    }

}
