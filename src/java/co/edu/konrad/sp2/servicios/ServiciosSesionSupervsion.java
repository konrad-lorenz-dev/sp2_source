package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.SesionSupervsion;
import java.util.List;
import java.util.Map;

public interface ServiciosSesionSupervsion {

    public List<SesionSupervsion> consultarSesionSupervsion();

    public SesionSupervsion consultarSesionSupervsionById(Long idSesionSupervsion);

    public void crearSesionSupervsion(SesionSupervsion sesionsupervsion);

    public void actualizarSesionSupervsion(SesionSupervsion sesionsupervsion);

    public void borrarSesionSupervsion(SesionSupervsion sesionsupervsion);

    public List<SesionSupervsion> consultarSesionSupervsionJoin();

    public SesionSupervsion consultarSesionSupervsionJoinById(Long idSesionSupervsion);

}
