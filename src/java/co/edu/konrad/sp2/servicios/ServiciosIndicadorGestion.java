package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.IndicadorGestion;
import java.util.List;
import java.util.Map;

public interface ServiciosIndicadorGestion {

    public List<IndicadorGestion> consultarIndicadorGestion();

    public IndicadorGestion consultarIndicadorGestionById(Long idIndicadorGestion);

    public void crearIndicadorGestion(IndicadorGestion indicadorgestion);

    public void actualizarIndicadorGestion(IndicadorGestion indicadorgestion);

    public void borrarIndicadorGestion(IndicadorGestion indicadorgestion);

}
