package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosCronograma;
import co.edu.konrad.sp2.bean.Cronograma;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosCronogramaImp implements ServiciosCronograma {

    private final static Logger log = Logger.getLogger(ServiciosCronogramaImp.class);

    @Override
    public List<Cronograma> consultarCronograma() {
        List<Cronograma> cronogramas = new ArrayList();
        try {
            cronogramas = (List<Cronograma>) ManagerConcrete.getManager().obtenerListado("consultarCronogramaList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return cronogramas;
    }

    @Override
    public Cronograma consultarCronogramaById(Long idCronograma) {
        Cronograma cronograma = new Cronograma();
        try {
            cronograma = (Cronograma) ManagerConcrete.getManager().obtenerRegistro("consultarCronograma", idCronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return cronograma;
    }

    @Override
    public void crearCronograma(Cronograma cronograma) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearCronograma", cronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarCronograma(Cronograma cronograma) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarCronograma", cronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarCronograma(Cronograma cronograma) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarCronograma", cronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<Cronograma> consultarCronogramaJoin() {
        List<Cronograma> cronogramas = new ArrayList();
        try {
            cronogramas = (List<Cronograma>) ManagerConcrete.getManager().obtenerListado("consultarCronogramaListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return cronogramas;
    }

    @Override
    public Cronograma consultarCronogramaJoinById(Long idCronograma) {
        Cronograma cronograma = new Cronograma();
        try {
            cronograma = (Cronograma) ManagerConcrete.getManager().obtenerRegistro("consultarCronogramaJoin", idCronograma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return cronograma;
    }

}
