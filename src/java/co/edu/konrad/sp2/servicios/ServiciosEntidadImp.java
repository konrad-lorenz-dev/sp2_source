package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosEntidad;
import co.edu.konrad.sp2.bean.Entidad;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosEntidadImp implements ServiciosEntidad {

    private final static Logger log = Logger.getLogger(ServiciosEntidadImp.class);

    @Override
    public List<Entidad> consultarEntidad() {
        List<Entidad> entidads = new ArrayList();
        try {
            entidads = (List<Entidad>) ManagerConcrete.getManager().obtenerListado("consultarEntidadList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return entidads;
    }

    @Override
    public Entidad consultarEntidadById(Long idEntidad) {
        Entidad entidad = new Entidad();
        try {
            entidad = (Entidad) ManagerConcrete.getManager().obtenerRegistro("consultarEntidad", idEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return entidad;
    }

    @Override
    public void crearEntidad(Entidad entidad) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearEntidad", entidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarEntidad(Entidad entidad) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarEntidad", entidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarEntidad(Entidad entidad) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarEntidad", entidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<Entidad> consultarEntidadJoin() {
        List<Entidad> entidads = new ArrayList();
        try {
            entidads = (List<Entidad>) ManagerConcrete.getManager().obtenerListado("consultarEntidadListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return entidads;
    }

    @Override
    public Entidad consultarEntidadJoinById(Long idEntidad) {
        Entidad entidad = new Entidad();
        try {
            entidad = (Entidad) ManagerConcrete.getManager().obtenerRegistro("consultarEntidadJoin", idEntidad);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return entidad;
    }

}
