package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosCompetenciaIndicador;
import co.edu.konrad.sp2.bean.CompetenciaIndicador;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosCompetenciaIndicadorImp implements ServiciosCompetenciaIndicador {

    private final static Logger log = Logger.getLogger(ServiciosCompetenciaIndicadorImp.class);

    @Override
    public List<CompetenciaIndicador> consultarCompetenciaIndicador() {
        List<CompetenciaIndicador> competenciaindicadors = new ArrayList();
        try {
            competenciaindicadors = (List<CompetenciaIndicador>) ManagerConcrete.getManager().obtenerListado("consultarCompetenciaIndicadorList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return competenciaindicadors;
    }

    @Override
    public CompetenciaIndicador consultarCompetenciaIndicadorById(Long idCompetenciaIndicador) {
        CompetenciaIndicador competenciaindicador = new CompetenciaIndicador();
        try {
            competenciaindicador = (CompetenciaIndicador) ManagerConcrete.getManager().obtenerRegistro("consultarCompetenciaIndicador", idCompetenciaIndicador);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return competenciaindicador;
    }

    @Override
    public void crearCompetenciaIndicador(CompetenciaIndicador competenciaindicador) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearCompetenciaIndicador", competenciaindicador);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarCompetenciaIndicador(CompetenciaIndicador competenciaindicador) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarCompetenciaIndicador", competenciaindicador);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarCompetenciaIndicador(CompetenciaIndicador competenciaindicador) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarCompetenciaIndicador", competenciaindicador);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
