package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosFacultadEscolaris;
import co.edu.konrad.sp2.bean.FacultadEscolaris;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosFacultadEscolarisImp implements ServiciosFacultadEscolaris {

    private final static Logger log = Logger.getLogger(ServiciosFacultadEscolarisImp.class);

    @Override
    public List<FacultadEscolaris> consultarFacultadEscolaris() {
        List<FacultadEscolaris> facultadescolariss = new ArrayList();
        try {
            facultadescolariss = (List<FacultadEscolaris>) ManagerConcrete.getManager().obtenerListado("consultarFacultadEscolarisList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return facultadescolariss;
    }

    @Override
    public FacultadEscolaris consultarFacultadEscolarisById(Long idFacultadEscolaris) {
        FacultadEscolaris facultadescolaris = new FacultadEscolaris();
        try {
            facultadescolaris = (FacultadEscolaris) ManagerConcrete.getManager().obtenerRegistro("consultarFacultadEscolaris", idFacultadEscolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return facultadescolaris;
    }

    @Override
    public void crearFacultadEscolaris(FacultadEscolaris facultadescolaris) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearFacultadEscolaris", facultadescolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarFacultadEscolaris(FacultadEscolaris facultadescolaris) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarFacultadEscolaris", facultadescolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarFacultadEscolaris(FacultadEscolaris facultadescolaris) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarFacultadEscolaris", facultadescolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
