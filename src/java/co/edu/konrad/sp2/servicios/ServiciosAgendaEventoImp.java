package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosAgendaEvento;
import co.edu.konrad.sp2.bean.AgendaEvento;
import co.edu.konrad.sp2.constant.Constantes;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosAgendaEventoImp implements ServiciosAgendaEvento {

    private final static Logger log = Logger.getLogger(ServiciosAgendaEventoImp.class);

    @Override
    public List<AgendaEvento> consultarAgendaEvento() {
        List<AgendaEvento> agendaeventos = new ArrayList();
        try {
            agendaeventos = (List<AgendaEvento>) ManagerConcrete.getManager().obtenerListado("consultarAgendaEventoList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return agendaeventos;
    }

    @Override
    public AgendaEvento consultarAgendaEventoById(Long idAgendaEvento) {
        AgendaEvento agendaevento = new AgendaEvento();
        try {
            agendaevento = (AgendaEvento) ManagerConcrete.getManager().obtenerRegistro("consultarAgendaEvento", idAgendaEvento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return agendaevento;
    }

    @Override
    public AgendaEvento crearAgendaEvento(AgendaEvento agendaevento) {
        AgendaEvento constantes = new AgendaEvento();
        try {
            ManagerConcrete.getManager().insertarRegistro("crearAgendaEvento", agendaevento);
            constantes.setConstante(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            constantes.setConstante(Constantes.FAILED);
        }
        return constantes;
    }

    @Override
    public AgendaEvento actualizarAgendaEvento(AgendaEvento agendaevento) {
        AgendaEvento constante = new AgendaEvento();
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarAgendaEvento", agendaevento);
            constante.setConstante(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            constante.setConstante(Constantes.FAILED);
        }
        return constante;
    }

    @Override
    public AgendaEvento borrarAgendaEvento(AgendaEvento agendaevento) {
        AgendaEvento constantes = new AgendaEvento();
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarAgendaEvento", agendaevento);
            constantes.setConstante(Constantes.OK);
        } catch (Exception e) {
            log.error(e.getMessage());
            constantes.setConstante(Constantes.FAILED);
        }
        return constantes;
    }

    @Override
    public List<AgendaEvento> consultarAgendaEventoJoin() {
        List<AgendaEvento> agendaeventos = new ArrayList();
        try {
            agendaeventos = (List<AgendaEvento>) ManagerConcrete.getManager().obtenerListado("consultarAgendaEventoListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return agendaeventos;
    }

    @Override
    public List<AgendaEvento> consultarAgendaEventoJoinPorRol(String UsrCreacion) {
        List<AgendaEvento> agendaeventos = new ArrayList();
        try {
            agendaeventos = (List<AgendaEvento>) ManagerConcrete.getManager().obtenerListado("consultarAgendaEventoListJoinPorRol", UsrCreacion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return agendaeventos;
    }

    @Override
    public AgendaEvento consultarAgendaEventoJoinById(Long idAgendaEvento) {
        AgendaEvento agendaevento = new AgendaEvento();
        try {
            agendaevento = (AgendaEvento) ManagerConcrete.getManager().obtenerRegistro("consultarAgendaEventoJoin", idAgendaEvento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return agendaevento;
    }

}
