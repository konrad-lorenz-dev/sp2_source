package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Convenio;
import java.util.List;
import java.util.Map;

public interface ServiciosConvenio {

    public List<Convenio> consultarConvenio();

    public Convenio consultarConvenioById(Long idConvenio);

    public void crearConvenio(Convenio convenio);

    public void actualizarConvenio(Convenio convenio);

    public void borrarConvenio(Convenio convenio);

    public List<Convenio> consultarConvenioJoin();

    public Convenio consultarConvenioJoinById(Long idConvenio);

}
