package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.Entidad;
import java.util.List;
import java.util.Map;

public interface ServiciosEntidad {

    public List<Entidad> consultarEntidad();

    public Entidad consultarEntidadById(Long idEntidad);

    public void crearEntidad(Entidad entidad);

    public void actualizarEntidad(Entidad entidad);

    public void borrarEntidad(Entidad entidad);

    public List<Entidad> consultarEntidadJoin();

    public Entidad consultarEntidadJoinById(Long idEntidad);

}
