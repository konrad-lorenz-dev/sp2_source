package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.bean.DocumentosAdjuntos;
import java.util.List;
import java.util.Map;

public interface ServiciosDocumentosAdjuntos {

    public List<DocumentosAdjuntos> consultarDocumentosAdjuntos();

    public DocumentosAdjuntos consultarDocumentosAdjuntosById(Long idDocumentosAdjuntos);

    public void crearDocumentosAdjuntos(DocumentosAdjuntos documentosadjuntos);

    public void actualizarDocumentosAdjuntos(DocumentosAdjuntos documentosadjuntos);

    public void borrarDocumentosAdjuntos(DocumentosAdjuntos documentosadjuntos);

    public List<DocumentosAdjuntos> consultarDocumentosAdjuntosJoin();

    public DocumentosAdjuntos consultarDocumentosAdjuntosJoinById(Long idDocumentosAdjuntos);

}
