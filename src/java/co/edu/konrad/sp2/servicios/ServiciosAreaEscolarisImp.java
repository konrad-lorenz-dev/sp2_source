package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosAreaEscolaris;
import co.edu.konrad.sp2.bean.AreaEscolaris;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosAreaEscolarisImp implements ServiciosAreaEscolaris {

    private final static Logger log = Logger.getLogger(ServiciosAreaEscolarisImp.class);

    @Override
    public List<AreaEscolaris> consultarAreaEscolaris() {
        List<AreaEscolaris> areaescolariss = new ArrayList();
        try {
            areaescolariss = (List<AreaEscolaris>) ManagerConcrete.getManager().obtenerListado("consultarAreaEscolarisList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return areaescolariss;
    }

    @Override
    public AreaEscolaris consultarAreaEscolarisById(Long idAreaEscolaris) {
        AreaEscolaris areaescolaris = new AreaEscolaris();
        try {
            areaescolaris = (AreaEscolaris) ManagerConcrete.getManager().obtenerRegistro("consultarAreaEscolaris", idAreaEscolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return areaescolaris;
    }

    @Override
    public void crearAreaEscolaris(AreaEscolaris areaescolaris) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearAreaEscolaris", areaescolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarAreaEscolaris(AreaEscolaris areaescolaris) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarAreaEscolaris", areaescolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarAreaEscolaris(AreaEscolaris areaescolaris) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarAreaEscolaris", areaescolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<AreaEscolaris> consultarAreaEscolarisJoin() {
        List<AreaEscolaris> areaescolariss = new ArrayList();
        try {
            areaescolariss = (List<AreaEscolaris>) ManagerConcrete.getManager().obtenerListado("consultarAreaEscolarisListJoin");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return areaescolariss;
    }

    @Override
    public AreaEscolaris consultarAreaEscolarisJoinById(Long idAreaEscolaris) {
        AreaEscolaris areaescolaris = new AreaEscolaris();
        try {
            areaescolaris = (AreaEscolaris) ManagerConcrete.getManager().obtenerRegistro("consultarAreaEscolarisJoin", idAreaEscolaris);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return areaescolaris;
    }

}
