package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosTipoDocumento;
import co.edu.konrad.sp2.bean.TipoDocumento;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosTipoDocumentoImp implements ServiciosTipoDocumento {

    private final static Logger log = Logger.getLogger(ServiciosTipoDocumentoImp.class);

    @Override
    public List<TipoDocumento> consultarTipoDocumento() {
        List<TipoDocumento> tipodocumentos = new ArrayList();
        try {
            tipodocumentos = (List<TipoDocumento>) ManagerConcrete.getManager().obtenerListado("consultarTipoDocumentoList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tipodocumentos;
    }

    @Override
    public TipoDocumento consultarTipoDocumentoById(Long idTipoDocumento) {
        TipoDocumento tipodocumento = new TipoDocumento();
        try {
            tipodocumento = (TipoDocumento) ManagerConcrete.getManager().obtenerRegistro("consultarTipoDocumento", idTipoDocumento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tipodocumento;
    }

    @Override
    public void crearTipoDocumento(TipoDocumento tipodocumento) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearTipoDocumento", tipodocumento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarTipoDocumento(TipoDocumento tipodocumento) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarTipoDocumento", tipodocumento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarTipoDocumento(TipoDocumento tipodocumento) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarTipoDocumento", tipodocumento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
