package co.edu.konrad.sp2.servicios;

import co.edu.konrad.sp2.servicios.ServiciosTipoEvento;
import co.edu.konrad.sp2.bean.TipoEvento;
import co.edu.konrad.sp2.framework.manager.ManagerConcrete;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;

public class ServiciosTipoEventoImp implements ServiciosTipoEvento {

    private final static Logger log = Logger.getLogger(ServiciosTipoEventoImp.class);

    @Override
    public List<TipoEvento> consultarTipoEvento() {
        List<TipoEvento> tipoeventos = new ArrayList();
        try {
            tipoeventos = (List<TipoEvento>) ManagerConcrete.getManager().obtenerListado("consultarTipoEventoList");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tipoeventos;
    }

    @Override
    public TipoEvento consultarTipoEventoById(Long idTipoEvento) {
        TipoEvento tipoevento = new TipoEvento();
        try {
            tipoevento = (TipoEvento) ManagerConcrete.getManager().obtenerRegistro("consultarTipoEvento", idTipoEvento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tipoevento;
    }

    @Override
    public void crearTipoEvento(TipoEvento tipoevento) {
        try {
            ManagerConcrete.getManager().insertarRegistro("crearTipoEvento", tipoevento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void actualizarTipoEvento(TipoEvento tipoevento) {
        try {
            ManagerConcrete.getManager().actualizarRegistro("actualizarTipoEvento", tipoevento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public void borrarTipoEvento(TipoEvento tipoevento) {
        try {
            ManagerConcrete.getManager().borrarRegistro("borrarTipoEvento", tipoevento);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

}
