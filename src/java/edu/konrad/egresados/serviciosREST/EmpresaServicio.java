//package edu.konrad.egresados.serviciosREST;
//
//import edu.konrad.egresados.bean.Empresa;
//import edu.konrad.egresados.servicios.ServiciosEgresados;
//import edu.konrad.egresados.servicios.ServiciosEgresadosImpl;
//import java.util.List;
//import java.util.ArrayList;
//import javax.ws.rs.*;
//import javax.ws.rs.core.*;
//
///**
//  * org.eclipse.emf.ecore.impl.EAnnotationImpl@bc1045b (source: genmymodel)
//  *   * org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl@9bb94d1 (key: uuid, value: _x3R6oe1FEDSO56xokANW_Q)
//    * uuid-_x3R6oe1FEDSO56xokANW_Q
//  *  @generated
//  */
//
//@Path("/Empresa")
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//public class EmpresaServicio {
//    
//	private ServiciosEgresados servicios;
//
//	
//	/**
//	* retorna una lista con  las empresas que se encuentran en la base de datos
//	* @return retorna una lista de Empresa
//	*/
//	@GET
//	public String consultarEmpresas(){
//                 servicios= new ServiciosEgresadosImpl();
//		//return servicios.consultarEmpresas();
//                return "{dato:valor}";
//	}
//	
//	/**
//	* @param id identificador del elemento Concesionario
//	* @return Concesionario del id dado
//	* @generated
//	*/
//	@GET
//	@Path("/{id}")
//	public Empresa obtenerConcesionario(@PathParam("id") Long id){
//                servicios= new ServiciosEgresadosImpl();
//		return servicios.consultarEmpresas().get(0);
//	}
//	
//	
//	/**
//	 * almacena la informacion de Concesionario
//	 * @param dto Concesionario a guardar
//	 * @return Concesionario con los cambios realizados por el proceso de guardar
//	 * @generated
//	 */
//	@POST
//	public Empresa guardarEmpresa(Empresa dto){
//	    if(dto.getIdEmpresa()!=null){
//	         servicios.creaEmpresa(dto); 
//	        return dto;
//	    }else{
//                servicios.actualizarEmpresa(dto); 
//	        return dto;
//	    }
//	}
//	
//	
//	/**
//	 * elimina el registro Concesionario con el identificador dado
//	 * @param id identificador del Concesionario
//	 * @generated 
//	 */
//	@DELETE
//	@Path("/{id}")
//	public void borrarEmpresa(@PathParam("id") Long id){
//                Empresa empresa=new Empresa();
//		servicios.borrarEmpresa(empresa);
//	}
//	
//	
//}
