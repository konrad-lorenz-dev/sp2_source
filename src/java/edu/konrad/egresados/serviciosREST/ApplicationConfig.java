//package edu.konrad.egresados.serviciosREST;
//import java.util.Set;
//import javax.ws.rs.core.Application;
//
///**
// *
// * @author daniel
// */
//@javax.ws.rs.ApplicationPath("webresources")
//public class ApplicationConfig extends Application {
//
//    @Override
//    public Set<Class<?>> getClasses() {
//        Set<Class<?>> resources = new java.util.HashSet<Class<?>>();
//        addRestResourceClasses(resources);
//        return resources;
//    }
//    
//    private void addRestResourceClasses(Set<Class<?>> resources) {
//        resources.add(edu.konrad.egresados.serviciosREST.ItemResource.class);
//        resources.add(edu.konrad.egresados.serviciosREST.ItemsResource.class);
//    }
//    
//}
//
