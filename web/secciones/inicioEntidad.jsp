<%-- 
    Document   : errordb
    Created on : 21/06/2019, 11:39:08 AM
    Author     : cesard.chacond
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="mvc" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="${pageContext.request.contextPath}/css/sp2.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/iniciojsp.css" rel="stylesheet">
        <title>Fundación Universitaria Konrad Lorenz</title>
    </head>
    <body styleClass="main-body">
        <div id="wrapper">
            <div id="main" align="center">         
                <div class="bg layout-main-content" style="max-width: 130rem;" >
                    <div class="vertical-center" style="min-height: 70vh;">
                        <div class="card mx-auto card-1">
                            <div class="card-header">
                                <img class="mx-auto d-block" src="${pageContext.request.contextPath}/resources/images/logo.png" alt="Card image cap">
                            </div>
                            <div class="card-body bg-white" align="center">
                                <p:outputLabel class="card-title" style="font-weight: 700; font-size: 15px;">Ingreso Supervisor Entidad</p:outputLabel>
                                <form method="get" action="entidad">
                                    <table>
                                        <tr>
                                            <td><label>Usuario</label></td>
                                            <td><input type="text" name="usuario"></td>
                                        </tr>
                                        <tr>
                                            <td>Contraseña</td>
                                            <td><input type="password" name="contrasenia"></td>
                                        </tr>
                                        <tr>
                                            <td><input class="btn botonSalir" style="font-weight: 600;" type="submit" value="Iniciar Sesión"></td>
                                        </tr>
                                    </table>
                                </form> 
                            </div> 
                        </div>
                    </div>                    
                </div>
            </div>  
        </div>
    </body>
</html>
